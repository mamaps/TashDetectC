/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file libtash.h
 * @author Michael Flau
 * @brief
 * @details
 * @date June 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started June 2015
 *
 * @mainpage Tashtego
 *
 * @section LibTash manual pages
 *
 * @subsection Introduction
 * The LibTash library is an image acquisition and processing library
 * which offers functionality for various monitoring tasks, which are based
 * on visual information. This library merges functionality which are required
 * by applications of the Tashtego project.
 *
 * @subsection Dependencies
 * So far, LibTash relies heavily on the Qt framework (see http://www.qt.io/) which is used in almost any module.
 * However, as this library is statically compiled, no version of Qt needs to be present,
 * as the lib files include all necessary data, to create Qt dependent applications.
 * All current versions of tashtego are being built against Qt-5.9. Earlier version were built
 * against Qt-5.5.1. Tested operating systems for deployment are Ubuntu 14.04 LTS and Ubuntu 16.04 LTS.
 *
 * @warning Further dependencies might be included in future!
 *
 * @subsection Library-installation
 * Using this library is straightforward.
 * Simply invoke Qt's meta object compiler calling \code $HOME/tashtego/src/libTash$> qmake \endcode
 * Afterwards a call to \code $HOME/tashtego/src/libTash$> make \endcode will compile the library.
 * To install the library in order to compile the tashtego apps,
 * call \code $HOME/tashtego/src/libTash$> make install \endcode To delete all compiled files, a
 * simple call to \code $HOME/tashtego/src/libTash$> make distclean \endcode will suffice.
 *
 * @subsection App-installation
 * The tashtego software as solitary project can be build by calling
 * \code $> cd $HOME\tashtego\ && qmake && make \endcode
 * This will first trigger the build process of libTash followed by the single
 * build calls for all apps which are registered as subproject to libTash.
 * The call to `make` will also install all build apps to `$HOME/tashtego/bin/`
 * which will each refer to name-sibling ini file in `$HOME/tashtego/cfg/`
 * The command \code $> cd tashtego/ && make distclean \endcode will clear the project
 * from all built binaries and include files.
 *
 * @warning This library is still under constant development. Changes
 * to interfaces and logic, can occure at any time!
 */

#ifndef LIBTASH_H
#define LIBTASH_H
#include "tashhelper.h"
#include "tashcore.h"
#include "tashcom.h"
#include "taship.h"
#include "tashcmd.h"
#endif // LIBTASH_H


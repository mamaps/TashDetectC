/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashservicetags.h
 * @author Michael Flau
 * @brief
 * File contains all DBus service descriptors which exist throughout
 * the tashtego DBus environment
 * @details
 * @date August 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Cape Race, NF - Canada, Started August 2015
 */


#ifndef TASHSERVICETAGS
#define TASHSERVICETAGS

#define ST_TASHSTART        "awi.oza.tashtego.tashstart"
#define ST_TASHDETECT       "awi.oza.tashtego.tashdetect"
#define ST_TASHBUFFER       "awi.oza.tashtego.tashbuffer"
#define ST_TASHCONTROL      "awi.oza.tashtego.tashcontrol"
#define ST_TASHPREPROCESS   "awi.oza.tashtego.tashpreprocess"
#define ST_TASHPUMP         "awi.oza.tashtego.tashpump" //Will be replaced later by tashPreProcess
#define ST_MMAP2WBF         "awi.oza.tashtego.mmap2wbf"
#define ST_WBF2FILE         "awi.oza.tashtego.wbf2file"
#define ST_TASHCONTROL      "awi.oza.tashtego.tashcontrol"
#define ST_TASHDISPLAY      "awi.oza.tashtego.tashdisplay"
#define ST_TASHEVENT        "awi.oza.tashtego.tashevent"
#define ST_TASHGPS          "awi.oza.tashtego.tashgps"
#define ST_TASHCONTRAST     "awi.oza.tashtego.tashcontrast"
#define ST_TASHZOOM         "awi.oza.tashtego.tashzoom"
#define ST_TASHLOGGER       "awi.oza.tashtego.tashlogger"
#define ST_LIBTASH          "awi.oza.tashtego.libtash"
#define ST_TASHCOMMANDER    "awi.oza.tashtego.tashcommander"
#define ST_TASHFVIPROVIDER  "awi.oza.tashtego.tashfviprovider"
#define ST_UNITTEST         "awi.oza.tashtego.unittest"
#define ST_TASHJOHNDOE      "awi.oza.tashtego.tashjohndoe"

#endif // TASHSERVICETAGS


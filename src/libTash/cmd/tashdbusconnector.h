/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashdbusconnector.h
 * @author Michael Flau
 * @brief
 * TashDBusConnector - Client communication class to send
 * messages via DBus system to tashLogger application
 * @details
 * @date August 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Cape Race, NF - Canada, Started August 2015
 */

#ifndef TASHDBUSCONNECTOR_H
#define TASHDBUSCONNECTOR_H

#include "tashservicetags.h"
#include "../core/tashobject.h"

#include <QString>

class QDBusInterface;
class QTimer;

namespace tashtego{
namespace cmd{

/**
 * @brief The TashDBusConnector class
 *
 * The TashDBusConnector class provides a simple logging interface to the
 * tashtego DBus logging server. This class is intended to be used by clients
 * who wish to log their message to a centralized point rather than printing
 * messages to stdout.
 */
class TashDBusConnector final: public core::TashObject
{
	Q_OBJECT
    Q_PROPERTY(QString DefaultMsgType READ getDefaultMsgType)
    Q_PROPERTY(QString ServiceName READ getServiceName WRITE setServiceName NOTIFY serviceNameChanged)
    Q_PROPERTY(bool IsConnected READ isConnected NOTIFY loggerConnected)
    Q_ENUMS(eDBusMsgType)

public:

    /**
     * Log message types which are
     * sent via DBUS to TashLogger
     */
    enum eDBusMsgType {
        INF = 0x01, // Informal messages
        LOG = 0x02, // Common log messages
        DBG = 0x04, // Debug messages
        CRI = 0x08, // Critical messages
        ERR = 0x10, // Error messages
        WAR = 0x20  // Warning messages
    };

	/**
	 * @brief TashDBusConnector
     * Default ctor required by libTash for MetaClass instanciation
	 * @param parent
     * Parent object
	 */
    explicit TashDBusConnector(core::TashObject *parent = 0);

	/**
	 * @brief TashDBusConnector
     * Ctor to be called from client code, in order to initialize a working
     * DBusConnector instance.
     * @warning
     * A running DBus service must be present in order to create an initialized
     * instance of this class. This will be done by running Tashlogger from
     * another process
	 * @param id
     * Instance ID, used for internal reference
	 * @param parent
     * Parent Object
	 */
    TashDBusConnector(qint64 id,
                      const QString& serviceName,
                      eDBusMsgType defMsgType = LOG,
                      core::TashObject *parent = 0);

    /**
     * @brief ~TashDBusConnector
     * Default dtor, destroying DBus interface, if created
     */
    virtual ~TashDBusConnector();

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() override;

signals:

    /**
     * @brief serviceNameChanged
     * Signal being emitted, if the referenced service name is changed,
     * e.g. when the app's name will be changed during runtime.
     * @param id
     * Unique libTash ID of instance
     */
    void serviceNameChanged(quint64 id);

    /**
     * @brief loggerConnected
     * Signal being emitted, if the logger instance connected successfully to
     * the DBus logger during init phase.
     * @param id
     * Unique libTash ID of instance
     */
    void loggerConnected(quint64 id);


public slots:

    /**
     * @brief isConnected
     * Simple checker, to ask for connection state of connector
     * @return
     * Boolean value, <code>true</code> if connected, otherwise <code>false</code>
     */
    virtual bool isConnected();

    /**
     * @brief log
     * Logging routine, which will send the passed message to the connected DBus
     * service
     * @param message
     * Actual message content which is supposed to be sent
     * @param type
     * Type of message as defined by eDBusMsgType
     */
    virtual void log(const QString& message, eDBusMsgType type = LOG);

    /**
     * @brief setServiceName
     * Reset service name to another descriptor
     * @param address
     * Associated DBus address as string
     */
    virtual void setServiceName(QString &address);

    /**
     * @brief getServiceName
     * Returns the name of the DBus service being used
     * @return
     * Service name as QString
     */
    virtual QString& getServiceName();

    /**
     * @brief getDefaultMsgType
     * Simple getter to extract the current default message type as a string
     * @return
     * QString default message type
     */
    virtual QString getDefaultMsgType();

protected:
    /**
     * @brief trySwitchToDBus
     * Method attempts to switch initial DBus usage state to enabled,
     * by probing the connection to the DBus message server
     * @return
     * Boolean value, <code>true</code> if connected, otherwise <code>false</code>
     */
    virtual bool trySwitchToDBus();

    /**
     * @brief init
     * Initialization routine called in ctor to follow RAII idiom.
     * Takes care of all initialization logic, at acquisition time (within ctor).
     */
    virtual bool init() override;

    /**
     * @brief msgTypeToString
     * Convenience method to transform an eDBusMsgType into a string representation
     * @return
     * String describing the message type
     */
    inline virtual QString msgTypeToString(const eDBusMsgType& type) const {
        QString msgType;

        switch ( type ) {
            case INF:
                msgType = "info";
            break;
            case LOG:
                msgType = "log";
            break;
            case DBG:
                msgType = "debug";
            break;
            case CRI:
                msgType = "critical";
            break;
            case ERR:
                msgType = "error";
            break;
            case WAR:
                msgType = "warning";
            break;
            default:
                msgType = msgTypeToString(m_cDefaultMessagType);
            break;
        }

        return msgType;
    }


    bool                            m_bDBusMode;          ///> Flag denotes DBus usage
    QString                         m_ServiceName;        ///> Holds the serive name of the DBus Connection
    QPointer<QDBusInterface>        m_pDBusInterface;     ///> Holds the Qt DBus Interface object
    const eDBusMsgType              m_cDefaultMessagType; ///> Holds default msg type which is used when none is specified
    QTimer*                         m_pTimer;             ///> Timer object for mode switching of DBus usage

};

}
}

Q_DECLARE_METATYPE(tashtego::cmd::TashDBusConnector::eDBusMsgType)
Q_DECLARE_METATYPE(tashtego::cmd::TashDBusConnector)

#endif //TASHDBUSCONNECTOR_H


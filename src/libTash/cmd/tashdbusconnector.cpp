/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashdbusconnector.cpp
 * @author Michael Flau
 * @brief
 * TashDBusConnector - Client communication class to send
 * messages via DBus system to tashLogger application
 * @details
 * @date August 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Cape Race, NF - Canada, Started August 2015
 */

#include <QDebug>
#include <QtDBus/QtDBus>
#include <QMap>
#include <QVariant>
#include <QTimer>

#include "../helper/tashtime.h"

#include "tashdbusconnector.h"

namespace tashtego{
namespace cmd{

TashDBusConnector::TashDBusConnector(TashObject *parent):
    TashObject(parent),
    m_bDBusMode(TASHUSEDBUS),
    m_ServiceName(ST_TASHJOHNDOE),
    m_pDBusInterface(nullptr),
    m_cDefaultMessagType(LOG),
    m_pTimer(nullptr)
{

}

TashDBusConnector::TashDBusConnector(qint64 id,
                                     const QString& ServiceName,
                                     eDBusMsgType defMsgType,
                                     TashObject *parent):
    TashObject(id, parent),
    m_bDBusMode(true),
    m_ServiceName(ServiceName),
    m_pDBusInterface(nullptr),
    m_cDefaultMessagType(defMsgType),
    m_pTimer(nullptr)
{
    if ( !init() ) {
#ifdef TASHVERBOSE
        qWarning() << "Failed to initialize TashDBUSConnector instance!";
#endif
    }
}

TashDBusConnector::~TashDBusConnector () {

    if ( this->isInitialized() && m_pDBusInterface ) {
        delete m_pDBusInterface;
    }
}

QString TashDBusConnector::getClassName() {
    return QString("TashDBusConnector");
}

bool TashDBusConnector::init() {

    if ( !m_bDBusMode ) {
        qWarning() << "DBus use disabled, won't transmit any data via DBUS!";
        return (m_bIsInitialized = true);
    }

    auto status = QDBusConnection::sessionBus().isConnected();
    if( !status ) {
        qCritical() << "Cannot connect to DBus!";
        return (m_bIsInitialized = status);
    }

    status = this->m_ServiceName.isEmpty();
    if ( status ) {
        qCritical() << "No correct service name set! Call setServiceName() before logging!";
        return (m_bIsInitialized = !status);
    }

    auto pI = new QDBusInterface(m_ServiceName, "/", "", QDBusConnection::sessionBus());

    Q_CHECK_PTR(pI);
    status = pI ? true : false;

    if (!status) {
        qCritical() << "Invalid reference to DBus interface, abort!";
        return (m_bIsInitialized = status);
    }

    auto up = QPointer<QDBusInterface>(pI);
    m_pDBusInterface.swap(up);

    status = m_pDBusInterface->isValid();
    if ( !status ) {
        m_bIsInitialized = status;
        return m_bIsInitialized;
    }

    emit initialized(this->m_InstanceID);
    return (m_bIsInitialized = true);
}

bool TashDBusConnector::isConnected() {
    return QDBusConnection::sessionBus().isConnected();
}

void TashDBusConnector::log(const QString &message, eDBusMsgType type) {

    //Check instance state
    if ( !this->isInitialized() ) {
        qCritical() << tashtego::helper::TashTime::getNumericFullTimestamp() << \
                       " -- DBus interface not initialized, start TashLogger to see application output!";
        return;
    }

    auto final = QString("[%1] - ").arg(tashtego::helper::TashTime::getNumericFullTimestamp(), 14);

    //If for any reason DBus flag is disabled, it will print the message to stdout
    if ( !m_bDBusMode ) {
        qDebug() << final.append(message);
        return;
    }

    //Check DBus interface state
    auto status = m_pDBusInterface ? true : false;
    Q_ASSERT(status);
    if ( !status ) {
        qCritical() << "Invalid reference to DBus interface!";
        return;
    }

    //Check DBus state
    if ( !this->isConnected() ) {
        qCritical() << "Cannot send messages via DBus, connection error!";
        return;
    }

    if ( !m_pDBusInterface->isValid() ) {
        qCritical() << "Invalid interface, cannot send message! Switch to local logging!";
        QTimer::singleShot(5000, this, SLOT(trySwitchToDBus()));
        m_bDBusMode = false;
    }


    //Prepend timestamp before sending
    final.append(message);

    //Hash table which holds additional
    //information about the sent message
    QMap<QString, QVariant> vMap;

    vMap["service"]   = this->m_ServiceName;       //Set service name
    vMap["msg_type"]  = msgTypeToString( type );   //Set message type
    vMap["interface"] = m_pDBusInterface->interface(); //Set interface name
    vMap["path"]      = m_pDBusInterface->path();      //Set path name

    //Send message via DBus
    QDBusMessage reply = m_pDBusInterface->call("log", final, vMap);

#if QT_VERSION_MAJOR <= 5 && QT_VERSION_MINOR <= 6
    if ( !reply.isValid() ) {
#elif QT_VERSION_MAJOR <= 5 && QT_VERSION_MINOR >= 9
    if (reply.type() == QDBusMessage::SignalMessage ||
            reply.type() == QDBusMessage::InvalidMessage) {
#endif
        qCritical() << "Could not send message via DBus: " << qPrintable(QDBusConnection::sessionBus().lastError().message());
        m_bDBusMode = false;
        QTimer::singleShot(5000, this, SLOT(trySwitchToDBus()));
    }
}

void TashDBusConnector::setServiceName(QString &address) {
    this->m_ServiceName = address;
}

QString& TashDBusConnector::getServiceName() {
    return m_ServiceName;
}

QString TashDBusConnector::getDefaultMsgType() {
    return msgTypeToString(m_cDefaultMessagType);
}

bool TashDBusConnector::trySwitchToDBus() {
    qDebug() << "Try to reenable DBus logging!";
    return m_bDBusMode = true;
}

REGISTER_METATYPE(TashDBusConnector, "TashDBusConnector")
}
}

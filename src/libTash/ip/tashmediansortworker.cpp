/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashmediansortworker.cpp
 * @author Michael Flau
 * @date January 2016
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started January 2016
 */

//tashtego includes
#include "tashmediansortworker.h"
#include "../core/tashtypes.h"
#include "tashtile.h"

//Qt includes
#include <QSharedPointer>
#include <QThread>
#include <QPoint>
#include <QVector>

//std c++ includes
#include <vector>
#include <functional>
#include <algorithm>
#include <iterator>
#include <cmath>
#include <iostream>
#include <limits>
#include <iomanip>
#include <chrono>
#include <thread>

namespace tashtego{
namespace ip{

TashMedianSortWorker::TashMedianSortWorker(TashAbstractImageProcessingKernel *parent):
    TashAbstractImageProcessingKernel(parent),
    m_bHasMSSWFrameSet(false),
    m_bApplyBackgroundModel(true),
    m_bApplyPresenceRatioFilter(false),
    m_ThreadIdx(-1),
    m_numMaxValues(0),
    m_numROIs(0),
    m_numStdDevValues(0),
    m_numScoresToAccumulate(0),
    m_applyTimesStdDev(0),
    m_ScoreValidityBoundary(0),
    m_OverallTileAmount(0),
    m_PresenceRatioThreshold(1.0f),
    m_PresenceRatioScoreHold(2),
    m_OutputMap(nullptr),
    m_pMSSWFrame(nullptr),
    m_pBackgroundModel(nullptr)
{
    m_bIsInitialized = false;
}

TashMedianSortWorker::TashMedianSortWorker(qint64 id,
                                           qint32 threadidx,
                                           QVector<qint32> numMaxValues,
                                           qint32 numROIs,
                                           qint32 numStdDevValues,
                                           qint32 numScoresToAccumulate,
                                           double applyTimesStdDev,
                                           quint32 scoreValidityBoundary,
                                           QVector<tileWorkloadBoundary<qint32> > &workload,
                                           QSharedPointer<rawtypes::scoreMap_r> outputMap,
                                           TashAbstractImageProcessingKernel *parent):
    TashAbstractImageProcessingKernel(id, parent),
    m_bHasMSSWFrameSet(false),
    m_bApplyBackgroundModel(true),
    m_bApplyPresenceRatioFilter(false),
    m_ThreadIdx(threadidx),
    m_numMaxValues(numMaxValues),
    m_numROIs(numROIs),
    m_numStdDevValues(numStdDevValues),
    m_numScoresToAccumulate(numScoresToAccumulate),
    m_applyTimesStdDev(applyTimesStdDev),
    m_ScoreValidityBoundary(scoreValidityBoundary),
    m_OverallTileAmount(0),
    m_PresenceRatioThreshold(1.0f),
    m_PresenceRatioScoreHold(2),
    m_OutputMap(outputMap),
    m_pMSSWFrame(nullptr),
    m_pBackgroundModel(nullptr),
    m_Workload(workload)
{
    auto status = init();
    Q_ASSERT(status);

    if ( !status ) {
#ifdef TASHVERBOSE
        qCritical() << "Failed to initialize TashMedianSortWorker instance!";
#endif
    }
}

TashMedianSortWorker::TashMedianSortWorker(qint64 id,
                                           qint32 threadidx,
                                           QVector<qint32> numMaxValues,
                                           qint32 numROIs,
                                           qint32 numStdDevValues,
                                           qint32 numScoresToAccumulate,
                                           double applyTimesStdDev,
                                           quint32 scoreValidityBoundary,
                                           QVector< tileWorkloadBoundary<qint32> > &workload,
                                           QSharedPointer<rawtypes::scoreMap_r> outputMap,
                                           QPointer<TashMSSWAccessor<qint16>> backgroundModel,
                                           TashAbstractImageProcessingKernel *parent):
    TashAbstractImageProcessingKernel(id, parent),
    m_bHasMSSWFrameSet(false),
    m_bApplyBackgroundModel(true),
    m_bApplyPresenceRatioFilter(false),
    m_ThreadIdx(threadidx),
    m_numMaxValues(numMaxValues),
    m_numROIs(numROIs),
    m_numStdDevValues(numStdDevValues),
    m_numScoresToAccumulate(numScoresToAccumulate),
    m_applyTimesStdDev(applyTimesStdDev),
    m_ScoreValidityBoundary(scoreValidityBoundary),
    m_OverallTileAmount(0),
    m_PresenceRatioThreshold(1.0f),
    m_PresenceRatioScoreHold(2),
    m_OutputMap(outputMap),
    m_pMSSWFrame(nullptr),
    m_pBackgroundModel(backgroundModel),
    m_Workload(workload)
{
    Q_CHECK_PTR( m_pBackgroundModel.data() );

    auto status = m_pBackgroundModel.isNull();
    Q_ASSERT(!status);

    if ( status ) {
        qCritical() << "Given pointer to background model data is invalid, stopping worker initialization...";
        m_bIsInitialized = !status;
        return;
    }

    status = init();
    Q_ASSERT(status);

    if ( !status ) {
#ifdef TASHVERBOSE
        qCritical() << "Failed to initialize TashMedianSortWorker instance!";
#endif
    }
}

TashMedianSortWorker::TashMedianSortWorker(qint64 id,
                     qint32 threadidx,
                     QVector<qint32> numMaxValues,
                     qint32 numROIs,
                     qint32 numStdDevValues,
                     qint32 numScoresToAccumulate,
                     double applyTimesStdDev,
                     quint32 scoreValidityBoundary,
                     QVector< tileWorkloadBoundary<qint32> > &workload,
                     QSharedPointer<rawtypes::scoreMap_r> outputMap,
                     QPointer<TashMSSWAccessor<qint16>> backgroundModel,
                     bool usePresenceRatioFilter,
                     float presenceRatioThreshold,
                     TashAbstractImageProcessingKernel *parent):
    TashAbstractImageProcessingKernel(id, parent),
    m_bHasMSSWFrameSet(false),
    m_bApplyBackgroundModel(true),
    m_bApplyPresenceRatioFilter(usePresenceRatioFilter),
    m_ThreadIdx(threadidx),
    m_numMaxValues(numMaxValues),
    m_numROIs(numROIs),
    m_numStdDevValues(numStdDevValues),
    m_numScoresToAccumulate(numScoresToAccumulate),
    m_applyTimesStdDev(applyTimesStdDev),
    m_ScoreValidityBoundary(scoreValidityBoundary),
    m_OverallTileAmount(0),
    m_PresenceRatioThreshold(presenceRatioThreshold),
    m_PresenceRatioScoreHold(2),
    m_OutputMap(outputMap),
    m_pMSSWFrame(nullptr),
    m_pBackgroundModel(backgroundModel),
    m_Workload(workload) {

    Q_CHECK_PTR( m_pBackgroundModel.data() );

    auto status = m_pBackgroundModel.isNull();
    Q_ASSERT(!status);

    if ( status ) {
        qCritical() << "Given pointer to background model data is invalid, stopping worker initialization...";
        m_bIsInitialized = !status;
        return;
    }

    status = init();
    Q_ASSERT(status);

    if ( !status ) {
#ifdef TASHVERBOSE
        qCritical() << "Failed to initialize TashMedianSortWorker instance!";
#endif
    }

}

bool TashMedianSortWorker::init() {

    auto status = m_ThreadIdx > QThread::idealThreadCount();
    Q_ASSERT(!status);

    if ( status ) {
        qWarning() << "The given thread index of worker instance " << this->m_InstanceID <<
                      " exceeds recommend number of threads.\n It is not guaranteed that the ouput " <<
                      "memorymap has as many slots as threads are being used. Stopping usage of this instance here...!";
        m_bIsInitialized = !status;
        return m_bIsInitialized;
    }

    status = m_Workload.isEmpty();
    Q_ASSERT( !status );

    if ( status ) {
        qCritical() << "Vector of input indeces is empty...cannot proceed!";
        m_bIsInitialized = !status;
        return m_bIsInitialized;
    }

    m_IntermediateDataBuffer.resize(m_numROIs);

    for ( auto idx1 = 0; idx1 < m_numROIs; ++idx1 ) {
    /**
     * @todo
     * This will only work for first ROI. Any following ROI will have dimensions of first.
     * This is faulty an will lead to Crashes!
     */
        m_IntermediateDataBuffer[idx1].resize(m_Workload.size());

        for ( auto idx2 = 0; idx2 < m_Workload.size(); ++idx2 ) {

            Q_ASSERT( m_Workload[idx2].upper > m_Workload[idx2].lower );

            if ( m_Workload[idx2].upper <= m_Workload[idx2].lower )
                qCritical() << "Upper thread boundary must be greater than lower thread boundary!";

            const auto cnt = m_Workload[idx2].upper - m_Workload[idx2].lower;
            m_OverallTileAmount += cnt;
            m_IntermediateDataBuffer[idx1][idx2].resize(cnt);

            for ( auto idx3 = 0; idx3 < cnt; ++idx3 ) {
                m_IntermediateDataBuffer[idx1][idx2][idx3].cmpResult = 0.0;
                m_IntermediateDataBuffer[idx1][idx2][idx3].stdDevStack.resize(m_numStdDevValues, 1.0);
            }
        }
    }

    m_InternalScoreList.resize(m_numROIs);

    for ( auto idx1 = 0; idx1 < m_numROIs; ++idx1 ) {

    /**
     * @todo
     * This will only work for first ROI. Any following ROI will have dimensions of first.
     * This is faulty an will lead to Crashes!
     */
        m_InternalScoreList[idx1].resize(m_Workload.size());

        for ( auto idx2 = 0; idx2 < m_Workload.size(); ++idx2 ) {
            const auto cnt = m_Workload[idx2].upper - m_Workload[idx2].lower;
            m_InternalScoreList[idx1][idx2].resize(cnt);

            for ( auto idx3 = 0; idx3 < cnt; ++idx3 ) {

                m_InternalScoreList[idx1][idx2][idx3] = {idx1,
                                                         m_Workload[idx2].tridx,
                                                         m_Workload[idx2].lower + idx3,
                                                         0,
                                                         0,
                                                         0,
                                                         0,
                                                         0,
                                                         0,
                                                         0,
                                                         {},
                                                         {},
                                                         QPoint(0,0),
                                                         false,
                                                         0.0f};

                m_InternalScoreList[idx1][idx2][idx3].scorelist.resize(m_numScoresToAccumulate,0);
                m_InternalScoreList[idx1][idx2][idx3].scoreBlacklist.resize((m_numScoresToAccumulate * m_PresenceRatioScoreHold) , 0);
            }
        }
    }

    Q_CHECK_PTR(m_OutputMap.data());
    status = m_OutputMap.isNull();
    Q_ASSERT(!status);

    if ( status ) {
        qCritical() << "Pointer to output memory map is invalid. Cannot proceed!";
        m_bIsInitialized = !status;
        return m_bIsInitialized;
    }

    return (m_bIsInitialized = !status);
}

QString TashMedianSortWorker::getClassName() {
    return QString("TashMedianSortWorker");
}

void TashMedianSortWorker::run() {

    Q_ASSERT(m_bIsInitialized);
    Q_ASSERT(m_bHasMSSWFrameSet);

    // Initialization check
    if ( !m_bIsInitialized ) {
        qWarning() << "Worker with thread id " << QThread::currentThreadId() << " instance is not initialized, skip processing!";
        return;
    }

    // Valid frame check
    if ( !m_bHasMSSWFrameSet ) {
        qCritical() << "Worker instance has no input data set to be processed! Abort!";
        return;
    }

    /**
     * Variable declaration only once per thread run
     */
    auto workloadLower              = 0;   // Lower workload index boundary
    auto workloadUpper              = 0;   // Upper workload index boundary
    auto workloadROI                = 0;   // current region of interest to process
    auto workloadTR                 = 0;   // current tilerange to process
    auto hsz                        = 0;   // contains index of centre pixel value after sorting aka median value of tile
    auto row                        = 0;   // current tilerow which is being processed
    auto bgmScore                   = 0;   // current background model score for respective tile
    const auto cApplyXTimesStdDev   = m_applyTimesStdDev; // boost value for std-dev of a tile
    auto result                     = 0.0;  // result of tile comparison
    auto stddev                     = 0.0;  // standard deviation of tile
    auto result_l                   = 0.0;  // computaion result of tile left of centre tile
    auto result_c                   = 0.0;  // computaion result of centre tile
    auto result_r                   = 0.0;  // computaion result of tile right of centre tile
    auto mStdDev_l                  = 0.0;  // standard deviation of left tile
    auto mStdDev_r                  = 0.0;  // standard deviation of right tile
    auto divider                    = 0.0;  // Divider of sum of std devs and results
    std::vector<qint16>             data(0);// std::vector which hold pixel values of tile
    auto decision                   = false;// final decision storage
    const qint32 cNumTilesToAnalyze = 5;    ///> @todo This should go into tashDetct.ini, to be changeable!!!
    const qint32 tileOffset         = (cNumTilesToAnalyze-(cNumTilesToAnalyze % 2))/2; //Tile offset to mind depending on cNumTilesToAnalyze
    auto currentTIDx                = 0; //Just for debugging, remove later

    //Iterate over workload and process every tile
    for ( auto boundaryIt = m_Workload.begin(); boundaryIt != m_Workload.end(); ++boundaryIt, ++row) {

        /*
         * Get newest indeces
         */
        workloadLower               = boundaryIt->lower;
        workloadUpper               = boundaryIt->upper;
        workloadROI                 = boundaryIt->roiidx;
        workloadTR                  = boundaryIt->tridx;

        /**
         * Get num max value based on tile range which is being processed.
         * This assumes that tilerange are distance related defined. The higher
         * the tile index, the closer the area which is being monitored.
         *
         * @todo Not perfect needs fix. Would be better to associate value with each tilerange.
         */
        const auto  cNumMaxValues   = m_numMaxValues[workloadTR];

        /*
         * Get tiles which are in workload range in on line, to be processed
         */
        auto tiles = m_pMSSWFrame->getTileSubset(workloadROI,
                                                 workloadTR,
                                                 workloadLower,
                                                 workloadUpper);
        /*
         * Get background model tiles which are in workload range in on line, to be processed
         */
        auto bgmTiles = m_pBackgroundModel->getTileSubset(workloadROI,
                                                          workloadTR,
                                                          workloadLower,
                                                          workloadUpper);


        auto tileidx    = workloadLower;   // Main tileindex set to workload start position
        currentTIDx     = tileidx;
        auto imdBufIdx  = 0;               // Main index for referencing intermediate result buffer

        /*
         * Do First computation step for first and second tile.
         * This may look awful, but it's way more performant than having it handled in
         * an if clause!
         */
        for ( imdBufIdx = 0; imdBufIdx < 2; ++imdBufIdx, ++tileidx ) {

            Q_ASSERT(tileidx < workloadUpper);

            //Ignore zero check, we need std-dev and result anyway
            tileDataToVector(data, tiles[tileidx]);

            hsz = data.size() >> 1;

            std::partial_sort(data.begin(),
                              data.begin() + hsz,
                              data.end(),
                              std::greater<qint16>());

            result = static_cast<double>(std::accumulate(data.begin(), data.begin() + cNumMaxValues, 0) / cNumMaxValues) - static_cast<double>(data.at(hsz));
            stddev = stdDev(data);

            m_IntermediateDataBuffer[workloadROI][row][imdBufIdx].cmpResult = result;
            m_IntermediateDataBuffer[workloadROI][row][imdBufIdx].stdDevStack.pop_front();
            m_IntermediateDataBuffer[workloadROI][row][imdBufIdx].stdDevStack.push_back(stddev);
        }

        const auto cSz = m_IntermediateDataBuffer[workloadROI][row].size();

        /*
         * Do actual computation for rest of tiles in workload vector
         */
        for ( /*already initialized*/ ; imdBufIdx < cSz; ++imdBufIdx, ++tileidx, decision = false ) {

            Q_ASSERT(tileidx < workloadUpper);

            //Get score of tile to be analyzed
            bgmScore = computeBackgroundModelScore(bgmTiles[tileidx-1]);

            //Don't use deprecated single tile zero check
            tileDataToVector(data, tiles[tileidx]);

            //Compute std deviation of pixels in tile
            stddev = stdDev(data);
            m_IntermediateDataBuffer[workloadROI][row][imdBufIdx].stdDevStack.pop_front();
            m_IntermediateDataBuffer[workloadROI][row][imdBufIdx].stdDevStack.push_back(stddev);

            //Check tilerange for zeroes, to exclude tiles which are covered by binary mask
            try {

                if ( (((tileidx) - tileOffset) > workloadLower) && (((tileidx) + tileOffset) < workloadUpper) ) {

                    Q_ASSERT((((tileidx)+tileOffset)-((tileidx)-tileOffset)) <= (workloadUpper-workloadLower));

                    //Fetch tile subset
                    auto tilesInRange = m_pMSSWFrame->getTileSubset(workloadROI,
                                                                    workloadTR,
                                                                    ((tileidx) - tileOffset),
                                                                    ((tileidx) + tileOffset));

                    std::vector<TashTile<qint16>*> subRange = std::vector<TashTile<qint16>*>(tilesInRange.size(), nullptr);
                    QList<quint32>                 keys     = tilesInRange.keys();
                    qint32                         vidx     = 0;

                    //Store in vector
                    for ( auto it = keys.begin(); it != keys.end(); ++it, ++vidx ) {
                        subRange[vidx] = tilesInRange[*it];
                        Q_CHECK_PTR(subRange[vidx]);
                    }

                    //If one of tiles contains zero-value, blacklist tile
                    if ( checkTileRangeForZeroes(subRange) ) {
                        m_IntermediateDataBuffer[workloadROI][row][imdBufIdx].cmpResult = 99.0;
                        setScore(tiles[tileidx-1], workloadROI, row, imdBufIdx-1, true, bgmScore, true);
                        continue;
                    }
                }
            } catch (std::bad_alloc e) {
                qCritical() << "Subrange-low: " << ((tileidx) - tileOffset) << " - Subrange-high: " << ((tileidx) + tileOffset) << " - Workload lower idx: " << workloadLower << " - Workload upper idx: " << workloadUpper << " - Tileidx: " << currentTIDx << " - Catched Error: " << QString(e.what());
            }

            //Get position of middle value
            hsz = data.size() >> 1;

            //Do a partial sort of pixels up to middle
            std::partial_sort(data.begin(),
                              data.begin() + hsz,
                              data.end(),
                              std::greater<qint16>());

            //Compute result value for current tile
            result = static_cast<double>(std::accumulate(data.begin(), data.begin() + cNumMaxValues, 0) / cNumMaxValues) - static_cast<double>(data.at(hsz));

            //Store result and standard deviation for later analyzation
            m_IntermediateDataBuffer[workloadROI][row][imdBufIdx].cmpResult = result;

            //Actual decision computation
            result_l  = m_IntermediateDataBuffer[workloadROI][row][imdBufIdx-2].cmpResult;
            result_c  = m_IntermediateDataBuffer[workloadROI][row][imdBufIdx-1].cmpResult;
            result_r  = m_IntermediateDataBuffer[workloadROI][row][imdBufIdx].cmpResult;
            mStdDev_l = meanOfStdDev(m_IntermediateDataBuffer[workloadROI][row][imdBufIdx-2].stdDevStack);
            mStdDev_r = meanOfStdDev(m_IntermediateDataBuffer[workloadROI][row][imdBufIdx].stdDevStack);
            divider   = ((mStdDev_l <= 0.0) || (mStdDev_r <= 0.0)) ? 1.0 : 2.0;
            decision  = (result_c - ((result_l + result_r) / 2.0)) > (cApplyXTimesStdDev * ((mStdDev_l + mStdDev_r) / divider));

            //Handle decision and write result to internal result map
            setScore(tiles[tileidx-1], workloadROI, row, imdBufIdx-1, decision, bgmScore);
//            decision = false;
        }
    }

    /**
     * @todo This won't work as such. A second region of interest will overwrite results of first one!
     */
    writeResultToMemoryMap();
    m_bHasMSSWFrameSet = false;
}

void TashMedianSortWorker::setMultiScaleFrame(QPointer<TashMSSWAccessor<qint16>> frame) {

    Q_CHECK_PTR(frame.data());

    if ( frame.isNull() ) {
        qCritical() << "Given pointer to source data is invalid! Abort!";
        m_bHasMSSWFrameSet = false;
        return;
    }

    m_pMSSWFrame        = frame;
    m_bHasMSSWFrameSet  = true;
}

void TashMedianSortWorker::toggleBackgroundModel(const bool useBGM) {
    m_bApplyBackgroundModel = useBGM;
}

bool TashMedianSortWorker::tileDataToVector(std::vector<qint16>& targetVec, TashTile<qint16> *pTile) {

    Q_CHECK_PTR(pTile);

    targetVec.clear();
    targetVec.reserve(pTile->pixelcount());

    const auto cHeight = pTile->getSegmentHeight();
    const auto cWidth  = pTile->getSegmentWidth();

    for ( quint32 idx = 0; idx < cHeight; ++idx) {
        qint16* line = pTile->scanline(idx);
        Q_CHECK_PTR(line);
        std::copy(line, line + cWidth, std::back_inserter(targetVec));
    }

    //Check for binary mask error tiles at rim
    return (std::find(targetVec.begin(), targetVec.end(), 0) != targetVec.end());
}

bool TashMedianSortWorker::checkTileRangeForZeroes(std::vector<TashTile<qint16>*> range) {

    for( auto it = range.begin(); it != range.end(); ++it) {
        Q_CHECK_PTR(*it);
    }

    const auto cHeight = range[0]->getSegmentHeight();
    const auto cWidth  = range[0]->getSegmentWidth();

    std::vector<qint16> targetVector;
    targetVector.reserve(cWidth);
    qint16* line = nullptr;

    for ( auto idx = 0u; idx < range.size(); ++idx ) {
        for ( auto idx2 = 0u; idx2 < cHeight; ++idx2 ) {

            line = range[idx]->scanline(idx2);

            for ( auto idx3 = 0u; idx3 < cWidth; ++idx3, ++line ) {
                if ( *line == 0 ) return true;
            }

            targetVector.clear();
        }
    }

    return false;
}

double TashMedianSortWorker::stdDev(std::vector<qint16>& pValues) {

    const double cSize  = pValues.size();
    const double cSum   = std::accumulate(pValues.begin(), pValues.end(), 0);
    const auto cMean    = cSum / cSize;
    auto accum          = 0.0;

    std::for_each(std::begin(pValues), std::end(pValues), [&](const qint16 v) {
        accum += std::pow(( static_cast<double>(v) - cMean ), 2.0);
    });

    return std::sqrt(accum / (cSize-1.0));
}

double TashMedianSortWorker::meanOfStdDev( const std::deque<double>& input ) {
    const double    cCount = input.size();
    double          sum = std::accumulate(input.begin(), input.end(), 0.0);
    return sum/cCount;
}

qint16 TashMedianSortWorker::computeBackgroundModelScore( TashTile<qint16> *pBGMTile) {

    Q_CHECK_PTR(pBGMTile);

    if ( !m_bApplyBackgroundModel || !pBGMTile ) {
        qWarning() << "No application of BGM!";
        return 1;
    }

    std::vector<qint16> bgmVec(0);
    this->tileDataToVector(bgmVec, pBGMTile);

    Q_ASSERT( bgmVec.size() > 0 );

    return std::accumulate(bgmVec.begin(), bgmVec.end(), 0) / bgmVec.size();
}

void TashMedianSortWorker::setScore(TashTile<qint16> *pTile, const qint32& roi, const qint32& row, const qint32& tileidx, const bool decs, const qint16& bgmScore, const bool ignore) {

    Q_CHECK_PTR(pTile);
    Q_ASSERT(roi >= 0);
    Q_ASSERT(row >= 0);
    Q_ASSERT(tileidx >= 0);

    //Write new score into queue
    m_InternalScoreList[roi][row][tileidx].scorelist.pop_back();
    m_InternalScoreList[roi][row][tileidx].scorelist.push_front(decs ? 1 : 0);

    m_InternalScoreList[roi][row][tileidx].scoreBlacklist.pop_back();
    m_InternalScoreList[roi][row][tileidx].scoreBlacklist.push_front(decs ? 1 : 0);

    const auto score = std::accumulate(m_InternalScoreList[roi][row][tileidx].scorelist.begin(), m_InternalScoreList[roi][row][tileidx].scorelist.end(), 0);

    //Set score with(out) BGM application
    m_InternalScoreList[roi][row][tileidx].currentrawscore = score;
    m_InternalScoreList[roi][row][tileidx].currentscore    = score * bgmScore;

    m_InternalScoreList[roi][row][tileidx].scoreidx += 1;
    m_InternalScoreList[roi][row][tileidx].scoreidx = m_InternalScoreList[roi][row][tileidx].scoreidx >= m_numScoresToAccumulate ? 0 : m_InternalScoreList[roi][row][tileidx].scoreidx;

    m_InternalScoreList[roi][row][tileidx].falseScoreidx += 1;
    m_InternalScoreList[roi][row][tileidx].falseScoreidx = m_InternalScoreList[roi][row][tileidx].falseScoreidx >= (m_numScoresToAccumulate*m_PresenceRatioScoreHold) ? 0 : m_InternalScoreList[roi][row][tileidx].falseScoreidx;

    m_InternalScoreList[roi][row][tileidx].height = pTile->getSegmentHeight();
    m_InternalScoreList[roi][row][tileidx].width  = pTile->getSegmentWidth();
    m_InternalScoreList[roi][row][tileidx].start  = pTile->getStartPoint();
    m_InternalScoreList[roi][row][tileidx].uniqueTileId = pTile->getSegmentIndexNumber();

    if ( m_bApplyPresenceRatioFilter ) {
        determinePresenceRatio(m_InternalScoreList[roi][row][tileidx], bgmScore, ignore);
    }
}

void TashMedianSortWorker::resetScore(TashTile<qint16> *pTile, const qint32& roi, const qint32& row, const qint32& tileidx, const qint16& bgmScore) {

    Q_CHECK_PTR(pTile);
    Q_ASSERT(roi >= 0);
    Q_ASSERT(row >= 0);
    Q_ASSERT(tileidx >= 0);

    //Write new score into queue
    m_InternalScoreList[roi][row][tileidx].scorelist.pop_back();
    m_InternalScoreList[roi][row][tileidx].scorelist.push_front(0);

    const auto score = std::accumulate(m_InternalScoreList[roi][row][tileidx].scorelist.begin(), m_InternalScoreList[roi][row][tileidx].scorelist.end(), 0);

    //Set score with(out) BGM application
    m_InternalScoreList[roi][row][tileidx].currentrawscore = score;
    m_InternalScoreList[roi][row][tileidx].currentscore    = score * bgmScore;

    m_InternalScoreList[roi][row][tileidx].scoreidx += 1;
    m_InternalScoreList[roi][row][tileidx].scoreidx = m_InternalScoreList[roi][row][tileidx].scoreidx >= m_numScoresToAccumulate ? 0 : m_InternalScoreList[roi][row][tileidx].scoreidx;

    //Set tile specs
    m_InternalScoreList[roi][row][tileidx].height = pTile->getSegmentHeight();
    m_InternalScoreList[roi][row][tileidx].width  = pTile->getSegmentWidth();
    m_InternalScoreList[roi][row][tileidx].start  = pTile->getStartPoint();
    m_InternalScoreList[roi][row][tileidx].uniqueTileId = pTile->getSegmentIndexNumber();
}

void TashMedianSortWorker::determinePresenceRatio(scoreContainer &score, const qint16 &bgmScore, const bool ignore) {

    float static_th = m_PresenceRatioThreshold;
    float frames    = m_numScoresToAccumulate*m_PresenceRatioScoreHold;
    float currentsc = static_cast<float>( std::accumulate(score.scoreBlacklist.begin(), score.scoreBlacklist.end(), 0));
    float bgmsc     = bgmScore;

    score.presenceRatio = (score.presenceRatio + ((currentsc/frames) * bgmsc)) / 2.0f;

    if ( ignore ) {
        score.ignore = true;
    } else {
        score.ignore = score.presenceRatio > static_th ? true : false;
    }
}

void TashMedianSortWorker::writeResultToMemoryMap() {

    using namespace tashtego::rawtypes;

    //Custom sorter function
    auto       mySort               = [] (scoreHolder_r &a, scoreHolder_r &b) {return a.score > b.score;};
    const auto cOverallTileAmount   = m_OverallTileAmount;
    auto       outputindex          = 0;
    const auto cNumROIs             = m_InternalScoreList.size();
    QVector<scoreHolder_r> sortedResultList(cOverallTileAmount, {0,0,0,0,0,0,0,0});

    //Transfer all tiles from intermediate list into vector instance
    for ( auto idx1 = 0; idx1 < cNumROIs; ++idx1) {
        const auto cNumRows = m_InternalScoreList[idx1].size();

        for ( auto idx2 = 0; idx2 < cNumRows; ++idx2 ) {
            const auto cNumTiles = m_InternalScoreList[idx1][idx2].size();

            for ( auto idx3 = 0; idx3 < cNumTiles; ++idx3, ++outputindex) {

                sortedResultList[outputindex].height  = m_InternalScoreList[idx1][idx2][idx3].height;
                sortedResultList[outputindex].width   = m_InternalScoreList[idx1][idx2][idx3].width;

                if ( m_InternalScoreList[idx1][idx2][idx3].ignore ) {
                    sortedResultList[outputindex].score          = 0;
                    sortedResultList[outputindex].score2         = 0;
                    sortedResultList[outputindex].ignore         = 1;
                    m_InternalScoreList[idx1][idx2][idx3].ignore = false;
                } else {
                    sortedResultList[outputindex].score  = m_InternalScoreList[idx1][idx2][idx3].currentscore;
                    sortedResultList[outputindex].score2 = m_InternalScoreList[idx1][idx2][idx3].currentrawscore;
                    sortedResultList[outputindex].ignore = 0;
                }

                sortedResultList[outputindex].start_x = m_InternalScoreList[idx1][idx2][idx3].start.x();
                sortedResultList[outputindex].start_y = m_InternalScoreList[idx1][idx2][idx3].start.y();
                sortedResultList[outputindex].tile_id = m_InternalScoreList[idx1][idx2][idx3].uniqueTileId;

                Q_ASSERT( outputindex < cOverallTileAmount );
            }
        }
    }

    //Sort vector
    qSort(sortedResultList.begin(), sortedResultList.end(), mySort);

    Q_CHECK_PTR(m_OutputMap.data());

    if ( m_OutputMap.isNull() ) {
        qCritical() << "Output memory map seems to be corrupted! Skip writing to map!";
        return;
    }

    qint32 cSz = sortedResultList.size();

    Q_ASSERT(cSz < tashtego::SCORE_MAP_DEFAULT_NUM_EVENT_FIELDS);

    if( cSz > tashtego::SCORE_MAP_DEFAULT_NUM_EVENT_FIELDS ) {
        qWarning() << "Amount of tile events exceeds memory map limit, consider only first "
                   << tashtego::SCORE_MAP_DEFAULT_NUM_EVENT_FIELDS << " events!";

        cSz = tashtego::SCORE_MAP_DEFAULT_NUM_EVENT_FIELDS;
    }

    while ( m_OutputMap->threadList[m_ThreadIdx].readlock > 0 ) {
        std::this_thread::sleep_for( std::chrono::milliseconds(10));
    }

    m_OutputMap->threadList[m_ThreadIdx].writelock = 1;
    m_OutputMap->threadList[m_ThreadIdx].elements = cSz;

    //Write sorted list to memory map
    for ( auto idx = 0; idx < cSz; ++idx ) {
        m_OutputMap->threadList[m_ThreadIdx].scoreList[idx][0] = sortedResultList[idx].tile_id;
        m_OutputMap->threadList[m_ThreadIdx].scoreList[idx][1] = sortedResultList[idx].start_x;
        m_OutputMap->threadList[m_ThreadIdx].scoreList[idx][2] = sortedResultList[idx].start_y;
        m_OutputMap->threadList[m_ThreadIdx].scoreList[idx][3] = sortedResultList[idx].width;
        m_OutputMap->threadList[m_ThreadIdx].scoreList[idx][4] = sortedResultList[idx].height;
        m_OutputMap->threadList[m_ThreadIdx].scoreList[idx][5] = sortedResultList[idx].score;
        m_OutputMap->threadList[m_ThreadIdx].scoreList[idx][6] = sortedResultList[idx].score2;
        m_OutputMap->threadList[m_ThreadIdx].scoreList[idx][7] = sortedResultList[idx].ignore;
    }

    m_OutputMap->threadList[m_ThreadIdx].writelock = 0;
}

REGISTER_METATYPE(TashMedianSortWorker, "TashMedianSortWorker")
}
}

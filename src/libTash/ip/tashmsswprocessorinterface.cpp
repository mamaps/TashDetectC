/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashmsswprocessorinterface.cpp
 * @author Michael Flau
 * @brief
 * @details
 * @date November 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started November 2015
 */

#include "tashmsswprocessorinterface.h"
#include "../helper/tashmath.h"
#include "tashabstractprocessor.h"
#include "../com/tashtypedmemorymap.h"

#include <QVector>
#include <QPointer>
#include <QElapsedTimer>

namespace tashtego{
namespace ip{

TashMSSWProcessorInterface::TashMSSWProcessorInterface(tashtego::core::TashObject *parent):
    tashtego::core::TashObject(parent),
    m_bDoStatistics(false),
    m_numProcessedFrames(0),
    m_lastProcessingTime(0),
    m_integrationRange(0),
    m_meanComputationTime(0),
    m_pProcessor(nullptr) {
    this->writeDefaultProperties();
}

TashMSSWProcessorInterface::TashMSSWProcessorInterface(qint64 id,
                                                       QPointer<core::TashRuntime> runtime,
                                                       TashAbstractProcessor* processor,
                                                       bool doStatistics,
                                                       TashObject *parent):
    tashtego::core::TashObject(id, runtime, parent),
    m_bDoStatistics(doStatistics),
    m_numProcessedFrames(0),
    m_lastProcessingTime(0),
    m_integrationRange(0),
    m_meanComputationTime(0),
    m_pProcessor(processor) {

    bool status = parsePropertiesFromRuntime();

    Q_ASSERT( status );

    if ( !status ) {
        qCritical() << "Can't proceed construction of TashMSSWProcessorInterface instance, parsing of runtime values failed!";
        return;
    }

    status = this->init();

    Q_ASSERT( status );

    if ( !status ) {
        qCritical() << "Failed to initialize TashMSSWProcessorInterface instance!";
        return;
    }

    m_processingTimes = std::deque<qint64>(m_integrationRange, 1);
}

TashMSSWProcessorInterface::~TashMSSWProcessorInterface () {

    Q_CHECK_PTR(m_pProcessor);

    if (this->m_bIsInitialized &&  m_pProcessor ) {
        m_pProcessor->deleteLater();
        this->m_bIsInitialized = false;
    }
}

QString TashMSSWProcessorInterface::getClassName() {
    return QString("TashMSSWProcessorInterface");
}

bool TashMSSWProcessorInterface::init() {

    Q_CHECK_PTR(m_pProcessor);

    if( !m_pProcessor ) {
        qCritical() << "Pointer to given processor instance is invalid, can't proceed";
        m_bIsInitialized = false;
        return m_bIsInitialized;
    }

    Q_ASSERT(m_pProcessor->isInitialized());

    if ( !m_pProcessor->isInitialized() ) {
        qCritical() << "Given processor instance is notinitialized, can't proceed!";
        m_bIsInitialized = false;
        return m_bIsInitialized;
    }

    this->connect(m_pProcessor, SIGNAL(frameProcessed(int)), this, SIGNAL(processed(qint32)));
    return (m_bIsInitialized = true);
}

void TashMSSWProcessorInterface::invoke(quint32 frameidx, qint32 slot) {

    qDebug() << "New frame " << frameidx;

    QElapsedTimer timer;

    if ( m_bDoStatistics ) {
        m_processingTimes.pop_front();
        timer.start();
    }

    m_pProcessor->process( slot );

    if ( m_bDoStatistics ) {
        m_lastProcessingTime = timer.elapsed();
        m_processingTimes.push_back( m_lastProcessingTime );
        computeMeanRuntime();
        ++m_numProcessedFrames;
    }
}

bool TashMSSWProcessorInterface::hasProcessor() const {
    return m_bIsInitialized;
}

bool TashMSSWProcessorInterface::dockProcessor(TashAbstractProcessor *processor) {

    Q_CHECK_PTR(processor);

    if ( !processor ) {
        qCritical() << "Error, invalid pointer to processor instance given, can't dock processor to interface!";
        return false;
    }

    Q_ASSERT(processor->isInitialized());

    if ( !processor->isInitialized() ) {
        qCritical() << "Given processor instance is notinitialized, can't proceed!";
        return m_bIsInitialized = false;;
    }

    if ( this->hasProcessor() ) {
        this->disconnect(m_pProcessor, SIGNAL(frameProcessed(int)), this, SIGNAL(processed(qint32)));
        m_pProcessor->deleteLater();
    }

    m_pProcessor = processor;
    this->init();

    return this->hasProcessor();
}

qint64 TashMSSWProcessorInterface::getAvgProcessingTime() const {
    return m_meanComputationTime;
}

quint64 TashMSSWProcessorInterface::getNumberOfProcessedFrames() const {
    return m_numProcessedFrames;
}

void TashMSSWProcessorInterface::writeDefaultProperties() {

   bool status = this->setProperty("do_statistics", false);
   Q_ASSERT(!status);

   status = this->setProperty("runtime_samples_integration_range", 50);
   Q_ASSERT(!status);
}

bool TashMSSWProcessorInterface::parsePropertiesFromRuntime() {

    QString      classname        = QString( this->metaObject()->className()).split("::").last();
    QStringList  rawProperties    = m_pRunTime->getPropertyNamesContaining( classname, m_pProcessor->getCameraIndex());
    const qint8  checkValue       = 0x3;
    qint8        toBeCheckedValue = 0x0;

    foreach (QString str, rawProperties) {

        QString strsplit = str.split("-").last();

        if ( strsplit == "do_statistics" ) {
            bool tmp = m_pRunTime->property(str).toBool();
            m_bDoStatistics = tmp;
            toBeCheckedValue |= 1;
        }

        if ( strsplit == "runtime_samples_integration_range" ) {
            qint32 tmp = m_pRunTime->property(str).toInt();
            Q_ASSERT(tmp > 1);

            if ( tmp <= 1 ) {
                qWarning() << "Integration range for mean processing time is too small default to 50 values!";
                tmp = 50;
            }

            m_integrationRange = tmp;

            qDebug() << "Computing mean processing time over " << tmp << " last frames";

            toBeCheckedValue |= (1 << 1);
        }
    }

    return (checkValue & toBeCheckedValue) ? true : false;
}

void TashMSSWProcessorInterface::computeMeanRuntime() {
    m_meanComputationTime = helper::TashMath::avg<qint64>(m_processingTimes);
}

REGISTER_METATYPE(TashMSSWProcessorInterface, "TashMSSWProcessorInterface")
}
}

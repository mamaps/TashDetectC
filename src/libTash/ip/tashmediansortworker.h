/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file tashmediansortworker.h
 * @author Michael Flau
 * @brief
 * @details
 * @date January 2016
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started January 2016
 */

#ifndef TASHMEDIANSORTWORKER_H
#define TASHMEDIANSORTWORKER_H

#include "tashabstractimageprocessingkernel.h"
#include "../core/tashtypes.h"
//#include "tashmediansortcommon.h"
#include "tashmsswaccessor.h"

#include <deque>

//@cond DOXYGEN_IGNORE
template <class T> class QSharedPointer;
//@endcond

namespace tashtego{
namespace ip{

//tashtego::ip forwardings
template <class T> class TashTile;

/**
 * @brief The tashmediansortworker class
 *
 * This is the leaf implementation of the actual detection algorithm as designed
 * for the ETAW project.
 *
 * This class is not usable as instance on it's own. It is rather designed as a thread object,
 * which is managed bei it's processing instance. Therefor multiple instances of the detction algorithm
 * can be run in parallel over a subset of tiles of the actual tiling window.
 *
 * As the very analyzation of tiles is an independet operation, it is a perfect
 * task for a multithreading applications.
 *
 * The instances of this object must be managed by a QThreadPool instance, as the class is
 * derived from QRunnable for easy thread management.
 *
 * Furthermore does this class not support the extraction of config values via inifile, as one
 * would might expect, being part of the tashtego softare project.
 * Every value which is to be passed to an instance of this class, must be passed in the ctor call
 * at the very beginning. The design approach for this class was mainly focused on perfomance and not
 * usability.
 *
 * @todo
 * - Still lacks multi ROI support
 */
class TashMedianSortWorker final: public TashAbstractImageProcessingKernel
{
	Q_OBJECT

    /**
     * @brief MedianSortDataContainer
     * Helper container to store stack of standard deviations and compare result
     * for each tile being processed
     */
    typedef struct {
        std::deque<double> stdDevStack; // double ended queue for stdev values
        double cmpResult;               // comparison result of tile
    } MedianSortDataContainer;

    /**
     * @brief scoreContainer
     * Internal struct definition, to provide a minimum of
     * handling convenience for data workload
     */
    typedef struct {
        qint32              roiid;              // id of regionof interest
        qint32              trid;               // id of tilerange
        qint32              tileid;             // id of tile
        qint32              uniqueTileId;       // unique id of tile for application's lifecycle
        qint32              width;              // width of tile
        qint32              height;             // height of tile
        qint8               currentscore;       // current score held by the tile, augmented by background model
        qint8               currentrawscore;    // current score as computed without augmentation
        qint8               scoreidx;           // Index of score value
        qint16              falseScoreidx;      // Index of blacklist score value
        std::deque<qint32>  scorelist;          // History of scores occupied over size of imagestack
        std::deque<qint32>  scoreBlacklist;     // History of scores occupied over size of imagestack + blacklist size
        QPoint              start;              // Startpoint of tile as (x,y) coordinate
        bool                ignore;             // Determines whether to ignore tile or not
        float               presenceRatio;      // Presence ratio value
    } scoreContainer;

    //Workload convenience substitute
    using Workload   = QVector< tileWorkloadBoundary<qint32> >;
    //Databuffer convenience substitute
    using DataBuffer = QVector<QVector<QVector<MedianSortDataContainer>>>;
    //Scorelist convenience substitute
    using ScoreList  = QVector<QVector<QVector<scoreContainer>>>;


public:

	/**
	 * @brief tashmediansortworker
     * Default ctor, only needed for introspection
	 * @param parent
	 * @warning
     * This header is only meant to be used for introspection discovery, do not call manually!
	 */
    explicit TashMedianSortWorker(TashAbstractImageProcessingKernel *parent = 0);

    /**
     * @brief tashmediansortworker
     * Constructor to be used when no backgroundmodel is incorporated
     * @param id
     * Id of worker instance
     * @param threadidx
     * Index of thread, worker instance is associated with
     * @param numMaxValues
     * Number of maximum values for index 0 to x to be stored for comparison
     * @param numROIs
     * Number of existing region of interests
     * @param numStdDevValues
     * Number of standard deviation values to be stored
     * @param numScoresToAccumulate
     * Number of tile score which will be accumulated over time to one value
     * @param applyTimesStdDev
     * Amount of standard deviation influence
     * @param scoreValidityBoundary
     * Minimum score value to be regared as possible candidate
     * @param workload
     * Actual workload of tiles, all tiles which are processed by the worker instance
     * are contained in this variable
     * @param outputMap
     * Reference to the output memory map, to write the tile candidates to
     * @param parent
     * Parent object reference of current instance
     */
    TashMedianSortWorker(qint64 id,
                         qint32 threadidx,
                         QVector<qint32> numMaxValues,
                         qint32 numROIs,
                         qint32 numStdDevValues,
                         qint32 numScoresToAccumulate,
                         double applyTimesStdDev,
                         quint32 scoreValidityBoundary,
                         QVector< tileWorkloadBoundary<qint32> > &workload,
                         QSharedPointer<rawtypes::scoreMap_r> outputMap,
                         TashAbstractImageProcessingKernel *parent = 0);

    /**
     * @brief tashmediansortworker
     * Constructor to be used when backgroundmodel data is incorporated
     * @param id
     * Id of worker instance
     * @param threadidx
     * Index of thread, worker instance is associated with
     * @param numMaxValues
     * Number of maximum values for index 0 to x to be stored for comparison
     * @param numROIs
     * Number of existing region of interests
     * @param numStdDevValues
     * Number of standard deviation values to be stored
     * @param numScoresToAccumulate
     * Number of tile score which will be accumulated over time to one value
     * @param applyTimesStdDev
     * Amount of standard deviation influence
     * @param scoreValidityBoundary
     * Minimum score value to be regared as possible candidate
     * @param workload
     * Actual workload of tiles, all tiles which are processed by the worker instance
     * are contained in this variable
     * @param outputMap
     * Reference to the output memory map, to write the tile candidates to
     * @param backgroundModel
     * Reference to memory map accessor, to background model data
     * @param parent
     * Parent object reference of current instance
     */
    TashMedianSortWorker(qint64 id,
                         qint32 threadidx,
                         QVector<qint32> numMaxValues,
                         qint32 numROIs,
                         qint32 numStdDevValues,
                         qint32 numScoresToAccumulate,
                         double applyTimesStdDev,
                         quint32 scoreValidityBoundary,
                         QVector< tileWorkloadBoundary<qint32> > &workload,
                         QSharedPointer<rawtypes::scoreMap_r> outputMap,
                         QPointer<TashMSSWAccessor<qint16>> backgroundModel,
                         TashAbstractImageProcessingKernel *parent = 0);

    /**
     * @brief tashmediansortworker
     * Constructor to be used when backgroundmodel data and a presence ratio
     * filter is incorporated.
     * @param id
     * Id of worker instance
     * @param threadidx
     * Index of thread, worker instance is associated with
     * @param numMaxValues
     * Number of maximum values for index 0 to x to be stored for comparison
     * @param numROIs
     * Number of existing region of interests
     * @param numStdDevValues
     * Number of standard deviation values to be stored
     * @param numScoresToAccumulate
     * Number of tile score which will be accumulated over time to one value
     * @param applyTimesStdDev
     * Amount of standard deviation influence
     * @param scoreValidityBoundary
     * Minimum score value to be regared as possible candidate
     * @param workload
     * Actual workload of tiles, all tiles which are processed by the worker instance
     * are contained in this variable
     * @param outputMap
     * Reference to the output memory map, to write the tile candidates to
     * @param backgroundModel
     * Reference to memory map accessor, to background model data
     * @param usePresenceRationFilter
     * Flag determines if presence ration filter will be enabled
     * @param presenceRatioThreshold
     * Threshold of the presence ratio filter, to trigger activation
     * @param parent
     * Parent object reference of current instance
     */
    TashMedianSortWorker(qint64 id,
                         qint32 threadidx,
                         QVector<qint32> numMaxValues,
                         qint32 numROIs,
                         qint32 numStdDevValues,
                         qint32 numScoresToAccumulate,
                         double applyTimesStdDev,
                         quint32 scoreValidityBoundary,
                         QVector< tileWorkloadBoundary<qint32> > &workload,
                         QSharedPointer<rawtypes::scoreMap_r> outputMap,
                         QPointer<TashMSSWAccessor<qint16>> backgroundModel,
                         bool usePresenceRatioFilter,
                         float presenceRatioThreshold,
                         TashAbstractImageProcessingKernel *parent = 0);

	/**
	 * @brief ~tashmediansortworker
     * Default dtor
	 */
    virtual ~TashMedianSortWorker() = default;

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() override;

    /**
     * @brief run
     * Actual worker method, which is being executed within the thread context.
     * All tile processing is being done from here on as a starting point.
     */
    virtual void run() override;

    /**
     * @brief setMultiScaleFrame
     * Passes a reference of a multiscale frame to the worker instance
     * @param frame
     * Reference to next multiscale frame to be processed
     */
    virtual void setMultiScaleFrame(QPointer<TashMSSWAccessor<qint16>> frame);


public slots:

    /**
     * @brief toggleBackgroundModel
     * Enables or disables the usage of background model data
     * @param useBGM
     * Boolean flag which denotes backgroundmodel usage
     */
    virtual void toggleBackgroundModel(const bool useBGM);


protected:

    /**
     * @brief init
     * Initialization method of worker class. Initializes the score structure
     * and helper structures for temporary value storage.
     */
    virtual bool init() override;


private:

    /**
     * @brief tileDataToVector
     * Transforms raw tile image data to a formated data vector for easier
     * handling in following steps.
     * @param targetVec
     * Reference to output vector, stores formatted tile data of input tile
     * @param pTile
     * Reference to tile of which data is supposed to be transformed.
     * @return
     */
    inline bool tileDataToVector(std::vector<qint16>& targetVec, TashTile<qint16> *pTile);

    /**
     * @brief checkTileRangeForZeroes
     * Checks the current tile range for tiles which have no image data. That is
     * the case if a image mask is enabled.
     * @param range
     * Reference to current tile range
     * @return
     * True if yero values were foudn, otherwise false
     */
    inline bool checkTileRangeForZeroes(std::vector<TashTile<qint16> *> range);

    /**
     * @brief stdDev
     * Computes standard deviation of tile data
     * @param pValues
     * Tile data in STL vector
     * @return
     * Standard deviation of values as double
     */
    inline double stdDev(std::vector<qint16>& pValues);

    /**
     * @brief meanOfStdDev
     * Computes mean of standard deviations of all collected over last X frames
     * of tile.
     * @param input
     * Double ended queue with all collected standard deviation over last X frames
     * of tile.
     * @return
     * Mean value of standard deviations
     */
    inline double meanOfStdDev( const std::deque<double>& input );

    /**
     * @brief computeBackgroundModelScore
     * Computes the score of the specific tile of the background model and
     * returns it.
     * @param pBGMTile
     * Input tile of background model, if method is called without an input tile
     * the returned score is 1 and has no effect at all. This more performant than
     * handling a costly exception, if no background model data is required.
     * @return
     * background model score
     */
    inline qint16 computeBackgroundModelScore( TashTile<qint16> *pBGMTile = nullptr );

    /**
     * @brief setScore
     * Sets the current score of a tile
     * @param pTile
     * Reference to current tile
     * @param roi
     * Index of region of interest
     * @param row
     * Index of tile row
     * @param tileidx
     * index of tile in row
     * @param decs
     * Actual tile score
     * @param bgmScore
     * Background model score
     * @param ignore
     * Flag denotes if tile should be ignored. An appropriate entry will be made
     * for tile in output memory map
     */
    inline void setScore(TashTile<qint16> *pTile,
                         const qint32& roi,
                         const qint32& row,
                         const qint32& tileidx,
                         const bool decs,
                         const qint16& bgmScore = 1,
                         const bool ignore = false);

    /**
     * @brief resetScore
     * Reset specified score slot, defined by passed indeces
     * @param pTile
     * Reference to current tile
     * @param roi
     * Region interest index
     * @param row
     * Row index
     * @param tileidx
     * Tile index
     */
    inline void resetScore(TashTile<qint16> *pTile,
                           const qint32& roi,
                           const qint32& row,
                           const qint32& tileidx,
                           const qint16& bgmScore = 1);

    /**
     * @brief writeResultToMemoryMap
     * Dumps the current content of teh score structure to the output memory map.
     * This is the final write operation which will be done after the full tile
     * workload has be processed.
     */
    inline void writeResultToMemoryMap();

    /**
     * @brief determinePresenceRatio
     * Computes the presence ratio of a score container as opposed to a
     * statically selected threshold, if the tile presence is beyond the threshold
     * the tile will be ignored.
     * @param score
     * Current tile score
     * @param bgmScore
     * Backgroundmodel score
     * @param ignore
     * Determines if tile should be ignored, this overrides the threshold decision,
     * and flags teh tile to be igored anyway.
     */
    inline void determinePresenceRatio(scoreContainer &score,
                                       const qint16& bgmScore,
                                       const bool ignore = false);

    /*
     * Members
     */
    bool                                    m_bHasMSSWFrameSet;          ///> Flag which denotes whether a source frame is set or not
    bool                                    m_bApplyBackgroundModel;     ///> Flag denotes whether to apply background model mean score to tile score or not
    bool                                    m_bApplyPresenceRatioFilter; ///> Flag denotes whether to filter tiles which show permanent presence behaviour
    qint32                                  m_ThreadIdx;                 ///> Index of thread the respective instance will be associated with
    QVector<qint32>                         m_numMaxValues;              ///> Number of maximum values to take into consideration
    qint32                                  m_numROIs;                   ///> Number of Region of interests
    qint32                                  m_numStdDevValues;           ///> Number of stddev values to store in queue
    qint32                                  m_numScoresToAccumulate;     ///> Denotes over what amount of frames the score is to be collected and summed up
    double                                  m_applyTimesStdDev;          ///> Number of times respective stddev value is applied
    qint32                                  m_ScoreValidityBoundary;     ///> Score boundary depicts as at what amount of sucessing frames an event is regarded as true positive
    qint32                                  m_OverallTileAmount;         ///> The absolute amount of tiles associated with the respective class instance
    float                                   m_PresenceRatioThreshold;    ///> Value denotes whether to ignore a tile beacuase of too much presence
    qint32                                  m_PresenceRatioScoreHold;
    QSharedPointer<rawtypes::scoreMap_r>    m_OutputMap;                 ///> Output memorymap where the actual computed scores are written to
    QPointer<TashMSSWAccessor<qint16>>      m_pMSSWFrame;                ///> Pointer to multiscale frame instance which is associated with the current camera frame
    QPointer<TashMSSWAccessor<qint16>>      m_pBackgroundModel;          ///> Pointer to IMEM memory map field which contains data of background model
    Workload                                m_Workload;                  ///> Contains all workload boundaries as structures which denote one part of a tile row each
    DataBuffer                              m_IntermediateDataBuffer;    ///> Data buffer which stores intermediate results
    ScoreList                               m_InternalScoreList;         ///> Internal score representation of each tile asscoiated with resp. class instance
};

}
}

Q_DECLARE_METATYPE(tashtego::ip::TashMedianSortWorker)

#endif //TASHMEDIANSORTWORKER_H


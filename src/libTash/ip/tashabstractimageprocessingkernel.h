/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashabstractimageprocessingkernel.h
 * @author Michael Flau
 * @brief
 * @details
 * @date November 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started November 2015
 */

#ifndef TASHABSTRACTIMAGEPROCESSINGKERNEL_H
#define TASHABSTRACTIMAGEPROCESSINGKERNEL_H

#include "../core/tashobject.h"
#include "tashmediansortcommon.h"
#include <QRunnable>


namespace tashtego{
namespace ip{

/**
 * @brief The TashAbstractImageProcessingKernel class
 * Abstract processing kernel interface for all detectors to be implemented.
 * The interface is derived from TashObject, and QRunnable which is necessary
 * in order to have kernel instances running in threads managed by a QThreadPool
 * instance.
 */
class TashAbstractImageProcessingKernel: public core::TashObject, public QRunnable
{

	Q_OBJECT

public:

    /**
     * @brief TashAbstractImageProcessingKernel
     * Default ctor
     * @param parent
     * Reference to creating parent object
     */
    TashAbstractImageProcessingKernel(TashObject *parent = 0);

	/**
	 * @brief TashAbstractImageProcessingKernel
     * This constructor does technically nothing but to pass the instance id
     * onward to the base TashObject.
	 * @param id
     * Instance Id
	 * @param parent
     * Reference to creating parent object
	 */
    TashAbstractImageProcessingKernel(qint64 id, core::TashObject *parent = 0);

	/**
	 * @brief ~TashAbstractImageProcessingKernel
     * Default dtor
	 */
    virtual ~TashAbstractImageProcessingKernel() = default;

	/**
	 * @brief init
     * Pure virtual interface of init method, needs to be reimplemented by
     * every derived kernel class.
     * @return
     * True on successfull initialization, false on failure while initialization
     * is running.
	 */
    virtual bool init() = 0;

    /**
     * @brief getClassName
     * Pure virtual interface of getClassName method. Needs to be reimplemented
     * by every derived kernel class.
     * @return
     * QString stating the name of the class instance.
     */
    virtual QString getClassName() = 0;

    /**
     * @brief run
     * Pure virtual interface of run method of QRunnable, forwarded here, so that
     * every derived kernel class needs to reimplement it. This is the very method
     * which will be run in different thread managed by a QThreadpool instance.
     * The actual detection logic should be executed within the run scope.
     */
    virtual void run() = 0;

signals:
    void finished(qint64 id);
};

}
}

#endif //TASHABSTRACTIMAGEPROCESSINGKERNEL_H


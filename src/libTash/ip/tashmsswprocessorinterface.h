/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashmsswprocessorinterface.h
 * @author Michael Flau
 * @brief
 * @details
 * @date November 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Schwerin - Germany, Started November 2015
 */

#ifndef TASHMSSWPROCESSORINTERFACE_H
#define TASHMSSWPROCESSORINTERFACE_H

#include <deque>

#include "../core/tashobject.h"
#include "../core/tashtypes.h"

//@cond DOXYGEN_IGNORE
template <class T> class QVector;
template <class T> class QPointer;
//@endcond


namespace tashtego{
namespace ip{

class TashAbstractProcessor;

/**
 * @brief The TashMSSWProcessorInterface class
 */
class TashMSSWProcessorInterface final: public core::TashObject
{
	Q_OBJECT

public:

	/**
	 * @brief TashMSSWProcessorInterface
	 * @param parent
	 * @warning
	 * This header is only for meant to be used for introspection discovery, do not call manually!
	 */
	explicit TashMSSWProcessorInterface(TashObject *parent = 0);

    /**
     * @brief TashMSSWProcessorInterface
     * @param id
     * @param processor
     * @param doStatistics
     * @param parent
     */
    TashMSSWProcessorInterface(qint64 id,
                               QPointer<core::TashRuntime> runtime,
                               TashAbstractProcessor* processor,
                               bool doStatistics = false,
                               TashObject *parent = 0);

	/**
	 * @brief ~TashMSSWProcessorInterface
	 */
	virtual ~TashMSSWProcessorInterface();

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() override;

signals:
    /**
     * @brief processed
     * @param frameidx
     */
    void processed(qint32 frameidx);

public slots:

    /**
     * @brief invoke
     * @param frameidx
     * @param slot
     */
    void invoke(quint32 frameidx, qint32 slot);

    /**
     * @brief hasProcessor
     * @return
     */
    bool hasProcessor() const;

    /**
     * @brief dockProcessor
     * @param processor
     * @return
     */
    bool dockProcessor( TashAbstractProcessor* processor );

    /**
     * @brief getMeanProcessingTime
     * @return
     */
    qint64 getAvgProcessingTime() const;

    /**
     * @brief getNumberOfProcessFrames
     * @return
     */
    quint64 getNumberOfProcessedFrames() const;

protected:
    /**
     * @brief init
     */
    virtual bool init() override;

    /**
     * @brief writeDefaultProperties
     */
    void writeDefaultProperties();

    /**
     * @brief parsePropertiesFromRuntime
     */
    bool parsePropertiesFromRuntime();

    /**
     * @brief computeMeanRuntime
     */
    void computeMeanRuntime();

private:
    bool                    m_bDoStatistics;        ///> Flag which denotes whether to do runtime statistics whiel running
    quint64                 m_numProcessedFrames;   ///> Continuous counter which counts the number of processed frames
    qint64                  m_lastProcessingTime;   ///> Processing time of last frame
    quint32                 m_integrationRange;     ///> Range over which to integrate to compute mean computing time
    qint64                  m_meanComputationTime;  ///> Mean computation time as updated every cycle
    TashAbstractProcessor*  m_pProcessor;           ///> Processor instance
    std::deque<qint64>      m_processingTimes;      ///> Double ended queue which holds all runtime samples of the last m_integrationRange frames
};

}
}

Q_DECLARE_METATYPE(tashtego::ip::TashMSSWProcessorInterface)

#endif //TASHMSSWPROCESSORINTERFACE_H


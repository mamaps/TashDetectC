/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashmediansortprocessormodule.cpp
 * @author Michael Flau
 * @date November 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started November 2015
 */

#include "tashmediansortprocessormodule.h"
#include "../core/tashruntime.h"
#include "../com/tashtypedmemorymap.h"
#include "tashmsswaccessor.h"
#include "tashmediansortworker.h"

#include <QSharedPointer>
#include <QThreadPool>
#include <QElapsedTimer>
#include <QString>

namespace tashtego{
namespace ip{

TashMedianSortProcessorModule::TashMedianSortProcessorModule(TashAbstractProcessor *parent):
    TashAbstractProcessor(parent),
    m_bUseBackgroundModel(false),
    m_bUsePresenceRatioFilter(false),
    m_PresenceRatioThreshold(1.0f),
    m_considerXMax(0),
    m_timesStdDev(0.0),
    m_stddevStackSize(0),
    m_pBackgroundModel(nullptr),
    m_pOutputMap(nullptr)
{
    if ( !TashAbstractProcessor::setDefaultProperties() ) {
#ifdef TASHVERBOSE
        qCritical() << "Writing of parent class properties failed!";
#endif
    }

    if ( !setDefaultProperties() ) {
#ifdef TASHVERBOSE
        qCritical() << "Writing of TashMedianSortProcessorModule properties failed!";
#endif
    }
}

TashMedianSortProcessorModule::TashMedianSortProcessorModule(qint64 id,
                                                             const QPointer<core::TashRuntime> runtime,
                                                             const QMap<qint32, QPointer<TashMSSWAccessor<qint16>>> &accessorBuffer,
                                                             quint32 cameraIdx,
                                                             QSharedPointer<rawtypes::scoreMap_r> outputMap,
                                                             TashAbstractProcessor *parent):
    TashAbstractProcessor(id, runtime, accessorBuffer, cameraIdx, parent),
    m_bUseBackgroundModel(false),
    m_bUsePresenceRatioFilter(false),
    m_PresenceRatioThreshold(1.0f),
    m_considerXMax(0),
    m_timesStdDev(0.0),
    m_stddevStackSize(0),
    m_scoreAccumulationStackSize(0),
    m_pBackgroundModel(nullptr),
    m_pOutputMap(outputMap)
{
    auto status = TashAbstractProcessor::parseRuntimeForProperties();
    Q_ASSERT( status );
    if ( !status ) {
		qCritical() << "Parsing general runtime properties of detector failed!";
		m_bIsInitialized = status;
		return;
    }

    status = TashAbstractProcessor::init();
    Q_ASSERT( status );

    if ( !status ) {
		qCritical() << "Initialization of abstract processor logic failed!";
		m_bIsInitialized = status;
		return;
    }

    status = parseRuntimeForProperties();
    Q_ASSERT( status );

    if ( !status ) {
        qCritical() << "Parsing runtime properties of median sort processor module failed!";
        m_bIsInitialized = status;
        return;
    }

    status = init();
    Q_ASSERT( status );

    if ( !status ) {
        qCritical() << "Initialization of median sort processor module failed!";
    }
}

TashMedianSortProcessorModule::TashMedianSortProcessorModule(qint64 id,
                              const QPointer<core::TashRuntime> runtime,
                              const QMap<qint32, QPointer<TashMSSWAccessor<qint16>>> &accessorBuffer,
                              const QPointer<ip::TashMSSWAccessor<qint16>> backgroundModel,
                              quint32 cameraIdx,
                              QSharedPointer<tashtego::rawtypes::scoreMap_r> outputMap,
                              TashAbstractProcessor *parent):
    TashAbstractProcessor(id, runtime, accessorBuffer, cameraIdx, parent),
    m_bUseBackgroundModel(false),
    m_bUsePresenceRatioFilter(false),
    m_PresenceRatioThreshold(1.0f),
    m_considerXMax(0),
    m_timesStdDev(0.0),
    m_stddevStackSize(0),
    m_scoreAccumulationStackSize(0),
    m_pBackgroundModel(backgroundModel),
    m_pOutputMap(outputMap)
{
    Q_CHECK_PTR( m_pBackgroundModel.data() );
    auto status = m_pBackgroundModel.isNull();
    Q_ASSERT( !status );

    if ( status ) {
        qCritical() << "Pointer to background model data is invalid, abort!";
        m_bIsInitialized = !status;
        return;
    }

    status = TashAbstractProcessor::parseRuntimeForProperties();
    Q_ASSERT( status );
    if ( !status ) {
		qCritical() << "Parsing general runtime properties of detector failed!";
		m_bIsInitialized = status;
		return;
    }

    status = TashAbstractProcessor::init();
    Q_ASSERT( status );

    if ( !status ) {
		qCritical() << "Initialization of abstract processor logic failed!";
		m_bIsInitialized = status;
		return;
    }

    status = parseRuntimeForProperties();
    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "Parsing runtime properties of median sort processor module failed!";
        m_bIsInitialized = status;
        return;
    }

    status = init();
    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "Initialization of median sort processor module failed!";
    }
}

TashMedianSortProcessorModule::TashMedianSortProcessorModule(qint64 id,
                                                             const QPointer<core::TashRuntime> runtime,
                                                             const QMap<qint32, QPointer<TashMSSWAccessor<qint16>>> &accessorBuffer,
                                                             quint32 cameraIdx,
                                                             qint32 maxNumThreads,
                                                             QSharedPointer<rawtypes::scoreMap_r> outputMap,
                                                             quint32 reserveNumThreads,
                                                             quint32 scoreValidityBoundary,
                                                             TashAbstractProcessor::ProcessingStrategy strategy,
                                                             qint32 frameBufferLimit,
                                                             QVector<qint32> considerNumMaxValues,
                                                             double timesApplyStdDev,
                                                             qint32 stdDevStackSize,
                                                             qint32 frameScoreStackSize,
                                                             TashAbstractProcessor *parent):
    TashAbstractProcessor(id,
                          runtime,
                          accessorBuffer,
                          cameraIdx,
                          maxNumThreads,
                          reserveNumThreads,
                          scoreValidityBoundary,
                          strategy,
                          frameBufferLimit,
                          parent),
    m_bUseBackgroundModel(false),
    m_bUsePresenceRatioFilter(false),
    m_PresenceRatioThreshold(1.0f),
    m_considerXMax(considerNumMaxValues),
    m_timesStdDev(timesApplyStdDev),
    m_stddevStackSize(stdDevStackSize),
    m_scoreAccumulationStackSize(frameScoreStackSize),
    m_pBackgroundModel(nullptr),
    m_pOutputMap(outputMap) {

    bool status = init();
    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "Initialization of median sort processor module failed!";
    }
}

TashMedianSortProcessorModule::TashMedianSortProcessorModule(qint64 id,
                              const QPointer<core::TashRuntime> runtime,
                              const QMap<qint32, QPointer<TashMSSWAccessor<qint16>>> &accessorBuffer,
                              quint32 cameraIdx,
                              qint32 maxNumThreads,
                              QSharedPointer<tashtego::rawtypes::scoreMap_r> outputMap,
                              const QPointer<ip::TashMSSWAccessor<qint16>> backgroundModel,
                              bool useBGMModel,
                              quint32 reserveNumThreads,
                              quint32 scoreValidityBoundary,
                              ProcessingStrategy strategy,
                              qint32 frameBufferLimit,
                              QVector<qint32> considerNumMaxValues,
                              double timesApplyStdDev,
                              qint32 stdDevStackSize,
                              qint32 frameScoreStackSize,
                              bool usePresenceRatioFilter,
                              float presenceRatioThreshold,
                              TashAbstractProcessor *parent):
    TashAbstractProcessor(id,
                          runtime,
                          accessorBuffer,
                          cameraIdx,
                          maxNumThreads,
                          reserveNumThreads,
                          scoreValidityBoundary,
                          strategy,
                          frameBufferLimit,
                          parent),
    m_bUseBackgroundModel(useBGMModel),
    m_bUsePresenceRatioFilter(usePresenceRatioFilter),
    m_PresenceRatioThreshold(presenceRatioThreshold),
    m_considerXMax(considerNumMaxValues),
    m_timesStdDev(timesApplyStdDev),
    m_stddevStackSize(stdDevStackSize),
    m_scoreAccumulationStackSize(frameScoreStackSize),
    m_pBackgroundModel(backgroundModel),
    m_pOutputMap(outputMap) {

    Q_CHECK_PTR(m_pBackgroundModel.data());
    bool status  = m_pBackgroundModel.isNull();
    Q_ASSERT(!status);

    if ( status) {
        qCritical() << "Pointer to background model data is invalid, abort!";
        m_bIsInitialized = !status;
        return;
    }

    status = TashAbstractProcessor::init();
    Q_ASSERT(status);

    if ( !status ) {
#ifdef TASHVERBOSE
        qCritical() << "Failed to initialize TashAbstractProcessor instance!";
#endif
		m_bIsInitialized = status;
		return;
    }

    status = init();
    Q_ASSERT(status);

    if ( !status ) {
#ifdef TASHVERBOSE
        qCritical() << "Failed to initialize TashMedianSortProcessorModule instance!";
#endif
		m_bIsInitialized = status;
    }
}

TashMedianSortProcessorModule::~TashMedianSortProcessorModule () {

    switch( m_strategy ) {

    case TILE_WISE:
        break;
    case TILELINE_WISE:
    {
        QHashIterator<qint32, QPointer<TashMedianSortWorker>> hashIt(m_workerKernels);

        while( hashIt.hasNext() ) {
            hashIt.next();
            QPointer<TashMedianSortWorker> worker = hashIt.value();
            if ( worker ) {
                delete worker;
                worker = nullptr;
            }
        }
    }
        break;
    case TR_WISE:
    break;
    case ROI_WISE:
        break;
    }
}

bool TashMedianSortProcessorModule::init() {

    bool status = false;

    switch( m_strategy ) {

    case TILE_WISE:
        qWarning() << "Tile wise processing is not implemented!";
        break;
    case TILELINE_WISE:
    {
        QElapsedTimer timer; timer.start();
        status = computeThreadWorkload();

        if ( !status ) {
            qCritical() << "Failed to compute workload for each thread!";
            m_bIsInitialized = status;
            return status;
        }
#ifdef TASHVERBOSE
        qDebug() << "Computed thread workload in " << timer.elapsed() << " msecs";
#endif
        //Set the number of used detection slots in outputmap once,
        //as they won't change during runtime ( for now )
#ifdef TASHVERBOSE
        qDebug() << "Writing number of used detection slots to scoremap...";
#endif
        for ( auto coreIdx = 0; coreIdx < tashtego::SCORE_MAP_DEFAULT_NUM_CORES; ++coreIdx ) {
            m_pOutputMap->threadList[coreIdx].numdetectthreads = m_maxAllowedThreads;
        }
#ifdef TASHVERBOSE
        qDebug() << "Done!";
#endif
        for ( auto threadIdx = 0; threadIdx < m_maxAllowedThreads; threadIdx++ ) {
            m_pOutputMap->threadList[threadIdx].readlock  = 0;
            m_pOutputMap->threadList[threadIdx].writelock = 0;
        }

        for ( auto threadIdx = 0; threadIdx < m_maxAllowedThreads; threadIdx++ ) {

            /**
             * @todo Ctor does only take workload of only one selected ROI,
             * multi ROI support needed for later!
             */
#ifdef TASHVERBOSE
            qDebug() << "Creating detector worker thread " << threadIdx << "/" << m_maxAllowedThreads;
#endif
            timer.restart();

            TashMedianSortWorker* pTmpWorker = nullptr;

            if ( m_bUseBackgroundModel ) {

                pTmpWorker = new TashMedianSortWorker(core::TashRuntime::genID(),
                                                      threadIdx,
                                                      m_considerXMax,
                                                      1,
                                                      m_stddevStackSize,
                                                      m_scoreAccumulationStackSize,
                                                      m_timesStdDev,
                                                      m_scoreValidityBoundary,
                                                      m_WorkloadBoundaries[0][threadIdx],
                                                      m_pOutputMap,
                                                      m_pBackgroundModel,
                                                      m_bUsePresenceRatioFilter,
                                                      m_PresenceRatioThreshold);

            } else {
                pTmpWorker = new TashMedianSortWorker(core::TashRuntime::genID(),
                                                      threadIdx,
                                                      m_considerXMax,
                                                      1,
                                                      m_stddevStackSize,
                                                      m_scoreAccumulationStackSize,
                                                      m_timesStdDev,
                                                      m_scoreValidityBoundary,
                                                      m_WorkloadBoundaries[0][threadIdx],
                                                      m_pOutputMap);
            }

            Q_CHECK_PTR(pTmpWorker);
            m_workerKernels[threadIdx] = QPointer<TashMedianSortWorker>(pTmpWorker);

            Q_CHECK_PTR(m_workerKernels[threadIdx]);
            m_workerKernels[threadIdx]->setAutoDelete(false);
#ifdef TASHVERBOSE
            qDebug() << "Thread " << threadIdx+1 << "/" << m_maxAllowedThreads << " created in " << timer.elapsed() << " msecs";
#endif
        }
    }
        break;
    case TR_WISE:
        qWarning() << "Range wise processing is not implemented!";
        break;
    case ROI_WISE:
        qWarning() << "ROI wise processing is not implemented!";
        break;
    }

    Q_ASSERT(status);
    m_bIsInitialized = status;
    return m_bIsInitialized;
}

void TashMedianSortProcessorModule::process(qint32 &frameindex) {

    if ( !this->isInitialized() ) {
        qCritical() << "TashMedianSortProcessorModule instance " << this->getObjectID() << " is not initialized! Check log for errors at instance startup!";
        return;
    }

    QPointer<TashMSSWAccessor<qint16>> &pAccessor = m_msswBuffer[frameindex];

    switch( m_strategy ) {
    case TILE_WISE:
        qWarning() << "Tile wise processing is not implemented!";
        break;
    case TILELINE_WISE:
    {
        for ( qint32 workeridx = 0; workeridx < m_maxAllowedThreads; ++workeridx ) {
            m_workerKernels[workeridx]->setMultiScaleFrame(pAccessor);
            m_pThreadPool->start(m_workerKernels[workeridx].data());
        }

        m_pThreadPool->waitForDone();
    }
        break;
    case TR_WISE:
        qWarning() << "Range wise processing is not implemented!";
        break;
    case ROI_WISE:
        qWarning() << "ROI wise processing is not implemented!";
        break;
    }


    emit frameProcessed(frameindex);
}

bool TashMedianSortProcessorModule::parseRuntimeForProperties() {

    QString      classname        = QString( this->metaObject()->className()).split("::").last();
    QStringList  rawProperties    = m_pRunTime->getPropertyNamesContaining( classname, m_cameraIdx);
    const qint16 checkValue       = 0x3F;
    qint16       toBeCheckedValue = 0x0;
    qint32       tmpValInt        = 0;
    double       tmpValDouble     = 0.0;
    bool         tmpValBool       = false;
    float        tmpValFloat      = 0.0;

#ifdef TASHVERBOSE
    qDebug() << "Meta object derived classname is " << classname << ", searching for properties";
#endif

    foreach (QString str, rawProperties) {

        QString strsplit = str.split("-").last();

        if ( strsplit == "consider_num_max_values" ) {
            QStringList raw = m_pRunTime->property(str).toString().split("|");

            Q_ASSERT(raw.size() > 0);

            if ( raw.size() <= 0 ) {
                qWarning() << "Invalid number of maximum values given, to consider for computation, default to 2, 4 and 8!";
                m_considerXMax = {2, 4, 8};
            } else {
                m_considerXMax = QVector<qint32>(0);

                foreach(QString val, raw) {
                    m_considerXMax.append(val.toInt());
                }
            }
#ifdef TASHVERBOSE
            qDebug() << "Parsed number of maximum values to consider: " << m_considerXMax;
#endif
            toBeCheckedValue |= 1;
        }

        if ( strsplit == "multiply_lr_stddev_mean_with" ) {
            tmpValDouble = m_pRunTime->property(str).toDouble();
            Q_ASSERT(tmpValDouble >= 0.0);

            if ( tmpValDouble < 0.0 ) {
                qWarning() << "Invalid multiplication factor computed standard deviations, default to 2!";
                tmpValDouble = 2.0;
            }

            m_timesStdDev = tmpValDouble;
#ifdef TASHVERBOSE
            qDebug() << "Parsed factor to multiply lr stddev mean with: " << m_timesStdDev;
#endif
            toBeCheckedValue |= (1 << 1);
        }

        if ( strsplit == "compute_stddev_over_num_frames" ) {
            tmpValInt = m_pRunTime->property(str).toInt();
            Q_ASSERT(tmpValInt > 0);

            if ( tmpValInt <= 0 ) {
                qWarning() << "Invalid number of frames to consider to compute mean of standard deviation, default to 50!";
                tmpValInt = 50;
            }

            m_stddevStackSize = tmpValInt;
#ifdef TASHVERBOSE
            qDebug() << "Parsed number of frames to consider for mean of std deviation: " << m_stddevStackSize;
#endif
            toBeCheckedValue |= (1 << 2);
        }

        if ( strsplit == "accumulate_score_over_num_frames" ) {
            tmpValInt = m_pRunTime->property(str).toInt();
            Q_ASSERT(tmpValInt > 0);

            if ( tmpValInt <= 0 ) {
                qWarning() << "Invalid number of frames, over which to accumulate final score, default to 30!";
                tmpValInt = 30;
            }

            m_scoreAccumulationStackSize = tmpValInt;
#ifdef TASHVERBOSE
            qDebug() << "Parsed number of scores values for accumulated score is: " << m_scoreAccumulationStackSize;
#endif
            toBeCheckedValue |= (1 << 3);
        }

        if ( strsplit == "use_background_model" ) {
            tmpValBool            = m_pRunTime->property(str).toBool();
            m_bUseBackgroundModel = tmpValBool;
            toBeCheckedValue |= (1 << 4);
#ifdef TASHVERBOSE
            auto dbg = qDebug().nospace();
            dbg << "Detector will " << (m_bUseBackgroundModel ? "" : "not") << " use Background Model for detection augmentation";
#endif
        }

        if ( strsplit == "use_presence_ratio_filter" ) {
            tmpValBool                = m_pRunTime->property(str).toBool();
            m_bUsePresenceRatioFilter = tmpValBool;
            toBeCheckedValue |= (1 << 5);
#ifdef TASHVERBOSE
            auto dbg = qDebug().nospace();
            dbg << "Detector will " << (m_bUsePresenceRatioFilter ? "" : "not") << " use Presence Ratio filter";
#endif
        }

        if ( strsplit == "presence_ratio_threshold" ) {
            tmpValFloat = m_pRunTime->property(str).toDouble();

            if ( tmpValFloat > 1.0f || tmpValFloat < 0.0f ) {
                qWarning() << strsplit << " field, is not in valid threshold range, choose value between 0.0 and 1.0, default to 1.0 now!";
                tmpValFloat = 1.0;
            }

            m_PresenceRatioThreshold = tmpValFloat;
            toBeCheckedValue |= (1 << 6);
#ifdef TASHVERBOSE
            qDebug() << "Parsed Presence Ratio Threshold is " << m_PresenceRatioThreshold;
#endif
        }
    }

    return (checkValue & toBeCheckedValue) ? true : false;
}

bool TashMedianSortProcessorModule::setAccessorBuffer(QMap<qint32, QPointer<TashMSSWAccessor<qint16>>> &buffer) {

    for ( auto it = buffer.begin(); it != buffer.end(); ++it ) {
        Q_CHECK_PTR(it.value());

        if ( !*it ) {
            qCritical() << "Found invalid pointer to multiscale instance! Abort!";
            return false;
        }
    }

    m_msswBuffer = buffer;

    if ( !computeThreadWorkload() ) {
        qCritical() << "Error while computing workloads for single threads!";
        return false;
    }

    return true;
}


bool TashMedianSortProcessorModule::setDefaultProperties() {

    auto status = false;

    status = this->setProperty("consider_num_max_values", "2|4|8|16|32");
    Q_ASSERT(!status);

    status = this->setProperty("multiply_lr_stddev_mean_with", 2.0);
    Q_ASSERT(!status);

    status = this->setProperty("compute_stddev_over_num_frames", 50);
    Q_ASSERT(!status);

    status = this->setProperty("accumulate_score_over_num_frames", 30);
    Q_ASSERT(!status);

    status = this->setProperty("processing_kernel_type", "sortmean");
    Q_ASSERT(!status);

    status = this->setProperty("use_background_model", false);
    Q_ASSERT(!status);

    status = this->setProperty("use_presence_ratio_filter", false);
    Q_ASSERT(!status);

    status = this->setProperty("presence_ratio_threshold", 0.9f);
    Q_ASSERT(!status);

    return status;
}

REGISTER_METATYPE(TashMedianSortProcessorModule, "TashMedianSortProcessorModule")
}
}

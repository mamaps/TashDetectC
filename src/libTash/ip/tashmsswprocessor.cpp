/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashmsswprocessor.cpp
 * @author Michael Flau
 * @brief
 * @details
 * @date November 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started November 2015
 */

#include "tashmsswprocessor.h"
#include "../core/tashruntime.h"
#include "tashmsswaccessor.h"
#include "tashmsswprocessorbuilder.h"
#include "tashmsswprocessorinterface.h"
#include <QPointer>
#include <QString>

namespace tashtego{
namespace ip{

TashMSSWProcessor::TashMSSWProcessor(tashtego::core::TashObject *parent):
    tashtego::core::TashObject(parent),
    m_cameraIdx(0),
    m_pBuilder(nullptr),
    m_pProcessor(nullptr),
    m_msswBuffer() {
    setDefaultProperties();
}

TashMSSWProcessor::TashMSSWProcessor(qint64 id,
                                     const QPointer<core::TashRuntime> runtime,
                                     const QMap<qint32, QPointer<TashMSSWAccessor<qint16>>> &accessorBuffer,
                                     quint32 cameraIdx,
                                     const QString &processorName,
                                     tashtego::core::TashObject *parent):
    tashtego::core::TashObject(id, runtime, parent),
    m_cameraIdx(cameraIdx),
    m_pBuilder(nullptr),
    m_pProcessor(nullptr),
    m_processorName(processorName),
    m_msswBuffer(accessorBuffer) {

    if ( parseRuntimeForProperties() ) {
        init();
    } else {
        qCritical("Initialization of MSSW processor failed!");
    }
}

TashMSSWProcessor::~TashMSSWProcessor () {

    if ( !m_pProcessor.isNull() && m_pProcessor->isInitialized() ) {
        m_pProcessor->deleteLater();
    }

    if ( m_pBuilder && m_pBuilder->isInitialized() ) {
        delete m_pBuilder;
    }
}

QString TashMSSWProcessor::getClassName() {
    return QString("TashMSSWProcessor");
}

bool TashMSSWProcessor::init() {

    bool status = this->isRuntimeAware();

    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "TashMSSWprocessor needs runtime instance to be instantiated correctly! Can't proceed!";
        m_bIsInitialized = status;
        return status;
    }
#ifdef TASHVERBOSE
    qDebug() << "MSSW processor has a valid runtime, can proceed!";
#endif
    {
        TashMSSWProcessorBuilder* pTmp = new TashMSSWProcessorBuilder(core::TashRuntime::genID(),
                                                                      m_pRunTime,
                                                                      m_cameraIdx,
                                                                      m_msswBuffer);
        Q_CHECK_PTR(pTmp);
        m_pBuilder = QPointer<TashMSSWProcessorBuilder>(pTmp);
    }

    Q_CHECK_PTR(m_pBuilder.data());
    status = m_pBuilder.isNull();
    if ( status ) {
        qCritical() << "Error while trying to allocate builder instance on heap! Can't proceed!";
        m_bIsInitialized = !status;
        return m_bIsInitialized;
    }

    Q_ASSERT(m_pBuilder->isInitialized());

#ifdef TASHVERBOSE
    qDebug() << "Success, building of processor builder went well!";
    qDebug() << "Now trying to build processor of type: " << m_processorName;
#endif

    m_pProcessor = m_pBuilder->build(m_processorName);

    Q_CHECK_PTR(m_pProcessor.data());
    status = m_pProcessor.isNull();

    Q_ASSERT(!status);
    if ( status ) {
        qCritical() << "Error while building processor instance with name " << m_processorName << ". Can't proceed!";
        m_bIsInitialized = !status;
        return m_bIsInitialized;
    }
#ifdef TASHVERBOSE
    qDebug() << "Success, fully working processor instance was being created!";
#endif
    return (m_bIsInitialized = true);
}

void TashMSSWProcessor::process(qint32 &slot) {

    Q_ASSERT(m_pProcessor->hasProcessor());

    m_pProcessor->invoke(0, slot);
}

TashMSSWProcessor::KernelType TashMSSWProcessor::getKernelType() {
    return SORTMEAN;
}

bool TashMSSWProcessor::parseRuntimeForProperties() {
    return true;
}

bool TashMSSWProcessor::setDefaultProperties() {
    return true;
}

TashMSSWProcessor::ProcessingStrategy TashMSSWProcessor::stringToStrategy(const QString& str) {

    if ( str == "tilerange_wise" ) {
        return TR_WISE;
    } else if ( str == "roi_wise" ) {
        return ROI_WISE;
    } else if ( str == "tile_wise" ) {
        return TILE_WISE;
    } else if ( str == "tileline_wise" ) {
        return TILELINE_WISE;
    } else {
        qWarning() << "Invalid processing strategy, defualt to tile wise processing!";
        return TILE_WISE;
    }
}

TashMSSWProcessor::KernelType TashMSSWProcessor::stringToKernelType(const QString& str) {

    if ( str == "sortmean" ) {
        return SORTMEAN;
    } else {
        qWarning() << "Invalid kernel type specified, default to sortmean kernel type!";
        return SORTMEAN;
    }
}

REGISTER_METATYPE(TashMSSWProcessor, "TashMSSWProcessor")
}
}

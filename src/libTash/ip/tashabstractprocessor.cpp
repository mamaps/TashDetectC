/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file tashabstractprocessor.cpp
 * @author Michael Flau
 * @date November 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Schwerin - Germany, Started November 2015
 */

#include "tashabstractprocessor.h"
#include "../com/tashtypedmemorymap.h"
#include "tashmsswaccessor.h"

#include <QThreadPool>
#include <QPointer>
#include <QString>
#include <memory>

Q_DECLARE_METATYPE (tashtego::ip::TashMSSWAccessor<qint16>)

namespace tashtego{
namespace ip{

TashAbstractProcessor::TashAbstractProcessor(tashtego::core::TashObject *parent):
    tashtego::core::TashObject(parent),
    m_cameraIdx(-1),
    m_reserveNumThreadsFromMax(0),
    m_scoreValidityBoundary(0),
    m_maxAllowedThreads(0),
    m_frameBufferLimit(0),
    m_overallNumTiles(0),
    m_strategy(TILELINE_WISE),
    m_pThreadPool(nullptr)
{
    auto status = this->setDefaultProperties();
    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "Failed to set default properties in " << getClassName();
    }
}

TashAbstractProcessor::TashAbstractProcessor(qint64 id,
                                             const QPointer<core::TashRuntime> runtime,
                                             const QMap<qint32, QPointer<TashMSSWAccessor<qint16>>> &accessorBuffer,
                                             quint32 cameraIdx,
                                             TashObject *parent):
    tashtego::core::TashObject(id, runtime, parent),
    m_cameraIdx(cameraIdx),
    m_reserveNumThreadsFromMax (0),
    m_scoreValidityBoundary(0),
    m_maxAllowedThreads(0),
    m_frameBufferLimit(0),
    m_overallNumTiles(0),
    m_strategy(TILELINE_WISE),
    m_pThreadPool(nullptr),
    m_msswBuffer(accessorBuffer) {
//    if ( parseRuntimeForProperties() ) {
////        init();
//    } else {
//        qCritical("Initialization of abstract processor part failed!");
//    }
}

TashAbstractProcessor::TashAbstractProcessor(qint64 id,
                                             const QPointer<core::TashRuntime> runtime,
                                             const QMap<qint32, QPointer<TashMSSWAccessor<qint16>>> &accessorBuffer,
                                             quint32 cameraIdx,
                                             qint32 maxThreads,
                                             quint32 reserveThreads,
                                             quint32 scoreValidityBoundary,
                                             ProcessingStrategy strategy ,
                                             qint32 bufferLimit,
                                             TashObject* parent):
    tashtego::core::TashObject(id, runtime, parent),
    m_cameraIdx(cameraIdx),
    m_reserveNumThreadsFromMax (reserveThreads),
    m_scoreValidityBoundary(scoreValidityBoundary),
    m_maxAllowedThreads(maxThreads),
    m_frameBufferLimit(bufferLimit),
    m_overallNumTiles(0),
    m_strategy(strategy),
    m_pThreadPool(nullptr),
    m_msswBuffer(accessorBuffer) {
//    init();
}

QString TashAbstractProcessor::getClassName() {
    return QString("TashAbstractProcessor");
}

bool TashAbstractProcessor::init() {

    m_pThreadPool = QPointer<QThreadPool>( QThreadPool::globalInstance() );

    Q_CHECK_PTR(m_pThreadPool);
    bool status = m_pThreadPool ? true : false;

    if ( !status ) {
        qCritical() << "Error, was not able to retrieve a threadpool instance from Qt runtime!";
        m_bIsInitialized = status;
        return status;
    }
#ifdef TASHVERBOSE
    qDebug() << "Successfully, created a valid threadpool instance!";
#endif
    const qint32 cIdealThreadCount = QThread::idealThreadCount();

    status = ((cIdealThreadCount - m_maxAllowedThreads) >= 0);
    Q_ASSERT_X( status , "init", "Too many threads requested in ini-file!");

    if ( !status ) {
        qWarning() << "Warning, number of max threads to be used exceeds number of available CPU cores, default to recommended number of threads: " << cIdealThreadCount;
        m_maxAllowedThreads = cIdealThreadCount;
    }

    qint32 reserveT = m_reserveNumThreadsFromMax;

    status = ((cIdealThreadCount - reserveT) > 0);
    Q_ASSERT_X( status , "init", "Asked to reserve more threads than requested in 1st place");
    if ( !status ) {
        qWarning() << "Warning, cannot reserve more threads from detector than actual available, logical cores provided by CPU! Default to 2 reserved threads!";
        m_reserveNumThreadsFromMax = reserveT = 2;
    }

#ifdef TASHVERBOSE
    qDebug() << "Recommended amount of threads are " << cIdealThreadCount << " threads";
    qDebug() << "Demanded number of threads are " << m_maxAllowedThreads << " threads";
    qDebug() << "User asks to reserve " << reserveT << " for other purposes...will see if I can do it!";
#endif

    if ( (cIdealThreadCount - m_maxAllowedThreads) >= reserveT ) {
#ifdef TASHVERBOSE
        qDebug() << "Success, demanded number of threads are reserved for other purposes!";
#endif
    } else {
        qWarning() << "Fail, need to cut down on max threads to be used to reserve demanded threads!";
        m_maxAllowedThreads -= (reserveT - (cIdealThreadCount - m_maxAllowedThreads));
    }

#ifdef TASHVERBOSE
    qDebug() << "Limiting usage of threadpool to " << m_maxAllowedThreads << " threads";
#endif

    m_pThreadPool->setMaxThreadCount(m_maxAllowedThreads);
    return (m_bIsInitialized = true);
}

void TashAbstractProcessor::setCameraIndex(quint32 idx) {
    m_cameraIdx = idx;
}

void TashAbstractProcessor::setMaxAllowedThreads(qint32 num) {
    Q_ASSERT( (num > 0) && (num <= QThread::idealThreadCount()) );

    qint32 maxThreads = QThread::idealThreadCount() - m_reserveNumThreadsFromMax;

    if ( (num < 0) || (num > maxThreads ) ) {
        qWarning() << "Cannot set thread count higher than recommended thread count, default to " << maxThreads;
        m_maxAllowedThreads = maxThreads;
        return;
    }

    m_maxAllowedThreads = num;
}

void TashAbstractProcessor::setProcessingStrategy(ProcessingStrategy strategy) {
    m_strategy = strategy;
}

void TashAbstractProcessor::setFramebufferLimit(qint32 lim) {
    m_frameBufferLimit = lim;
}

void TashAbstractProcessor::setNumReserveThreads( quint32 num ) {

    Q_ASSERT((QThread::idealThreadCount() - num) > 0);

    if ( QThread::idealThreadCount() - num <= 0 ) {
        qWarning() << "Warning cannot reserve more threads from detector than there are actually possible in system! Default to 2 reserved!";
        m_reserveNumThreadsFromMax = 2;
        return;
    }

    m_reserveNumThreadsFromMax = num;
}

void TashAbstractProcessor::setScoreValidityBoundary( quint32 num ) {
    Q_ASSERT( num > 0 );
    m_scoreValidityBoundary = num > 0 ? num : 5;
}

quint32 TashAbstractProcessor::getCameraIndex() {
    return m_cameraIdx;
}

qint32 TashAbstractProcessor::getMaxNumThreads() {
    return m_maxAllowedThreads;
}

qint32 TashAbstractProcessor::getFrameBufferSize() {
    return m_msswBuffer.size();
}

TashAbstractProcessor::ProcessingStrategy TashAbstractProcessor::getProcessingStrategy() {
    return m_strategy;
}

qint32 TashAbstractProcessor::getFramebufferLimit() {
    return m_frameBufferLimit;
}

quint32 TashAbstractProcessor::getNumReserveThreads() {
    return m_reserveNumThreadsFromMax;
}

quint32 TashAbstractProcessor::getScoreValidityBoundary() {
    return m_scoreValidityBoundary;
}

bool TashAbstractProcessor::parseRuntimeForProperties() {

    QString      classname        = QString( metaObject()->className()).split("::").last();

    qDebug() << "MetaObject classname of base class is: " << classname;

    QStringList  rawProperties    = m_pRunTime->getPropertyNamesContaining( classname, m_cameraIdx);
    const qint16 checkValue       = 0x1F;
    qint16       toBeCheckedValue = 0x00;
    qint32       tmpVal           = 0;

#ifdef TASHVERBOSE
    qDebug() << "Meta object derived classname is " << classname << ", searching for properties";
#endif
    foreach (QString str, rawProperties) {

        QString strsplit = str.split("-").last();

        if ( strsplit == "max_number_of_threads" ) {
            tmpVal = m_pRunTime->property(str).toInt();
            Q_ASSERT(tmpVal > 0);

            if ( tmpVal <= 0 ) {
                qWarning() << "Invalid number of maximum threads given! Fallback to default!";
                tmpVal = QThread::idealThreadCount() - 1;
            }

            m_maxAllowedThreads = tmpVal;
#ifdef TASHVERBOSE
            qDebug() << "Parsed max number of threads: " << m_maxAllowedThreads;
#endif
            toBeCheckedValue |= 1;
        }

        if ( strsplit == "processing_strategy" ) {
            QString ps = m_pRunTime->property(str).toString();
            Q_ASSERT(!(ps.isEmpty()));
            m_strategy = stringToStrategy(ps);
#ifdef TASHVERBOSE
            auto dbg = qDebug().noquote();
            dbg << "Parsed strategy: " << ps;
#endif
            toBeCheckedValue |= (1 << 1);
        }

        if ( strsplit == "frame_buffer_limit" ) {
            tmpVal = m_pRunTime->property(str).toInt();
            Q_ASSERT(tmpVal > 0);

            if ( tmpVal <= 0 ) {
                qWarning() << "Invalid maximum number of frames in buffer, default to 5!";
                tmpVal = 5;
            }

            m_frameBufferLimit = tmpVal;
#ifdef TASHVERBOSE
            qDebug() << "Parsed frame buffer limit: " << m_frameBufferLimit;
#endif
            toBeCheckedValue |= (1 << 2);
        }

        if ( strsplit == "score_validity_boundary" ) {
            tmpVal = m_pRunTime->property(str).toInt();
            Q_ASSERT(tmpVal > 0);

            if ( tmpVal <= 0 ) {
                qWarning() << "Invalid number maximum number score validity boundary, default to 5!";
                tmpVal = 5;
            }

            m_scoreValidityBoundary = tmpVal;
#ifdef TASHVERBOSE
            qDebug() << "Parsed score validity boundary: " << m_scoreValidityBoundary;
#endif
            toBeCheckedValue |= (1 << 3);
        }

        if ( strsplit == "reserve_num_threads_from_max" ) {
            tmpVal = m_pRunTime->property(str).toInt();
            Q_ASSERT(tmpVal >= 0);

            if ( tmpVal < 0 ) {
                qWarning() << "Invalid number of threads to reserve from being used for detection, default to 2!";
                tmpVal = 2;
            }

            m_reserveNumThreadsFromMax = tmpVal;
#ifdef TASHVERBOSE
            qDebug() << "Parsed reserve from max threads: " << m_reserveNumThreadsFromMax;
#endif
            toBeCheckedValue |= (1 << 4);
        }
    }

    return checkValue & toBeCheckedValue ? true : false;

}

bool TashAbstractProcessor::setDefaultProperties() {
    bool status = false;
    int  reserveThreads = TASHRESERVENUMTHREADSFROMDETECTOR;

    status = TashAbstractProcessor::setProperty("max_number_of_threads", QThread::idealThreadCount() - reserveThreads);
    Q_ASSERT(!status);

    status = TashAbstractProcessor::setProperty("processing_strategy", "tileline_wise");
    Q_ASSERT(!status);

    status = TashAbstractProcessor::setProperty("frame_buffer_limit", 5);
    Q_ASSERT(!status);

    status = TashAbstractProcessor::setProperty("score_validity_boundary", 5);
    Q_ASSERT(!status);

    status = TashAbstractProcessor::setProperty("reserve_num_threads_from_max", reserveThreads);
    Q_ASSERT(!status);

    return !status;
}

TashAbstractProcessor::ProcessingStrategy TashAbstractProcessor::stringToStrategy(const QString& str) {

    if ( str == "tilerange_wise" ) {
        return TR_WISE;
    } else if ( str == "roi_wise" ) {
        return ROI_WISE;
    } else if ( str == "tile_wise" ) {
        return TILE_WISE;
    } else if ( str == "tileline_wise" ) {
        return TILELINE_WISE;
    } else {
        qWarning() << "Invalid processing strategy, default to tileline wise processing!";
        return TILELINE_WISE;
    }
}

bool TashAbstractProcessor::computeThreadWorkload() {

    auto pAcc = m_msswBuffer[0];
    Q_CHECK_PTR(pAcc);

    if ( !pAcc ) {
        qCritical() << "Pointer pointing to accessor structure is invalid!";
        return false;
    }

    qint32 numRois = pAcc->getNumberOfROIs();
    const qint32 maxThreads = m_maxAllowedThreads;
    rawtypes::detectorDim_r dimensions;
    qint32 threadBlockOvlp = 3;

    dimensions.numRois = numRois;
    m_WorkloadBoundaries.resize(numRois);

    for ( qint32 roiidx = 0; roiidx < numRois; ++roiidx ) {

        qint32 numTrs = pAcc->getNumberOfTRs(roiidx);

        dimensions.rois[roiidx].roidim.n = numTrs;
        dimensions.rois[roiidx].roidim.h = pAcc->getRegionOfInterest(roiidx).getSegmentHeight();
        dimensions.rois[roiidx].roidim.w = pAcc->getRegionOfInterest(roiidx).getSegmentWidth();
        dimensions.rois[roiidx].roidim.x = pAcc->getRegionOfInterest(roiidx).getStartPoint().x();
        dimensions.rois[roiidx].roidim.y = pAcc->getRegionOfInterest(roiidx).getStartPoint().y();

        m_WorkloadBoundaries[roiidx].resize(maxThreads);

        for ( qint32 tridx = 0; tridx < numTrs; ++tridx ) {

            dimensions.rois[roiidx].trs[tridx].h = pAcc->getTileRange(roiidx, tridx).getSegmentHeight();
            dimensions.rois[roiidx].trs[tridx].w = pAcc->getTileRange(roiidx, tridx).getSegmentWidth();
            dimensions.rois[roiidx].trs[tridx].x = pAcc->getTileRange(roiidx, tridx).getStartPoint().x();
            dimensions.rois[roiidx].trs[tridx].y = pAcc->getTileRange(roiidx, tridx).getStartPoint().y();
            dimensions.rois[roiidx].trs[tridx].t = pAcc->getNumberOfTiles(roiidx, tridx);
            dimensions.rois[roiidx].trs[tridx].r = 0;

            /**
             * @todo Why does every tile needs to store information about its overlap,
             * overall amount of tiles, tiles per row and tiles per tiles per line?
             * The parent element should store thes information for the whole range,
             * as they are valid for all child segments.
             */
            qint32 numTiles         = pAcc->getNumberOfTiles(roiidx, tridx);
            qint32 numRows          = pAcc->getTile(roiidx, tridx, 0).getNumberOfRows();
            qint32 numTilesPerRow   = pAcc->getTile(roiidx, tridx, 0).getNumberSegmentsPerRow();
#ifdef TASHVERBOSE
            qDebug() << "Read num tile value is: " << numTiles;
            qDebug() << "Read num row value is: "  << numRows;
            qDebug() << "Read tiles per row value is: " << numTilesPerRow;
#endif
            /**
             * @todo
             * if the mind unfitting end flag is set to true,
             * tilesPerRow_a must be increased by one.
             */
            qint32 numTilesPerLinePerThread = (numTilesPerRow - (numTilesPerRow % maxThreads)) / maxThreads;//(tilesPerRow_b - restOfTilesByThreads) / maxThreads;

            for ( qint32 lineidx = 0; lineidx < numRows; ++lineidx ) {

                qint32 lower = lineidx * numTilesPerRow;
                //Add rest of tiles (-1 to stay in range) to first thread as the first thread has most likely less tiles
                qint32 upper = (lineidx * numTilesPerRow) + (numTilesPerLinePerThread + ((numTilesPerRow % maxThreads) - 1));

                for ( qint32 threadidx = 0; threadidx < maxThreads; ++threadidx ) {

                    if ( upper > numTiles ) {
                        qWarning() << "Upper boundary exceeds number of tiles per line!";
                    }

                    if ( lower > numTiles ) {
                        qWarning() << "Upper boundary exceeds number of tiles per line!";
                    }

                    qint32 hl = pAcc->getTile(roiidx, tridx, lower).getStartPoint().y();
                    qint32 hu = pAcc->getTile(roiidx, tridx, upper).getStartPoint().y();

                    Q_ASSERT( hl == hu );

                    while ( hl != hu ) {
                        qWarning() << "Lower and upper tile don't match in height! ( " << hl << " vs. " << hu << " )";
                        qWarning() << "Decreasing upper by one tile...!";

                        --upper;

                        hl = pAcc->getTile(roiidx, tridx, lower).getStartPoint().y();
                        hu = pAcc->getTile(roiidx, tridx, upper).getStartPoint().y();
                    }

                    Q_ASSERT( upper > lower );

                    tileWorkloadBoundary<qint32> tmp{ roiidx, tridx, lineidx, lower, upper };
                    m_WorkloadBoundaries[roiidx][threadidx].append(tmp);

                    lower = (upper - threadBlockOvlp) < 0 ? 0 : (upper - threadBlockOvlp);
                    upper = lower + threadBlockOvlp + numTilesPerLinePerThread;
                }
            }
        }
    }

    if ( !(this->writeDimsToMmap(dimensions)) ) {
        qCritical() << "Failed to write detector range dimensional data to mmap interface!";
        return false;
    }

    return true;
}

bool TashAbstractProcessor::writeDimsToMmap(const tashtego::rawtypes::detectorDim_r& dims) {

    using namespace tashtego::com;
    using namespace tashtego::rawtypes;

    auto pMap = std::unique_ptr<TashMapAcc_DetectDims>(new TashMapAcc_DetectDims(core::TashRuntime::genID(),
                                                                                  m_pRunTime,
                                                                                  0,
                                                                                  QString("DetectDims")));

    Q_CHECK_PTR(pMap);

    if ( !pMap ) {
        qCritical() << "Failed to allocate, mmap accesor for dimension mmap, abort!";
        return false;
    }

    bool status = pMap->isInitialized();

    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "Failed to initialize memory map access structure, for detector dimension values!";
//        delete pMap;
        return false;
    }

    status = pMap->openMap();

    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "Error, while trying to open memory map, for detector dimensions!";
//        delete pMap;
        return status;
    }

    QSharedPointer<detectorDim_r> pAcc = pMap->getSafeMapAccess();

    status = pAcc.isNull();

    Q_ASSERT( !status );

    if ( status ) {
        qCritical() << "Failed to acquire shared pointer instance to memory map! Abort!";
//        pMap->closeMap();
//        delete pMap;
        return status;
    }

    if ( !memcpy(pAcc.data(), &dims, sizeof(detectorDim_r))) {
        qCritical() << "Failed to write dimensional data to mmap";
        return false;
    }
#ifdef TASHVERBOSE
    qDebug() << "Writing detector dimensions to memory map was successful!";
#endif
//    status = pMap->closeMap();

//    Q_ASSERT(status);

//    if ( !status ) {
//        qWarning() << "Failed to close mmap accessor! Proceed wit caution!";
//    }

//    delete pMap;
//    pMap = nullptr;

    return !status;
}

QMap<QString,QVariant> TashAbstractProcessor::getBaseClassProperties() {
    return QMap<QString, QVariant>({
                                    {"max_number_of_threads",        QVariant(4u)},
                                    {"processing_strategy",          QVariant("tileline_wise")},
                                    {"frame_buffer_limit",           QVariant(30u)},
                                    {"score_validity_boundary",      QVariant(5u)},
                                    {"reserve_num_threads_from_max", QVariant(0u)}
                                   });
}

REGISTER_METATYPE(TashAbstractProcessor*, "TashAbstractProcessor")
}
}

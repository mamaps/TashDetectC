/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef TASHIMAGESEGMENTACCESSOR_H
#define TASHIMAGESEGMENTACCESSOR_H

#define BOUNDARY_CHECK
//#define USE_MAP

#include "tashimagesegmentinfo.h"
#include "tashabstractsegmentaccessor.h"

#include <QDebug>
#ifdef USE_MAP
#include <QMap>
#else
#include <QHash>
#endif


namespace tashtego {
namespace ip {

/**
 * @brief The TashImageSegmentAccessor Template
 *
 * This template class provides means to realise container formats
 * for different levels of the Multi Scale Sliding Window data
 * representation. Using this template a multi layer hierachy can be created.
 * The very bottom template types instance should take the tashtego::ip::TashTile
 * class as argument. From that on going the template can include itself in
 * various layers. As example a three layered hierachy could look like this:
 *
 * @code
 * //Instantiation
 * TashImageSegmentAccessor<TashImageSegmentAccessor<TashTile<qint16> > > mssw_instance;
 *
 * //Data Access
 * quint32 pixelsPerTile, tilesPerRange, tileRanges = mssw_instance.childSegments();
 * quint32 idx1, idx2, idx3;
 * for ( idx1 = 0; idx1 < tileRanges; ++idx1 ) {
 *
 *      tilesPerRange = mssw_instance[idx1].childSegments();
 *      for ( idx2 = 0; idx2 < tilesPerRange; ++idx2 ) {
 *
 *          pixelsPerTile = mssw_instance[idx1][idx2].pixelcount();
 *          for ( idx3 = 0; idx3 < pixelsPerTile; ++idx3 ) {
 *
 *            quint32 pixel = mssw_instance[idx1][idx2][idx3];
 *
 *          }
 *      }
 * }
 * @endcode
 */
template <typename T1, typename T2>
class TashImageSegmentAccessor: public TashAbstractSegmentAccessor<T1> {

public:

    /**
     * @brief TashImageSegmentAccessor
     * Default constructor must be supported as being inherited from QObject
     */
    explicit TashImageSegmentAccessor(TashAbstractSegmentAccessor<T1> *parent = 0);

    /**
     * @brief TashImageSegmentAccessor
     * Constructor to be used for creating instances manually. The template requires
     * a bunch of arguments which are compulsory for it's operation, although most
     * of the arguments are directly passed on to the abstract base class constructor.
     * @param id
     * Holds the unique id which identifies each object which is derived from TashObject
     * To generate a unique id call the static function TashRuntime::genId().
     * @param segmentIndex
     * The instances index number among all instances of the same level as the just created
     * instance. This serves as a level based identifier, to access correct child elemnts
     * from their respective parent.
     * @param startX
     * The numeric horizontal start position of the segment instance
     * @param startY
     * The numeric vertical start position of the segment instance
     * @param metaData
     * meta data object which holds varoius information about all segments of a certain type and/or level
     * @param data
     * Smart pointer to the actual image data stored in the memory map
     * @param children
     * List of child segments which should be already initialized
     */
    TashImageSegmentAccessor(qint64 id,
                             const quint32 segmentIndex,
                             const quint32 startX,
                             const quint32 startY,
                             QSharedPointer<TashImageSegmentInfo> metaData,
                             QSharedPointer<T1> data,
                         #ifdef USE_MAP
                             QMap<quint32,T2*> children = QMap<quint32,T2*>(),
                         #else
                             QHash<quint32,T2*> children = QHash<quint32,T2*>(),
                         #endif
                             TashAbstractSegmentAccessor<T1> *parent = 0);

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() override;

    /**
     * @brief operator []
     * Convenience overload to access child segments via the subscript opeorator.
     * @param index
     * Child segment to be returned is denoted by that index.
     * @warning
     * Check boundaries of child segments before calling. Compiled in operational mode
     * there will be no internal check if index is within boundary limitations as
     * it would slow down the element access.
     * @return
     * Reference to the child segment instance which was requested
     */
    virtual T2& operator[](const quint32& index);

    /**
     * @brief setChildSegments
     * Simple setter function to set an alternative list of child segments to the respective instance,
     * as it was passed at construction time. If called the passed collection of items will be
     * checked for validity by calling internally TashImageSegmentAccessor::init again.
     * To check whether the intance is still operational after calling this method, check instance
     * by calling TashImageSegmentAccessor::isInitialized.
     * @param segments
     * Collection of child segments to manage as QMap
     */

#ifdef USE_MAP
    virtual void setChildSegments(QMap<quint32, T2 *> &&segments);
#else
    virtual void setChildSegments(QHash<quint32, T2 *> &&segments);
#endif

    /**
     * @brief getSegment
     * Simple getter function to return a reference to the child segment with index 'index'
     * @warning
     * Compiled for operational mode, this function will not check whether the passed index
     * number is within bounds of the managed child segments collection. This is because additional
     * boundary checks slow down the data access. To know in advance what the maximum
     * index number is call TashImageSegmentAccessor::childSegments
     * @param index
     * Index number of child segment as unsigned integer
     * @return
     */
    virtual T1 &getSegment(const quint32& index);

    /**
     * @brief getSegment
     * Simple getter function to return a reference to the child segment with index 'index'
     * @warning
     * Compiled for operational mode, this function will not check whether the passed index
     * number is within bounds of the managed child segments collection. This is because additional
     * boundary checks slow down the data access. To know in advance what the maximum
     * index number is call TashImageSegmentAccessor::childSegments
     * @param index
     * Index number of child segment as unsigned integer
     * @return
     */
    virtual T2& getSubSegment(const quint32& index);

    /**
     * @brief getSegmentPointer
     * Returns a pointer to the object at given index
     * @param index
     * integer index which references demanded object
     * @return
     * Generic object being stored in this container
     */
    virtual T2* getSegmentPointer(const quint32& index);

    /**
     * @brief getSegmentPointerRange
     * Return a range of segment references, which contains all elements which have an index number
     * between <code>low</code> and <code>high</code>.
     * @param low
     * Lower bound index number as unsigned integer
     * @param high
     * Upper bound index number as unsigned integer
     * @return
     * Collection of child segments which are in the requested range as QMap object
     */
#ifdef USE_MAP
    virtual QMap<quint32,T2*> getSegmentPointerRange(const quint32& low, const quint32& high);
#else
    virtual QHash<quint32,T2*> getSegmentPointerRange(const quint32& low, const quint32& high);
#endif

    /**
     * @brief childSegments
     * Returns the number of child segments which is managed by the respective instance being called.
     * @return
     * number of child segments as unsigned integer
     */
    quint32 childSegments();

protected:

    /**
     * @brief init
     * Default initialization function which must be
     * reimplemented by each class which is derived from TashAbstractSegmentAccessor.
     * It is called automatically upon construction. However, it is callable manually.
     * Also if a new collection of child segments is passed to the respective instance, init will be
     * called again, to check whether the passed list of child segments is valid.
     * @return
     * True if succesful, false if failure
     */
    virtual bool init() override;

    /**
     * @brief reinit
     * Does basically the same, as init(), but skips some checks which would lead to uninitialized tiles
     * as tile vectors are already created when reninit is invoked()
     * @return
     * True if succesful, false if failure
     */
    virtual bool reinit();

#ifdef USE_MAP
    QMap<quint32, T2*> m_AccessorList; ///> List of child elements
#else
    QHash<quint32, T2*> m_AccessorList; ///> List of child elements
#endif
    quint32            m_numElements;  ///> Number of child elements, should resemble m_AccessorList.size()
};

template <typename T1, typename T2>
TashImageSegmentAccessor<T1, T2>::TashImageSegmentAccessor(TashAbstractSegmentAccessor<T1> *parent):
    TashAbstractSegmentAccessor<T1>(parent){

}

template <typename T1, typename T2>
TashImageSegmentAccessor<T1, T2>::TashImageSegmentAccessor(qint64 id,
                                                           const quint32 segmentIndex,
                                                           const quint32 startX,
                                                           const quint32 startY,
                                                           QSharedPointer<TashImageSegmentInfo> metaData,
                                                           QSharedPointer<T1> data,
                                                           #ifdef USE_MAP
                                                           QMap<quint32, T2*> children,
                                                           #else
                                                           QHash<quint32, T2*> children,
                                                           #endif
                                                           TashAbstractSegmentAccessor<T1> *parent):
    TashAbstractSegmentAccessor<T1>(id,
                                    segmentIndex,
                                    startX,
                                    startY,
                                    metaData,
                                    data,
                                    parent),
    m_AccessorList(children),
    m_numElements(m_AccessorList.size()) {

    if ( !init() ) {
        qWarning() << "Error while initializing instance of TashImageSegmentAccessor<T>!";
    }
}

template <typename T1, typename T2>
QString TashImageSegmentAccessor<T1, T2>::getClassName() {
    return QString("TashImageSegmentAccessor");
}

template <typename T1, typename T2>
bool TashImageSegmentAccessor<T1, T2>::init() {

    auto status = this->m_pData.isNull();

    Q_ASSERT( !status );

    if ( status ) {
        qCritical() << "Given data pointer is invalid, abort!";
        this->m_bIsInitialized = false;
        return this->m_bIsInitialized;
    }

    status = true;

    m_numElements = m_AccessorList.size();

    Q_ASSERT(m_numElements == 0);

    if ( m_numElements > 0 ) {
        qCritical() << "Size of accessor list is greater 0, no elements must be contained at init";
        return (this->m_bIsInitialized = false);
    }

    return (this->m_bIsInitialized = status);
}

template <typename T1, typename T2>
bool TashImageSegmentAccessor<T1, T2>::reinit() {
    bool status = this->m_pData.isNull();

    Q_ASSERT( !status );

    if ( status ) {
        qCritical() << "Given data pointer is invalid, abort!";
        this->m_bIsInitialized = false;
        return this->m_bIsInitialized;
    }

    status = true;

    m_numElements = m_AccessorList.size();

    Q_ASSERT(m_numElements >= 0);

    if ( m_numElements < 0 ) {
        qCritical() << "Size of accessor list is smaller 0, no elements are contained.";
        return (this->m_bIsInitialized = false);
    }

    if ( m_AccessorList.isEmpty() ) {
        qCritical() << "accessor list isEmpty() returned true";
        this->m_bIsInitialized = false;
        return this->m_bIsInitialized;
    }

    return (this->m_bIsInitialized = status);
}

#ifdef USE_MAP
template <typename T1, typename T2> void TashImageSegmentAccessor<T1, T2>::setChildSegments(QMap<quint32, T2*>&& segments) {

    Q_ASSERT(!segments.isEmpty());

    if ( segments.isEmpty() ) {
        qWarning() << "Input list is empty! Operations on this instance will have no effect!";
    }

    m_AccessorList = segments;

    this->init();
}
#else
template <typename T1, typename T2>
void TashImageSegmentAccessor<T1, T2>::setChildSegments(QHash<quint32, T2*>&& segments) {

    Q_ASSERT(!segments.isEmpty());

    if ( segments.isEmpty() ) {
        qWarning() << "Input list is empty! Operations on this instance will have no effect!";
    }

    m_AccessorList = segments;

    if ( !reinit() ) {
        qCritical() << "Error while reinitializing instance of TashImageSegmentAccessor<T>, marking segment as unusable!";
        this->m_bIsInitialized = false;
    }
}
#endif

template <typename T1, typename T2>
T2& TashImageSegmentAccessor<T1, T2>::operator[](const quint32& index) {
    Q_ASSERT(m_AccessorList.contains(index));
    return *m_AccessorList[index];
}

template <typename T1, typename T2>
T1& TashImageSegmentAccessor<T1, T2>::getSegment(const quint32& index) {
    return *(this->m_pData.data() + index);
}

template <typename T1, typename T2>
T2& TashImageSegmentAccessor<T1, T2>::getSubSegment(const quint32 &index) {
    return this->operator [](index);
}

template <typename T1, typename T2>
T2* TashImageSegmentAccessor<T1, T2>::getSegmentPointer(const quint32& index) {
#ifdef TASHVERBOSE
    qDebug() << "Trying to access index " << index;
    qDebug() << "Max accessible access " << m_AccessorList.size();
#endif
    Q_ASSERT(m_AccessorList.contains(index));
    return m_AccessorList[index];
}

#ifdef USE_MAP
template <typename T1, typename T2>
QMap<quint32,T2*> TashImageSegmentAccessor<T1, T2>::getSegmentPointerRange(const quint32& low, const quint32& high) {
#else
template <typename T1, typename T2>
QHash<quint32,T2*> TashImageSegmentAccessor<T1, T2>::getSegmentPointerRange(const quint32& low, const quint32& high) {
#endif

    if ( low > high ) {
        qCritical() << "Lower boundary must not be greater than higher boundary!";
    }

    //must contain
    Q_ASSERT(m_AccessorList.contains(low));
    Q_ASSERT(m_AccessorList.contains(high));

#ifdef USE_MAP
    QMap<quint32, T2*> ret;

    typename QMap<quint32, T2*>::iterator it_idx      = m_AccessorList.lowerBound(low);
    typename QMap<quint32, T2*>::iterator upperBound  = m_AccessorList.upperBound(high);

    for(; it_idx != upperBound; ++it_idx) {
        ret[it_idx.key()] = it_idx.value();
    }
#else
    QHash<quint32, T2*> ret;
    ret.reserve((high-low)+1);

    for ( auto idx = low; idx <= high; ++idx) {
        ret[idx] = m_AccessorList[idx];
    }
#endif

    return ret;
}

template <typename T1, typename T2>
quint32 TashImageSegmentAccessor<T1, T2>::childSegments() {
    quint32 tmp = m_AccessorList.size();
    Q_ASSERT(tmp == m_numElements);
    return (tmp == m_numElements) ? m_numElements : tmp;
}

}
}
#endif // TASHIMAGESEGMENTACCESSOR_H


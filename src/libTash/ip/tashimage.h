/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * @file tashimage.h
 * @author Michael Flau
 * @brief
 * @details
 * @date May 2016
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Schwerin - Germany, Started May 2016
 */

#ifndef TASHIMAGE_H
#define TASHIMAGE_H

#include "../core/tashobject.h"

#include <QSharedPointer>


namespace tashtego {
namespace ip {

/**
 * @brief The TashImage class template
 *
 * Abstractional layer for better handling of raw image data
 * within tashtego based context.
 * The template provides a container for storing image data
 * and access relevant meta information. It also supports some
 * basic image data operations.
 *
 * Datawise, the container template can be used in two ways:
 *  - Usage as a reference to an already existing memory chunk somewhere e.g. memory map
 *  - Usage as an independent, self managing data container,
 *    data will be destroyed at end of lifetime
 *
 * Usage example:
 * @code
 *
 * // Creates an image object with internally managed image space of
 * // 256x256 pixels and 16 bit depth grayscale values.
 *
 * using namespace tashtego::ip;
 * using namespace tashtego::core;
 *
 * auto img = TashImage<qint16>(TashRuntime::genID(),
 *                              0xff,
 *                              0xff,
 *                              TashImage<qint16>::Format_Grayscale16);
 * @endcode
 */
template <typename T>
class TashImage final: public core::TashObject {

public:

    /**
     * Supported image formats of this template
     */
    typedef enum {
        Format_Invalid,    // Used when datatype and format do not match
        Format_Grayscale8, // 1 byte grayscale pixel values
        Format_Grayscale16,// 2 byte grayscale pixel values
        Format_Grayscale24,// 3 byte grayscale pixel values
        Format_RGB16,      // 5-6-5 bit        pixel value
        Format_RGB24,      // 8-8-8 bit        pixel value ( alpha channel is ignored )
        Format_RGB32       // 8-8-8-8 bit      pixel value ( including alpha channel )
    } Format;

    typedef struct {
        quint8 r;
        quint8 g;
        quint8 b;
        quint8 a;
    } TashRGBAPixel;

    /**
     * @brief TashImage
     * Default ctor
     * @param parent
     * Parent instance
     */
    explicit TashImage(core::TashObject* parent = 0);

    /**
     * @brief TashImage
     * Dummy stump ctor
     * @param id
     * Instance id
     * @param parent
     * Parent instance
     */
    TashImage(qint64 id, core::TashObject* parent = 0);

    /**
     * @brief TashImage
     * Ctor to call for an independent image object. Instance will alocate their own chunk of
     * memory and handle it until the instance dies.
     * @param id
     * Instance id of image object
     * @param width
     * Width of image in pixel
     * @param height
     * Height of image in pixel
     * @param format
     * Description of how pixel information are arranged in memory
     */
    TashImage(qint64 id,
              quint64 width,
              quint64 height,
              TashImage::Format format,
              core::TashObject* parent = 0);

    /**
     * @brief TashImage
     * Ctor to call for a managing instance of TashImage for an external data source.
     * Tash image will only 'work' with the reference to the image data but not take care of
     * it's deallocation at the end of the instances lifecycle.
     * @param id
     * Instance id of image object
     * @param width
     * Width of image in pixel
     * @param height
     * Height of image in pixel
     * @param format
     * Description of how pixel information are arranged in memory
     * @param data
     * External image data source which is to be managed by the TashImage instance
     */
    TashImage(qint64 id,
              quint64 width,
              quint64 height,
              TashImage::Format format,
              T *data,
              core::TashObject* parent = 0);


    /**
     * @brief TashImage
     * @param rimage
     */
    TashImage<T>& operator=(const TashImage<T>& rimage);

    /**
     *@brief dtor
     */
    ~TashImage();
    /**
     * @brief width
     * Returns image width in pixels
     * @return
     * Width as unsigned long long int
     */
    quint64 width() noexcept;
    quint64 width() const  noexcept;

    /**
     * @brief height
     * Returns image height in pixels
     * @return
     * Height as unsigned long long int
     */
    quint64 height() noexcept;
    quint64 height() const noexcept;

    /**
     * @brief format
     * Returns image format
     * @return
     * Format as defined in TashImage::Format
     */
    TashImage::Format format() noexcept;
    TashImage::Format format() const noexcept;

    /**
     * @brief padding
     * Returns number of padded bytes at the end of each image line
     * @return
     * Padding value as char
     */
    quint8 padding() noexcept;

    /**
     * @brief bytesPerLine
     * Returns the number of bytes which each line contains
     * @return
     * Number of bytes as unsigned long long int
     */
    quint64 bytesPerLine() noexcept;

    /**
     * @brief dataUnsafe
     * Returns a raw pointer to the image data.
     *
     * @warning This function does not assume any image format, but returns
     * a per-byte representation of the image data. The returned (smart-)pointer must be either
     * casted or treated with bitshifting to extract actual pixel values from it.
     *
     * @return
     * unsigned char pointer to image data, T*
     */
    T* dataUnsafe() noexcept;

    /**
     * @brief data
     * Returns a smart pointer to the image data.
     *
     * @warning This function does not assume any image format, but returns
     * a per-byte representation of the image data. The returned (smart-) pointer
     * must be either casted or treated with bitshifting to extract actual pixel values from it.
     *
     * @return
     * smart shared pointer, QSharedPointer<T>
     */
    QSharedPointer<T> data() noexcept;

    /**
     * @brief typedUnsafeData
     * Returns a raw pointer of the defined type,
     * which is most likely to be the correct datatype for the image data.
     * @return
     * pointer of type T to image data
     */
    T* dataTypedUnsafe() noexcept;
    T* dataTypedUnsafe() const noexcept;

    /**
     * @brief data
     * Returns a smart pointer of the defined type which points to the actual image data
     * @return
     * QSharedPointer with type T pointing to the actual image data
     */
    QSharedPointer<T> dataTyped() noexcept;

    /**
     * @brief pixel
     * Returns the value of a pixel at position (x,y)
     * @warning
     * If this class is being used for color images, don't use this function. Rather use pixelRGB()
     * which is meant for multichannel images. It returns the 'whole' pixel as is, means all channel
     * information are in the returned value. Instead, pixelRGB() retrieves a TashPixel object which
     * stores all channel information in an dedicated pixel object structure.
     * @param x
     * horizontal coordinate of the pixel
     * @param y
     * vertical coordinate of the pixel
     * @return
     * Value of the respective pixel of type T
     */
    T pixel(const quint64 &x, const quint64 &y) noexcept;

    /**
     * @brief pixelRGB
     * Returns a RGBA pixel with color information rgb + alpha channel
     * ( if present ) at the given position in the image
     * @param x
     * horizontal coordinate of the pixel
     * @param y
     * vertical coordinate of the pixel
     * @return
     * Color channel information condensed in a pixel object of type TashPixel
     */
    TashRGBAPixel pixelRGB(const quint64 &x, const quint64 &y) noexcept;

    /**
     * @brief setPixel
     * Method for setting the value of a single pixel in a grayscale image
     * @param value
     * @param x
     * @param y
     */
    void setPixel(const T& value, const quint64 &x, const quint64 &y) noexcept;

    /**
     * @brief setPixelRGB
     * @param value
     * @param x
     * @param y
     */
    void setPixelRGB(const TashImage<T>::TashRGBAPixel& value,
                     const quint64 &x,
                     const quint64 &y) noexcept;

    /**
     * @brief scanline
     * Provides a pointer to the start of an image line
     * @warning This function does not assume any image format, but returns
     * a per-byte representation of the image data. The returned (smart-)pointer must
     * be either casted or treated with bitshifting to extract actual pixel values from it.
     *
     * @param line
     * Defines the line to which the returned pointers is expected to point to,
     * technically this is the y-coordinate.
     *
     * @return
     * Raw unsigned char pointer pointing to image data
     */
    T *scanlineUnsafe( const quint64 &line ) noexcept;

    /**
     * @brief scanline
     * Provides a smart pointer to the start of an image line.
     *
     * @warning This function does not assume any image format, but returns
     * a per-byte representation of the image data. The returned (smart-)pointer
     * must be either casted or treated with bitshifting to extract actual pixel values from it.
     *
     * @param line
     * Defines the line to which the returned pointers is expected to point to,
     * technically this is the y-coordinate.
     *
     * @return
     * Smart pointer of type unsigned char pointing to image data
     */
    QSharedPointer<T> scanline( const quint64 &line ) noexcept;

    /**
     * @brief scanlineTypedUnsafe
     * Provides a pointer of the defined type to the image line passed to the function
     * @param line
     * Defines the line to which the returned pointers is expected to point to,
     * technically this is the y-coordinate.
     * @return
     * Raw pointer of type T pointing to image data in line Y
     */
    T* scanlineTypedUnsafe( const quint64 &line ) noexcept;

    /**
     * @brief scanlineTyped
     * Provides a smart pointer of the defined type to the image line passed to the function
     * @param line
     * Defines the line to which the returned pointers
     * is expected to point to, technically this is the y-coordinate.
     * @return
     * Smart pointer of type T pointing to image data in line Y
     */
    QSharedPointer<T> scanlineTyped( const quint64 &line ) noexcept;

    /**
     * @brief pixelcount
     * Return number of pixels in image
     * @return
     * Number of pixels in image
     */
    quint64 pixelcount() noexcept;

    /**
     * @brief setData
     * Function to set image data to manage to an existing image container instance.
     * Depending on in what mode the instance is, the image will delete it's allocated image memory
     * with all containing data, or just swap the reference to another external data source.
     * @param data
     * Image data pointer of type T
     * @param format
     * Image format as defined in TashImage::Format
     * @param width
     * Width of image in pixels
     * @param height
     * Height of image in pixels
     */
    void setData(T* data, TashImage::Format format, quint64 width, quint64 height) noexcept;

    /**
     * @brief hasExternDataSource
     * Simple getter which gives info about instance managing an intern or extern data source.
     * @return
     * Boolean depicts whether it has (not) extern source
     */
    bool hasExternDataSource() noexcept;

protected:
    /**
     * @brief init
     * Initialization routine which is internally executed at construction
     * time and when the image instance is resetted or referenced to a new image.
     */
    virtual bool init() override;

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() override;

    /**
     * @brief computePadding
     * Computes the offset bytes at the end of each line of the image according to the image format
     */
    void computePadding() noexcept;

    /**
     * @brief bytesFromFormat
     * Returns the number of bytes one pixel has depending on the format
     * @return
     * Number of bytes as char value
     */
    quint8 bytesFromFormat(TashImage::Format format) noexcept;

    /**
     * @brief pixelOffset
     * Computes the address offset from the image start
     * taking padding into consideration
     * @param x
     * position in X direction
     * @param y
     * position Y direction
     * @return
     * offset in bytes from the beginning of the image
     */
    inline quint64 pixelOffset(quint64 x, quint64 y) noexcept;

private:
    bool                    m_bHasExternalSource; ///> Determines whether to allocate memory or not
    QSharedPointer<T>       m_Data;               ///> Data pointer
    quint64                 m_Width;              ///> Width of image
    quint64                 m_Height;             ///> Height of image
    TashImage::Format       m_Format;             ///> Format
    quint8                  m_Padding;            ///> Padding at the end of a line in bytes
    quint64                 m_BytesPerLine;       ///> Number of bytes in a line as in bytesPerPixel*Width
    quint8                  m_BytesPerPixel;      ///> Number of bytes in a pixel
    quint64                 m_Pitch;              ///> Number of bytes in line plus padding bytes
};

template <typename T>
TashImage<T>::TashImage(core::TashObject *parent):
core::TashObject(parent){m_bIsInitialized = false;}

template <typename T>
TashImage<T>::TashImage(qint64 id, core::TashObject *parent):
core::TashObject(id, parent){m_bIsInitialized = false;}

template <typename T>
TashImage<T>::TashImage(qint64 id,
                        quint64 width,
                        quint64 height,
                        TashImage::Format format,
                        core::TashObject* parent):
    core::TashObject(id, parent),
    m_bHasExternalSource(false),
    m_Data(nullptr),
    m_Width(width),
    m_Height(height),
    m_Format(format),
    m_Padding(0),
    m_BytesPerLine(0) {

    size_t sz = m_Width * m_Height * sizeof(T);
    T* pTmp = new T[sz];

    Q_CHECK_PTR(pTmp);
    auto status = pTmp ? true : false;
    if (!status) {
        qCritical() << "Failed to allocate memory for image data!";
        m_bIsInitialized = status;
        return;
    }

    status = memset(pTmp, 0, sz) ? true : false;
    if ( !status ) {
        qCritical() << "Initial filling of allocated memory failed!";
        delete[] pTmp;
        m_bIsInitialized = status;
        return;
    }

    m_Data = QSharedPointer<T>(pTmp, [](T* p){delete[] p; p=nullptr;});

    status = init();
    Q_ASSERT(status);

    if (!status) {
#ifdef TASHVERBOSE
        qCritical() << "Failed to initialize TashImage<T> instance";
#endif
    }
}

template <typename T>
TashImage<T>::TashImage(qint64 id,
                        quint64 width,
                        quint64 height,
                        TashImage::Format format,
                        T* data,
                        core::TashObject* parent):
    core::TashObject(id, parent),
    m_bHasExternalSource(true),
    m_Data(nullptr),
    m_Width(width),
    m_Height(height),
    m_Format(format),
    m_Padding(0),
    m_BytesPerLine(0) {

    Q_CHECK_PTR(data);

    if ( !data ) {
        qCritical() << "Invalid image data pointer passed to object constructor, abort!";
        m_bIsInitialized = false;
        return;
    }

    m_Data = QSharedPointer<T>(data, [](T* p){Q_UNUSED(p);});

    auto status = init();
    Q_ASSERT(status);

    if (!status) {
#ifdef TASHVERBOSE
        qCritical() << "Failed to initialize TashImage<T> instance";
#endif
    }
}

template <typename T>
TashImage<T>::~TashImage() {

    if ( m_bHasExternalSource && !m_Data.isNull() ) {
        m_Data.clear();
        m_Data.reset(nullptr);
    } else {
        m_Data.clear();
    }

    m_bIsInitialized = false;
}

template <typename T>
TashImage<T>& TashImage<T>::operator=(const TashImage<T>& rimage) {
    this->setData(rimage.dataTypedUnsafe(), rimage.format(), rimage.width(), rimage.height());
    return *this;
}

template <typename T>
quint64 TashImage<T>::width() noexcept {
    return m_Width;
}

template <typename T>
quint64 TashImage<T>::width() const noexcept {
    return m_Width;
}

template <typename T>
quint64 TashImage<T>::height() noexcept {
    return m_Height;
}

template <typename T>
quint64 TashImage<T>::height() const noexcept {
    return m_Height;
}

template <typename T>
typename TashImage<T>::Format TashImage<T>::format() noexcept {
    return m_Format;
}

template <typename T>
typename TashImage<T>::Format TashImage<T>::format() const noexcept {
    return m_Format;
}

template <typename T>
quint8 TashImage<T>::padding() noexcept {
    return m_Padding;
}

template <typename T>
quint64 TashImage<T>::bytesPerLine() noexcept {
    return m_BytesPerLine;
}

template <typename T>
T *TashImage<T>::dataUnsafe() noexcept
{
    return m_Data.data();
}

template <typename T>
QSharedPointer<T> TashImage<T>::data() noexcept {
    return m_Data;
}

template <typename T>
T* TashImage<T>::dataTypedUnsafe() noexcept {
    return m_Data.data();
}

template <typename T>
T* TashImage<T>::dataTypedUnsafe() const noexcept{
    return m_Data.data();
}

template <typename T>
QSharedPointer<T> TashImage<T>::dataTyped() noexcept {
    return QSharedPointer<T>( this->dataTypedUnsafe(), [](T* p){Q_UNUSED(p)});
}

template <typename T>
T TashImage<T>::pixel(const quint64 &x, const quint64 &y) noexcept {

    Q_ASSERT( x < m_Width );
    Q_ASSERT( y < m_Height );

    if ( !m_bIsInitialized ) {
        qCritical() << "Object is not initilized!";
        return 0;
    }

    if ( x > m_Width ) {
        qCritical() << "Requested x coordinate is out of image bounds!";
        return 0;
    }

    if ( y > m_Height ) {
        qCritical() << "Requested y coordinate is out of image bounds!";
        return 0;
    }

    if ( m_Format == Format_RGB16 || m_Format == Format_RGB24 ||
         m_Format == Format_RGB32 || m_Format == Format_Invalid ) {
        qCritical() << "Format either invalid or set to a multichannel format. "
                       "Use pixelRGB for multichannel images.";
        return 0;
    }

    T* pTmp = m_Data.data();
    Q_CHECK_PTR( pTmp );

    return *(pTmp + pixelOffset(x, y));
}

template <typename T>
typename TashImage<T>::TashRGBAPixel TashImage<T>::pixelRGB(const quint64 &x, const quint64 &y) noexcept {

    Q_ASSERT( x < m_Width );
    Q_ASSERT( y < m_Height );

    TashImage<T>::TashRGBAPixel ret = {0,0,0,0};

    if ( !m_bIsInitialized ) {
        qCritical() << "Object is not initilized!";
        return ret;
    }

    if ( x > m_Width ) {
        qCritical() << "Requested x coordinate is out of image bounds!";
        return ret;
    }

    if ( y > m_Height ) {
        qCritical() << "Requested y coordinate is out of image bounds!";
        return ret;
    }

    //Get image pointer
    T* pTmp = m_Data.data();
    Q_CHECK_PTR( pTmp );
    //Set to correct offset
    pTmp += pixelOffset(x, y);

    switch (m_Format) {
    case Format_RGB16:
        T tmp = *pTmp;
        ret.r = ((ret.r & 0b00011111) | (tmp & 0b00011111));
        ret.g = ((ret.g & 0b00000111) | ((tmp & 0b11100000) >> 5));
        tmp = *(++pTmp);
        ret.g = ((ret.g & 0b00111000) | ((tmp & 0b00000111) << 3));
        ret.b = ((ret.b & 0b00011111) | ((tmp & 0b11111000) >> 3));
        ret.a = 0b00000000;
        break;
    case Format_RGB24:
        ret.r = *pTmp;
        ret.g = *(pTmp+1);
        ret.b = *(pTmp+2);
        ret.a = 0;
        break;
    case Format_RGB32:
        ret.r = *pTmp;
        ret.g = *(pTmp+1);
        ret.b = *(pTmp+2);
        ret.a = *(pTmp+3);
        break;
    default:
        qCritical() << "The image format is either set to single channel format, or invalid. "
                       "This method is for multichannel images. Use TashImage<T>::pixel() instead.";
        break;
    }

    return ret;
}

template <typename T>
void TashImage<T>::setPixel(const T &value, const quint64 &x, const quint64 &y) noexcept {
//NOT YET IMPLEMENTED
    Q_UNUSED(value);
    Q_UNUSED(x);
    Q_UNUSED(y);
    qWarning() << "Method not implemented!";
}

template <typename T>
void TashImage<T>::setPixelRGB(const TashImage<T>::TashRGBAPixel &value, const quint64 &x, const quint64 &y) noexcept {
    //NOT YET IMPLEMENTED
        Q_UNUSED(value);
        Q_UNUSED(x);
        Q_UNUSED(y);
        qWarning() << "Method not implemented!";
}

template <typename T>
T* TashImage<T>::scanlineUnsafe(const quint64 &line) noexcept {
    Q_ASSERT( line < m_Height );

    if ( !m_bIsInitialized ) {
        qCritical() << "Object is not initilized!";
        return nullptr;
    }

    if ( line > m_Height ) {
        qCritical() << "Requested y coordinate is out of image bounds!";
        return nullptr;
    }

    T* pTmp = m_Data.data();

    Q_CHECK_PTR(pTmp);
    if( !pTmp ) {
        qCritical() << "Invalid pointer to image data!";
        return nullptr;
    }

    pTmp += ((m_BytesPerLine + m_Padding) * line);

    return pTmp;
}

template <typename T>
QSharedPointer<T> TashImage<T>::scanline(const quint64 &line) noexcept {
    return QSharedPointer<T>(this->scanlineUnsafe(line), [](T* p){Q_UNUSED(p)});
}

template <typename T>
T* TashImage<T>::scanlineTypedUnsafe(const quint64 &line) noexcept {
    return static_cast<T*>(this->scanlineUnsafe(line));
}

template <typename T>
QSharedPointer<T> TashImage<T>::scanlineTyped(const quint64 &line) noexcept {
    return QSharedPointer<T>(this->scanlineTypedUnsafe(line), [](T* p){Q_UNUSED(p);});
}

template <typename T>
quint64 TashImage<T>::pixelcount() noexcept {
    return m_Width * m_Height;
}

template <typename T>
void TashImage<T>::setData(T *data,
                           TashImage::Format format,
                           quint64 width,
                           quint64 height) noexcept {

    Q_CHECK_PTR(data);

    if ( !data ) {
        qCritical() << "Invalid image data pointer passed to object! Abort!";
        m_bIsInitialized = false;
        return;
    }

    m_Data               = QSharedPointer<T>(data,[](T* p){Q_UNUSED(p);});
    m_Width              = width;
    m_Height             = height;
    m_Format             = format;
    m_bHasExternalSource = true;

    this->init();
}

template <typename T>
bool TashImage<T>::init() {
    computePadding();
    m_BytesPerLine = m_Width * bytesFromFormat(m_Format) + m_Padding;

    if ( m_Format == TashImage::Format_Invalid ) {
        qWarning() << "Image format set to invalid, won't be able to do any processing on image data!";
        return (m_bIsInitialized = false);
    }

    return (m_bIsInitialized = true);
}

template <typename T>
QString TashImage<T>::getClassName() {
    return QString("TashImage");
}

template <typename T>
bool TashImage<T>::hasExternDataSource() noexcept {
    return m_bHasExternalSource;
}

template <typename T>
void TashImage<T>::computePadding() noexcept {
    switch( m_Format ) {
    case Format_Invalid :
        m_Padding = 0;
        m_BytesPerPixel = 0;
        break;
    case Format_Grayscale8:
        m_Padding = 0;
        m_BytesPerPixel = 1;
        break;
    case Format_Grayscale16:
    case Format_RGB16:
        m_Padding = 2;
        m_BytesPerPixel = 2;
        break;
    case Format_Grayscale24:
    case Format_RGB24:
        m_Padding = 1;
        m_BytesPerPixel = 4;
        break;
    case Format_RGB32:
        m_Padding = 0;
        m_BytesPerPixel = 4;
        break;
    }
}

template <typename T>
quint8 TashImage<T>::bytesFromFormat(Format format) noexcept {

    quint8 bytes = 0;

    switch( format ) {
    case Format_Invalid :
        bytes = 0;
        break;
    case Format_Grayscale8:
        bytes = 1;
        break;
    case Format_Grayscale16:
    case Format_RGB16:
        bytes = 2;
        break;
    case Format_Grayscale24:
    case Format_RGB24:
        bytes = 3;
        break;
    case Format_RGB32:
        bytes = 4;
        break;
    }

    return bytes;
}

template <typename T>
quint64 TashImage<T>::pixelOffset(quint64 x, quint64 y) noexcept {
    return y * m_Pitch + x * m_BytesPerPixel;
}

}
}


#endif // TASHIMAGE_H

/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashmsswprocessor.h
 * @author Michael Flau
 * @brief
 * @details
 * @date November 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started November 2015
 */

#ifndef TASHMSSWPROCESSOR_H
#define TASHMSSWPROCESSOR_H

#include "../core/tashobject.h"
#include "../core/tashtypes.h"

#include <QMap>
#include <QHash>
#include <QThread>

//@cond DOXYGEN_IGNORE
template <class T> class QPointer;
//@endcond

namespace tashtego{
namespace ip{

template <typename T> class TashMSSWAccessor;
class TashMSSWProcessorBuilder;
class TashMSSWProcessorInterface;

/**
 * @brief The TashMSSWProcessor class
 */
class TashMSSWProcessor: public core::TashObject
{
	Q_OBJECT

public:

    typedef enum {
        TR_WISE,
        ROI_WISE,
        TILE_WISE,
        TILELINE_WISE
    } ProcessingStrategy;

    typedef enum {
        SORTMEAN
    } KernelType;

	/**
	 * @brief TashMSSWProcessor
	 * @param parent
	 * @warning
	 * This header is only for meant to be used for introspection discovery, do not call manually!
	 */
	explicit TashMSSWProcessor(TashObject *parent = 0);

	/**
	 * @brief TashMSSWProcessor
	 * @param id
	 * @param parent
	 */
    TashMSSWProcessor(qint64 id,
                      const QPointer<core::TashRuntime> runtime,
                      const QMap<qint32, QPointer<TashMSSWAccessor<qint16>>> &accessorBuffer,
                      quint32 cameraIdx,
                      const QString& processorName,
                      TashObject *parent = 0);

	/**
	 * @brief ~TashMSSWProcessor
	 */
	virtual ~TashMSSWProcessor();

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() override;

public slots:
    /**
     * @brief process
     * @param frameindex
     */
    virtual void process(qint32 &slot);

    /**
     * @brief getKernelType
     * @return
     */
    virtual KernelType getKernelType();

protected:
    /**
     * @brief init
     */
    virtual bool init();

    /**
     * @brief parseSettingsFromIniFile
     * @return
     */
    virtual bool parseRuntimeForProperties() override;

    /**
     * @brief setDefaultProperties
     * @return
     */
    virtual bool setDefaultProperties() override;

    /**
     * @brief stringToStrategy
     * @param str
     * @return
     */
    virtual ProcessingStrategy stringToStrategy(const QString& str);

    /**
     * @brief stringToKernelType
     * @param str
     * @return
     */
    virtual KernelType stringToKernelType(const QString& str);

private:

    /*
     * Member
     */
    quint32                                  m_cameraIdx;       ///> Camera index
    QPointer<TashMSSWProcessorBuilder>       m_pBuilder;        ///> Instance Builder
    QPointer<TashMSSWProcessorInterface>     m_pProcessor;      ///> Processor interface instance
    QString                                  m_processorName;   ///> Name of processor
    QMap<qint32, QPointer<TashMSSWAccessor<qint16>>> m_msswBuffer;      ///> Map of MSSW Accessor instances
};

}
}

Q_DECLARE_METATYPE(tashtego::ip::TashMSSWProcessor)

#endif //TASHMSSWPROCESSOR_H


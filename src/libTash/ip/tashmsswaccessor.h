/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashmsswaccessor.h
 * @author Michael Flau
 * @date October 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started October 2015
 */

#ifndef TASHMSSWACCESSOR_H
#define TASHMSSWACCESSOR_H

#include "../core/tashobject.h"
#include "../core/tashruntime.h"
#include "tashimagesegmentinfo.h"
#include "tashimagesegmentaccessor.h"
#include "tashtile.h"
#include "tashimage.h"
#include <string>
namespace tashtego{
namespace ip{

struct tilerangeDescriptor;
struct roiDescriptor;

/**
 * @brief Tile
 * Helper definition for a multiscale tile
 */
template <typename T> using Tile = TashTile<T>;

/**
 * @brief TileRange
 * Helper definition for a multiscale tile range
 */
template <typename T> using TileRange = TashImageSegmentAccessor<T, TashTile<T> >;

/**
 * @brief RegionOfInterest
 * Helper definition for a multiscale region of interest
 */
template <typename T> using RegionOfInterest = TashImageSegmentAccessor<T, TashImageSegmentAccessor< T, TashTile<T> > >;

/**
 * @brief The TashMSSWAccessor class
 * Access template for a multiscale frame. T defines the datatypes which resembles
 * the data in the raw image frame. T will be used to initialize all modules
 * required to access an image frame via a multiscale tile structure.
 */
template <typename T>
class TashMSSWAccessor: public core::TashObject
{

public:

    /**
     * @brief TashMSSWAccessor
     * Default ctor, meant for introspection only
     * @param parent
     * Parent object of TashMSSWAccessor instances
     * @warning
     * This header is only for meant to be used for introspection discovery, do not call manually!
     */
    explicit TashMSSWAccessor(core::TashObject *parent = 0);

    /**
     * @brief TashMSSWAccessor
     * Constructor of a multiscale frame instance.
     * @param id
     * Id of the TashMSSWAccessor instance which is about to be created
     * @param cameraIdx
     * Index of camera, from which the TashMSSWAccessor will retrieve it's image data
     * @param data
     * TashImage object of type T which represents a driect access to the raw image data
     * @param runtime
     * libTash runtime object which grants access to instance settings.
     * @param parent
     * Parent object reference
     */
    TashMSSWAccessor(qint64 id,
                     quint32 cameraIdx,
                     const tashtegoRuntime runtime,
                     TashImage<T> data,
                     core::TashObject *parent = 0);

    /**
     * @brief ~TashMSSWAccessor
     * Default dtor
     */
    virtual ~TashMSSWAccessor();

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() override;

    /**
     * @brief operator []
     * Operator overload, to access all region of interest of type T via operator[]
     * @param idx
     * Index of region of interest to be extracted
     * @return
     * Region of interest as shared pointer instance
     */
    QSharedPointer<RegionOfInterest<T>> operator [](const quint32& idx);

    /**
     * @brief getTile
     * Return a reference to a single multiscale tile within the multiscale frame instance
     * @param roiindex
     * Index of region of interest
     * @param rangeindex
     * Index of tile range index
     * @param tileindex
     * Index of tile
     * @return
     * Reference to indexed Tile<T> instance.
     */
    virtual Tile<T>& getTile(const quint32& roiindex,
                             const quint32& rangeindex,
                             const quint32& tileindex);

    /**
     * @brief getTilePointer
     * Returns a pointer to a Tile<T> instance referenced by the passed indeces.
     * @param roiindex
     * Index of region of interest
     * @param rangeindex
     * Index of tile range index
     * @param tileindex
     * Index of tile
     * @return
     * Pointer to a Tile<T> instance within the reference TashMSSWAccessor
     */
    virtual Tile<T>* getTilePointer(const quint32& roiindex,
                                    const quint32& rangeindex,
                                    const quint32& tileindex);

    /**
     * @brief getTileRange
     * Returns a reference to a multiscale TileRange object
     * @param rangeindex
     * Index of tile range index
     * @param tileindex
     * Index of tile
     * @return
     * Reference to indexed TileRange<T> object
     */
    virtual TileRange<T>& getTileRange(const quint32& roiindex,
                                       const quint32& rangeindex);

    /**
     * @brief getRegionOfInterest
     * Returns a reference to a multiscale region of interest object
     * @param tileindex
     * Index of tile
     * @return
     * Reference to a RegionOfInterest<T> object
     */
    virtual RegionOfInterest<T>& getRegionOfInterest(const quint32& roiindex);

    /**
     * @brief getTileSubset
     * Returns a hash container of tiles which are specified by input arguments
     * @param roiindex
     * Index of region of interest
     * @param rangeindex
     * Index of tile range index
     * @param start
     * Start of selected range
     * @param end
     * End of selected range
     * @return
     * Hash object of tile pointers indexed by start to end
     */
    virtual QHash<quint32, Tile<T> *> getTileSubset(const quint32& roiindex,
                                                    const quint32& rangeindex,
                                                    const quint32& start,
                                                    const quint32& end);

    /**
     * @brief getNumberOfROIs
     * Returns the number of created region of interests
     * @return
     * Number of region of interests as integer
     */
    virtual qint32 getNumberOfROIs();

    /**
     * @brief getNumberOfTRs
     * Returns the number of created tile ranges
     * @param roiIdx
     * Index of region of interest
     * @return
     * Number of tile ranges contained by region of interest index by roiIdx
     */
    virtual qint32 getNumberOfTRs(const quint32 &roiIdx);

    /**
     * @brief getNumberOfTiles
     * Returns the number of tile contained in specified tilerange in specified
     * region of interest.
     * @param roiIdx
     * Index of region of interest
     * @param trIdx
     * Index of tile range
     * @return
     * Number of tiles as integer
     */
    virtual qint32 getNumberOfTiles(const quint32 &roiIdx, const quint32 & trIdx);

protected:

    /**
     * @brief The tilerangeDescriptor struct
     * Helper struct which contains information abot a tile range
     */
    struct tilerangeDescriptor {
        quint32 index;          ///> Tilerange index
        quint32 roiindex;       ///> Parent ROI index
        quint32 tilewidth;      ///> Absolute tilewidth
        quint32 tileheight;     ///> Absolute tileheight
        quint32 tileovlp_x;     ///> Tile overlap in x-direction
        quint32 tileovlp_y;     ///> Tile overlap in y-direction
        quint32 rangewidth;     ///> Width of the tilerange
        quint32 rangeheight;    ///> Height of the tilerange
        QPoint startPos;        ///> Range start position
        bool mindUnfitting;     ///> Denotes wether to care about unfitting end of range
    };

    /**
     * @brief The roiDescriptor struct
     * Helper struct which contains information about a region of interest
     */
    struct roiDescriptor {
        quint32 index;                                  ///> ROI index
        quint32 width;                                  ///> Width of ROI
        quint32 height;                                 ///> Height of ROI
        QPoint  startPos;                               ///> Range start position
        quint32 numberTRs;                              ///> Number of child ranges
        QVector<struct tilerangeDescriptor> tileranges; ///> Vector of child range definitions
    };

    /**
     * @brief init
     * Initialization method to set up TashMSSWAccessor instance
     */
    virtual bool init() override;

    /**
     * @brief setDefaultProperties
     * Defines object properties with default values
     */
    bool setDefaultProperties() override;

    /**
     * @brief parseRuntimeForProperties
     * Reads instance property values from runtime meta object space and initializes
     * internal structures with these values.
     * @return
     * True if successful, otherwise false
     */
    virtual bool parseRuntimeForProperties(QVector<struct roiDescriptor> &rois);

    /**
     * @brief computeTilePosition
     * Method computes positions of all tiles within a tile range of a region of
     * interest.
     * @param trwidth
     * Width of tile range
     * @param trheight
     * Height of tile range
     * @param desc
     * Descriptor of tile range
     * @param metaObject
     * Info object of tile which holds all meta data
     * @param tilePositionList
     * Reference to Map with all tile positions
     * @return
     * True on successful computation, false if an error occurs
     */
    bool computeTilePositions(const quint32 &trwidth,
                              const quint32 &trheight,
                              const struct tilerangeDescriptor &desc,
                              QSharedPointer<TashImageSegmentInfo> &metaObject,
                              QMap<quint32,QPoint>& tilePositionList);

    /**
     * @brief computeTileRangePositions
     * Method computes positions of all tile ranges within a region of
     * interest.
     * @param roiDesc
     * Descriptor of region if interest
     * @param metaObjects
     * Vector of info objects of tiles which holds all tile's meta data
     * @param tileRangePositionList
     * Reference to map with all tilerange positions
     * @return
     * True on successful computation, false if an error occurs
     */
    bool computeTileRangePositions(const struct roiDescriptor &roiDesc,
                                   QVector<QSharedPointer<TashImageSegmentInfo> > &metaObjects,
                                   QMap<quint32, QPoint> &tileRangePositionList);

    /**
     * @brief computeROIPositions
     * Method computes positions of all region of interests within an image
     * @param roiDesc
     * Vector of ROI descriptors
     * @param roiMetaObjects
     * Reference to map containing all info object pointers of region of interest
     * @param roiPositionList
     * Reference to map containing all ROI positions
     * @return
     * True if success, false if failure
     */
    bool computeROIPositions(const QVector<struct roiDescriptor> &roiDesc,
                             QMap<quint32, QSharedPointer<TashImageSegmentInfo>> &roiMetaObjects,
                             QMap<quint32, QPoint> &roiPositionList);

    /**
     * @brief releaseDummy
     * Static dummy function, passed to smart pointers in order to avoid auto
     * deletion of managed resource.
     * @param data
     * Resource of type RegionOfInterest<T>
     */
    static void releaseDummy(RegionOfInterest<T> *data);

    /*Members*/
    bool                                 m_bUseTiling;  ///> Flag which denotes whether to use tiling
    quint32                              m_cameraIdx;   ///> Camera index
    TashImage<T>                         m_RootData;    ///> Reference to memory map stored in an image container
    QMap<quint32, RegionOfInterest<T>*>  m_Rois;        ///> Image access structure
    QVector<struct roiDescriptor>        m_roiInfo;     ///> Image segment info structure
    static QMutex*                       m_spInitMutex; ///> Static mutex pointer for multithreaded lock operations
};

// Needs to be allocated on heap, as QObject prohibit copying
template <typename T>
QMutex* TashMSSWAccessor<T>::m_spInitMutex = new QMutex();

template <typename T>
TashMSSWAccessor<T>::TashMSSWAccessor(tashtego::core::TashObject *parent):
    tashtego::core::TashObject(parent),
    m_bUseTiling(true),
    m_cameraIdx(0)
{
    setDefaultProperties();
}

template <typename T>
TashMSSWAccessor<T>::TashMSSWAccessor(qint64 id,
                                      quint32 cameraIdx,
                                      const tashtegoRuntime runtime,
                                      TashImage<T> data,
                                      TashObject *parent):

    tashtego::core::TashObject(id, runtime, parent),
    m_bUseTiling(true),
    m_cameraIdx(cameraIdx),
    m_RootData(data),
    m_Rois({})
{
    if ( !runtime->configFileExists() ) {
#ifdef TASHVERBOSE
        qDebug() << "No inifile, set default properies";
#endif
        setDefaultProperties();
    } else {
#ifdef TASHVERBOSE
        qDebug() << "Found ini file, start parsing propertiers from ini file!";
#endif
        if ( !init() ) {
            qCritical() << "Error while initializing TashMSSWAccessor instance!";
        }
    }
}

template <typename T>
TashMSSWAccessor<T>::~TashMSSWAccessor() {
    if ( m_spInitMutex ) {
        delete m_spInitMutex;
        m_spInitMutex = nullptr;
    }
}

template <typename T>
bool TashMSSWAccessor<T>::init() {

    //Status indicator of initialization
    bool status = this->isRuntimeAware();
    // Tile ID counter increased as per call
    static auto uniqueTileID = 0;

    Q_ASSERT(status);

    if ( !status ) {
        m_bHasRuntimeObject = m_bIsInitialized = false;
        qCritical() << "A reference to the runtime object is required to initialize this module!";
        return m_bIsInitialized;
    }

    status = m_pRunTime.isNull();
    Q_ASSERT(!status);

    if ( status ) {
        m_bHasRuntimeObject = m_bIsInitialized = false;
        qCritical() << "A reference to the runtime object is required to initialize this module!";
        return m_bIsInitialized;
    }

    status = m_pRunTime->isInitialized();
    Q_ASSERT(status);

    if ( !status ) {
        m_bHasRuntimeObject = m_bIsInitialized = false;
        qCritical() << "Refernce to runtime object points to an instance which is not initialized!";
        return m_bIsInitialized;
    }

    QVector<struct roiDescriptor> roiPropertyValues;

    //Parse properties from ini-file
    status = parseRuntimeForProperties(roiPropertyValues);

    Q_ASSERT(status);
    Q_ASSERT(!roiPropertyValues.isEmpty());

    if ( !status || roiPropertyValues.isEmpty() ) {
        m_bIsInitialized = false;
        qCritical() << "Error while reading values from property list!";
        return m_bIsInitialized;
    }

    QMap<quint32, QSharedPointer<TashImageSegmentInfo>> roiMetaInfo;
    QMap<quint32, QPoint>                               roiPositions;

    //Compute all ROI positions
    status = computeROIPositions(roiPropertyValues, roiMetaInfo, roiPositions);

    Q_ASSERT( status );
    Q_ASSERT( (roiMetaInfo.size() == roiPositions.size()) == roiPropertyValues.length() );

    QVectorIterator<struct roiDescriptor> roiIt(roiPropertyValues);

    auto tashidseed = core::TashRuntime::genIDPara(m_spInitMutex);

    //foreach detected region of interest
    while( roiIt.hasNext() ) {

        auto currentROI = roiIt.next();
        auto *pRoi      = new RegionOfInterest<T> (core::TashRuntime::genIDPara(m_spInitMutex/*m_pInitMutex*/, tashidseed),
                                                   currentROI.index,
                                                   roiPositions[currentROI.index].x(),
                                                   roiPositions[currentROI.index].y(),
                                                   roiMetaInfo[currentROI.index],
                                                   m_RootData.dataTyped());

        QVector<QSharedPointer<TashImageSegmentInfo>> rangeMetaObjects;
        QMap<quint32, QPoint>          trPositions;

        status = computeTileRangePositions(currentROI, rangeMetaObjects, trPositions);

        Q_ASSERT( status );
        Q_ASSERT( rangeMetaObjects.size() == trPositions.size() );

        if ( rangeMetaObjects.size() != trPositions.size() ) {

            qCritical() << "The number of created metaobjects does not concise with the number of computed range positions!";

            for ( auto it = rangeMetaObjects.begin(); it != rangeMetaObjects.end(); ++it ) {
                delete it;
            }

            return m_bIsInitialized;
        }

        Q_ASSERT( currentROI.tileranges.length() == trPositions.size() );

        if ( !status ) {
            m_bIsInitialized = false;
            qCritical() << "Error while computing tilerange positions! Abort!";
            return m_bIsInitialized;
        }

        for ( auto it = rangeMetaObjects.begin(); it != rangeMetaObjects.end(); ++it ) {

            Q_CHECK_PTR( it );

            if ( !it ) {
                m_bIsInitialized = false;
                qCritical() << "Returned pointer to allocated meta info structure about tileranges is invalid";
                return m_bIsInitialized;
            }
        }

        QVectorIterator<struct tilerangeDescriptor> trIt(currentROI.tileranges);

        QHash<quint32, TileRange<T>*> currTilerangeList;
        currTilerangeList.reserve(currentROI.tileranges.size());

        //foreach tilerange in region of interest
        while( trIt.hasNext() ) {

            auto currentTR = trIt.next();

            currTilerangeList[currentTR.index] = new TileRange<T> (core::TashRuntime::genIDPara(m_spInitMutex, tashidseed),
                                                                        currentTR.index * currentROI.index,
                                                                        trPositions[currentTR.index].x(),
                                                                        trPositions[currentTR.index].y(),
                                                                        rangeMetaObjects[currentTR.index],
                                                                        m_RootData.dataTyped());

            auto pTileMeta = QSharedPointer<TashImageSegmentInfo>( nullptr );
            QMap<quint32, QPoint> tilePositions;

            currentTR.startPos = trPositions[currentTR.index];
            status             = computeTilePositions(currentTR.rangewidth,
                                                      currentTR.rangeheight,
                                                      currentTR,
                                                      pTileMeta,
                                                      tilePositions);

            Q_ASSERT( status );
            Q_CHECK_PTR( pTileMeta.data() );
            Q_ASSERT( !tilePositions.isEmpty() );

            /**
             * @todo If anything goes wrong here, instances in pTrMetaObjects must be deleted,
             * which have not been passed to a Segment accessor instance...it is better to switch to
             * smart pointers!
             */

            if ( !status ) {
                m_bIsInitialized = false;
                qCritical() << "Error while computing tilerange positions! Abort!";
                return m_bIsInitialized;
            }

            if ( tilePositions.isEmpty() ) {
                qWarning() << "No tile positions computed, is tiling disabled?";
                continue;
            }

            QHash<quint32, Tile<T>*> currTileList;
            currTileList.reserve(tilePositions.size());
            quint32 tileIdx = 0;

            //For each computed tile position
            for ( auto tileIt = tilePositions.begin(); tileIt != tilePositions.end(); ++tileIt, ++tileIdx, ++uniqueTileID ) {

                currTileList.insert( tileIdx, qMove(new Tile<T>(core::TashRuntime::genIDPara(m_spInitMutex, tashidseed),
                                                                     uniqueTileID,
                                                                     tileIt->x(),
                                                                     tileIt->y(),
                                                                     pTileMeta,
                                                                     m_RootData.dataTyped() ) ) );
            }

            if ( currTileList.isEmpty() ) {
                qDebug() << "Found empty tile list!";
            }

            currTilerangeList[currentTR.index]->setChildSegments(qMove(currTileList));
        }

        if ( currTilerangeList.isEmpty() ) {
            qDebug() << "Found empty tilerange list!";
        }

        pRoi->setChildSegments(qMove(currTilerangeList));
        m_Rois[currentROI.index] = pRoi;
    }

    //Store parsed value for later usage in getters
    uniqueTileID = 0;
    m_roiInfo    = roiPropertyValues;

    Q_ASSERT(status);
    return (m_bIsInitialized = status);
}

template <typename T>
QString TashMSSWAccessor<T>::getClassName() {
    return QString("TashMSSWAccessor");
}

template <typename T>
QSharedPointer< RegionOfInterest<T> > TashMSSWAccessor<T>::operator [](const quint32& idx) {

    if ( !this->isInitialized() ) {
        qCritical() << "Warning, MSSW accessor instance was never initialized! Call TashMSSWAccessor::init or use dedicated constructor!";
        return QSharedPointer< RegionOfInterest<T> >();
    }

    return QSharedPointer< RegionOfInterest<T> >(m_Rois[idx], TashMSSWAccessor::releaseDummy );
}

template <typename T>
Tile<T> &TashMSSWAccessor<T>::getTile(const quint32& roiindex,
                                                        const quint32& rangeindex,
                                                        const quint32& tileindex) {
    return (*m_Rois[roiindex])[rangeindex][tileindex];
}

template <typename T>
Tile<T> *TashMSSWAccessor<T>::getTilePointer(const quint32& roiindex,
                                                               const quint32& rangeindex,
                                                               const quint32& tileindex) {
    return m_Rois[roiindex]->getSegmentPointer(rangeindex)->getSegmentPointer(tileindex);
}

template <typename T>
TileRange<T> &TashMSSWAccessor<T>::getTileRange(const quint32& roiindex,
                                                                     const quint32& rangeindex){
    return (*m_Rois[roiindex])[rangeindex];
}

template <typename T>
RegionOfInterest<T> &TashMSSWAccessor<T>::getRegionOfInterest(const quint32& roiindex){
    return *m_Rois[roiindex];
}

template <typename T>
QHash<quint32, Tile<T> *> TashMSSWAccessor<T>::getTileSubset(const quint32& roiindex,
                                                                            const quint32& rangeindex,
                                                                            const quint32& start,
                                                                            const quint32& end) {
    return getTileRange(roiindex,rangeindex).getSegmentPointerRange(start, end);

}

template <typename T>
qint32 TashMSSWAccessor<T>::getNumberOfROIs() {
    return m_roiInfo.length();
}

template <typename T>
qint32 TashMSSWAccessor<T>::getNumberOfTRs( const quint32 &roiIdx ) {

    auto rois = this->getNumberOfROIs();

    if ( roiIdx >= static_cast<quint32>(rois) ) {
        qWarning() << "Error, passed ROI index is to high, MSSW accessor has only " << rois << " region of interest defined! Note that indeces start with 0!";
        return -1;
    }

    return m_roiInfo.at(roiIdx).tileranges.length();
}

template <typename T>
qint32 TashMSSWAccessor<T>::getNumberOfTiles( const quint32 &roiIdx, const quint32 &trIdx ) {

    auto rois = this->getNumberOfROIs();

    if ( roiIdx >= static_cast<quint32>(rois) ) {
        qWarning() << "Error, passed ROI index is to high, MSSW accessor has only " << rois << " region of interest defined! Note that indeces start with 0!";
        return -1;
    }

    auto trs = this->getNumberOfTRs(roiIdx);

    if ( trIdx >= static_cast<quint32>(trs) ) {
        qWarning() << "Error, passed tilerange index is to high, ROI with index " << roiIdx << " in MSSW accessor instance has only " << trs << " tileranges defined! Note that indeces start with 0!";
        return -1;
    }

    return m_Rois[roiIdx]->operator [](trIdx).childSegments();
}

template <typename T>
bool TashMSSWAccessor<T>::setDefaultProperties() {

    //Set default properties which are required by this module
    //to function correctly

//    std::string groupDelimiter = "#";
    auto genROIentry = [=] (std::string group, std::string field) {
//        return QString("%1%2%3").arg(group).arg(groupDelimiter).arg(field).toStdString().c_str();
        auto ret = std::string(group + "__" + field);
        return ret;
    };

    //MSSW general settings
    this->setProperty("use_tiling", true);
    this->setProperty("number_of_rois", 1);

    //Region of interest settings
    this->setProperty( genROIentry("ROI_0", "width").c_str(),       400);
    this->setProperty( genROIentry("ROI_0", "height").c_str(),      300);
    this->setProperty( genROIentry("ROI_0", "x").c_str(),           200);
    this->setProperty( genROIentry("ROI_0", "y").c_str(),           50);
    this->setProperty( genROIentry("ROI_0", "number_trs").c_str(),  3);

    //Write 2 explanatory ranges
    for (auto idx = 0; idx < 2; ++idx) {
        auto s = std::string("ROI_0__Tilerange_"+std::to_string(idx)+"__range_start_x");
        this->setProperty( s.c_str(), idx);

        s = std::string("ROI_0__Tilerange_"+std::to_string(idx)+"__range_start_y");
        this->setProperty( s.c_str(), idx);

        s = std::string("ROI_0__Tilerange_"+std::to_string(idx)+"__range_height");
        this->setProperty( s.c_str(), (idx+1)*100);

        s = std::string("ROI_0__Tilerange_"+std::to_string(idx)+"__range_width");
        this->setProperty( s.c_str(), 1000);

        s = std::string("ROI_0__Tilerange_"+std::to_string(idx)+"__tile_width");
        this->setProperty( s.c_str(), (idx+1)*11);

        s = std::string("ROI_0__Tilerange_"+std::to_string(idx)+"__tile_height");
        this->setProperty( s.c_str(), (idx+1)*11);

        s = std::string("ROI_0__Tilerange_"+std::to_string(idx)+"__tile_overlap_x");
        this->setProperty( s.c_str(), (idx+1)*5);

        s = std::string("ROI_0__Tilerange_"+std::to_string(idx)+"__tile_overlap_y");
        this->setProperty( s.c_str(), (idx+1)*5);

        s = std::string("ROI_0__Tilerange_"+std::to_string(idx)+"__mind_unfitting_end");
        this->setProperty( s.c_str(),  false);
    }

    return true;
}

template <typename T>
bool TashMSSWAccessor<T>::parseRuntimeForProperties(QVector<struct roiDescriptor> &rois) {

    rois.clear();

    QStringList list  = m_pRunTime->getPropertyNamesContaining( this->getClassName(), m_cameraIdx);

    //Get number of ROIs, number of TRs and whether to apply tiling
    auto numTRs  = 0u;
    auto numROIs = 0u;
    const QString propertySeparator = "-";
    const QString propertySubSeparator = "__";

    foreach( QString str, list) {

//        if ( !str.contains(propertySubSeparator)) {
//            continue;
//        }

//        qDebug() << "Before: " << str;
//        str.replace(propertySubSeparator, propertySeparator);
//        qDebug() << "After: " << str;

        QStringList strparts = str.split( propertySeparator );

        if ( strparts.last() == "number_of_rois" ) {
#ifdef TASHVERBOSE
            qDebug() << "Found " << strparts.last();
#endif
            numROIs = m_pRunTime->property(str).toInt();
            continue;
        }

        if ( strparts.last() == "use_tiling" ) {
#ifdef TASHVERBOSE
            qDebug() << "Found " << strparts.last();
#endif
            m_bUseTiling = m_pRunTime->property(str).toBool();
            continue;
        }
    }

    //ROIwise property extraction
    for ( auto idx = 0u ; idx < numROIs; ++idx ) {

        struct roiDescriptor currentROIDescriptor = {
                    idx,
                    0u,
                    0u,
                    QPoint(0,0),
                    0u,
                    QVector<struct tilerangeDescriptor>()
        };

        foreach( QString str, list) {

            if ( !str.contains(propertySubSeparator)) {
                continue;
            }

            QString strcp = str;
            strcp.replace(propertySubSeparator, propertySeparator);
            QStringList strparts  = strcp.split( propertySeparator );

            const QString roiProperty   = strparts.takeLast();
            const QString roiIdentifier = strparts.takeLast();

            //First check for correct ROI
            if ( roiIdentifier == QString("ROI_%1").arg(idx) ) {

                if ( roiProperty == "width" ) {
#ifdef TASHVERBOSE
                    qDebug() << "Found ROI property " << roiProperty;
#endif
                    currentROIDescriptor.width = m_pRunTime->property( str ).toInt();
                    continue;
                }

                if ( roiProperty == "height" ) {
#ifdef TASHVERBOSE
                    qDebug() << "Found ROI property " << roiProperty;
#endif
                    currentROIDescriptor.height = m_pRunTime->property( str ).toInt();
                    continue;
                }

                if ( roiProperty == "x" ) {
#ifdef TASHVERBOSE
                    qDebug() << "Found ROI property " << roiProperty;
#endif
                    currentROIDescriptor.startPos.setX( m_pRunTime->property( str ).toInt() );
                    continue;
                }

                if ( roiProperty == "y" ) {
#ifdef TASHVERBOSE
                    qDebug() << "Found ROI property " << roiProperty;
#endif
                    currentROIDescriptor.startPos.setY( m_pRunTime->property( str ).toInt() );
                    continue;
                }

                if ( roiProperty == "number_trs" ) {
#ifdef TASHVERBOSE
                    qDebug() << "Found ROI property " << roiProperty;
#endif
                    currentROIDescriptor.numberTRs = m_pRunTime->property( str ).toInt();
                    numTRs = currentROIDescriptor.numberTRs;
                    currentROIDescriptor.tileranges.resize(currentROIDescriptor.numberTRs);
                    continue;
                }
            }
        }

        //For each tilerange in roi
        for ( auto idx2 = 0u; idx2 < numTRs; ++idx2 ) {

            struct tilerangeDescriptor currentTileRange = {
                        idx2,
                        idx,
                        0u,
                        0u,
                        0u,
                        0u,
                        0u,
                        0u,
                        QPoint(0,0),
                        false
            };

            foreach( QString str, list ) {
#ifdef TASHVERBOSE
                qDebug() << "Next property is: " << str;
#endif
                if ( !str.contains(propertySubSeparator)) {
                    continue;
                }

                QString strcp = str;
                strcp.replace(propertySubSeparator, propertySeparator);
                QStringList strparts  = strcp.split( propertySeparator );

                const QString trProperty    = strparts.takeLast();
                const QString trIdentifier  = strparts.takeLast();
                const QString roiIdentifier = strparts.takeLast();

                if ( (roiIdentifier == QString("ROI_%1").arg(idx)) && (trIdentifier == QString("Tilerange_%1").arg(idx2)) ) {

                    if ( trProperty == "tile_height" ) {
#ifdef TASHVERBOSE
                        qDebug() << "Found TR property " << trProperty;
#endif
                        currentTileRange.tileheight = m_pRunTime->property( str ).toInt();
                        continue;
                    }

                    if ( trProperty == "tile_width" ) {
#ifdef TASHVERBOSE
                        qDebug() << "Found TR property " << trProperty;
#endif
                        currentTileRange.tilewidth = m_pRunTime->property( str ).toInt();
                        continue;
                    }

                    if ( trProperty == "range_height" ) {
#ifdef TASHVERBOSE
                        qDebug() << "Found TR property " << trProperty;
#endif
                        currentTileRange.rangeheight = m_pRunTime->property( str ).toInt();
                        continue;
                    }

                    if ( trProperty == "range_width" ) {
#ifdef TASHVERBOSE
                        qDebug() << "Found TR property " << trProperty;
#endif
                        currentTileRange.rangewidth = m_pRunTime->property( str ).toInt();
                        continue;
                    }

                    if ( trProperty == "tile_overlap_x" ) {
#ifdef TASHVERBOSE
                        qDebug() << "Found TR property " << trProperty;
#endif
                        currentTileRange.tileovlp_x = m_pRunTime->property( str ).toInt();
                        continue;
                    }

                    if ( trProperty == "tile_overlap_y" ) {
                        currentTileRange.tileovlp_y = m_pRunTime->property( str ).toInt();
                        continue;
                    }

                    if ( trProperty == "range_start_x" ) {
#ifdef TASHVERBOSE
                        qDebug() << "Found TR property " << trProperty;
#endif
                        currentTileRange.startPos.setX( m_pRunTime->property( str ).toInt() );
                        continue;
                    }

                    if ( trProperty == "range_start_y" ) {
#ifdef TASHVERBOSE
                        qDebug() << "Found TR property " << trProperty;
#endif
                        currentTileRange.startPos.setY( m_pRunTime->property( str ).toInt() );
                        continue;
                    }

                    if ( trProperty == "mind_unfitting_end" ) {
#ifdef TASHVERBOSE
                        qDebug() << "Found TR property " << trProperty;
#endif
                        currentTileRange.mindUnfitting = m_pRunTime->property( str ).toBool();
                        continue;
                    }
                }
            }
            currentROIDescriptor.tileranges[idx2] = currentTileRange;
        }
        rois.append(currentROIDescriptor);
    }
    return true;
}

template <typename T>
bool TashMSSWAccessor<T>::computeTilePositions(const quint32 &trwidth,
                                               const quint32 &trheight,
                                               const struct tilerangeDescriptor& desc,
                                               QSharedPointer<TashImageSegmentInfo> &pInfoMetaObject,
                                               QMap<quint32, QPoint> &tilePositionList) {

#ifdef TASHVERBOSE
    auto prnt = qDebug();
#endif

    if( !this->isRuntimeAware() ) {
        qCritical() << "TashMSSWAccessor instance need reference to runtime to work correctly, abort!";
        return false;
    }

    const auto cStartPos               = desc.startPos;
    const auto cRangeWidth             = trwidth;
    const auto cRangeHeight            = trheight;
    const auto cAbsWidth               = desc.tilewidth;
    const auto cAbsHeight              = desc.tileheight;
    const auto cOverlap_X              = desc.tileovlp_x;
    const auto cOverlap_Y              = desc.tileovlp_y;

#ifdef TASHVERBOSE
    prnt << "Prospective Tiling information:\n";
    prnt << "TR width: " << trwidth << "\n";
    prnt << "TR height: " << trheight << "\n";
    prnt << "Abs. width: "  << cAbsWidth  << "\n";
    prnt << "Abs. height: " << cAbsHeight << "\n";
    prnt << "Ovlp.-X: "     << cOverlap_X << "\n";
    prnt << "Ovlp.-Y: "     << cOverlap_Y << "\n";
#endif

    Q_ASSERT(cAbsWidth  > cOverlap_X);
    Q_ASSERT(cAbsHeight > cOverlap_Y);
    const auto cRelWidth  = cAbsWidth  - cOverlap_X;
    const auto cRelHeight = cAbsHeight - cOverlap_Y;

#ifdef TASHVERBOSE
    prnt << "Rel. width: " << cRelWidth << "\n";
    prnt << "Rel. height: " << cRelHeight << "\n";
#endif

    Q_ASSERT( (cRelWidth > 0) );
    Q_ASSERT( (cRelHeight > 0) );

    const auto cConsiderRemaniningEnd = desc.mindUnfitting;

    //Get height and width of image
    const auto cImageWidth  = m_pRunTime->property( m_pRunTime->getPropertyNameContaining( TASHIMAGE_WIDTHID, m_cameraIdx) ).toUInt();
    const auto cImageHeight = m_pRunTime->property( m_pRunTime->getPropertyNameContaining( TASHIMAGE_HEIGHTID, m_cameraIdx) ).toUInt();
    const auto cStartX      = static_cast<quint32>(cStartPos.x());
    const auto cStartY      = static_cast<quint32>(cStartPos.y());

#ifdef TASHVERBOSE
    prnt << "Img. height: " << cImageHeight << "\n";
    prnt << "Img. width: "  << cImageWidth  << "\n";
    prnt << "Start-X Pos.: "<< cStartX << "\n";
    prnt << "Start-Y Pos.: "<< cStartY << "\n";
#endif

    Q_ASSERT( cStartX < cImageWidth );

    if ( cStartX > cImageWidth ) {
        qCritical() << "Dimension mismatch: Startpos_x >> " << cStartX << " vs. Image width >> " << cImageWidth;
        return false;
    }

    Q_ASSERT( cStartY < cImageHeight );

    if ( cStartY > cImageHeight ) {
        qCritical() << "Dimension mismatch: Startpos_y >> " << cStartY << " vs. Image height >> " << cImageHeight;
        return false;
    }

    Q_ASSERT((cAbsWidth <= cRangeWidth) && (cRangeWidth <= cImageWidth) && (cAbsWidth <= cImageWidth));

    if ( (cAbsWidth > cRangeWidth) || (cRangeWidth > cImageWidth) || (cAbsWidth > cImageWidth) ) {
        qCritical() << "Dimension mismatch: tile width >> " << cAbsWidth << " vs. range width >> " << cRangeWidth << " vs. image width >> " << cImageWidth;
        return false;
    }

    Q_ASSERT((cAbsHeight <= cRangeHeight) && (cRangeHeight <= cImageHeight) && (cAbsHeight <= cImageHeight));

    if ( (cAbsHeight > cRangeHeight) || (cRangeHeight > cImageHeight) || (cAbsHeight > cImageHeight) ) {
        qCritical() << "Dimension mismatch: tile height >> " << cAbsHeight << " vs. range height >> " << cRangeHeight << " vs. image height >> " << cImageHeight;
        return false;
    }

    Q_ASSERT( cOverlap_X < cAbsWidth );

    if ( cOverlap_X >= cAbsWidth ) {
        qWarning() << "Overlap warning: horizontal overlap in px >> " << cOverlap_X << " vs. tile width >> " << cAbsWidth;
    }

    Q_ASSERT( cOverlap_Y < cAbsHeight );

    if ( cOverlap_Y >= cAbsHeight ) {
        qWarning() << "Overlap warning: vertical overlap in px >> " << cOverlap_Y << " vs. tile height >> " << cAbsHeight;
    }

    //Calculate number of tiles in x and y direction
    const qint32 cNumTilesX = ((cRangeWidth  - (cRangeWidth  % cRelWidth))  / cRelWidth);
    const qint32 cNumTilesY = ((cRangeHeight - (cRangeHeight % cRelHeight)) / cRelHeight);

#ifdef TASHVERBOSE
    prnt << "Num. tiles in X-direction: " << cNumTilesX << "\n";
    prnt << "Num. tiles in Y-direction: " << cNumTilesY << "\n";
    prnt << "Num. tiles: " << cNumTilesX*cNumTilesY << "\n";
#endif

    if ( !cConsiderRemaniningEnd ) {
        Q_ASSERT( cRangeHeight >= (cNumTilesY * cRelHeight) );
        Q_ASSERT( cRangeWidth  >= (cNumTilesX * cRelWidth) );
    } else {
#ifdef TASHVERBOSE
    prnt << "Unfitting ends will be considered!";
#endif
    }

    auto x(0);
    auto y(0);
    auto tileindex(0);

    /**
     * @todo Consider switching to QVectors here for speedup in initialization stage
     */
    for ( auto idx_h = 0; idx_h < cNumTilesY; ++idx_h ) {

        //Fit last row to range height if enabled
        if ( cConsiderRemaniningEnd && (idx_h == (cNumTilesY-1)) ) {
            y = cStartY + (cRangeHeight - cAbsHeight);
        } else {
            y = cStartY + (idx_h * cRelHeight);
        }

        for (auto idx_w = 0; idx_w < cNumTilesX; ++idx_w, ++tileindex ) {

            //Fit last col to range width if enabled
            if (cConsiderRemaniningEnd && idx_w == (cNumTilesX - 1)) {
                x = cStartX + (cRangeWidth - cAbsWidth);
            } else {
                x = cStartX + (idx_w * cRelWidth);
            }

            tilePositionList[tileindex] = qMove(QPoint(x,y));
        }
    }

    Q_ASSERT( (cNumTilesX * cNumTilesY) == tilePositionList.size() );

    //Create meta object for all tiles of tilerange
    auto pTmpSegInfo = new TashImageSegmentInfo(core::TashRuntime::genIDPara(m_spInitMutex),
                                                m_cameraIdx,
                                                cAbsWidth,
                                                cAbsHeight,
                                                cOverlap_X,
                                                cOverlap_Y,
                                                cNumTilesX * cNumTilesY,
                                                cNumTilesX,
                                                cNumTilesY);

    pInfoMetaObject.reset(pTmpSegInfo);

    Q_CHECK_PTR( pInfoMetaObject.data() );

    if ( !pInfoMetaObject.data() ) {
        qCritical() << "Failed to allocate memory for TashImageSegmentInfo instance!";
        return false;
    }

    if ( !pInfoMetaObject->setRuntime(m_pRunTime) ) {
        qCritical() << "Failed to set runtime for tashtile instance!";
    }

    return true;
}

template<typename T>
bool TashMSSWAccessor<T>::computeTileRangePositions(const struct roiDescriptor& roiDesc,
                                                    QVector< QSharedPointer<TashImageSegmentInfo> > &metaObjects,
                                                    QMap<quint32, QPoint> &tileRangePositionList) {
    //Do value checks
    if( !this->isRuntimeAware() ) {
        qCritical() << "TashMSSWAccessor instance need reference to runtime to work correctly, abort!";
        return false;
    }

    const auto cStartPos  = roiDesc.startPos;
    const auto cROIWidth  = roiDesc.width;
    const auto cROIHeight = roiDesc.height;

    if ( roiDesc.tileranges.length() <= 0 ) {
        qCritical() << "roiDescriptor does not contain any tilerange information! Cannot proceed computing tilerange positions!";
        return false;
    }

    //Get height and width of image
    const auto cImageWidth  = m_pRunTime->property( m_pRunTime->getPropertyNameContaining( TASHIMAGE_WIDTHID, m_cameraIdx) ).toUInt();
    const auto cImageHeight = m_pRunTime->property( m_pRunTime->getPropertyNameContaining( TASHIMAGE_HEIGHTID, m_cameraIdx) ).toUInt();
    const auto cROIStart_X  = static_cast<quint32>(cStartPos.x());
    const auto cROIStart_Y  = static_cast<quint32>(cStartPos.y());

    Q_ASSERT( cROIStart_X <= cImageWidth );

    if ( cROIStart_X > cImageWidth ) {
        qCritical() << "Dimension mismatch: Startpos_x >> " << cROIStart_X << " vs. Image width >> " << cImageWidth;
        return false;
    }

    Q_ASSERT( cROIStart_Y <= cImageHeight );

    if ( cROIStart_Y > cImageHeight ) {
        qCritical() << "Dimension mismatch: Startpos_y >> " << cROIStart_Y << " vs. Image height >> " << cImageHeight;
        return false;
    }

    Q_ASSERT( !roiDesc.tileranges.isEmpty() );

    if ( roiDesc.tileranges.isEmpty() ) {
        qCritical() << "There are no tileranges defined in ROI, abort!";
        return false;
    }

    //Compute tilerange start positions
    auto trPosIdx = 0;

    for ( auto it = roiDesc.tileranges.begin(); it != roiDesc.tileranges.end(); ++it, ++trPosIdx ) {

        const auto cTRWidth  = it->rangewidth;
        const auto cTRHeight = it->rangeheight;

        Q_ASSERT( (cTRWidth <= cROIWidth) && (cROIWidth <= cImageWidth) && (cTRWidth <= cImageWidth) );

        if ( (cTRWidth > cROIWidth) || (cROIWidth > cImageWidth) || (cTRWidth > cImageWidth) ) {
            qCritical() << "Dimension mismatch: tile width >> " << cTRWidth << " vs. roi width >> " << cROIWidth << " vs. image width >> " << cImageWidth;
            return false;
        }

        Q_ASSERT((cTRHeight <= cROIHeight) && (cROIHeight <= cImageHeight) && (cTRHeight <= cImageHeight));

        if ( (cTRHeight > cROIHeight) || (cROIHeight > cImageHeight) || (cTRHeight > cImageHeight) ) {
            qCritical() << "Dimension mismatch: tile height >> " << cTRHeight << " vs. roi height >> " << cROIHeight << " vs. image height >> " << cImageHeight;
            return false;
        }

        auto y(0);
        TashImageSegmentInfo* pTmp = new TashImageSegmentInfo(core::TashRuntime::genIDPara(m_spInitMutex/*m_pInitMutex*/),
                                               m_cameraIdx,
                                               it->rangewidth,
                                               it->rangeheight,
                                               0,
                                               0,
                                               roiDesc.numberTRs,
                                               1,
                                               roiDesc.tileranges.size());

        Q_CHECK_PTR(pTmp);

        if ( !pTmp ) {
            qCritical() << "Failed to allocate memory for TashImageSegmentInfo instance!";

            for (auto idx = 0u; idx < (it->index-1); ++idx) {
                metaObjects[idx].clear();
            }

            return false;
        }

        if ( !pTmp->setRuntime( m_pRunTime) ) {
            qCritical() << "Failed to set runtime for tileRange instance!";
        }

        /**
         * @todo this operation is not very performant.
         * However only append works due to the fact that raw pointers
         * are stored, so created pointers to allocated space in memory
         * will go out of scope once the function returns.
         * Again, use smart pointers here to circumvent the issue!!!
         */
        metaObjects.append( QSharedPointer<TashImageSegmentInfo>(pTmp) );

        y = (it->startPos.y()) + cROIStart_Y;
        tileRangePositionList[trPosIdx] = QPoint(cROIStart_X + it->startPos.x(), y);
    }

    return true;
}

template <typename T>
bool TashMSSWAccessor<T>::computeROIPositions(const QVector<struct roiDescriptor>& roiDescriptors,
                                              QMap< quint32, QSharedPointer<TashImageSegmentInfo> >& roiMetaObjects,
                                              QMap< quint32, QPoint > &roiPositionList ) {

    Q_ASSERT(this->isRuntimeAware());

    //Do value checks
    if( !this->isRuntimeAware() ) {
        qCritical() << "TashMSSWAccessor instance need reference to runtime to work correctly, abort!";
        return false;
    }

    Q_ASSERT( !roiDescriptors.isEmpty() );

    if ( roiDescriptors.isEmpty() ) {
        qCritical() << "List which describes roi credentials is empty, can't proceed!";
        return false;
    }

    if ( !roiMetaObjects.isEmpty() ) {
        qWarning() << "Meta information about ROIs seems to be already computed, skip rest!";
        return true;
    }

    //Get height and width of image
    const auto cImageWidth  = m_pRunTime->property( m_pRunTime->getPropertyNameContaining( TASHIMAGE_WIDTHID, m_cameraIdx) ).toUInt();
    const auto cImageHeight = m_pRunTime->property( m_pRunTime->getPropertyNameContaining( TASHIMAGE_HEIGHTID, m_cameraIdx) ).toUInt();

    QVectorIterator<struct roiDescriptor> roiIt(roiDescriptors);

    //Foreach found region of interest
    while ( roiIt.hasNext() ) {

        auto roiDesc = roiIt.next();
        const auto cROI_X            = static_cast<quint32>(roiDesc.startPos.x());
        const auto cROI_Y            = static_cast<quint32>(roiDesc.startPos.y());
        const auto cROIWidth         = roiDesc.width;
        const auto cROIHeight        = roiDesc.height;

        Q_ASSERT( cROI_X < cImageWidth );

        if ( cROI_X > cImageWidth ) {
            qCritical() << "Dimension mismatch: Startpos_x >> " << cROI_X << " vs. Image width >> " << cImageWidth;
            return false;
        }

        Q_ASSERT( cROI_Y <= cImageHeight );

        if ( cROI_Y > cImageHeight ) {
            qCritical() << "Dimension mismatch: Startpos_y >> " << cROI_Y << " vs. Image height >> " << cImageHeight;
            return false;
        }

        Q_ASSERT( cROIWidth <= cImageWidth );

        if ( cROIWidth > cImageWidth ) {
            qCritical() << "Dimension mismatch: tile width >> " << cROIWidth << " vs. image width >> " << cImageWidth;
            return false;
        }

        Q_ASSERT( cROIHeight <= cImageHeight );

        if ( cROIHeight > cImageHeight ) {
            qCritical() << "Dimension mismatch: tile height >> " << cROIHeight << " vs. image height >> " << cImageHeight;
            return false;
        }

        auto* pTmp = new TashImageSegmentInfo(core::TashRuntime::genIDPara( m_spInitMutex ),
                                              m_cameraIdx,
                                              cROIWidth,
                                              cROIHeight,
                                              0, 0, 1, 0, 0);

        Q_CHECK_PTR(pTmp);

        roiMetaObjects[roiDesc.index] = QSharedPointer<TashImageSegmentInfo>(pTmp);

        if ( !roiMetaObjects[roiDesc.index] ) {
            qCritical() << "Failed to allocate memory for TashImageSegmentInfo instance!";
            return false;
        }

        if ( !roiMetaObjects[roiDesc.index]->setRuntime(m_pRunTime) ) {
            qCritical() << "Failed to set runtime for regionOfInterest instance!";
        }

        roiPositionList[roiDesc.index] = QPoint(cROI_X, cROI_Y);
    }

    return true;
}

template <typename T>
void TashMSSWAccessor<T>::releaseDummy(RegionOfInterest<T> *data){
    Q_UNUSED(data)
}

}
}

#endif //TASHMSSWACCESSOR_H


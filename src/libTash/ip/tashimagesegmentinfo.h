/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashimagesegmentinfo.h
 * @author Michael Flau
 * @brief
 * Simple getter class to describe segement types traits
 * @details
 * @date September 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started September 2015
 */

#ifndef TASHIMAGESEGMENTINFO_H
#define TASHIMAGESEGMENTINFO_H

#include "../core/tashobject.h"

namespace tashtego{
namespace ip{

/**
 * @brief The TashImageSegmentInfo class
 * This class holds all meta information which are
 * mandatory to describe a certain type of segments.
 * Segments can be of the following different types:
 * - Tile
 * - Tilerange (TR)
 * - Region of interest (ROI)
 * For a detailed depiction of each type see the respective
 * class implementations for TashTile and TashAbstractSegmentAccessor.
 * As each segment types describes features which count for all segments
 * of the same type, this class is used to comprise these features in
 * a central point rather than having a meta information instance for each
 * segment instance ever created at runtime.
 */
class TashImageSegmentInfo: public core::TashObject
{
    Q_OBJECT
    Q_PROPERTY(quint32 segmentWidth      READ getWidth)
    Q_PROPERTY(quint32 segmentHeight     READ getHeight)
    Q_PROPERTY(quint32 horizontalOverlap READ getHorizontalOverlap)
    Q_PROPERTY(quint32 verticalOverlap   READ getVerticalOverlap)
    Q_PROPERTY(quint32 segmentCount      READ getNumberChildSegments)
    Q_PROPERTY(quint32 segmentRows       READ getNumberOfSegmentRows)
    Q_PROPERTY(quint32 segmentsPerRow    READ getNumberOfChildSegmentsPerRow)
    Q_PROPERTY(quint8 dataSourceIndex    READ getDataSourceIndex)

public:

	/**
	 * @brief TashImageSegmentInfo
	 * @param parent
	 */
	explicit TashImageSegmentInfo(TashObject *parent = 0);


    /**
     * @brief TashImageSegmentInfo
     * @param id
     * @param width
     * @param height
     * @param hOverlap
     * @param vOverlap
     * @param segmentCount
     * @param segmentRows
     * @param parent
     */
    TashImageSegmentInfo(qint64 id,
                         quint8  dataSourceIndex,
                         quint32 width,
                         quint32 height,
                         quint32 hOverlap,
                         quint32 vOverlap,
                         quint32 segmentCount,
                         quint32 segmentsPerRow,
                         quint32 segmentRows,
                         TashObject *parent = 0);

	/**
	 * @brief ~TashImageSegmentInfo
	 */
    virtual ~TashImageSegmentInfo() = default;

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() override;

    /**
     * @brief operator =
     * @param other
     * @return
     */
    TashImageSegmentInfo& operator=(const TashImageSegmentInfo& other);

signals:

public slots:
    /**
     * @brief getWidth
     * @return
     */
    virtual quint32 getWidth() const;

    /**
     * @brief getHeight
     * @return
     */
    virtual quint32 getHeight() const;

    /**
     * @brief getHorizontalOverlap
     * @return
     */
    virtual quint32 getHorizontalOverlap() const;

    /**
     * @brief getVerticalOverlap
     * @return
     */
    virtual quint32 getVerticalOverlap() const;

    /**
     * @brief getNumberChildSegments
     * @return
     */
    virtual quint32 getNumberChildSegments() const;

    /**
     * @brief getNumberOfSegmentRows
     * @return
     */
    virtual quint32 getNumberOfSegmentRows() const;

    /**
     * @brief getNumberOfChildSegmentsPerRow
     * @return
     */
    virtual quint32 getNumberOfChildSegmentsPerRow() const;

    /**
     * @brief getDataSourceIndex
     * @return
     */
    virtual quint8 getDataSourceIndex() const;

    /**
     * @brief getImageHeight
     * @return
     */
    virtual quint32 getImageHeight() const;

    /**
     * @brief getImageWidth
     * @return
     */
    virtual quint32 getImageWidth() const;

protected:

    //Tile, tilerange, roi width
    quint32 m_width;
    //Tile, tilerange, roi height
    quint32 m_height;
    //horizontal segment overlap
    quint32 m_horzOverlap;
    //vertical segement overlap
    quint32 m_vertOverlap;
    //Number of child segments
    quint32 m_childSegments;
    //Number of segment rows
    quint32 m_segmentRows;
    //index number of image data source
    quint8 m_dataSourceIdx;
    //Number of child segments per row
    quint32 m_childSegmentsPerRow;
};

}
}

Q_DECLARE_METATYPE(tashtego::ip::TashImageSegmentInfo)

#endif //TASHIMAGESEGMENTINFO_H


/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef TASHMEDIANSORTCOMMON
#define TASHMEDIANSORTCOMMON

#include <QtGlobal>

/**
 * Convenience container to store the first and
 * last tile in a line per thread.
 */
namespace tashtego {
    inline namespace ip {
//        typedef struct {
//            qint32 roiidx;
//            qint32 tridx;
//            qint32 lineidx;
//            qint32 lower;
//            qint32 upper;
//        } tileWorkloadBoundary;

        template <typename T>
        struct tileWorkloadBoundary{
            T roiidx;
            T tridx;
            T lineidx;
            T lower;
            T upper;
        };
    }
}
#endif // TASHMEDIANSORTCOMMON


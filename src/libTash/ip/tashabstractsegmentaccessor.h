/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashabstractsegmentaccessor.h
 * @author Michael Flau
 * @brief
 * Pure virtual base class which implements methods and members
 * which all segment types have in common.
 * @details
 * @date September 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started September 2015
 */

#ifndef TASHABSTRACTSEGMENTACCESSOR_H
#define TASHABSTRACTSEGMENTACCESSOR_H

#include <QSharedPointer>
#include <QPoint>

#include "../core/tashobject.h"
#include "tashimagesegmentinfo.h"

//class QPoint;

namespace tashtego{
namespace ip{

template <typename T>
class TashAbstractSegmentAccessor: public core::TashObject
{

public:

	/**
	 * @brief TashAbstractSegmentAccessor
	 * @param parent
	 */
	explicit TashAbstractSegmentAccessor(TashObject *parent = 0);

    /**
     * @brief TashAbstractSegmentAccessor
     * @param parent
     */
    TashAbstractSegmentAccessor(quint64 id, TashObject *parent = 0);

    /**
     * @brief TashAbstractSegmentAccessor
     * @param id
     * @param segmentIndex
     * @param startX
     * @param startY
     * @param metaInfo
     * @param data
     * @param parent
     */
    TashAbstractSegmentAccessor(qint64 id,
                                const quint32 segmentIndex,
                                const quint32 startX,
                                const quint32 startY,
                                QSharedPointer<TashImageSegmentInfo> metaInfo,
                                QSharedPointer<T> data,
                                TashObject *parent = 0);

	/**
	 * @brief ~TashAbstractSegmentAccessor
	 */
	virtual ~TashAbstractSegmentAccessor();

    /**
     * @brief get
     * @param index
     * @return
     */
    virtual T& getSegment(const quint32 &index) = 0;

public slots:
    /**
     * @brief getStartPoint
     * @return
     */
    virtual QPoint getStartPoint();

    /**
     * @brief getSegmentWidth
     * @return
     */
    virtual quint32 getSegmentWidth();

    /**
     * @brief getSegmentHeight
     * @return
     */
    virtual quint32 getSegmentHeight();

    /**
     * @brief getSegmentXOverlap
     * @return
     */
    virtual quint32 getSegmentXOverlap();

    /**
     * @brief getSegmentYOverlap
     * @return
     */
    virtual quint32 getSegmentYOverlap();

    /**
     * @brief getSafeSegmentAccess
     * @return
     */
    virtual QSharedPointer<T> getSafeSegmentAccess();

    /**
     * @brief getUnsafeSegmentAccess
     * @return
     */
    virtual T* getUnsafeSegmentAccess();

    /**
     * @brief getSegmentIndexNumber
     * @return
     */
    virtual quint32 getSegmentIndexNumber();

protected:
    /**
     * @brief init
     */
    virtual bool init() = 0;

    /**
     * @brief getClassName
     * @return
     */
    virtual QString getClassName() = 0;

    /*
     * Members
     */
    const quint32 m_cIndex;             ///> Index number of segment instance
    quint32 m_cXStart;                  ///> X start position of segment
    quint32 m_cYStart;                  ///> Y start position of segment
    QSharedPointer<TashImageSegmentInfo> m_pMetaInfo;  ///> Pointer to metat info structure which describes all segments alike
    QSharedPointer<T> m_pData;          ///> Safe pointer to actual image data
};

template <typename T> TashAbstractSegmentAccessor<T>::TashAbstractSegmentAccessor(TashObject *parent):
tashtego::core::TashObject(parent),
  m_cIndex(0),
  m_cXStart(0),
  m_cYStart(0),
  m_pData(NULL) {

}

template <typename T> TashAbstractSegmentAccessor<T>::TashAbstractSegmentAccessor(quint64 id, TashObject *parent):
    tashtego::core::TashObject(id, parent),
    m_cIndex(0),
    m_cXStart(0),
    m_cYStart(0),
    m_pData(NULL) {

}

template <typename T> TashAbstractSegmentAccessor<T>::TashAbstractSegmentAccessor(qint64 id,
                            const quint32 segmentIndex,
                            const quint32 startX,
                            const quint32 startY,
                            QSharedPointer<TashImageSegmentInfo> metaInfo,
                            QSharedPointer<T> data,
                            TashObject *parent):
    tashtego::core::TashObject(id, parent),
    m_cIndex(segmentIndex),
    m_cXStart(startX),
    m_cYStart(startY),
    m_pMetaInfo(metaInfo),
    m_pData(data) {
    Q_CHECK_PTR(m_pData.data());
    Q_CHECK_PTR(m_pMetaInfo);
}

template <typename T> TashAbstractSegmentAccessor<T>::~TashAbstractSegmentAccessor() {
    if ( this->m_pMetaInfo ) { this->m_pMetaInfo->deleteLater(); }
}

template <typename T> QPoint TashAbstractSegmentAccessor<T>::getStartPoint() {
    return QPoint(this->m_cXStart, this->m_cYStart);
}

template <typename T> quint32 TashAbstractSegmentAccessor<T>::getSegmentWidth() {
    return m_pMetaInfo->getWidth();
}

template <typename T> quint32 TashAbstractSegmentAccessor<T>::getSegmentHeight() {
    return m_pMetaInfo->getHeight();
}

template <typename T> quint32 TashAbstractSegmentAccessor<T>::getSegmentXOverlap() {
    return m_pMetaInfo->getHorizontalOverlap();
}

template <typename T> quint32 TashAbstractSegmentAccessor<T>::getSegmentYOverlap() {
    return m_pMetaInfo->getVerticalOverlap();
}

template <typename T> QSharedPointer<T> TashAbstractSegmentAccessor<T>::getSafeSegmentAccess() {
    return m_pData;
}

template <typename T> T* TashAbstractSegmentAccessor<T>::getUnsafeSegmentAccess() {

    if ( m_pData.isNull() ) {
        return nullptr;
    }

    return m_pData.data();
}

template <typename T> quint32 TashAbstractSegmentAccessor<T>::getSegmentIndexNumber() {
    return m_cIndex;
}

}
}

//Q_DECLARE_METATYPE(tashtego::ip::TashAbstractSegmentAccessor*)

#endif //TASHABSTRACTSEGMENTACCESSOR_H


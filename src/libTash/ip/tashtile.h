/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashtile.h
 * @author Michael Flau
 * @brief
 * @details
 * @date September 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started September 2015
 */

#ifndef TASHTILE_H
#define TASHTILE_H

#define THREAD_SAFE
#define BOUNDARY_CHECK
//#define DEBUG

#include "tashabstractsegmentaccessor.h"

#include <QMap>
#include <QHash>
#include <QVarLengthArray>
#include <QDebug>
#ifdef THREAD_SAFE
#include <QMutex>
#include <QMutexLocker>
#include <QPointer>

#endif


namespace tashtego{
namespace ip{

/**
 * @brief The TashTile class template
 *
 * This template provides a description of a tile representation within
 * a multiscale sliding container. It is the very base class which has direct
 * access to the actual image data in the memory map. it can be seen as
 * simple getter, as there is not much logic contained in here. The
 * tile instance will get some meta information about the it's respective
 * position on the image along with some global information about the image dimensions,
 * it is therefore crucial, that the instance maintains a pointer to the tashtego runtime instance.
 * Having these credentials, the instance will compute it's own local lookup table to the pixel
 * data which it is representing. Once computed, the actual pixel values can be accessed
 * via getter functions or the subcript operator '[]'. That way a fast lookup is provided.
 * The actual lookup is secured by a mutex, as it may very well be that pointers to instances
 * are used parallely among a multitude of threads.
 */
template <typename T> class TashTile: public TashAbstractSegmentAccessor<T>
{

public:

    /**
     * @brief TashTile::TashTile
     * Default constructor for runtime introspection.
     * @param parent
     * Parent object of TashTile
     */
    explicit TashTile(TashAbstractSegmentAccessor<T> *parent = 0);

    /**
     * @brief TashTile::TashTile
     * Main constructor to initialize a TashTile instance.
     * An instance needs it's segment index, it's x/y start position,
     * general meta information and a pointer to the actual image data
     * to work.
     * @param id
     * instance ID
     * @param segmentIndex
     * index number within it's parental context
     * @param xStart
     * x position of the first adressable pixel
     * @param yStart
     * y position of the first addressable pixel
     * @param metaInfo
     * general meta information about all tiles of the same type
     * @param data
     * Smart pointer to the actual image data
     * @param parent
     * parent class
     *
     * @todo
     * CTor gets pointer to metainfo instance, as it will be allocated dynamically in TashMSSWImageAccessor.
     * Furthermore it must be deleted here in destructor, as parent will have no reference to it, after
     * allocating.
     * Plus, Startposition should be stored in meta info object as well!
     */
    TashTile(qint64 id,
             const quint32 segmentIndex,
             const quint32 xStart,
             const quint32 yStart,
             QSharedPointer<TashImageSegmentInfo> metaInfo,
             QSharedPointer<T> data,
             TashAbstractSegmentAccessor<T> *parent = 0);

    /**
     * @brief ~TashTile
     * Default destructor
     */
    virtual ~TashTile() = default;

    /**
     * @brief operator =
     * @param other
     * @return
     */
    TashTile<T>& operator=(const TashTile<T>& other);



    /**
     * @brief TashTile::operator []
     *
     * Convenience method to access image data via bracket operator
     *
     * @warning
     * This method is potentially unsafe as it is possible to access memory
     * out of image bounds. It is designed to be a rather fast data lookup method.
     * Therefore it lacks security and region checks which would slow down data access.
     *
     * It is possible to compile this module with a boundary check by defining <code>BOUNDARY_CHECK</code>.
     *
     * To safely and fast access the data of the tile instance, check the maximum index number
     * by calling TashTile::pixelcount() beforehand. All values within this index bound
     * are guaranteed to be part of the image tile.
     *
     * @param index
     * Indexnumber of the lookup table which represents the actual positional
     * offset of the image pointer to it's current position.
     *
     * @return
     * Image data at the respective indexposition.
     */
    virtual T& operator [](const quint32& index);

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() override;

    /**
     * @brief getSegment
     *
     * Provides same functionality as subscript operator, via method call.
     *
     * @param index
     * Indexnumber of the lookup table which represents the actual positional
     * offset of the image pointer to it's current position.
     *
     * @return
     * Image data at the respective indexposition.
     */
    virtual T& getSegment(const quint32& index);

    /**
     * @brief scanline
     * Provides a pointer to the request image line starting at tile's left hand boundary
     * @param line
     * Height index of line
     * @return
     * Pointer of type T to start address of line
     */
    virtual T* scanline(const quint32& line);

    /**
     * @brief getNumberOfRows
     * Returns the number of rows in which this specific instance of tashtile is organized
     * @return
     * Number of rows as qint32 value
     */
    virtual quint32 getNumberOfRows();

    /**
     * @brief getNumberSegmentsPerRow
     * @return
     */
    virtual quint32 getNumberSegmentsPerRow();

public slots:

    /**
     * @brief TashTile::pixelcount
     * Returns the number of pixels to which the tile instance has access to.
     * @return
     * Number of accessible pixels.
     */
    virtual quint32 pixelcount() const;

protected:
    /**
     * @brief TashTile::init
     *
     * Each TashTile instance holds a reference to a smart pointer which points
     * to the actual image data in the memroy map. For fast access an additional raw pointer is
     * provided which is used when data is accessed via the bracket reference automatism.
     * The actual data lookup is based on an internal look up table which holds all the offsets
     * for the respective part of the image which is represented by it's TashTile instance.
     * The lookup offset do not need to be recomputed at every lookup as long as the size of
     * the actual image footage does not change. Therefor it is computed here once for faster
     * access at runtime.
     *
     * Since the image data within the memory map is arranged in a 1 dimensional vector,
     * a two dimensional tile represents a multiple of dangling data chunks within the map.
     * This is considered in the computations below, as well as the fact that the correct position
     * of the desired image data must be computed with respect of the tiles relative position to the
     * whole image.
     *
     * NOTE:
     * Tileoverlap is not considered within the tiles each instance will already get it's horizontal
     * and vertical start position in respect to the overlap and is thus unaware of positions
     * of adjacent tiles.
     */
    virtual bool init() override;

    /*
     * Members
     */
    QHash<quint32, quint32> m_pixelAccessOffsetLUT;  ///> Offset lookup table for each pixel represented by the tile instance
    QVector<quint32>        m_lineAccessOffsetLUT;   ///> Offset lookup table for starting point of each line represented by the tile instance
    QSharedPointer<T>       m_pRawData;              ///> Raw pointer to image data for faster access
    quint32                 m_pixelAccessLUTSize;    ///> Size of the lookup table
    quint32                 m_lineAccessLUTSize;     ///> Size of line access LUT size
};

template <typename T>
TashTile<T>::TashTile(TashAbstractSegmentAccessor<T> *parent):
    TashAbstractSegmentAccessor<T>(parent),
    m_pixelAccessOffsetLUT(),
    m_lineAccessOffsetLUT(),
    m_pRawData(nullptr),
    m_pixelAccessLUTSize(0),
    m_lineAccessLUTSize(0){

}

template <typename T>
TashTile<T>::TashTile(qint64 id,
                                            const quint32 segmentIndex,
                                            const quint32 xStart,
                                            const quint32 yStart,
                                            QSharedPointer<TashImageSegmentInfo> metaInfo,
                                            QSharedPointer<T> data,
                                            TashAbstractSegmentAccessor<T> *parent):
    TashAbstractSegmentAccessor<T>(id,
                                   segmentIndex,
                                   xStart,
                                   yStart,
                                   metaInfo,
                                   data,
                                   parent),
    m_pixelAccessOffsetLUT(),
    m_lineAccessOffsetLUT(),
    m_pRawData(nullptr),
    m_pixelAccessLUTSize(0),
    m_lineAccessLUTSize(0){

    if ( !this->init() ) {
        qCritical()  << "Initialization of TashTile<T> instance failed!";
    }
}

template<typename T>
TashTile<T>& TashTile<T>::operator=(const TashTile<T>& other) {

    //Pixel access
    this->m_pixelAccessOffsetLUT    = other.m_pixelAccessOffsetLUT;
    this->m_pixelAccessLUTSize      = other.m_pixelAccessLUTSize;

    //Line access
    this->m_lineAccessOffsetLUT     = other.m_lineAccessOffsetLUT;
    this->m_lineAccessLUTSize       = other.m_lineAccessLUTSize;

    //Data pointer
    this->m_pRawData                = other.m_pRawData;

    //parent members
    this->m_bHasRuntimeObject       = other.m_bHasRuntimeObject;
    this->m_pRunTime                = other.m_pRunTime;
    this->m_bIsInitialized          = other.m_bIsInitialized;
    this->m_cIndex                  = other.m_cIndex;
    this->m_cXStart                 = other.m_cXStart;
    this->m_cYStart                 = other.m_cYStart;
    this->m_InstanceID              = other.m_InstanceID;
    this->m_pMetaInfo               = other.m_pMetaInfo;
    this->m_pData                   = other.m_pData;

    return *(this);
}

template <typename T>
bool TashTile<T>::init() {

    bool status =  this->m_pData.isNull();
    Q_CHECK_PTR( this->m_pData.data() );
    Q_ASSERT(!status);

    if ( status ) {
        qCritical() << "Given pointer to image data is NULL, stopping initialization!";
        this->m_bIsInitialized = !status;
        emit this->error(this->m_InstanceID);
        return this->m_bIsInitialized;
    }

    m_pRawData = this->m_pData;

    Q_ASSERT( this->m_pMetaInfo );
    status = this->m_pMetaInfo->isRuntimeAware();
    Q_ASSERT( status );

    if ( !status ) {
        qCritical() << "TashTile instance (" << this->m_InstanceID << ") needs to reference tashtego runtime instance to compute it's lookup tables!";
        this->m_bIsInitialized = status;
        emit this->error(this->m_InstanceID);
        return this->m_bIsInitialized;
    }

    const auto cImageWidth  = this->m_pMetaInfo->getImageWidth();
    const auto cImageHeight = this->m_pMetaInfo->getImageHeight();

    Q_ASSERT( this->m_cXStart < cImageWidth );
    if ( this->m_cXStart >= cImageWidth ) {
        qCritical() << "The given X starting position (" << this->m_cXStart << ") for this tile instance(" << this->m_InstanceID << ") is greater than the image's actual width (" << cImageWidth << "), abort!";
        this->m_bIsInitialized = false;
        emit this->error(this->m_InstanceID);
        return this->m_bIsInitialized;
    }

    Q_ASSERT(this->m_cYStart < cImageHeight);
    if ( this->m_cYStart >= cImageHeight ) {
        qCritical() << "The given Y starting position (" << this->m_cYStart << ") for this tile instance(" << this->m_InstanceID << ") is greater than the image's actual height (" << cImageHeight << "), abort!";
        this->m_bIsInitialized = false;
        emit this->error(this->m_InstanceID);
        return this->m_bIsInitialized;
    }

    //Index for map
    auto lutIndex           = 0;

    //Tiles's start offset
    const auto cStartOffset = (this->m_cYStart * cImageWidth) + this->m_cXStart;
    const auto tHeight      = this->m_pMetaInfo->getHeight();
    const auto tWidth       = this->m_pMetaInfo->getWidth();

    //Internal tile offset computation function
    auto compute = [&](const quint32& i, const quint32& i2) {
        return cStartOffset + (i * cImageWidth) + (this->m_cXStart + i2);
    };

    //Compute for each pixel in tile's range
    m_pixelAccessOffsetLUT.reserve(tHeight * tWidth);
    for ( auto idx = 0u; idx < tHeight; ++idx ) {
        for ( auto idx2 = 0u; idx2 < tWidth; ++idx2, ++lutIndex ) {
#ifdef DEBUG
            qDebug() << "Computed offset for tile instance(" << this->m_InstanceID << ") is " << compute(idx, idx2);
#endif
            m_pixelAccessOffsetLUT.insert(lutIndex, compute(idx, idx2));
        }
    }

    m_pixelAccessLUTSize = tHeight * tWidth;
    Q_ASSERT( static_cast<qint32>(m_pixelAccessLUTSize) == m_pixelAccessOffsetLUT.size() );

    //Compute lookup table for scanline function
    m_lineAccessOffsetLUT = QVector<quint32>(0);
    m_lineAccessOffsetLUT.reserve(tHeight);

    auto offset = 0;
    auto computeLineOffset = [&] (quint32 currY) {
        return cStartOffset + (currY * cImageWidth);
    };

    for( auto idx = 0u; idx < tHeight; ++idx ) {
        offset = computeLineOffset(idx);
        m_lineAccessOffsetLUT.insert(idx, offset);
    }

    m_lineAccessLUTSize = tHeight;
    Q_ASSERT( static_cast<qint32>(m_lineAccessLUTSize) == m_lineAccessOffsetLUT.size() );

    return (this->m_bIsInitialized = true);
}

template <typename T>
T& TashTile<T>::operator [] (const quint32& index) {
#ifdef THREAD_SAFE
    QMutex mutex;
    QMutexLocker locker(&mutex);
#endif

#ifdef BOUNDARY_CHECK
    Q_ASSERT( static_cast<qint32>(m_pixelAccessLUTSize) == m_pixelAccessOffsetLUT.size() );
#endif

    T* p = m_pRawData.data();

    return *(p + m_pixelAccessOffsetLUT[index]);
}

template <typename T>
QString TashTile<T>::getClassName() {
    return QString("TashTile");
}


template <typename T>
T& TashTile<T>::getSegment(const quint32& index) {
    return this->operator [](index);
}

template <typename T>
quint32 TashTile<T>::pixelcount() const {
    //Must always be true
    Q_ASSERT( static_cast<qint32>(m_pixelAccessLUTSize) == m_pixelAccessOffsetLUT.size());
    return m_pixelAccessLUTSize;
}

template <typename T>
T* TashTile<T>::scanline(const quint32& index) {
#ifdef THREAD_SAFE
    QMutex mutex;
    QMutexLocker locker(&mutex);
#endif

#ifdef BOUNDARY_CHECK
    Q_ASSERT( static_cast<qint32>(m_lineAccessLUTSize) == m_lineAccessOffsetLUT.size() );
    Q_ASSERT( index < m_lineAccessLUTSize );
#endif
    return m_pRawData.data() + m_lineAccessOffsetLUT.at(index);
}

template <typename T>
quint32 TashTile<T>::getNumberOfRows() {
    Q_CHECK_PTR(this->m_pMetaInfo);
    return this->m_pMetaInfo->getNumberOfSegmentRows();
}

template <typename T>
quint32 TashTile<T>::getNumberSegmentsPerRow() {
    Q_CHECK_PTR(this->m_pMetaInfo);
    return this->m_pMetaInfo->getNumberOfChildSegmentsPerRow();
}

}
}

#endif //TASHTILE_H


/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashmsswprocessorbuilder.h
 * @author Michael Flau
 * @brief
 * @details
 * @date November 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Schwerin - Germany, Started November 2015
 */

#ifndef TASHMSSWPROCESSORBUILDER_H
#define TASHMSSWPROCESSORBUILDER_H

#include "../core/tashobject.h"
#include "../core/tashruntime.h"

#include <QMap>

//Qt class forward declarations
class QStringList;

//@cond DOXYGEN_IGNORE
template <class T> class QPointer;
//@endcond

namespace tashtego{
namespace ip{

//Tashtego class forward declarations
template <typename T> class TashMSSWAccessor;
class TashMSSWProcessorInterface;
class TashAbstractProcessor;

/**
 * @brief The TashMSSWProcessorBuilder class
 */
class TashMSSWProcessorBuilder: public core::TashObject
{
    Q_OBJECT

public:

	/**
	 * @brief TashMSSWProcessorBuilder
	 * @param parent
	 * @warning
	 * This header is only for meant to be used for introspection discovery, do not call manually!
	 */
	explicit TashMSSWProcessorBuilder(TashObject *parent = 0);

    /**
     * @brief TashMSSWProcessorBuilder
     * @param id
     * @param runtime
     * @param camIdx
     * @param accList
     * @param parent
     */
    TashMSSWProcessorBuilder(qint64 id,
                             const QPointer<core::TashRuntime> runtime,
                             quint32 camIdx,
                             QMap<qint32, QPointer<TashMSSWAccessor<qint16>>> &accList,
                             TashObject *parent = 0);

	/**
	 * @brief ~TashMSSWProcessorBuilder
	 */
    virtual ~TashMSSWProcessorBuilder() = default;

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() override;

public slots:

    /**
     * @brief build
     * @param processorname
     * @return
     */
    virtual QPointer<TashMSSWProcessorInterface> build( const QString& processorname );

    /**
     * @brief listBuildables
     * @return
     */
    virtual QStringList listBuildables();

protected:

    /**
     * @brief discoverBuildable
     * @return
     */
    virtual bool discoverBuildables();

private:

    /*
     * Member
     */
    quint32                                  m_camIdx;       ///> Camera Index
    QMap<qint32, QPointer<TashMSSWAccessor<qint16>>> m_accessorList; ///> Accessor List
    QStringList                              m_buildables;   ///> List of createable processor modules
};

}
}

Q_DECLARE_METATYPE(tashtego::ip::TashMSSWProcessorBuilder)

#endif //TASHMSSWPROCESSORBUILDER_H


/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashmsswprocessorbuilder.cpp
 * @author Michael Flau
 * @brief
 * @details
 * @date November 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Schwerin - Germany, Started November 2015
 */

#include "tashmsswprocessorbuilder.h"

#include "tashmsswaccessor.h"
#include "tashabstractprocessor.h"
#include "tashmsswprocessorinterface.h"
#include "tashabstractprocessorbuilder.h"

#include <QPointer>
#include <QStringList>
#include <QMetaType>
#include <QRegularExpression>

#define INTERNAL_BUILDER

namespace tashtego{
namespace ip{

TashMSSWProcessorBuilder::TashMSSWProcessorBuilder(tashtego::core::TashObject *parent):
    tashtego::core::TashObject(parent)
{

}

TashMSSWProcessorBuilder::TashMSSWProcessorBuilder(qint64 id,
                                                   const QPointer<core::TashRuntime> runtime,
                                                   quint32 camIdx,
                                                   QMap<qint32, QPointer<TashMSSWAccessor<qint16>> > &accList,
                                                   TashObject *parent):
    tashtego::core::TashObject(id, runtime, parent),
    m_camIdx(camIdx),
    m_accessorList(accList)
{

}

QString TashMSSWProcessorBuilder::getClassName() {
    return QString("TashMSSWProcessorBuilder");
}

QPointer<TashMSSWProcessorInterface> TashMSSWProcessorBuilder::build( const QString& processorname ) {

    bool status = processorname.isEmpty();
    Q_ASSERT(!status);

    if ( status ) {
        qCritical() << "Can't instantiate a processor type when describing string is empty!";
        return QPointer<TashMSSWProcessorInterface>(nullptr);
    }

#ifndef INTERNAL_BUILDER

    int id = QMetaType::type(processorname.toStdString().c_str());

    if ( id == QMetaType::UnknownType ) {
        qCritical() << "Error creating a valid processor instance! There is no processor with such a name!";
        return QPointer<TashMSSWProcessorInterface>(nullptr);
    }

    TashAbstractProcessor* processor = static_cast<TashAbstractProcessor*>( QMetaType::create(id) );

    Q_CHECK_PTR(processor);


#else

    status = this->isRuntimeAware();

    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "Error,  builder needs runtime, to work properly!";
        return QPointer<TashMSSWProcessorInterface>(nullptr);
    }

    qDebug() << "Searching for builder instance in registry!";

    if ( !m_pRunTime->objectRegistry().contains("imageProcessors/" + processorname) ) {
        qCritical() << "Error,  builder cannot find processor with such a name!";
        return QPointer<TashMSSWProcessorInterface>(nullptr);
    }

    TashAbstractProcessorBuilder* procBuilder = dynamic_cast<TashAbstractProcessorBuilder*>(m_pRunTime->objectRegistry().value("imageProcessors/" + processorname));

    Q_CHECK_PTR(procBuilder);

    if ( !procBuilder ) {
        qCritical() << "Error, object registry returned invalid builder instance!";
        return QPointer<TashMSSWProcessorInterface>(nullptr);
    }

    qDebug() << "Apparently there is an instance, will now create processor of type: " << processorname;

    TashAbstractProcessor* processor = procBuilder->newProcessor();

    Q_CHECK_PTR(processor);

    if ( !processor ) {
        qCritical() << "Error, invalid processor instance built by generated builder!";
        return QPointer<TashMSSWProcessorInterface>(nullptr);
    }

    qDebug() << "Got a running processor instance at hand...start initialization!";

#endif

    status = processor->setRuntime(m_pRunTime);
    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "Error while passing runtime to created processor instance!";
        return QPointer<TashMSSWProcessorInterface>(nullptr);
    }

    qDebug() << "Success passing runtime to created processor worked!";

//    status = processor->parseRuntimeForProperties();
    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "Error while parsing runtime for properties of processor of type " << processorname;
        return QPointer<TashMSSWProcessorInterface>(nullptr);
    }

    qDebug() << "Setting camera index and input buffer.";

    processor->setCameraIndex(m_camIdx);
    processor->setAccessorBuffer(m_accessorList);
//    processor->init();

    TashMSSWProcessorInterface* interface = new TashMSSWProcessorInterface(core::TashRuntime::genID(),
                                                                           m_pRunTime,
                                                                           processor);
    return QPointer<TashMSSWProcessorInterface>(interface);
}

QStringList TashMSSWProcessorBuilder::listBuildables() {
    return m_buildables;
}

bool TashMSSWProcessorBuilder::discoverBuildables() {

    QRegularExpression regex(".+(P|p)rocessor(M|m)odule");
    TashObject mybase(core::TashRuntime::genID(), m_pRunTime);
    QList<TashAbstractProcessor*> processors_avail = mybase.findChildren<TashAbstractProcessor*>(regex);

    foreach(TashAbstractProcessor* pr, processors_avail) {
        qDebug() << "Found new processor module: " << pr->metaObject()->className();
        m_buildables.append(pr->metaObject()->className());
    }

    return true;
}

REGISTER_METATYPE(TashMSSWProcessorBuilder, "TashMSSWProcessorBuilder")
}
}

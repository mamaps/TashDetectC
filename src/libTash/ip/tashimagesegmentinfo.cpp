/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashimagesegmentinfo.cpp
 * @author Michael Flau
 * @brief
 * @details
 * @date September 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started September 2015
 */

#include "tashimagesegmentinfo.h"
#include "core/tashruntime.h"
#include <QPointer>
#include <QDebug>


namespace tashtego{
namespace ip{

TashImageSegmentInfo::TashImageSegmentInfo(tashtego::core::TashObject *parent):
    tashtego::core::TashObject(parent),
    m_width(0),
    m_height(0),
    m_horzOverlap(0),
    m_vertOverlap(0),
    m_childSegments(0),
    m_segmentRows(0),
    m_dataSourceIdx(0),
    m_childSegmentsPerRow(0)
	{

}

TashImageSegmentInfo::TashImageSegmentInfo(qint64 id,
                                           quint8 dataSourceIndex,
                                           quint32 width,
                                           quint32 height,
                                           quint32 hOverlap,
                                           quint32 vOverlap,
                                           quint32 segmentCount,
                                           quint32 segmentsPerRow,
                                           quint32 segmentRows,
                                           tashtego::core::TashObject *parent):
    tashtego::core::TashObject(id, parent),
    m_width(width),
    m_height(height),
    m_horzOverlap(hOverlap),
    m_vertOverlap(vOverlap),
    m_childSegments(segmentCount),
    m_segmentRows(segmentRows),
    m_dataSourceIdx(dataSourceIndex),
    m_childSegmentsPerRow(segmentsPerRow)
	{

}

QString TashImageSegmentInfo::getClassName() {
    return QString("TashImageSegmentInfo");
}

TashImageSegmentInfo& TashImageSegmentInfo::operator=(const TashImageSegmentInfo& other) {
    this->m_width             = other.m_width;
    this->m_height            = other.m_height;
    this->m_horzOverlap       = other.m_horzOverlap;
    this->m_vertOverlap       = other.m_vertOverlap;
    this->m_childSegments     = other.m_childSegments;
    this->m_segmentRows       = other.m_segmentRows;
    this->m_dataSourceIdx     = other.m_dataSourceIdx;
    this->m_bIsInitialized    = other.m_bIsInitialized;
    this->m_bHasRuntimeObject = other.m_bHasRuntimeObject;
    this->m_pRunTime          = other.m_pRunTime;

    return *(this);
}

quint32 TashImageSegmentInfo::getWidth() const {
    return m_width;
}

quint32 TashImageSegmentInfo::getHeight() const {
    return m_height;
}

quint32 TashImageSegmentInfo::getHorizontalOverlap() const {
    return m_horzOverlap;
}

quint32 TashImageSegmentInfo::getVerticalOverlap() const {
    return m_vertOverlap;
}

quint32 TashImageSegmentInfo::getNumberChildSegments() const {
    return m_childSegments;
}

quint32 TashImageSegmentInfo::getNumberOfSegmentRows() const {
    return m_segmentRows;
}


quint32 TashImageSegmentInfo::getNumberOfChildSegmentsPerRow() const {
    return m_childSegmentsPerRow;
}

quint8 TashImageSegmentInfo::getDataSourceIndex() const {
    return m_dataSourceIdx;
}

quint32 TashImageSegmentInfo::getImageHeight() const {

    if ( !this->isRuntimeAware() ) {
        qCritical() << "This instance has no runtime object set! Return 0!";
        return 0;
    }

    QVariant tmp = m_pRunTime->property( m_pRunTime->getPropertyNameContaining( TASHIMAGE_HEIGHTID, m_dataSourceIdx));
    return tmp.isValid() ? tmp.toUInt() : 0;
}

quint32 TashImageSegmentInfo::getImageWidth() const {

    if ( !this->isRuntimeAware() ) {
        qCritical() << "This instance has no runtime object set! Return 0!";
        return 0;
    }

    QVariant tmp = m_pRunTime->property( m_pRunTime->getPropertyNameContaining( TASHIMAGE_WIDTHID, m_dataSourceIdx));
    return tmp.isValid() ? tmp.toUInt() : 0;
}

REGISTER_METATYPE(TashImageSegmentInfo, "TashImageSegmentInfo")
}
}

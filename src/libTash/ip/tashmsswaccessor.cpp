/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashmsswaccessor.cpp
 * @author Michael Flau
 * @date October 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started October 2015
 */

//header
#include "tashmsswaccessor.h"

//Qt
#include <QSharedPointer>
#include <QMap>
#include <QPoint>
#include <QString>

namespace tashtego{
namespace ip{

TashMSSWAccessor::TashMSSWAccessor(tashtego::core::TashObject *parent):
    tashtego::core::TashObject(parent),
    m_bUseTiling(true),
    m_cameraIdx(0)
{
    setDefaultProperties();
}

TashMSSWAccessor::TashMSSWAccessor(qint64 id,
                                   quint32 cameraIdx,
//                                   const QPointer<core::TashRuntime> runtime,
                                   const tashtegoRuntime runtime,
                                   const TashImage<T> &data,
                                   QMutex *initMutex,
                                   TashObject *parent):

    tashtego::core::TashObject(id, runtime, parent),
    m_bUseTiling(true),
    m_cameraIdx(cameraIdx),
    m_RootData(data),
    m_Rois(QMap<quint32, RegionOfInterest<qint16>*>()),
    m_pInitMutex(initMutex)
{
    if ( !runtime->configFileExists() ) {
#ifdef TASHVERBOSE
        qDebug() << "No inifile, set default properies";
#endif
        setDefaultProperties();
    } else {
#ifdef TASHVERBOSE
        qDebug() << "Found ini file, start parsing propertiers from ini file!";
#endif
        init();
    }
}

bool TashMSSWAccessor::init() {

    bool status = this->isRuntimeAware();
    static auto uniqueTileID = 0;

    Q_ASSERT(status);

    if ( !status ) {
        m_bHasRuntimeObject = m_bIsInitialized = false;
        qCritical() << "A reference to the runtime object is required to initialize this module!";
        return m_bIsInitialized;
    }

    status = m_pRunTime.isNull();
    Q_ASSERT(!status);

    if ( status ) {
        m_bHasRuntimeObject = m_bIsInitialized = false;
        qCritical() << "A reference to the runtime object is required to initialize this module!";
        return m_bIsInitialized;
    }

    status = m_pRunTime->isInitialized();
    Q_ASSERT(status);

    if ( !status ) {
        m_bHasRuntimeObject = m_bIsInitialized = false;
        qCritical() << "Reference to runtime object points to an instance which is not initialized!";
        return m_bIsInitialized;
    }

    //Check Image pointer for validity
    Q_CHECK_PTR(m_RootData.data());

    status = m_RootData.isNull();
    Q_ASSERT(!status);

    if ( status ) {
        qCritical() << "Invalid reference to image data passed to TashMSSWAccessor instance, abort!";
        return (m_bIsInitialized = false);
    }

    QVector<struct roiDescriptor> roiPropertyValues;

    //Parse properties from ini-file
    status = parseRuntimeForProperties(roiPropertyValues);

    Q_ASSERT(status);
    Q_ASSERT(!roiPropertyValues.isEmpty());

    if ( !status || roiPropertyValues.isEmpty() ) {
        m_bIsInitialized = false;
        qCritical() << "Error while reading values from property list!";
        return m_bIsInitialized;
    }

    QMap<quint32, TashImageSegmentInfo*> roiMetaInfo;
    QMap<quint32, QPoint>                roiPositions;

    //Compute all ROI positions
    status = computeROIPositions(roiPropertyValues, roiMetaInfo, roiPositions);

    Q_ASSERT( status );
    Q_ASSERT( (roiMetaInfo.size() == roiPositions.size()) == roiPropertyValues.length() );

    QVectorIterator<struct roiDescriptor> roiIt(roiPropertyValues);

    auto tashidseed = core::TashRuntime::genIDPara(m_pInitMutex);
    //foreach detected region of interest
    while( roiIt.hasNext() ) {

        auto currentROI = roiIt.next();
        auto *pRoi      = new RegionOfInterest<qint16> (core::TashRuntime::genIDPara(m_pInitMutex, tashidseed),
                                                        currentROI.index,
                                                        roiPositions[currentROI.index].x(),
                                                        roiPositions[currentROI.index].y(),
                                                        roiMetaInfo[currentROI.index],
                                                        m_RootData.dataTyped());

        QVector<TashImageSegmentInfo*> rangeMetaObjects;
        QMap<quint32, QPoint>          trPositions;

        status = computeTileRangePositions(currentROI, rangeMetaObjects, trPositions);

        Q_ASSERT( status );
        Q_ASSERT( rangeMetaObjects.size() == trPositions.size() );

        if ( rangeMetaObjects.size() != trPositions.size() ) {

            qCritical() << "The number of created metaobjects does not concise with the number of computed range positions!";

            for ( auto it = rangeMetaObjects.begin(); it != rangeMetaObjects.end(); ++it ) {

                Q_CHECK_PTR(it);

                if ( it ) {
                    delete it;
                }
            }

            return m_bIsInitialized;
        }

        Q_ASSERT( currentROI.tileranges.length() == trPositions.size() );

        if ( !status ) {
            m_bIsInitialized = false;
            qCritical() << "Error while computing tilerange positions! Abort!";
            return m_bIsInitialized;
        }

        for ( auto it = rangeMetaObjects.begin(); it != rangeMetaObjects.end(); ++it ) {

            Q_CHECK_PTR( it );

            if ( !it ) {
                m_bIsInitialized = false;
                qCritical() << "Returned pointer to allocated meta info structure about tileranges is invalid";
                return m_bIsInitialized;
            }
        }

        QVectorIterator<struct tilerangeDescriptor> trIt(currentROI.tileranges);

        QHash<quint32, TileRange<qint16>*> currTilerangeList;
        currTilerangeList.reserve(currentROI.tileranges.size());

        //foreach tilerange in region of interest
        while( trIt.hasNext() ) {

            auto currentTR = trIt.next();

            currTilerangeList[currentTR.index] = new TileRange<qint16> (core::TashRuntime::genIDPara(m_pInitMutex, tashidseed),
                                                                        currentTR.index * currentROI.index,
                                                                        trPositions[currentTR.index].x(),
                                                                        trPositions[currentTR.index].y(),
                                                                        rangeMetaObjects[currentTR.index],
                                                                        m_RootData.dataTyped());

            TashImageSegmentInfo* pTileMeta = nullptr;
            QMap<quint32, QPoint> tilePositions;

            currentTR.startPos = trPositions[currentTR.index];
            status             = computeTilePositions(currentTR.rangewidth,
                                                      currentTR.rangeheight,
                                                      currentTR,
                                                      &pTileMeta,
                                                      tilePositions);

            Q_ASSERT( status );
            Q_CHECK_PTR( pTileMeta );
            Q_ASSERT( !tilePositions.isEmpty() );

            /**
             * @todo If anything goes wrong here, instances in pTrMetaObjects must be deleted,
             * which have not been passed to a Segment accessor instance...it is better to switch to
             * smart pointers!
             */

            if ( !status ) {
                m_bIsInitialized = false;
                qCritical() << "Error while computing tilerange positions! Abort!";
                return m_bIsInitialized;
            }

            if ( tilePositions.isEmpty() ) {
                qWarning() << "No tile positions computed, is tiling disabled?";
                continue;
            }

            QHash<quint32, Tile<qint16>*> currTileList;
            currTileList.reserve(tilePositions.size());
            quint32 tileIdx = 0;

            //For each computed tile position
            for( auto tileIt = tilePositions.begin(); tileIt != tilePositions.end(); ++tileIt, ++tileIdx, ++uniqueTileID ) {

                currTileList.insert( tileIdx, qMove(new Tile<qint16>(core::TashRuntime::genIDPara(m_pInitMutex, tashidseed),
                                                                     uniqueTileID,
                                                                     tileIt->x(),
                                                                     tileIt->y(),
                                                                     pTileMeta,
                                                                     m_RootData.dataTyped() ) ) );
            }
///DEBUG
            if ( currTileList.isEmpty() )
                qDebug() << "Found empty tile list!";
///DEBUG
            currTilerangeList[currentTR.index]->setChildSegments(qMove(currTileList));
        }
///DEBUG
        if ( currTilerangeList.isEmpty() )
            qDebug() << "Found empty tilerange list!";
///DEBUG
        pRoi->setChildSegments(qMove(currTilerangeList));
        m_Rois[currentROI.index] = pRoi;
    }

    //Store parsed value for later usage in getters
    uniqueTileID = 0;
    m_roiInfo    = roiPropertyValues;

    Q_ASSERT(status);
    return (m_bIsInitialized = status);
}

QSharedPointer<RegionOfInterest<T> > TashMSSWAccessor::operator [](const quint32& idx) {

    if ( !this->isInitialized() ) {
        qCritical() << "Warning, MSSW accessor instance was never initialized! Call TashMSSWAccessor::init or use dedicated constructor!";
        return QSharedPointer<TashMSSWAccessor::RegionOfInterest<qint16>>();
    }

    return QSharedPointer<TashMSSWAccessor::RegionOfInterest<qint16>>(m_Rois[idx], TashMSSWAccessor::releaseDummy );
}

Tile<T> &TashMSSWAccessor::getTile(const quint32& roiindex,
                                                  const quint32& rangeindex,
                                                  const quint32& tileindex) {
    return (*m_Rois[roiindex])[rangeindex][tileindex];

}

Tile<T> *TashMSSWAccessor::getTilePointer(const quint32& roiindex,
                             const quint32& rangeindex,
                             const quint32& tileindex) {
    return m_Rois[roiindex]->getSegmentPointer(rangeindex)->getSegmentPointer(tileindex);
}

TileRange<T> &TashMSSWAccessor::getTileRange(const quint32& roiindex, const quint32& rangeindex){
    return (*m_Rois[roiindex])[rangeindex];
}

RegionOfInterest<T> &TashMSSWAccessor::getRegionOfInterest(const quint32& roiindex){
    return *m_Rois[roiindex];
}

QHash<quint32, TashMSSWAccessor::Tile<T> *> TashMSSWAccessor::getTileSubset(const quint32& roiindex,
                                           const quint32& rangeindex,
                                           const quint32& start,
                                           const quint32& end) {
    return getTileRange(roiindex,rangeindex).getSegmentPointerRange(start, end);

}

qint32 TashMSSWAccessor::getNumberOfROIs() {
    return m_roiInfo.length();
}

qint32 TashMSSWAccessor::getNumberOfTRs( const quint32 &roiIdx ) {

    auto rois = this->getNumberOfROIs();

    if ( roiIdx >= static_cast<quint32>(rois) ) {
        qWarning() << "Error, passed ROI index is to high, MSSW accessor has only " << rois << " region of interest defined! Note that indeces start with 0!";
        return -1;
    }

    return m_roiInfo.at(roiIdx).tileranges.length();
}

qint32 TashMSSWAccessor::getNumberOfTiles( const quint32 &roiIdx, const quint32 &trIdx ) {

    auto rois = this->getNumberOfROIs();

    if ( roiIdx >= static_cast<quint32>(rois) ) {
        qWarning() << "Error, passed ROI index is to high, MSSW accessor has only " << rois << " region of interest defined! Note that indeces start with 0!";
        return -1;
    }

    auto trs = this->getNumberOfTRs(roiIdx);

    if ( trIdx >= static_cast<quint32>(trs) ) {
        qWarning() << "Error, passed tilerange index is to high, ROI with index " << roiIdx << " in MSSW accessor instance has only " << trs << " tileranges defined! Note that indeces start with 0!";
        return -1;
    }

    return m_Rois[roiIdx]->operator [](trIdx).childSegments();
}

/**
 * @todo tilerange heights should be of variable size
 */
bool TashMSSWAccessor::setDefaultProperties() {

    //Set default properties which are required by this module
    //to function correctly

    QString groupDelimiter = "#";
    auto genROIentry = [&] (const QString& group, const QString& field) {return QString("%1%2%3").arg(group).arg(groupDelimiter).arg(field).toStdString().c_str();};

    //MSSW general settings
    this->setProperty("use_tiling", true);
    this->setProperty("number_of_rois", 1);

    //Region of interest settings
    this->setProperty( genROIentry("ROI_0", "width"), 400);
    this->setProperty( genROIentry("ROI_0", "height"), 300);
    this->setProperty( genROIentry("ROI_0", "x"), 200);
    this->setProperty( genROIentry("ROI_0", "y"), 50);
    this->setProperty( genROIentry("ROI_0", "number_trs"), 3);

    //Write 2 explanatory ranges
    for (auto idx = 0; idx < 2; ++idx) {
        this->setProperty( genROIentry(QString("ROI_0#Tilerange_%1").arg(idx), "range_start_x"),       idx);
        this->setProperty( genROIentry(QString("ROI_0#Tilerange_%1").arg(idx), "range_start_y"),       idx);
        this->setProperty( genROIentry(QString("ROI_0#Tilerange_%1").arg(idx), "range_height"),        (idx+1)*100);
        this->setProperty( genROIentry(QString("ROI_0#Tilerange_%1").arg(idx), "range_width"),         1000);
        this->setProperty( genROIentry(QString("ROI_0#Tilerange_%1").arg(idx), "tile_width"),          (idx+1)*11);
        this->setProperty( genROIentry(QString("ROI_0#Tilerange_%1").arg(idx), "tile_height"),         (idx+1)*11);
        this->setProperty( genROIentry(QString("ROI_0#Tilerange_%1").arg(idx), "tile_overlap_x"),      (idx+1)*5);
        this->setProperty( genROIentry(QString("ROI_0#Tilerange_%1").arg(idx), "tile_overlap_y"),      (idx+1)*5);
        this->setProperty( genROIentry(QString("ROI_0#Tilerange_%1").arg(idx), "mind_unfitting_end"),  false);
    }

    return true;
}

bool TashMSSWAccessor::parseRuntimeForProperties(QVector<struct roiDescriptor> &rois) {

    rois.clear();

    QString classname = getClassName(); //QString(this->metaObject()->className()).split("::").last();
    QStringList list  = m_pRunTime->getPropertyNamesContaining( classname, m_cameraIdx);

    //Get number of ROIs, number of TRs and whether to apply tiling
    auto numTRs  = 0u;
    auto numROIs = 0u;
    foreach( QString str, list) {
#ifdef TASHVERBOSE
        qDebug() << "Next property is: " << str;
#endif

        QStringList strparts = str.split("-");

        if ( strparts.last() == "number_of_rois" ) {
            numROIs = m_pRunTime->property(str).toInt();
            continue;
        }

        if ( strparts.last() == "use_tiling" ) {
            m_bUseTiling = m_pRunTime->property(str).toBool();
            continue;
        }
    }

    //ROIwise property extraction
    for ( auto idx = 0u ; idx < numROIs; ++idx ) {

        struct roiDescriptor currentROIDescriptor = {
                    idx,
                    0u,
                    0u,
                    QPoint(0,0),
                    0u,
                    QVector<struct tilerangeDescriptor>()
        };

        foreach( QString str, list) {

            QStringList strparts  = str.split("-");
            QString roiProperty   = strparts.takeLast();
            QString roiIdentifier = strparts.takeLast();

            //First check for correct ROI
            if ( roiIdentifier == QString("ROI_%1").arg(idx) ) {

                if ( roiProperty == "width" ) {
                    currentROIDescriptor.width = m_pRunTime->property( str ).toInt();
                    continue;
                }

                if ( roiProperty == "height" ) {
                    currentROIDescriptor.height = m_pRunTime->property( str ).toInt();
                    continue;
                }

                if ( roiProperty == "x" ) {
                    currentROIDescriptor.startPos.setX( m_pRunTime->property( str ).toInt() );
                    continue;
                }

                if ( roiProperty == "y" ) {
                    currentROIDescriptor.startPos.setY( m_pRunTime->property( str ).toInt() );
                    continue;
                }

                if ( roiProperty == "number_trs" ) {
                    currentROIDescriptor.numberTRs = m_pRunTime->property( str ).toInt();
                    numTRs = currentROIDescriptor.numberTRs;
                    currentROIDescriptor.tileranges.resize(currentROIDescriptor.numberTRs);
                    continue;
                }
            }
        }

        //For each tilerange in roi
        for ( auto idx2 = 0u; idx2 < numTRs; ++idx2 ) {

            struct tilerangeDescriptor currentTileRange = {
                        idx2,
                        idx,
                        0u,
                        0u,
                        0u,
                        0u,
                        0u,
                        0u,
                        QPoint(0,0),
                        false
            };

            foreach( QString str, list ) {

                QStringList strparts = str.split("-");

                QString trProperty    = strparts.takeLast();
                QString trIdentifier  = strparts.takeLast();
                QString roiIdentifier = strparts.takeLast();

                if ( (roiIdentifier == QString("ROI_%1").arg(idx)) && (trIdentifier == QString("Tilerange_%1").arg(idx2)) ) {

                    if ( trProperty == "tile_height" ) {
                        currentTileRange.tileheight = m_pRunTime->property( str ).toInt();
                        continue;
                    }

                    if ( trProperty == "tile_width" ) {
                        currentTileRange.tilewidth = m_pRunTime->property( str ).toInt();
                        continue;
                    }

                    if ( trProperty == "range_height" ) {
                        currentTileRange.rangeheight = m_pRunTime->property( str ).toInt();
                        continue;
                    }

                    if ( trProperty == "range_width" ) {
                        currentTileRange.rangewidth = m_pRunTime->property( str ).toInt();
                        continue;
                    }

                    if ( trProperty == "tile_overlap_x" ) {
                        currentTileRange.tileovlp_x = m_pRunTime->property( str ).toInt();
                        continue;
                    }

                    if ( trProperty == "tile_overlap_y" ) {
                        currentTileRange.tileovlp_y = m_pRunTime->property( str ).toInt();
                        continue;
                    }

                    if ( trProperty == "range_start_x" ) {
                        currentTileRange.startPos.setX( m_pRunTime->property( str ).toInt() );
                        continue;
                    }

                    if ( trProperty == "range_start_y" ) {
                        currentTileRange.startPos.setY( m_pRunTime->property( str ).toInt() );
                        continue;
                    }

                    if ( trProperty == "mind_unfitting_end" ) {
                        currentTileRange.mindUnfitting = m_pRunTime->property( str ).toBool();
                        continue;
                    }
                }
            }
            currentROIDescriptor.tileranges[idx2] = currentTileRange;
        }
        rois.append(currentROIDescriptor);
    }
    return true;
}

bool TashMSSWAccessor::computeTilePositions(const quint32 &trwidth,
                                            const quint32 &trheight,
                                            const struct tilerangeDescriptor& desc,
                                            TashImageSegmentInfo **pInfoMetaObject,
                                            QMap<quint32, QPoint> &tilePositionList) {

#ifdef TASHVERBOSE
    auto prnt = qDebug();
#endif

    //Do value checks
    if( !this->isRuntimeAware() ) {
        qCritical() << "TashMSSWAccessor instance need reference to runtime to work correctly, abort!";
        return false;
    }

    const auto cStartPos               = desc.startPos;
    const auto cRangeWidth             = trwidth;
    const auto cRangeHeight            = trheight;
    const auto cAbsWidth               = desc.tilewidth;
    const auto cAbsHeight              = desc.tileheight;
    const auto cOverlap_X              = desc.tileovlp_x;
    const auto cOverlap_Y              = desc.tileovlp_y;

#ifdef TASHVERBOSE
    prnt << "Prospective Tiling information:\n";
    prnt << "TR width: " << trwidth << "\n";
    prnt << "TR height: " << trheight << "\n";
    prnt << "Abs. width: "  << cAbsWidth  << "\n";
    prnt << "Abs. height: " << cAbsHeight << "\n";
    prnt << "Ovlp.-X: "     << cOverlap_X << "\n";
    prnt << "Ovlp.-Y: "     << cOverlap_Y << "\n";
#endif

    Q_ASSERT(cAbsWidth  > cOverlap_X);
    Q_ASSERT(cAbsHeight > cOverlap_Y);
    const auto cRelWidth  = cAbsWidth  - cOverlap_X;
    const auto cRelHeight = cAbsHeight - cOverlap_Y;

#ifdef TASHVERBOSE
    prnt << "Rel. width: " << cRelWidth << "\n";
    prnt << "Rel. height: " << cRelHeight << "\n";
#endif

    Q_ASSERT( (cRelWidth > 0) );
    Q_ASSERT( (cRelHeight > 0) );

    const auto cConsiderRemaniningEnd = desc.mindUnfitting;

    //Get height and width of image
    const auto cImageWidth  = m_pRunTime->property( m_pRunTime->getPropertyNameContaining( TASHIMAGE_WIDTHID, m_cameraIdx) ).toUInt();
    const auto cImageHeight = m_pRunTime->property( m_pRunTime->getPropertyNameContaining( TASHIMAGE_HEIGHTID, m_cameraIdx) ).toUInt();
    const auto cStartX      = static_cast<quint32>(cStartPos.x());
    const auto cStartY      = static_cast<quint32>(cStartPos.y());

#ifdef TASHVERBOSE
    prnt << "Img. height: " << cImageHeight << "\n";
    prnt << "Img. width: "  << cImageWidth  << "\n";
    prnt << "Start-X Pos.: "<< cStartX << "\n";
    prnt << "Start-Y Pos.: "<< cStartY << "\n";
#endif

    Q_ASSERT( cStartX < cImageWidth );

    if ( cStartX > cImageWidth ) {
        qCritical() << "Dimension mismatch: Startpos_x >> " << cStartX << " vs. Image width >> " << cImageWidth;
        return false;
    }

    Q_ASSERT( cStartY < cImageHeight );

    if ( cStartY > cImageHeight ) {
        qCritical() << "Dimension mismatch: Startpos_y >> " << cStartY << " vs. Image height >> " << cImageHeight;
        return false;
    }

    Q_ASSERT((cAbsWidth <= cRangeWidth) && (cRangeWidth <= cImageWidth) && (cAbsWidth <= cImageWidth));

    if ( (cAbsWidth > cRangeWidth) || (cRangeWidth > cImageWidth) || (cAbsWidth > cImageWidth) ) {
        qCritical() << "Dimension mismatch: tile width >> " << cAbsWidth << " vs. range width >> " << cRangeWidth << " vs. image width >> " << cImageWidth;
        return false;
    }

    Q_ASSERT((cAbsHeight <= cRangeHeight) && (cRangeHeight <= cImageHeight) && (cAbsHeight <= cImageHeight));

    if ( (cAbsHeight > cRangeHeight) || (cRangeHeight > cImageHeight) || (cAbsHeight > cImageHeight) ) {
        qCritical() << "Dimension mismatch: tile height >> " << cAbsHeight << " vs. range height >> " << cRangeHeight << " vs. image height >> " << cImageHeight;
        return false;
    }

    Q_ASSERT( cOverlap_X < cAbsWidth );

    if ( cOverlap_X >= cAbsWidth ) {
        qWarning() << "Overlap warning: horizontal overlap in px >> " << cOverlap_X << " vs. tile width >> " << cAbsWidth;
    }

    Q_ASSERT( cOverlap_Y < cAbsHeight );

    if ( cOverlap_Y >= cAbsHeight ) {
        qWarning() << "Overlap warning: vertical overlap in px >> " << cOverlap_Y << " vs. tile height >> " << cAbsHeight;
    }

    //Calculate number of tiles in x and y direction
    const qint32 cNumTilesX = ((cRangeWidth  - (cRangeWidth  % cRelWidth))  / cRelWidth);
    const qint32 cNumTilesY = ((cRangeHeight - (cRangeHeight % cRelHeight)) / cRelHeight);

#ifdef TASHVERBOSE
    prnt << "Num. tiles in X-direction: " << cNumTilesX << "\n";
    prnt << "Num. tiles in Y-direction: " << cNumTilesY << "\n";
    prnt << "Num. tiles: " << cNumTilesX*cNumTilesY << "\n";
#endif

    if ( !cConsiderRemaniningEnd ) {
        Q_ASSERT( cRangeHeight >= (cNumTilesY * cRelHeight) );
        Q_ASSERT( cRangeWidth  >= (cNumTilesX * cRelWidth) );
    } else {
#ifdef TASHVERBOSE
    prnt << "Unfitting ends will be considered!";
#endif
    }

    auto x(0);
    auto y(0);
    auto tileindex(0);

    /**
     * @todo Consider switching to QVectors here for speedup in initialization stage
     */
    for ( auto idx_h = 0; idx_h < cNumTilesY; ++idx_h ) {

        //Fit last row to range height if enabled
        if ( cConsiderRemaniningEnd && (idx_h == (cNumTilesY-1)) ) {
            y = cStartY + (cRangeHeight - cAbsHeight);
        } else {
            y = cStartY + (idx_h * cRelHeight);
        }

        auto idx_w = 0;
        for ( ; idx_w < cNumTilesX; ++idx_w, ++tileindex ) {

            //Fit last col to range width if enabled
            if (cConsiderRemaniningEnd && idx_w == (cNumTilesX - 1)) {
                x = cStartX + (cRangeWidth - cAbsWidth);
            } else {
                x = cStartX + (idx_w * cRelWidth);
            }

            tilePositionList[tileindex] = qMove(QPoint(x,y));
        }
    }

    Q_ASSERT( (cNumTilesX * cNumTilesY) == tilePositionList.size() );

    //Create meta object for all tiles of tilerange
    *pInfoMetaObject = new TashImageSegmentInfo(core::TashRuntime::genIDPara(m_pInitMutex),
                                           m_cameraIdx,
                                           cAbsWidth,
                                           cAbsHeight,
                                           cOverlap_X,
                                           cOverlap_Y,
                                           cNumTilesX * cNumTilesY,
                                           cNumTilesX,
                                           cNumTilesY);

    Q_CHECK_PTR(*pInfoMetaObject);

    if ( !(*pInfoMetaObject) ) {
        qCritical() << "Failed to allocate memory for TashImageSegmentInfo instance!";
        return false;
    }

    if ( !(*pInfoMetaObject)->setRuntime(m_pRunTime) ) {
        qCritical() << "Failed to set runtime for tashtile instance!";
    }

    return true;
}

bool TashMSSWAccessor::computeTileRangePositions(const struct roiDescriptor& roiDesc,
                                                 QVector<TashImageSegmentInfo*> &metaObjects,
                                                 QMap<quint32, QPoint> &tileRangePositionList) {
    //Do value checks
    if( !this->isRuntimeAware() ) {
        qCritical() << "TashMSSWAccessor instance need reference to runtime to work correctly, abort!";
        return false;
    }

    const auto cStartPos  = roiDesc.startPos;
    const auto cROIWidth  = roiDesc.width;
    const auto cROIHeight = roiDesc.height;

    if ( roiDesc.tileranges.length() <= 0 ) {
        qCritical() << "roiDescriptor does not contain any tilerange information! Cannot proceed computing tilerange positions!";
        return false;
    }

    //Get height and width of image
    const auto cImageWidth  = m_pRunTime->property( m_pRunTime->getPropertyNameContaining( TASHIMAGE_WIDTHID, m_cameraIdx) ).toUInt();
    const auto cImageHeight = m_pRunTime->property( m_pRunTime->getPropertyNameContaining( TASHIMAGE_HEIGHTID, m_cameraIdx) ).toUInt();
    const auto cROIStart_X  = static_cast<quint32>(cStartPos.x());
    const auto cROIStart_Y  = static_cast<quint32>(cStartPos.y());

    Q_ASSERT( cROIStart_X <= cImageWidth );

    if ( cROIStart_X > cImageWidth ) {
        qCritical() << "Dimension mismatch: Startpos_x >> " << cROIStart_X << " vs. Image width >> " << cImageWidth;
        return false;
    }

    Q_ASSERT( cROIStart_Y <= cImageHeight );

    if ( cROIStart_Y > cImageHeight ) {
        qCritical() << "Dimension mismatch: Startpos_y >> " << cROIStart_Y << " vs. Image height >> " << cImageHeight;
        return false;
    }

    Q_ASSERT( !roiDesc.tileranges.isEmpty() );

    if ( roiDesc.tileranges.isEmpty() ) {
        qCritical() << "There are no tileranges defined in ROI, abort!";
        return false;
    }

    //Compute tilerange start positions
    auto trPosIdx = 0;

    for ( auto it = roiDesc.tileranges.begin(); it != roiDesc.tileranges.end(); ++it, ++trPosIdx ) {

        const auto cTRWidth  = it->rangewidth;
        const auto cTRHeight = it->rangeheight;

        Q_ASSERT( (cTRWidth <= cROIWidth) && (cROIWidth <= cImageWidth) && (cTRWidth <= cImageWidth) );

        if ( (cTRWidth > cROIWidth) || (cROIWidth > cImageWidth) || (cTRWidth > cImageWidth) ) {
            qCritical() << "Dimension mismatch: tile width >> " << cTRWidth << " vs. roi width >> " << cROIWidth << " vs. image width >> " << cImageWidth;
            return false;
        }

        Q_ASSERT((cTRHeight <= cROIHeight) && (cROIHeight <= cImageHeight) && (cTRHeight <= cImageHeight));

        if ( (cTRHeight > cROIHeight) || (cROIHeight > cImageHeight) || (cTRHeight > cImageHeight) ) {
            qCritical() << "Dimension mismatch: tile height >> " << cTRHeight << " vs. roi height >> " << cROIHeight << " vs. image height >> " << cImageHeight;
            return false;
        }

        auto y(0);
        TashImageSegmentInfo* pTmp = new TashImageSegmentInfo(core::TashRuntime::genIDPara(m_pInitMutex),
                                               m_cameraIdx,
                                               it->rangewidth,
                                               it->rangeheight,
                                               0,
                                               0,
                                               roiDesc.numberTRs,
                                               1,
                                               roiDesc.tileranges.size());

        Q_CHECK_PTR(pTmp);

        if ( !pTmp ) {
            qCritical() << "Failed to allocate memory for TashImageSegmentInfo instance!";

            for (auto idx = 0u; idx < (it->index-1); ++idx) {
                delete metaObjects[idx];
            }

            return false;
        }

        if ( !pTmp->setRuntime(m_pRunTime) ) {
            qCritical() << "Failed to set runtime for tileRange instance!";
        }

        /**
         * @todo this operation is not very performant.
         * However only append works due to the fact that raw pointers
         * are stored, so created pointers to allocated space in memory
         * will go out of scope once the function returns.
         * Again, use smart pointers here to circumvent the issue!!!
         */
        metaObjects.append(pTmp);

        y = (it->startPos.y()) + cROIStart_Y;
        tileRangePositionList[trPosIdx] = QPoint(cROIStart_X + it->startPos.x(), y);
    }
    return true;
}

bool TashMSSWAccessor::computeROIPositions(const QVector<struct roiDescriptor>& roiDescriptors,
                                           QMap<quint32,TashImageSegmentInfo*>& roiMetaObjects,
                                           QMap<quint32, QPoint> &roiPositionList ) {

    Q_ASSERT(this->isRuntimeAware());

    //Do value checks
    if( !this->isRuntimeAware() ) {
        qCritical() << "TashMSSWAccessor instance need reference to runtime to work correctly, abort!";
        return false;
    }

    Q_ASSERT( !roiDescriptors.isEmpty() );

    if ( roiDescriptors.isEmpty() ) {
        qCritical() << "List which describes roi credentials is empty, can't proceed!";
        return false;
    }

    if ( !roiMetaObjects.isEmpty() ) {
        qWarning() << "Meta information about ROIs seems to be already computed, skip rest!";
        return true;
    }

    //Get height and width of image
    const auto cImageWidth  = m_pRunTime->property( m_pRunTime->getPropertyNameContaining( TASHIMAGE_WIDTHID, m_cameraIdx) ).toUInt();
    const auto cImageHeight = m_pRunTime->property( m_pRunTime->getPropertyNameContaining( TASHIMAGE_HEIGHTID, m_cameraIdx) ).toUInt();

    QVectorIterator<struct roiDescriptor> roiIt(roiDescriptors);

    //Foreach found region of interest
    while ( roiIt.hasNext() ) {

        auto roiDesc = roiIt.next();
        const auto cROI_X            = static_cast<quint32>(roiDesc.startPos.x());
        const auto cROI_Y            = static_cast<quint32>(roiDesc.startPos.y());
        const auto cROIWidth         = roiDesc.width;
        const auto cROIHeight        = roiDesc.height;

        Q_ASSERT( cROI_X < cImageWidth );

        if ( cROI_X > cImageWidth ) {
            qCritical() << "Dimension mismatch: Startpos_x >> " << cROI_X << " vs. Image width >> " << cImageWidth;
            return false;
        }

        Q_ASSERT( cROI_Y <= cImageHeight );

        if ( cROI_Y > cImageHeight ) {
            qCritical() << "Dimension mismatch: Startpos_y >> " << cROI_Y << " vs. Image height >> " << cImageHeight;
            return false;
        }

        Q_ASSERT( cROIWidth <= cImageWidth );

        if ( cROIWidth > cImageWidth ) {
            qCritical() << "Dimension mismatch: tile width >> " << cROIWidth << " vs. image width >> " << cImageWidth;
            return false;
        }

        Q_ASSERT( cROIHeight <= cImageHeight );

        if ( cROIHeight > cImageHeight ) {
            qCritical() << "Dimension mismatch: tile height >> " << cROIHeight << " vs. image height >> " << cImageHeight;
            return false;
        }

        quint64 id = core::TashRuntime::genIDPara(m_pInitMutex);

        auto* pTmp = new TashImageSegmentInfo(id,
                                              m_cameraIdx,
                                              cROIWidth,
                                              cROIHeight,
                                              0,
                                              0,
                                              1,
                                              0,
                                              0);

        Q_CHECK_PTR(pTmp);

        roiMetaObjects[roiDesc.index] = pTmp;

        if ( !roiMetaObjects[roiDesc.index] ) {
            qCritical() << "Failed to allocate memory for TashImageSegmentInfo instance!";
            return false;
        }

        if ( !roiMetaObjects[roiDesc.index]->setRuntime(m_pRunTime) ) {
            qCritical() << "Failed to set runtime for regionOfInterest instance!";
        }

        roiPositionList[roiDesc.index] = QPoint(cROI_X,cROI_Y);
    }

    return true;
}

void TashMSSWAccessor::releaseDummy(RegionOfInterest<T> *data){
    Q_UNUSED(data)
}

REGISTER_METATYPE(TashMSSWAccessor, "TashMSSWAccessor")
}
}

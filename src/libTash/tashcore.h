/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef CORE
#define CORE
#include "core/tashconst.h"
#include "core/tashtypes.h"
#include "core/abstracttashobject.h"
#include "core/tashobject.h"
#include "core/tashruntime.h"
#include "core/tashfactory.h"
#include "core/tashabstractframesorter.h"
#include "core/tashframesorter.h"
#include "core/tashossignalhandler.h"
#endif // CORE


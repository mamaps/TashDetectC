/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashabstractframesorter.h
 * @author Michael Flau
 * @brief
 * @details
 * @date August 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Cape Race, NF - Canada, Started August 2015
 */

#ifndef TASHABSTRACTFRAMESORTER_H
#define TASHABSTRACTFRAMESORTER_H

#include "tashobject.h"

namespace tashtego{
namespace core{

/**
 * @brief The TashAbstractFrameSorter class
 *
 * Base class implementation of common methods and members
 * which are used for varying sorting objects and strategies
 * as a starting point. Leaf classes which will be derived from
 * this abstract class are required to reimplement the methods
 * init() and sort(). This makes sure specialized objects provide
 * a sorting strategy shaped to the specific need. The abstract
 * base class does NOT provide a general sorting strategy!
 *
 * It is recommended to implement derived leaf classes as
 * singletons as there is usually just one camera/data sink
 * from which data comes from, which needs to be sorted.
 */
class TashAbstractFrameSorter: public TashObject
{
	Q_OBJECT

public:

    /**
     * @brief The SorterState_e enum
     * Internal sorter state fields for a convenient representation
     * of the sorter instance's internal state.
     */
    enum SorterState_e {
        OK,
        IDLE,
        ERROR
    };

	/**
	 * @brief TashAbstractFrameSorter
	 * @param parent
	 */
	explicit TashAbstractFrameSorter(TashObject *parent = 0);

    /**
     * @brief TashAbstractFrameSorter
     * @param id
     * @param framecount
     * @param searchHighest
     * @param parent
     */
    TashAbstractFrameSorter(qint64 id,
                            quint32 framecount,
                            quint32 allowedIdle,
                            bool searchHighest = true,
                            TashObject *parent = 0);

    /**
     * @brief sort
     * Pure virtual function, which has to be implemented by all child classes which inherit from
     * TashAbstractFrameSorter. This method is intended to be invoked at a certain time interval,
     * to check for new frames.
     */
    virtual void sort() = 0;

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() = 0;

signals:
    /**
     * @brief newFrame
     * Is emitted by instance, when new frame is available for processing.
     * Triggers further execution of program.
     */
    void newFrame();

    /**
     * @brief idle
     * Is emitted when no new frame is available from camera
     */
    void idle();

    /**
     * @brief error
     * Is emitted when the sorter instance experiences an internal error
     */
    void error();

    /**
     * @brief skipped
     * Is emitted when there were skipped frames detected
     */
    void skipped();

    /**
     * @brief newFrame
     * @param slot
     */
    void newFrame(quint32 slot, quint32 frameidx);

    /**
     * @brief skippedFrames
     * @param skip
     */
    void skippedFrames(quint32 skip);

public slots:
    /**
     * @brief getCurrent
     * Returns the current memory map slot number where the newest frame is stored.
     * @return
     * qint32 value which denotes the slot
     */
    virtual qint32 current();

    /**
     * @brief status
     * Returns the internal state of the sorter instance.
     * @return
     * Enum value of SorterState_e enum
     */
    virtual SorterState_e status();

    /**
     * @brief frameCount
     * Getter method to request the buffer size which is handled by the actual sorter instance.
     *
     * @return
     * 32 bit signed integer
     */
    virtual qint32 frameCount();

    /**
     * @brief searchHighest
     * Getter method which returns info whether slots are sorted by highest frame number
     * or not.
     *
     * @return
     * Boolean value true or false.
     */
    virtual bool searchHighest();

    /**
     * @brief setFrameCount
     * Setter method, which changes the maximum number of frames in buffer.
     * @param framecount
     * Number of buffer slots to consider as unsigned 32 bit integer
     */
    virtual void setFrameCount(quint32 framecount);

    /**
     * @brief framesSkipped
     * Getter method which returns the number of skipped frames since the sorter instance was started.
     * @return
     */
    virtual quint64 framesSkipped();

protected:

    /**
     * @brief init
     * Initialization function which must be called for this class to work properly,
     * in advance to it's actual operational state.
     */
    virtual bool init() = 0;

    /**
     * Class Member
     */
    bool            m_bSearchHighest;     ///> flag denotes to search for the highest frame when true
    quint32         m_Framecount;         ///> Field holds number of frames in buffer which is passed to ctor at construction time
    quint32         m_OldMaxCount;        ///> Field holds old maximum frame count value
    quint32         m_IdleSince;          ///> Counter denotes since how many cycles no new frame has arrived
    quint32         m_AllowedIdleMax;     ///> Maximum number of cycles to allow before camera is regarded as offline
    quint32         m_CurrentMaxSlot;     ///> Current slots number which holds newest frame
    quint64         m_FramesSkippedCycle; ///> Number of skipped frames since last valid frame
    quint64         m_FramesSkippedAll;   ///> Overall number of skipped frames since start
    SorterState_e   m_SorterState;        ///> Internal state description of sorter instance
    tashtegoRuntime m_pRuntime;           ///> Smart pointer to runtime instance
};

}
}

//Q_DECLARE_METATYPE(tashtego::core::TashAbstractFrameSorter*)

#endif //TASHABSTRACTFRAMESORTER_H


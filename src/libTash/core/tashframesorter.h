/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef TASHFRAMESORTER_H
#define TASHFRAMESORTER_H

//LibTash
#include "tashconst.h"
#include "tashtypes.h"
#include "tashabstractframesorter.h"
#include "../helper/tashtime.h"
#include "../helper/tashwaiter.h"
//Qt
#include <QDebug>
#include <QSharedPointer>
//c++
#include <algorithm>
#include <vector>
//system
#include <limits.h>

namespace tashtego {
namespace core {

template <typename T, typename T2>
/**
 * @brief The TashFrameSorter class
 */
class TashFrameSorter: public TashAbstractFrameSorter {

public:
    /**
     * @brief TashFrameSorter
     * @param parent
     */
    explicit TashFrameSorter(TashAbstractFrameSorter *parent = 0);

    /**
     * @brief TashFrameSorter
     * @param id
     * @param map
     * @param allowedMaxIdle
     * @param framecount
     * @param searchHighest
     * @param parent
     */
    TashFrameSorter(qint64 id,
                    QSharedPointer<T> map,
                    T2 indexFinder,
                    quint32 allowedMaxIdle,
                    quint32 framecount,
                    bool searchHighest = true,
                    TashAbstractFrameSorter *parent = 0);

    virtual ~TashFrameSorter();

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() override;

    /**
     * @brief setFIRSTMap
     * @param map
     */
    void setMap(QSharedPointer<T> map);

    /**
     * @brief sort
     */
    virtual void sort();

protected:
    /**
     * @brief init
     */
    virtual bool init() override;

    /*
     * Members
     */
    bool                m_bHasMap;          ///> Flag denotes if map reference has been set
    QSharedPointer<T>   m_FirstMap;         ///> Reference object to mmap
    quint32             m_LastFrameSlot;    ///> Lanst frame index
    qint32              m_CurrentMinSlot;   ///> Current min index slot
    T2                  m_IndexFinder;      ///> Lambda object holding sorter
};

template <typename T, typename T2>
TashFrameSorter<T, T2>::TashFrameSorter(TashAbstractFrameSorter *parent):
    TashAbstractFrameSorter(parent),
    m_bHasMap(false),
    m_FirstMap(nullptr),
    m_LastFrameSlot(0) {
}

template <typename T, typename T2>
TashFrameSorter<T, T2>::TashFrameSorter(qint64 id,
                                        QSharedPointer<T> map,
                                        T2 indexFinder,
                                        quint32 allowedMaxIdle,
                                        quint32 framecount,
                                        bool searchHighest,
                                        TashAbstractFrameSorter *parent):
    TashAbstractFrameSorter(id, framecount, allowedMaxIdle, searchHighest, parent),
    m_bHasMap(false),
    m_FirstMap(map),
    m_LastFrameSlot(0),
    m_IndexFinder(indexFinder) {
    auto status = init();
    Q_ASSERT(status);
    if ( !status ) {
        qCritical() << "Initialization of TashFrameSorter failed!";
    }
}


template <typename T, typename T2>
TashFrameSorter<T,T2>::~TashFrameSorter() {
#ifdef TASHVERBOSE
    qDebug() << "Releasing mmap handle...";
#endif
    m_FirstMap.reset();
#ifdef TASHVERBOSE
    qDebug() << "...done!";
#endif
}

template <typename T, typename T2>
bool TashFrameSorter<T,T2>::init() {
    auto status = m_FirstMap.isNull();
    Q_ASSERT(!status);

    if ( status ) {
        qCritical() << "Invalid reference to mmap!";
    }

    return (m_bIsInitialized = m_bHasMap = !status);
}

template <typename T, typename T2>
void TashFrameSorter<T,T2>::sort() {
    using namespace tashtego::rawtypes;
    using namespace tashtego::helper;

    if ( !m_bIsInitialized ) {
        qWarning() << "TashFrameSorter instance is not initialized!";
        return;
    }

    if ( !m_bHasMap ) {
        qWarning() << "No valid pointer to memory map, abort!";
        return;
    }

    QVector<qint32> biggerAsLast;
    QMap<qint8, quint32> indeces;
    auto currentMaxCount = UINT_MAX;
    auto currentMinCount = 0u;
    // Get current available counter values in map aka create
    // local copy with c++11 lambda function injection
    indeces = m_IndexFinder( m_Framecount, m_FirstMap);


    //Get newest counter and corresponding slot
    for ( auto it = indeces.begin(); it != indeces.end(); ++it ) {
        if ( it.value() > currentMaxCount ) {
            currentMaxCount = it.value();
            m_CurrentMaxSlot   = it.key();
        }

        if ( it.value() < currentMaxCount ) {
            currentMinCount = it.value();
            m_CurrentMinSlot   = it.key();
        }

        if ( it.value() > m_LastFrameSlot ) {
            biggerAsLast.append(it.value());
        }
    }

    if (biggerAsLast.empty()) {
        currentMaxCount = m_LastFrameSlot;
    } else {
        std::sort(biggerAsLast.begin(), biggerAsLast.end());
        currentMinCount = biggerAsLast.first();
        currentMaxCount = biggerAsLast.last();
    }

    if ( currentMaxCount > m_LastFrameSlot ) {
        m_IdleSince     = 0;
        m_SorterState   = OK;

        emit newFrame(indeces.key(currentMinCount), currentMinCount);
        m_LastFrameSlot = currentMinCount;
    } else {

        if ( m_IdleSince == m_AllowedIdleMax ) {
            qDebug() << "FIRST is offline...";
            m_IdleSince     = 0;
            m_SorterState   = IDLE;
            m_LastFrameSlot = 0;
        }

        emit idle();

        //Increase idle cycles
        ++m_IdleSince;
        TashWaiter::msecWait(20);
    }

    //Handle skipped frames
    if ( currentMaxCount != (m_OldMaxCount + 1) ) {

        qint32 skipped  = currentMaxCount - (m_OldMaxCount-1);

        m_FramesSkippedCycle = skipped;
        m_FramesSkippedAll += skipped;

        emit skippedFrames(skipped);
    }

    //Store current counter as old counter
    m_OldMaxCount = currentMaxCount;
}

template <typename T, typename T2>
QString TashFrameSorter<T, T2>::getClassName() {
    return QString("TashFrameSorter");
}

template <typename T, typename T2>
void TashFrameSorter<T, T2>::setMap(QSharedPointer<T> map) {

    if ( map.isNull() ) {
        qCritical() << "Given shared pointer does not contain a memory map structure, abort!";
        return;
    }

    //set map
    m_FirstMap = map;

    //Test if map is valid
    auto status = init();
    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "Failed to reinitialize TashFrameSorter instance with new map!";
    }
}

}
}

#endif // TASHFRAMESORTER_H

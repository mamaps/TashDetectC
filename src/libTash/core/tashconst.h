/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashconst.h
 * @author Michael Flau
 * @brief
 * @details
 * @date June 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started June 2015
 */

#ifndef TASHCONST
#define TASHCONST

#include <QDebug>
#include <QList>
#include <QVector>
#include <QMap>

class QObject;

namespace tashtego {

namespace core {
    class TashObject;
}

/**
 * @brief getRuntimeInstanceIDs
 * Returns a reference to a list with all runtime instance IDs
 * @return
 * reference to QVector of all runtime instances
 */
QVector<qint64> &getRuntimeInstanceIDs();

/**
 * @brief getMetaObjectIDs
 * Returns a reference to a list with all meta object IDs
 * @return
 * reference to QVector of all meta object IDs
 */
QVector<int>& getMetaObjectIDs();

/**
 * @brief getObjectRegistry
 * Returns a reference to a list with all meta object IDs
 * @return
 * Reference to list of QString identifiable list of QObject pointers
 */
QMap<QString, QObject*>& getObjectRegistry();

/*
 * General constants
 */

/// Earth radius constant
static const double TASHEARTHRADIUS                 = 6.371E+6;
/// Pi constant witin tashtego scope
static const double TASHPI                          = 3.1416;
/// Field of view FIRST camera
static const double TASHFIRSTVERTFOV                = 18.0;
/// Vertical resolution FIRST camera
static const double TASHFIRSTVERTPIXEL              = 576.0;
/// Horizontal resolution FIRST camera
static const double TASHFIRSTHORZPIXEL              = 7200.0;
/// Vertical pixel to angle ratio of FIRST camera
static const double TASHFIRSTANGLE2PIXEL            = TASHFIRSTVERTFOV / TASHFIRSTVERTPIXEL;

/*
 * Default ini file values
 */

/// Default image width of tashtego image
static const int  TASHIMAGEWIDTH                    = TASHFIRSTHORZPIXEL;
/// Default image height of tashtego image
static const int  TASHIMAGEHEIGHT                   = TASHFIRSTVERTPIXEL;
/// Overall number of pixels in FIRST image
static const int  TASHIMAGEPIXELCOUNT               = TASHIMAGEWIDTH * TASHIMAGEHEIGHT;
/// Amount of images in a RDE memory map
static const int  TASHIMAGECOUNTPERMAP              = 5;
/// Max 14 Bit Color value
static const int  TASH14BITCOLOR                    = 16384;
/// Number of color channels
static const int  TASHNUMBEROFCHANNELS              = 3;
/// Tashtego dummy size value
static const int  TASHDUMMYSZ                       = 100;
/// Tashtego dummy blow size
static const int  TASHBLOWSSZ                       = 3;
/// Tashtego dummy last blow time value
static const int  TASHLASTBLOW                      = 2;
/// Tashtego dummy last click time value
static const int  TASHLASTCLICK                     = 2;
/// Legacy value, unclear what this value is about
static const int  TASHPERSBLOWS                     = 22;
/// Default DBus activation value
static const bool TASHUSEDBUS                       = false;
/// Default OS Interrupt handling value
static const bool TASHHANDLEOSSIGNALS               = true;
/// Default grace time value when dry run is triggered
static const int  TASHDRYRUNGRACETIME               = 10;
/// Default number of header fields in an FVI file
static const int  TASHFVIHEADERFIELDS               = 94;
/// Default time ti wait in useconds
static const unsigned long TASHDEFAULTUSECWAIT      = 1000;
/// Default time ti wait in milliseconds
static const unsigned long TASHDEFAULTMSECWAIT      = 100;
/// Default time ti wait in seconds
static const unsigned long TASHDEFAULTSECWAIT       = 1;
/// Number of FVI buffer slots in write pipeling
static const int TASHRAMDISKSLOTS                   = 100;
/// Default thread maximum per tashtego app
static const int TASHMAXTHREADSPERAPP               = 10;
/// Value determines if memory map has a global header
static const bool TASHMAPHASGLOBALHEADER            = true;
/// Value determines if a memory map slot has a local header
static const bool TASHMAPHASFRAMEHEADER             = true;
/// Default number of threads to reserve from main threadpool
static const int TASHRESERVENUMTHREADSFROMDETECTOR  = 2;

/*
 * TashEvent default values
 */

/// Number of single frames in a TashEvent sequence
static const int TASHEVENTIMAGESCOUNT               = 30;
/// Image width of TashEvent sequences
static const int TASHEVENTIMAGEWIDTH                = 101;
/// Image height of TashEvent sequences
static const int TASHEVENTIMAGEHEIGHT               = TASHIMAGEHEIGHT;
/// Small image height of TashEvent sequences
static const int TASHEVENTIMAGEHEIGHTSMALL          = 23;
/// Small image width of TashEvent sequences
static const int TASHEVENTIMAGEWIDTHSMALL           = TASHEVENTIMAGEHEIGHTSMALL;

/*
 * Score map dimensions
 */

/// Default number of core slots in score map
static const char SCORE_MAP_DEFAULT_NUM_CORES        = 64;
/// Default number of events per core slot in score map
static const int  SCORE_MAP_DEFAULT_NUM_EVENT_FIELDS = 10000;
/// Default number of fields per event in core slot of a score map
static const char SCORE_MAP_DEFAULT_DATA_FIELDS      = 8;

/*
 * Counterdetection dimensions
 */

/// Maximum allowed number of counter detectors
static const char MAX_COUNTER_DETECTORS            = 100;
/// Maximum number of counter detections per counter detector
static const int  MAX_COUNTER_DETECTIONS           = 100000;
/// Defualt number of fields per counter detection
static const char MAX_FIELDS_PER_COUNTER_DETECTION = 6;

/// Imagestack map dimension
static const char IMGSTACK_MAP_DEFAULT_DEPTH       = TASHEVENTIMAGESCOUNT;

/// Default region of interest value
static const char DIM_MAP_DEFAULT_ROI_NUM          = 10;
/// Default tile range value
static const char DIM_MAP_DEFAULT_TR_NUM           = 50;

/// Detection cluster map dimensions
static const char CLUSTER_MAP_DEFAULT_CLUSTER_NUM  = 100;

/*
 * Inifile default group identifier
 */

/// Runtime ini-value identifier
static const char* const TASHGENERALGROUP       = "Runtime";
/// Camera ini-value identifier
static const char* const TASHCAMGROUP           = "Cameras";
/// Output ini-value identifier
static const char* const TASHOUTPUTFILEGROUP    = "OutputFile";
/// Camera ini-value sub-identifier
static const char* const TASHCAMIDENTIFIERGROUP = "Camera_%1";
/// Header ini-value identifier
static const char* const TASHHEADERGROUP        = "Header_Description";
/// Global header ini-value identifier
static const char* const TASHGLOBALHEADERGROUP  = "Global_Header_Description";
/// Frame header ini-value identifier
static const char* const TASHFRAMEHEADERGROUP   = "Frame_Header_Description";
/// Memory map ini-value identifier
static const char* const TASHMMAPGROUP          = "Mmap_Settings";

/*
 * Inifile default field identifier
 */

/// ini file path identifier
static const char* const TASHGENERAL_INIFILEPATHID      = "inifilepath";
/// DBus usage field identifier
static const char* const TASHGENERAL_USEDBUSID          = "use_dbus";
/// Maximum worker threads field identifier
static const char* const TASHGENERAL_MAXWORKERTHREADSID = "max_worker_threads";
/// Interrupt handling field identifier
static const char* const TASHGENERAL_HANDLEOSSIGNALSID  = "enable_OS_signal_handling";
/// Dry run grace time field identifier
static const char* const TASHGENERAL_DRYRUN_GRACE_TIME  = "dry_run_grace_time";

/*
 * cameras
 */

/// Camera number field identifier
static const char* const TASHCAM_CAMCOUNTID         = "number_of_cameras";

/*
 * memory map field identifier
 */

/// mmap fileoption
static const char* const TASHMAP_FILEOPENID         = "fileoptions";
/// mmap fileaccess
static const char* const TASHMAP_FILEACCESSID       = "fileaccess";
/// mmap map options
static const char* const TASHMAP_OPTIONSID          = "mapoptions";
/// mmap protection
static const char* const TASHMAP_PROTECTIONID       = "mapprotection";
/// mmap type
static const char* const TASHMAP_TYPEID             = "mmap_type";
/// mmap path
static const char* const TASHMAP_PATHID             = "mmap_path";
/// mmap open offset from file start
static const char* const TASHMAP_OFFSETID           = "mmap_opening_offset";
/// mmap global header denominator
static const char* const TASHMAP_HASGLOBALHEADERID  = "mmap_has_global_header";
/// mmap local header denominator
static const char* const TASHMAP_HASFRAMEHEADERID   = "mmap_has_frame_header";

/*
 * image data ini-field identifier
 */

/// image height identifier
static const char* const TASHIMAGE_HEIGHTID      = "image_height";
/// image width identifier
static const char* const TASHIMAGE_WIDTHID       = "image_width";
/// image data datatype identifier
static const char* const TASHIMAGE_DATATYPEID    = "data_type";
/// image frames per map identifier
static const char* const TASHIMAGE_FRAMECOUNTID  = "frames_per_map";
/// image frames per second identifier
static const char* const TASHIMAGE_FPSID         = "framerate_in_Hz";

/*
 * output file
 */

/// output file field identifier
static const char* const TASHOUTPUTFILE_TYPEID  = "filetype";
/// output file frames per field, field identifier
static const char* const TASHOUTPUTFILE_FRAMESPERFILEID  = "frames_per_file";

/*
 * Regular expressions
 */

/// Camera field regular expression
static const char* const TASHCAMERAIDTAG            = "Camera_\\d{1,2}.+" ;
/// Data type field regular expression
static const char* const TASHDATATYPEREGEX          = "(bool|char|uchar|short|ushort|int|uint|\\blong\\b|\\bulong\\b|longlong|ulonglong|float|double|longdouble){1}";
/// Global header field regular expression
static const char* const TASHGLOBALHEADERFIELDREGEX = "Camera_\\d-.+-Header_Description-Global_Header_Description-(bool|char|uchar|short|ushort|int|uint|\\blong\\b|\\bulong\\b|longlong|ulonglong|float|double|longdouble)-.+-\\d{1,2}";
/// Frame header field regular expression
static const char* const TASHFRAMEHEADERFIELDREGEX  = "Camera_\\d-.+-Header_Description-Frame_Header_Description-(bool|char|uchar|short|ushort|int|uint|\\blong\\b|\\bulong\\b|longlong|ulonglong|float|double|longdouble)-.+-\\d{1,2}";
/// Memory map file open field regular expression
static const char* const TASHMAP_FILEOPENFLAGSREGEX = "((ReadOnly|WriteOnly|ReadWrite|Append|Truncate|Create|Exclusive)\\|){0,}(ReadOnly|WriteOnly|ReadWrite|Append|Truncate|Create|Exclusive){1}";
/// Memory map file access field regular expression
static const char* const TASHMAP_FILEACCESSREGEX    = "((OwnerCanRead|OwnerCanWrite|OwnerCanExec|GroupCanRead|GroupCanWrite|GroupCanExec|OtherCanRead|OtherCanWrite|OtherCanExec)\\|){0,}(OwnerCanRead|OwnerCanWrite|OwnerCanExec|GroupCanRead|GroupCanWrite|GroupCanExec|OtherCanRead|OtherCanWrite|OtherCanExec){1}";
/// Memory map protection field regular expression
static const char* const TASHMAP_PROTECTIONREGEX    = "((MapRead|MapWrite|MapNone|MapExec)\\|){0,}(MapRead|MapWrite|MapNone|MapExec){1}";
/// Memory map file options field regular expression
static const char* const TASHMAP_OPTIONSREGEX       = "((MapShared|MapPrivate|MapAnonymous|MapPopulate)\\|){0,}(MapShared|MapPrivate|MapAnonymous|MapPopulate){1}";
/// Memory map additional linux options field regular expression
static const char* const TASHMAP_OPTIONSLINUXREGEX  = "((MapShared|MapPrivate|MapAnonymous|MapPopulate|MapLocked|MapDenyWrite|MapGrowsDown|MapNonBlock)\\|){0,}(MapShared|MapPrivate|MapAnonymous|MapPopulate|MapLocked|MapDenyWrite|MapGrowsDown|MapNonBlock){1}";
/// TashGPS latitude field regular expression
static const char* const TASHGPS_LATITUDEREGEX      = "(-|)([0-9]|[0-8][0-9])(D|d|deg|DEG|Deg|°)([0-9]|[0-5][0-9])(M|m|MIN|min|Min|')([0-9]|[0-5][0-9])(\\.\\d{1,}|)(S|s|SEC|sec|Sec|\")(N|S|n|s|North|South)";
/// TashGPS latitude field regular expression
static const char* const TASHGPS_LONGITUDEREGEX     = "(-|)([0-9]|[0-8][0-9])(D|d|deg|DEG|Deg|°)([0-9]|[0-5][0-9])(M|m|MIN|min|Min|')([0-9]|[0-5][0-9])(\\.\\d{1,}|)(S|s|SEC|sec|Sec|\")(E|W|e|w|East|West)";
}
#endif // TASHCONST


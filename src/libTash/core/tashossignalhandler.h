/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashmsswprocessor.cpp
 * @author Michael Flau
 * @brief
 * @details
 * Based on example code from here:
 * http://doc.qt.io/qt-5/unix-signals.html
 * @date December 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started December 2015
 */

#ifndef TASHOSSIGNALHANDLER_H
#define TASHOSSIGNALHANDLER_H

#include <QObject>

class QSocketNotifier;

namespace tashtego {
namespace core {

/**
 * @brief The TashOSSignalHandler class
 */
class TashOSSignalHandler : public QObject {
    Q_OBJECT
public:

    /**
     * @brief TashOSSignalHandler
     * @param parent
     */
    explicit TashOSSignalHandler(QObject *parent = 0);

    /**
     * @brief ~TashOSSignalHandler
     */
    virtual ~TashOSSignalHandler();

#ifdef Q_OS_UNIX

    /**
     * @brief hupSignalHandler
     * @param unused
     */
    static void hupSignalHandler(int unused);

    /**
     * @brief termSignalHandler
     * @param unused
     */
    static void termSignalHandler(int unused);

    /**
     * @brief termSignalHandler
     * @param unused
     */
    static void intSignalHandler(int unused);

#elif Q_OS_WIN32

#endif

public slots:

#ifdef Q_OS_UNIX

    /**
     * @brief handleSigHup
     */
    void handleSigHup(int unused);

    /**
     * @brief handleSigTerm
     */
    void handleSigTerm(int unused);

    /**
     * @brief handleSigInt
     */
    void handleSigInt(int unused);

#elif Q_OS_WIN32

#endif

signals:
    /**
     * @brief shutdown
     */
    void shutdown();

private:
#ifdef Q_OS_UNIX
    static int sighupFd[2]; ///>
    static int sigtermFd[2];///>
    static int sigintFd[2];///>
#elif Q_OS_WIN32

#endif

    QSocketNotifier *pSigHup;  ///>
    QSocketNotifier *pSigTerm; ///>
    QSocketNotifier *pSigInt; ///>
};

}
}
#endif // TASHOSSIGNALHANDLER_H

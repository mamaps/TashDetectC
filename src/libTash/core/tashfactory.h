/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashfactory.h
 * @author Michael Flau
 * @brief
 * @details
 * @date June 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started June 2015
 */

#ifndef TASHFACTORY_H
#define TASHFACTORY_H

#include <QObject>
#include <QSharedPointer>

#include "tashobject.h"

class QString;

namespace tashtego {

    namespace core {


        /**
         * @brief The TashFactory class
         */
        class TashFactory : public QObject
        {
            Q_OBJECT

            /**
             * @brief TashFactory
             * @param parent
             */
            explicit TashFactory(QObject *parent = 0);

            /**
             * @brief operator =
             * @param old
             * @return
             */
            const TashFactory &operator= (const TashFactory &old);

        public:
            /**
             * @brief createFactory
             * @return
             */
            static TashFactory *createFactory();

            /**
             * @brief destroyFactory
             */
            static void destroyFactory();

        signals:

            /**
             * @brief objectCreated
             * @param msg
             */
            void objectCreated(QString msg);

            /**
             * @brief objectCreated
             * @param id
             */
            void objectCreated(int id);

            /**
             * @brief creationError
             * @param msg
             */
            void creationError(QString msg);

            /**
             * @brief creationError
             */
            void creationError(int id);

        public slots:

            /**
             * @brief createSharableObject
             * @param name
             * @return
             */
            QSharedPointer<TashObject> createSharableObject(QString &name);

            /**
             * @brief createUnsafeObject
             * @param name
             * @return
             */
            TashObject* createUnsafeObject(QString &name);

            /**
             * @brief createSafeObjectFromID
             * @param id
             * @return
             */
            QSharedPointer<TashObject> createSafeObjectFromID(const int& id);

            /**
             * @brief createUnsafeObjectFromID
             * @param id
             * @return
             */
            TashObject* createUnsafeObjectFromID(const int& id);

        private:
            /**
             * @brief getObjectID
             * @param name
             * @return
             */
            inline int getQMetaTypeID(QString &name);

            /**
             * @brief releaseDummy
             * @param obj
             */
            static void releaseDummy(TashObject* obj);

            /*
             * Member
             */
            static TashFactory* m_pFactory;
        };
    }
}
#endif // TASHFACTORY_H

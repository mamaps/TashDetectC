/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashobject.h
 * @author Michael Flau
 * @brief
 * @details
 * @date June 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started June 2015
 */

#ifndef TASHOBJECT_H
#define TASHOBJECT_H

#include <QPointer>

#include "tashruntime.h"
#include "abstracttashobject.h"

namespace tashtego {

namespace core {

/**
 * @brief The TashObject class
 *
 * libTash's base class object from which all library classes are derived.
 * Any functionality which should be present in all child classes should
 * have a virtual defintion here.
 */
class TashObject : public AbstractTashObject
{
    Q_OBJECT
    Q_PROPERTY(qlonglong instanceID READ getObjectID WRITE setObjectID)
    Q_PROPERTY(bool isInitialized READ isInitialized)
    Q_PROPERTY(bool isRuntimeAware READ isRuntimeAware)

public:

    /**
     * @brief TashObject
     * Default ctor which is called on introspection events
     * @param parent
     * Parent object or 0 if none
     */
    explicit TashObject(AbstractTashObject *parent = 0);

    /**
     * @brief TashObject
     * ctor to be called when the object instance is supposed to be registered
     * to the runtime instance register via it's id, but has no runtime argument
     * to use for property extraction.
     * @param parent
     * Parent object or 0 if none
     * @param id
     * Unique ID by which the instance can be identified via runtime methods
     */
    TashObject(qint64 id, AbstractTashObject *parent = 0);

    /**
     * @brief TashObject
     * ctor to be called when object instance is supposed to use the runtime to extract
     * settings from property system.
     * @param id
     * Unique ID by which the instance can be identified via runtime methods
     * @param runtime
     * Reference to runtime object, to get access to all runtime functionality within
     * object space
     * @param parent
     * Parent object or 0 if none
     */
    TashObject(qint64 id, const QPointer<core::TashRuntime> runtime, AbstractTashObject *parent = 0);

    /**
     * @brief ~TashObject
     * Default dtor object, virtual to be able to delete child class instances
     * through a base class pointer.
     */
    virtual ~TashObject() = default;

signals:
    /*
     * Common signals which can be emitted by all modules who inherit from TashObject
     */
    void error(qint64 id);
    void finished(qint64 id);
    void started(qint64 id);
    void initialized(qint64 id);

public slots:

    /**
     * @brief getObjectID
     * Simple getter to return the unique ID of a class instance
     * @return
     * Registered instance ID as <code>unsigned long long int</code>
     */
    qint64& getObjectID();

    /**
     * @brief isInitialized
     * Simple getter method, to check if instance is initialized or not
     * @return
     * Returns <code>true</code> in initialized, otherwise <code>false</code>
     */
    bool isInitialized() const;

    /**
     * @brief runtimeAware
     * Simple getter method, to check if instance has reference to runtime or not
     * @return
     * Returns <code>true</code> if ref to runtime exists, otherwise <code>false</code>
     */
    bool isRuntimeAware() const;

    /**
     * @brief setRuntime
     * Simple setter method, to pass a reference to a runtime object to the respective
     * class instance of TashObject, or a derived class of it.
     * @param runtime
     * <code>QPointer</code>, pointing to <code>TashRuntime</code> instance
     * @return
     * Returns <code>true</code> if ref to runtime was stored sucessfully, otherwise <code>false</code>
     */
    bool setRuntime(const QPointer<TashRuntime> &runtime);

protected:
    /**
     * @brief init
     * Initialization function, which will be executed once TashObject is instanciated.
     * Carries out basic logic which is required by every libTash module.
     */
    virtual bool init() override;

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() override;

    /**
     * @brief setObjectID
     * Setter method, to manually change the ID of a libTash object
     * @param id
     * ID as unsigned long long int
     */
    void setObjectID(qint64 id);

    /**
     * @brief setDefaultProperties
     * @return
     */
    virtual bool setDefaultProperties();

    /**
     * @brief parseRuntimeForProperties
     * @return
     */
    virtual bool parseRuntimeForProperties();

    /*
     * Members
     */
    QPointer<TashRuntime> m_pRunTime;
    qint64                m_InstanceID;       ///> Unique id which identfies each instance
    bool                  m_bIsInitialized;   ///> Flag which determines the state of the object instance and whether it is usable
    bool                  m_bHasRuntimeObject;
};

}
}
Q_DECLARE_METATYPE(tashtego::core::TashObject)


#endif // TASHOBJECT_H

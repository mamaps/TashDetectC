/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "tashossignalhandler.h"

#include <QDebug>

#ifdef Q_OS_UNIX
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#elif Q_OS_WIN32

#endif

#include <QSocketNotifier>

namespace tashtego {
namespace core {

int TashOSSignalHandler::sighupFd[]  = {0};
int TashOSSignalHandler::sigtermFd[] = {0};
int TashOSSignalHandler::sigintFd[]  = {0};

TashOSSignalHandler::TashOSSignalHandler(QObject *parent) : QObject(parent),
#ifdef Q_OS_UNIX

#elif Q_OS_WIN32

#endif
    pSigHup(nullptr),
    pSigTerm(nullptr),
    pSigInt(nullptr)
{
#ifdef Q_OS_UNIX

    qDebug() << "Initializing Signalhandlers...";

    if ( ::socketpair(AF_UNIX, SOCK_STREAM, 0, sighupFd)) {
        qFatal("Could not create HUP socketpair!");
    }

    if ( ::socketpair(AF_UNIX, SOCK_STREAM, 0, sigtermFd)) {
        qFatal("Could not create TERM socketpair!");
    }

    if ( ::socketpair(AF_UNIX, SOCK_STREAM, 0, sigintFd)) {
        qFatal("Could not create INT socketpair!");
    }

    pSigHup = new QSocketNotifier( sighupFd[1], QSocketNotifier::Read, this );
    Q_CHECK_PTR(pSigHup);
    QObject::connect( pSigHup, SIGNAL(activated(int)), this, SLOT(handleSigHup(int)));

    pSigTerm = new QSocketNotifier( sigtermFd[1], QSocketNotifier::Read, this );
    Q_CHECK_PTR(pSigTerm);
    QObject::connect( pSigTerm, SIGNAL(activated(int)), this, SLOT(handleSigTerm(int)));

    pSigInt = new QSocketNotifier( sigintFd[1], QSocketNotifier::Read, this );
    Q_CHECK_PTR(pSigInt);
    QObject::connect( pSigInt, SIGNAL(activated(int)), this, SLOT(handleSigInt(int)));

#elif Q_OS_WIN32

#endif
}

TashOSSignalHandler::~TashOSSignalHandler() {
    if ( pSigHup ) {
        pSigHup->deleteLater();
    }

    if ( pSigTerm ) {
        pSigTerm->deleteLater();
    }

    if ( pSigInt ) {
        pSigInt->deleteLater();
    }
}

#ifdef Q_OS_UNIX

    /**
     * @brief hupSignalHandler
     * @param unused
     */
    void TashOSSignalHandler::hupSignalHandler(int unused) {
        Q_UNUSED(unused);
        char a = 1;
        auto sz = ::write(sighupFd[0], &a, sizeof(a));
        Q_UNUSED(sz);
    }

    /**
     * @brief termSignalHandler
     * @param unused
     */
    void TashOSSignalHandler::termSignalHandler(int unused) {
        Q_UNUSED(unused);
        char a = 1;
        auto sz = ::write(sigtermFd[0], &a, sizeof(a));
        Q_UNUSED(sz);
    }

    /**
     * @brief intSignalHandler
     * @param unused
     */
    void TashOSSignalHandler::intSignalHandler(int unused) {
        Q_UNUSED(unused);
        char a = 1;

        auto sz = ::write(sigintFd[0], &a, sizeof(a));
        Q_UNUSED(sz);
    }

    /**
     * @brief handleSigHup
     */
    void TashOSSignalHandler::handleSigHup(int unused) {
        Q_UNUSED(unused);
        pSigHup->setEnabled(false);
        char tmp;

        auto sz = ::read(sighupFd[1], &tmp, sizeof(tmp));
        Q_UNUSED(sz);

        qDebug("Shutdown requested via SIGHUP!");
        emit shutdown();
        pSigHup->setEnabled(true);
    }

    /**
     * @brief handleSigTerm
     */
    void TashOSSignalHandler::handleSigTerm(int unused) {
        Q_UNUSED(unused);
        pSigTerm->setEnabled(false);
        char tmp;

        auto sz = ::read(sigtermFd[1], &tmp, sizeof(tmp));
        Q_UNUSED(sz);

        qDebug("Shutdown requested via SIGTERM!");
        emit shutdown();
        pSigTerm->setEnabled(true);
    }

    /**
     * @brief handleSigInt
     */
    void TashOSSignalHandler::handleSigInt(int unused) {
        Q_UNUSED(unused);
        pSigInt->setEnabled(false);
        char tmp;

        auto sz = ::read(sigintFd[1], &tmp, sizeof(tmp));
        Q_UNUSED(sz);

        qDebug("Shutdown requested via SIGINT!");
        emit shutdown();
        pSigInt->setEnabled(true);
    }

#elif Q_OS_WIN32

#endif

}
}

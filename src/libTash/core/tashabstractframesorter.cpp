/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashabstractframesorter.cpp
 * @author Michael Flau
 * @brief
 * @details
 * @date August 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Cape Race, NF - Canada, Started August 2015
 */

#include "tashabstractframesorter.h"

#include <QDebug>

namespace tashtego{
namespace core{

TashAbstractFrameSorter::TashAbstractFrameSorter(TashObject *parent):
    TashObject(parent),
    m_bSearchHighest(true),
    m_Framecount(1),
    m_OldMaxCount(0),
    m_IdleSince(0),
    m_AllowedIdleMax(0),
    m_CurrentMaxSlot(0),
    m_FramesSkippedCycle(0),
    m_SorterState(TashAbstractFrameSorter::IDLE)
{
}

TashAbstractFrameSorter::TashAbstractFrameSorter(qint64 id,
                                                 quint32 framecount,
                                                 quint32 allowedIdle,
                                                 bool searchHighest,
                                                 TashObject *parent):
    TashObject(id, parent),
    m_bSearchHighest(searchHighest),
    m_Framecount(framecount),
    m_OldMaxCount(0),
    m_IdleSince(0),
    m_AllowedIdleMax(allowedIdle),
    m_CurrentMaxSlot(0),
    m_FramesSkippedCycle(0),
    m_SorterState(TashAbstractFrameSorter::IDLE)
{
}

qint32 TashAbstractFrameSorter::current() {
    return m_CurrentMaxSlot;
}

TashAbstractFrameSorter::SorterState_e TashAbstractFrameSorter::status() {
    return m_SorterState;
}

qint32 TashAbstractFrameSorter::frameCount() {
    return m_Framecount;
}

bool TashAbstractFrameSorter::searchHighest() {
    return m_bSearchHighest;
}

void TashAbstractFrameSorter::setFrameCount(quint32 framecount) {

    if ( framecount <= 0 ) {
        qWarning() << "Framesorter must have a frame count value greater than zero, abort!";
        return;
    }

    m_Framecount = framecount;
}

quint64 TashAbstractFrameSorter::framesSkipped() {
    return m_FramesSkippedCycle;
}

//REGISTER_METATYPE(TashAbstractFrameSorter*, "TashAbstractFrameSorter")
}
}

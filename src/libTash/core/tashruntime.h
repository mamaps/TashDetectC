/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashruntime.h
 * @author Michael Flau
 * @brief
 * @details
 * @date September 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started September 2015
 */

#ifndef TASHRUNTIME_H
#define TASHRUNTIME_H

#include <QObject>
#include <QMap>
#include <QVector>
#include <QtGlobal>
#include <QVariant>


class QSettings;
class QMutex;

namespace tashtego {

namespace helper {
    class TashCLIParser;
}

namespace core {

class TashObject;
class TashOSSignalHandler;

/**
 * @brief The TashRuntime class
 * is the core object which is required by all Tashtego applications which want use
 * libTash to it's full extent.
 * @details
 * Features which are provided by this class are
 * - Access to a unified property system which enables End-Users to quickly change up-start settings via config file
 * - Full built-in support of ad-hoc behaviour changes via user defineable command-line flags
 * - Connection to OS-signals for easier app management on Linus systems
 * - DBus integration for better debugging of client applications
 * - Lifetime management of all libTash objects created
 *
 * Single Instance Usage
 * ---------------------
 * This object is meant to be instanciated once at program upstart. That is why this class makes usage of the
 * singleton design pattern to ensure, that only one instance at a time of this class can be run inside of
 * an application using libTash library.
 *
 * Properties & ini-files
 * ----------------------
 * Any app which will use the runtime obect as a starting point, automatically uses the
 * [Qt Property System](http://doc.qt.io/qt-5/properties.html).
 * That means certain requirements must be met before the runtime can be used. Internally 3 values must be predefined
 * in any case:
 * - The name of the client application
 * - The number of cameras which will be used as input sources by the app
 * - The default path to the location where the ini-file will be searched for, or placed if there is none
 *
 * Ongoing, the app will look at startup for a present ini-file at the specified location. Here one of two modes
 * will be activated. In case of a lacking ini-file, automatic object introspection will be invoked by the runtime using Qt's
 * [Meta-Object System](http://doc.qt.io/qt-5/metaobjects.html).
 * This basically means, that all defined libTash objects which are needed by the client app will be asked to provide
 * their default up-start settings via property systems. These properties are collected by the current runtime instance
 * and stored inside of an ini-file at the specified location. Afterwards, the runtime will destroy itself and end the application.
 * This approach ensures, that all object have default values to work with right away. The user can now edit the created ini-file
 * to his/her needs. At next upstart the runtime instance will find the written ini-file at the specified location and read all
 * from the ini file into the property system. The runtime object can than be used to request the value of any property
 * which is defined in the ini file.
 *
 * Command line arguments
 * ----------------------
 * The runtime class offers the option to define user-specific command-line switches to ad-hoc alter the behaviour of
 * the client application. Along with that, the runtime itself implements command-line switches which any client
 * application using the runtime class will have by default. This is a list of the default application switches:
 *
 * |Single-Flag   |Multi-Flag          |Value                                    |Description                                                                       |
 * |:------------:|:------------------:|:---------------------------------------:|----------------------------------------------------------------------------------|
 * |<code>h</code>|<code>help</code>   |<code>none</code>                        |Displays information about all cli switches and exits application                 |
 * |<code>n</code>|<code>dry</code>    |<code>none</code>                        |Executes the application until the upstart routine is finished and than exits     |
 * |<code>i</code>|<code>ini</code>    |<code>[path/to/alternate/file.ini]</code>|Specifies an alternate location of an ini file                                    |
 * |<code>d</code>|<code>dbus</code>   |<code>none</code>                        |Dis-/Enables the usage of the DBus for message logging, default is set by ini-file|
 * |<code>d</code>|<code>dbus</code>   |<code>none</code>                        |Dis-/Enables the usage of the DBus for message logging, default is set by ini-file|
 * |<code>o</code>|<code>ossh</code>   |<code>none</code>                        |Dis-/Enables the usage of the internal Signal handler for connecting to OS signals|
 * |<code>r</code>|<code>rewrite</code>|<code>none</code>                        |Forces the application to rewrite the ini-file at specified location              |
 *
 * @todo
 * For correct behaviour when using these switches, too much static logic is required by the client side, this should all go into the
 * runtime object an only be handled by events.
 *
 * Needs another switch to force a rewrite of an ini file if new tash modules were added in client code
 *
 * Event-Handling
 * --------------
 * One of the main benefits of using Qt is the [Event-System](http://doc.qt.io/qt-5/eventsandfilters.html), which is utilized by the runtime to alter it's
 * behaviour according to user interaction. At the moment, the runtime uses 3 signals to inform all instances which are connected to the runtime via
 * [Signal-Slot Mechanism](http://doc.qt.io/qt-5/signalsandslots.html) about the state of the runtime object. Basically 3 states are possible:
 * - The runtime is running normally, this will be indicated by the signal <code>void running()</code> in each event-loop cycle
 * - The runtime could not find an ini file, so it will write one and let everybody know about it with <code>void iniFileWritten()</code>.
 *   This signal will be emitted just before the destructor of the runtime finishes, as it is the last point in time where the file can possibly be written.
 * - The runtime is about to shutdown. By emitting <code>void shutdown()</code> It signals any other object that it is about to destroy itself,
 *   which should trigger the program termination in the client code in some way.
 *
 * Connecting to a runtime signal is straight forward.
 * @code
 * //Setup signal for shutdown
 * connect(m_runtime.data(), SIGNAL(shutdown()), this, SLOT(stop()));
 * @endcode
 * This will connect the <code>void shutdown()</code> signal to a slot in the client code called <code>void stop()</code>.
 *
 * OS-Signal Handling
 * ------------------
 * In order to use features like OS-signal handling or DBus support, client side apps must follow the design rules recommended by Qt.
 * As a base requirement, the client app must provide a [Qt Event-Loop](http://doc.qt.io/qt-5/eventsandfilters.html)
 * in order to use certain features. However, other features like the property system can be used without the
 * presence of an event loop like the property system.
 *
 * Usage Example
 * -------------
 * Using a runtime object requires the user to adhere to certain design rules to use the runtime to it's full extent.
 * Basically any app which uses libTash should implement a 2 thread approach. The main thread will take care of the Qt-Eventloop
 * execution, while a worker thread executes libTash logic. A client application implements at it's minimum 3 files
 * - main.cpp
 * - backendworker.h
 * - backenworker.cpp
 *
 * The following code snippets represent a minimal example on how to create and use a TashRuntime object in client code:
 * <br><br><b><code>main.cpp</code></b>
 * @code
 * #include "backendworker.h"
 * #include <QCoreApplication>
 * #include <QThread>
 *
 * int main(int argc, char *argv[])
 * {
 *     QCoreApplication a(argc, argv);
 *
 *     auto *pWorkerThread = new QThread();
 *
 *     Q_CHECK_PTR(pWorkerThread);
 *
 *     if ( !pWorkerThread ) {
 *         qDebug() << "Error while creating worker thread object, shutdown gracefully...!";
 *         return EXIT_FAILURE;
 *     }
 *
 *     auto *pWorker = new BackendWorker(argc, argv);
 *
 *     Q_CHECK_PTR(pWorker);
 *
 *     if ( !pWorker ) {
 *         qDebug() << "Error while creating worker object, shutdown gracefully...!";
 *         return EXIT_FAILURE;
 *     }
 *
 *     //Attach processing method to thread
 *     pWorker->moveToThread(pWorkerThread);
 *
 *     // Termination chain
 *     // This is the succession in which the objects should be deinitialized
 *     // 1. worker emits BackendWorker::finished() --> QThread::quit()
 *     // 2. thread emits QThread::finished() --> BackendWorker::deleteLater()
 *     // 3. thread emits QThread::finished() --> QThread::deleteLater()
 *     // 4. thread emits QThread::finished() --> QCoreApplication::quit()
 *
 *     //Worker invoked connections
 *     QObject::connect(pWorker, SIGNAL(finished()), pWorkerThread, SLOT(quit()));
 *
 *     //Thread invoked connections
 *     QObject::connect(pWorkerThread, SIGNAL(finished()), pWorkerThread, SLOT(deleteLater()));
 *     QObject::connect(pWorkerThread, SIGNAL(finished()), pWorker, SLOT(deleteLater()));
 *     QObject::connect(pWorkerThread, SIGNAL(finished()), &a, SLOT(quit()));
 *     QObject::connect(pWorkerThread, SIGNAL(started()), pWorker, SLOT(process()));
 *
 *     //Core application invoked connections
 *     QObject::connect(&a, SIGNAL(aboutToQuit()), pWorker, SLOT(stop()));
 *
 *     //start thread
 *     pWorkerThread->start();
 *
 *     //Enter Qt event loop
 *     return a.exec();
 * }
 * @endcode
 * <br><b><code>backendworker.h</code></b>
 * @code
 * #ifndef BACKENDWORKER_H
 * #define BACKENDWORKER_H
 *
 * #include <QObject>
 *
 * #include "libtash.h"
 *
 * class BackendWorker final: public QObject
 * {
 *     Q_OBJECT
 *
 * public:
 *     explicit BackendWorker(const int argc, char** argv, QObject *parent = 0);
 *     ~BackendWorker();
 *     BackendWorker(const BackendWorker& other) = default;
 *
 * signals:
 *     void error();
 *     void finished();
 *     void running();
 *     void newFrame();
 *     void started();
 *
 * public slots:
 *     virtual void process();
 *     virtual void stop();
 *
 * protected slots:
 *     void handleNewDetections(quint32 idx, quint32 frameidx);
 *     void handleIdle();
 *     void emitFinished();
 *
 * private:
 *     bool initThread();
 *     static void releaseDummy(qint16* data);
 *     void cleanup();
 *
 *     bool                      m_bIsInitialized;
 *     bool                      m_bStop;
 *     const int                 m_argc;
 *     char**                    m_argv;
 *     tashtego::tashtegoRuntime m_pRuntime;
 * };
 *
 * #endif // BACKENDWORKER_H
 * @endcode
 * <br><b><code>backendworker.cpp</code></b>
 * @code
 * #include "backendworker.h"
 *
 * #include <QCoreApplication>
 * #include <QTimer>
 *
 * BackendWorker::BackendWorker(const int argc, char **argv, QObject *parent) :
 *     QObject(parent),
 *     m_bIsInitialized(false),
 *     m_bStop(false),
 *     m_argc(argc),
 *     m_argv(argv)
 * {
 *
 * }
 *
 * BackendWorker::~BackendWorker() {
 * #ifdef TASHVERBOSE
 *     auto dbg = qDebug().noquote();
 *     dbg << "Starting destruction of backendworker...";
 * #endif
 *
 *     cleanup();
 *
 * #ifdef TASHVERBOSE
 *     dbg << "done!\n";
 * #endif
 * }
 *
 * void BackendWorker::process() {
 *
 *     using namespace tashtego;
 *
 * #ifdef TASHVERBOSE
 *     qDebug() << "Entering worker thread init phase...";
 * #endif
 *
 *     m_bIsInitialized = this->initThread();
 *
 *     if ( !m_bIsInitialized || m_pRuntime->doDryRun() || m_pRuntime->doHelpPrint() ) {
 *         QCoreApplication::processEvents();
 *         emit finished();
 *         return;
 *     }
 *
 * #ifdef TASHVERBOSE
 *     qDebug() << "Entering worker thread processing phase...";
 * #endif
 *
 *     auto waitTime = m_pRuntime->property("duty_cycle_break").toUInt();
 *
 *     forever {
 *
 *         if ( m_bStop ) { break; }
 *
 *         helper::TashWaiter::msecWait(waitTime);
 *         QCoreApplication::processEvents();
 *         emit running();
 *
 *         qDebug() << "New cycle...";
 *     }
 *
 * #ifdef TASHVERBOSE
 *     qDebug() << "Terminating worker thread...";
 * #endif
 *
 *     emitFinished();
 * }
 *
 * void BackendWorker::stop() {
 *     m_bStop = true;
 * }
 *
 * void BackendWorker::handleNewDetections(quint32 idx, quint32 frameidx) {
 *     using namespace tashtego;
 *     Q_UNUSED(idx);
 *     Q_UNUSED(frameidx);
 * }
 *
 * void BackendWorker::handleIdle() {
 * #ifdef TASHVERBOSE
 *     qDebug() << "Catched idlce cycle!";
 * #endif
 * }
 *
 * void BackendWorker::emitFinished() {
 *     emit finished();
 * }
 *
 * bool BackendWorker::initThread() {
 *
 *     using namespace tashtego;
 *
 *     //Initial app values
 *     const QString appname = "runtime_test";
 *     const qint32  cams    = 1;
 *     const QString path    = QString("../cfg/%1.ini").arg(appname);
 *
 *     //Get a runtime instance
 *     m_pRuntime = core::TashRuntime::getRuntimeObject(appname,
 *                                                      path,
 *                                                      cams,
 *                                                      m_argc,
 *                                                      m_argv);
 *
 *     Q_ASSERT(!m_pRuntime.isNull());
 *     Q_CHECK_PTR(m_pRuntime.data());
 *
 *     //Connect runtime to shutdown routine
 *     connect(m_pRuntime.data(), SIGNAL(shutdown()), this, SLOT(stop()));
 *
 *     //Initialize runtime
 *     auto hasValidRuntime = m_pRuntime->init();
 *
 *     if ( hasValidRuntime ) {
 *         //Do further libTash initialization here
 *     } else {
 * #ifdef TASHVERBOSE
 *         qDebug() << "App has no ini-file, creating one and exit!";
 * #endif
 *     }
 *
 *     return hasValidRuntime;
 * }
 *
 * void BackendWorker::releaseDummy(qint16 *data) {
 *     Q_UNUSED(data)
 * }
 *
 * void BackendWorker::cleanup() {
 * #ifdef TASHVERBOSE
 *     qDebug() << "Trigger runtime destruction...";
 * #endif
 *     if ( !m_pRuntime.isNull() ) {
 *         auto* pR = m_pRuntime.data();
 *         delete pR;
 *         pR = nullptr;
 *     }
 * }
 * @endcode
 * When compiling this code the Qt [meta-object compiler(moc)](http://doc.qt.io/qt-5/moc.html) must be invoked before actual compling.
 * @code
 * ~/tashtego/src/tashruntime_test/$> make distclean && qmake && make
 * @endcode
 * Will compile the project and create a binary in <code>~/tashtego/bin</code>. The actual application
 * can then be started by executing
 * @code
 * ~/tashtego/bin/$> ./tashruntime_test
 * @endcode
 * The first run of the project will cause the application to look for an ini file in <code>~/tashtego/cfg/</code>.
 * If there is no such file present the application create one based on libTash objects which are instanciated in the client code.
 * This the output of the first run of the test application:
 * @code{.sh}
 * ~/tashtego/bin/$> ./tashruntime_test
 * Entering worker thread init phase...
 * Creating the tashtego runtime instance...
 *
 * tashruntime_test build with libTash v0.1.908
 * Developed for:
 * Alfred Wegener Institut
 * Ocean Acoustics Group (OZA)
 *
 * Developers:
 * Dr. Daniel P. Zitterbart
 * Michael Flau
 *
 * Dump of parsed command line arguments:
 *
 * Using absolute path to ini file  "/home/tashtego/src/tashtego_dev/cfg/tashruntime_test.ini"
 * No config file found, creating default config file at ' "../cfg/tashruntime_test.ini" '
 * App has no ini-file, creating one and exit!
 * Trigger runtime destruction...
 * Application name:  "tashruntime_test"
 * Inifilepath:  "../cfg/tashruntime_test.ini"
 * Cameras:  1
 * Factory object created!
 * Got  6  factory creatable modules available!
 * Examining module  "TashDBusConnector"
 * Examining module  "TashTime"
 * Examining module  "TashWaiter"
 * Examining module  "TashFileInfo"
 * Examining module  "TashCLIParser"
 * Examining module  "TashObject"
 * Factory object destroyed!
 * Start writing default ini-file...
 * Writing default ini-file settings for  "tashruntime_test"  application
 * Write camera specific settings...with  0  elements!
 * Ini-file successfully written!
 * Starting destruction of backendworker... done!
 * ~/tashtego/bin/$>
 * @endcode
 * @note
 * This output was generated with the <code>TASHVERBOSE</code> macro enabled
 *
 * The genrated ini-file will have a similar look to the following example:
 * <br><b><code>tashruntime_test.ini</code></b>
 * @code{.ini}
 * [tashruntime_test]
 * Camera_0\data_type=short [string]
 * Camera_0\framerate_in_Hz=5 [integer]
 * Camera_0\frames_per_map=5 [integer]
 * Camera_0\image_height=576 [integer]
 * Camera_0\image_width=7200 [integer]
 * Runtime\dry_run_grace_time=10 [integer]
 * Runtime\enable_OS_signal_handling=true [bool]
 * Runtime\inifilepath=../cfg/runtime_test.ini [string]
 * Runtime\max_worker_threads=10 [integer]
 * Runtime\use_dbus=true [bool]
 * @endcode
 *
 * With the ini-file present, the application output looks like the following example (here with the dry run switch enabled):
 * @code{.sh}
 * ~/tashtego/bin/$> ./tashruntime_test
 * Entering worker thread init phase...
 * Creating the tashtego runtime instance...
 *
 * runtime_test build with libTash v0.1.908
 * Developed for:
 * Alfred Wegener Institut
 * Ocean Acoustics Group (OZA)
 *
 * Developers:
 * Dr. Daniel P. Zitterbart
 * Michael Flau
 *
 * Found new single flag  "-n"
 * Trimmed single flag is  "n"
 *
 * Dump of parsed command line arguments:
 *
 * Single flag:  'n'
 * Multi flag:  "dry"
 * Has value:  false
 * Value:  QVariant(Invalid)
 *
 * Using absolute path to ini file  "/home/tashtego/src/tashtego_dev/cfg/runtime_test.ini"
 * Config file found, reading settings into property system...
 * Parse general setting:  "data_type"
 * Parse general setting:  "framerate_in_Hz"
 * Parse general setting:  "frames_per_map"
 * Parse general setting:  "image_height"
 * Parse general setting:  "image_width"
 * Parse general setting:  "dry_run_grace_time"
 * Parse general setting:  "enable_OS_signal_handling"
 * Parse general setting:  "inifilepath"
 * Parse general setting:  "max_worker_threads"
 * Parse general setting:  "use_dbus"
 * Setting camera property:  "Camera_0-data_type"
 * Setting camera property:  "Camera_0-framerate_in_Hz"
 * Setting camera property:  "Camera_0-frames_per_map"
 * Setting camera property:  "Camera_0-image_height"
 * Setting camera property:  "Camera_0-image_width"
 * Initializing Signalhandlers...
 * Setting up OS signal handling for Linux systems...
 * Setting signalhandler for SIGHUP...
 * Setting signalhandler for SIGTERM...
 * Setting signalhandler for SIGINT...
 * Installed signal handlers for Linux systems...
 * Dryrun enabled, initiate shutdown in  10  seconds...
 * Trigger runtime destruction...
 * Starting destruction of backendworker... done!
 * ~/tashtego/bin/$>
 * @endcode
 */
class TashRuntime: public QObject {

    Q_OBJECT

    /**
     * @brief The tashRuntimeStates enum
     * Internal helper class
     */
    enum class tashRuntimeStates : std::uint8_t {
        PRINT_HELP              = 1,
        DRY_RUN                 = 2,
        REWRITE_INI             = 3,
        USE_OS_SIGNALS          = 4,
        USE_DBUS                = 5,
        ALTERNATE_INI_FILE      = 6
    };

    /**
     * @brief TashRuntime
     *
     * Default constructor, to instanciate a runtime object without
     * support of command line arguments. Creating an instance using this constructor
     * will cause the application to ignore any possible command line arguments.
     * @note
     * If this constructor is used, the application will require it's ini-file,
     * in it's default location. No other path will be accepted by the client application.
     * @param applicationName
     * Name of the client application
     * @param inifile
     * Default location and name of ini-file.
     * @param cameras
     * The number of cameras, which will utilized by the client application
     * @param parent
     * Parent instance, from where runtime instance is created
     */
    explicit TashRuntime(const QString& applicationName,
                         const QString& inifile,
                         const quint32& cameras,
                         QObject *parent = 0);

    /**
     * @brief TashRuntime
     *
     * Constructor to use if the utilization of command line arguments
     * is required. Apart from that, the behaviour of runtime instanciation
     * is the same. For further infos on command line usage, read manual page of
     * TashRuntime.
     * @param applicationName
     * Name of the client application
     * @param inifile
     * Default location and name of ini-file
     * @param cameras
     * The number of cameras, which will utilized by the client application
     * @param argc
     * Number of command line arguments
     * @param argv
     * Command line arguments as pointer to list of char pointers
     * @param parent
     * Parent instance, from where runtime instance is created
     */
    TashRuntime(const QString& applicationName,
                const QString& inifile,
                const quint32& cameras,
                int argc,
                char** argv,
                QObject *parent = 0);

    /**
     * @brief operator =
     *
     * Implementation of assignment operator, to ensure singleton use od TashRuntime
     * in each client application. Declaring the assignment operator in private space,
     * basically renders it unusable for client usage.
     * @param old
     * Reference to rvalue TashRuntime instance
     * @return
     * Reference to self
     */
    const TashRuntime &operator= (const TashRuntime &old);

public:

    /**
     * @brief The ConfType enum
     *
     * This public enums defines placeholders for different datatypes,
     * in order to let the runtime know, of which type a property is.
     * The placeholder names are self-explaining.
     */
    enum ConfType : unsigned short {
        String,
        Character,
        Integer,
        Double,
        Float,
        Boolean,
        Unsigned,
        Long,
        LongLong,
        UnsignedLongLong,
        Short,
        UnsignedShort,
        Unknown
    };

    /**
     * @brief getRuntimeObject
     *
     * If called for the first time, this method will create a runtime object and store it in a
     * static smart pointer which is available throughout the application's lifecycle.
     * Any further call of this method, returns a reference to the very same object, to ensure
     * that no second instance of the runtime can be created.
     * @param applicationName
     * Name of the client application
     * @param inifile
     * Default location and name of ini-file
     * @param cameras
     * The number of cameras, which will utilized by the client application
     * @return
     * Returns smartpointer to static runtime instance s_runtimeInstance
     */
    static QPointer<core::TashRuntime> getRuntimeObject(const QString& applicationName,
                                                        const QString& inifile,
                                                        const quint32& cameras);


    /**
     * @brief getRuntimeObject
     *
     * This method has the same behaviour as it's pendant, with respect to
     * the utilization of the commandline.
     * @param applicationName
     * Name of the client application
     * @param inifile
     * Default location and name of ini-file
     * @param cameras
     * The number of cameras, which will utilized by the client application
     * @param argc
     * Number of command line arguments
     * @param argv
     * Command line arguments as pointer to list of char pointers
     * @return
     * Returns smartpointer to static runtime instance s_runtimeInstance
     */
    static QPointer<core::TashRuntime> getRuntimeObject(const QString& applicationName,
                                                        const QString& inifile,
                                                        const quint32& cameras,
                                                        int argc,
                                                        char** argv);
	/**
	 * @brief ~TashRuntime
     * Destructor called at end of runtime lifecycle.
     *
     * Two different behaviours will take place based on the existence of the
     * client applications ini-file. If an ini file is present, the destructor
     * will simply destroy all member varables and ecit. Otherwise the runtime
     * will write a new ini-file at the specified ini-file location, which usually
     * is <code>~/tashtego/cfg/</code>. All properties will be written which could
     * be retrieved during introspection phase of the runtime's lifecycle.
	 */
	virtual ~TashRuntime();


signals:

    /**
     * @brief shutdown
     *
     * Signal which is emitted, when the runtime
     * is in need of a full shutdown.
     */
    void shutdown();

    /**
     * @brief running
     *
     * Signal which is emitted each processing cycle as a 'still-alive' signal,
     * which can be used by client applications to make sure, the runtime remains
     * intact throughout the app's lifecycle.
     */
    void running();

    /**
     * @brief iniFileWritten
     *
     * Signal which is being emitted, when an initial ini-file was written to
     * specifed daefault location.
     */
    void iniFileWritten();

public slots:

    /**
     * @brief getInputSourceHeight
     * Convenience getter function to easily access the height of input image data
     * @param cameraIdx
     * Index of camera of which data is being accessed
     * @return
     * Height of input image footage as float value
     */
    virtual qint32 getInputSourceHeight(const qint32& cameraIdx = 0) const;

    /**
     * @brief getInputSourceWidth
     * Convenience getter function to easily access the width of input image data
     * @param cameraIdx
     * Index of camera of which data is being accessed
     * @return
     * Width of input image footage as float value
     */
    virtual qint32 getInputSourceWidth(const qint32& cameraIdx = 0) const;

    /**
     * @brief getInputSourceFPS
     * Convenience getter function to easily access the FPS value of input image data
     * @param cameraIdx
     * Index of camera of which data is being accessed
     * @return
     * FPS of input image source as float value
     */
    virtual qint32 getInputSourceFPS(const qint32& cameraIdx = 0) const;

    /**
     * @brief getInputSourceDatatype
     * Convenience getter function to easily access the datatype of input image data
     * @param cameraIdx
     * Index of camera of which data is being accessed
     * @return
     * Input datatype of input image footage as QString
     */
    virtual QString getInputSourceDatatype(const qint32& cameraIdx = 0) const;

    /**
     * @brief getInputSourceFPM
     * Convenience getter function to easily access the frames per map count of input image data
     * @param cameraIdx
     * Index of camera of which data is being accessed
     * @return
     * FPM count of input image footage as float
     */
    virtual qint32 getInputSourceFPM(const qint32& cameraIdx = 0) const;

    /**
     * @brief getNumberInputSources
     *
     * Simple getter which can be called to retrieve
     * the number of cameras being used.
     * @return
     * Number of cameras as integer value
     */
    virtual int getNumberOfInputSources() const;

    /**
     * @brief listRuntimePropeties
     *
     * Debbuging method to print all found runtime properties to standard output.
     */
    virtual void listRuntimeProperties();

    /**
     * @brief property
     *
     * Getter method, to retrieve an ini-file property from runtime.
     * If the property exists, it will be returned as a [QVariant](http://doc.qt.io/qt-5/qvariant.html) object,
     * which than can be transformed to it's proper datatype by using
     * QVariant toX() gettter methods.
     * @warning
     * If the input property name is not present, an empty
     * QVariant object will be returned. Returned QVarionts should
     * always be checked for validity by checking QVariant::isValid() method.
     * @param name
     * Property name as QString
     * @return
     * QVariant object
     */
    virtual QVariant property(const QString& name);

    /**
     * @brief setProperty
     *
     * Simple setter method, to define a new runtime property. This methods is usually
     * incorporated along with the default setDefaultProperties() methods, which every
     * class which inherits from TashObject should implement. Calling this method
     * will define a new property in the meta-object space. The property value
     * stored in <code>value</code> can then be accessed by calling <code>TashRuntime::property()</code>
     * with it's associated property identifier specified in <code>name</code>
     * @param name
     * Property identifier associated with <code>value</code>
     * @param value
     * Property value to be stored in meta-object space. Any value which is accepted by
     * [QVariant](http://doc.qt.io/qt-5/qvariant.html), can be passed to this method.
     * @return
     * <code>true</code> on success, <code>false</code> if there was an error.
     */
    virtual bool setProperty(const QString& name, const QVariant& value);

    /**
     * @brief setProperties
     *
     * Simple wrapper for TashRuntime::setProperty(). It takes a map consisting of QStrings as
     * identifiers and QVariants as values and stores each id-value pair in meta-object space.
     * @param properties
     * QMap containing all id-property pairs to be set. IDs must be unique strings.
     * @return
     * <code>true</code> on success, <code>false</code> if there was an error.
     */
    virtual bool setProperties(QMap<QString, QVariant> properties);

    /**
     * @brief runtimePropertyExists
     *
     * Simple getter method, to check whether there already exists a property with the
     * passed identifer string.
     * @param key
     * ID as string reference to be searched for within meta-object space.
     * @return
     * <code>true</code> if property is already defines, <code>false</code> if there is no such property.
     */
    virtual bool runtimePropertyExists(const QString &key) const;

    /**
     * @brief init
     *
     * Wrapper function, to sum up all initialization logic of the runtime class
     * into one entity. All features which the runtime offers are invoked within
     * this function:
     * - Support of CLI FLags
     * - DBus utilization
     * - OS signal handling
     */
    virtual bool init();

    /**
     * @brief exploreLibTashModuleProperties
     *
     * Debugging method to list all module properties found to
     * given output. If DBus usage is invoked, everthing will be outputted to TashLogger.
     * Otherwise stdout will be used to list all properties.
     */
    virtual void exploreLibTashModuleProperties();

    /**
     * @brief getPropertyNamesContaining
     *
     * Getter method which can be used to search all properties which contain a certain
     * character string within it's identifier. This utility function is helpful if
     * the exact name of the property can not be recalled.
     * @param stringpart
     * String to be searched for in meta-object space.
     * @param cameraID
     * Camera index to specifiy in which camera sub space is to be searched
     * @return
     * Returns a list of all identfiers which match with the input string.
     */
    virtual QStringList getPropertyNamesContaining(const QString& stringpart, const quint32& cameraID);

    /**
     * @brief getPropertyNameContaining
     *
     * Getter method to get the first property which meta-object space,
     * which matches the string part. Further matches are ignored.
     * @param stringpart
     * String to be searched for in meta-object space.
     * @param cameraID
     * Camera index to specifiy in which camera sub space is to be searched
     * @return
     */
    virtual QString getPropertyNameContaining(const QString& stringpart, const quint32& cameraID);

    /**
     * @brief getLibTashVersion
     *
     * Returns a QString stating the current version of libTash in
     * <code>*.*.*</code> format. First number is major version. Second
     * number is minor version. Third numbers resembles to build index.
     * @return
     * Version number as QString
     */
    QString getLibTashVersion();

    /**
     * @brief getCredits
     *
     * Returns a credits statement, containing all involved
     * developers.
     * @return
     * Credits string as QString
     */
    QString getCredits();

    /**
     * @brief TashRuntime::genID
     *
     * Generates a unique random identifier, by which each
     * instance can be identified at runtime. This function is not reentrant
     * and not thread safe. Use TashRuntime::genIDPara() for a thread safe version.
     * @warning
     * using this function in code section which are executed parallely, can yield
     * to equal id numbers which must be avoided to keep the object index unique for
     * each created instance.
     * @return
     * Unique identfier as qint64 number.
     */
    static qint64 genID();

    /**
     * @brief genIDPara
     *
     * Generates a unique random identifier, by which an object can be identified.
     * This is the read safe version of the unique number generation. Each call to
     * this function from different threads is guarded by a mutex.
     * @param mutex
     * Reference to in built runtime mutex.
     * @return
     * Unique identfier as qint64 number.
     */
    static qint64 genIDPara(QMutex* mutex = nullptr, qint64 seed = 0);

    /**
     * @brief configFileExists
     *
     * Simple getter to request runtime state in terms of ini-file presence.
     * @return
     *<code>true</code> if ini-file is present, otherwise <code>false</code>.
     */
    virtual bool configFileExists();

    /**
     * @brief isInitialized
     *
     * Simple getter which returns the initialization state of the runtime.
     * @note
     * This is probably one of the most important functions in libTash. Every class
     * derived from TashObject implements this method. Always call this function from
     * an assert or similar checking construct before using the instance of a libTash object.
     * @return
     * <code>true</code> if ini-file is present, otherwise <code>false</code>.
     */
    virtual bool isInitialized() const;

    /**
     * @brief doDryRun
     *
     * Simple getter to check whether the dry run flag was enabled via CLI.
     * @warning
     * This will only work if application support command line argument usage.
     * @return
     * <code>true</code> if dry-run flag was set, otherwise <code>false</code>.
     */
    virtual bool doDryRun();

    /**
     * @brief doHelpPrint
     *
     * Simple getter to check whether the user requested to print the help dialog.
     * @return
     * <code>true</code> if help flag was set, otherwise <code>false</code>.
     */
    virtual bool doHelpPrint();

    /**
     * @brief doIniFileRewrite
     *
     * Simple getter function which can be checked to acknowledge wheter user wants
     * the ini-file to be rewritten.
     * @return
     * <code>true</code> if a rewrite was requested, <code>false</code> if no rewrite
     * was requested.
     */
    virtual bool doIniFileRewrite();

    /**
     * @brief qMetaTypeToConfType
     *
     * Getter method to retrieve the associated ConfType value from it's QVariant pendant.
     * This method is mostly used internally.
     * @todo
     * It's is not necessary to expose this method to client space. Consider moving it
     * into protected class space.
     * @param type
     * QVariant type of which the ConfType pendant is requested.
     * @return
     * ConfType value. If the input QVariant type is not referenced by any ConfType value,
     * the ConfType will be set to ConfType::Unknown.
     * @warning
     * Check return value for ConfType::Unknown, to avoid errors when checking for properties!
     */
    virtual ConfType qMetaTypeToConfType(QVariant::Type type);

    /**
     * @brief registerOpenedMemoryMap
     *
     * Registers an opened mmap to the runtime instance, to keep track of opened maps.
     * @param filename
     * Filename of openend memory map in filesystem
     * @param maptype
     * Type of openend memory map. By this the internal datatype if the
     * map structre is supporsed to be referenced.
     * @return
     * Descriptor as QString, which must be kept during runtime, to unregister
     * maps in the end of an libTash application's lifecycle.
     * @warning
     * Check returned string for validity, e.g. with QString::isEmpty(), this will
     * indicate, if map was correctly registered or not.
     */
    virtual QString registerOpenedMemoryMap(const QString& filename, const QString &maptype);

    /**
     * @brief unregisterClosedMemoryMap
     *
     * Unregisters a memory map from runtime by passing the descriptor to
     * method which was retrieved when TashRuntime::registerOpenedMemoryMap() was called.
     * @param descriptor
     * Decriptor of open memory map as QString
     * @return
     * <code>true</code> if deregistration was successful, otherwise <code>false</code>.
     */
    virtual bool unregisterClosedMemoryMap(const QString& descriptor);

    /**
     * @brief hasOpenMemoryMaps
     *
     * Simple boolean getter will state whether there are open memory maps or not.
     * @return
     * <code>true</code> if there are open mmaps, otherwise <code>false</code>.
     */
    virtual bool hasOpenMemoryMaps();

    /**
     * @brief getOpenMemoryMaps
     *
     * Getter method to retrieve a list of all open memory maps.
     * @return
     * List of open mmaps as QStringList object.
     * @note
     * To avoid retrieving an empty call TashRuntime::hasOpenMemoryMaps()
     * first, before using this method.
     */
    virtual QStringList getOpenMemoryMaps();

    /**
     * @brief hasOpenMemoryMapRegistered
     * Simple boolean checker to look up a registered name for an
     * openen memory map.
     * @param strref
     * const reference to searched memory map name.
     * @return
     * <code>true</code> if there is such an memory map opened, <code>false</code> if not.
     */
    virtual bool hasOpenMemoryMapRegistered(const QString& strref);

    /**
     * @brief registerObject
     *
     * Register an QObject derived instance to the runtime. This will basically
     * store a reference pointer to the object in an internal list. It can be used to
     * keep track of all instanciated libTash object for final destruction and
     * intermediate referencing.
     * @param className
     * Name of class which is being instanciated.
     * @param instance
     * Reference to class instance as QObject pointer.
     */
    static void registerObject(QString className, QObject* instance);

    /**
     * @brief objectRegistry
     *
     * Getter which returns a map with all registered instance.
     * Single instances are identified by their QString key descriptors.
     * @return
     * QMap containing all registered instances as QObject pointers.
     */
    static QMap<QString, QObject *> objectRegistry();

protected:

    /**
     * @brief hasRuntimeState
     * Sanity function to check whether a specific runtime state has been invoked
     * either by user defined cli arguments, infi-file values of abnormal program
     * execution
     * @param state
     * runtime state which is being checked for
     * @return
     * <code>true</code> on active state, <code>false</code> if state is not active
     */
    inline bool hasRuntimeState(const tashRuntimeStates& state) {
        if ( m_RuntimeStates.empty() ) return false;
        for( auto listedState: m_RuntimeStates) {
            if ( listedState == state ) return true;
        }
        return false;
    }

    /**
     * @brief setupCLIHandling
     * Initialize cli handling, if cli arguments were passed
     * to ctor of backendworker.
     *
     * Steps for successful initialization are
     * - Definition of default runtime arguments which are always available
     * - Creating a valid tashtego::helper::TashCLIParser instance
     * - Handling flags according to their meaning/behaviour
     * @return
     * <code>true</code> on success, <code>false</code> if an error occured.
     */
    bool setupCLIHandling();

    /**
     * @brief setupINIFileHandling
     *
     * Method defines how access to ini-files is granted to runtime, and by that
     * to the whole tashtego scope.
     * @return
     * <code>true</code> on success, <code>false</code> if an error occured.
     */
    bool setupINIFileHandling();

    /**
     * @brief tashtegoMessageHandler
     *
     * Static custom message handler, implementing logic to print all application messages
     * to DBus rather than to stdout.
     * @param type
     * Describes the importance of the message. Types can be:
     * - QtMsgType::QtDebugMsg
     * - QtMsgType::QtInfoMsg
     * - QtMsgType::QtWarningMsg
     * - QtMsgType::QtCriticalMsg
     * - QtMsgType::QtFatalMsg
     * @param context
     * The message context describes where the message invocation took place. It is internally
     * used to specify the filename and linenumber of the message invocation.
     * @param msg
     * Actual message as const reference to a QString
     */
    static void tashtegoMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

    /**
     * @brief initSignalOSSignalHandling
     *
     * Static method to install custom handling of operating system signals within linux systems. This method will
     * be invoked, if custom signal handling defined by the ini file setting or via CLI-flag.
     * @return
     * Returns an integer value, which states the error code of the execution,
     * following error handling in C style fashion. If 0 is returned, the function executed normally, otherwise an error occured,
     * which can be checked with perror()
     */
    static int initOSSignalHandling();

private:
    /**
     * @brief internalTypeDescriptor
     *
     * Helper struct which internally describes a property
     * type by name QVariant type and ConfType.
     */
    typedef struct itd {

        itd():
            name(""),
            value(QVariant()),
            type(ConfType::Unknown) {}

        itd(QString n, QVariant v, ConfType t):
            name(n),
            value(v),
            type(t) {}

        QString name;
        QVariant value;
        ConfType type;
    } internalTypeDescriptor;

    /**
     * @brief fastAccessCameraValues
     *
     * Helper struct, to combine essential information of each
     * camera for fast access via runtime methods.
     */
    typedef struct facv{

        facv():
            cameraImageHeight(),
            cameraImageWidth(),
            cameraFPS(),
            cameraFPM(),
            cameraImageDatatype() {}

        facv(internalTypeDescriptor imgHeight,
             internalTypeDescriptor imgWidth,
             internalTypeDescriptor imgFPS,
             internalTypeDescriptor imgFPM,
             internalTypeDescriptor imgDT):
            cameraImageHeight(imgHeight),
            cameraImageWidth(imgWidth),
            cameraFPS(imgFPS),
            cameraFPM(imgFPM),
            cameraImageDatatype(imgDT){}

        internalTypeDescriptor cameraImageHeight;
        internalTypeDescriptor cameraImageWidth;
        internalTypeDescriptor cameraFPS;
        internalTypeDescriptor cameraFPM;
        internalTypeDescriptor cameraImageDatatype;
    } fastAccessCameraValues;

    /**
     * @brief propertyContainer
     *
     * Internal representation of a property.
     * A porperty has a name and a vector of
     * internalTypeDescriptor's which describe the property
     */
    typedef struct {
        QString moduleName;
        QVector<internalTypeDescriptor> properties;
    } propertyContainer;

    /**
     *@brief cliArgs
     *
     * Helper container, to store argument count
     * and reference to CLI arguments, in one place.
     */
    struct cliArgs {
        qint32 argc;
        char** argv;
    };

    using TashRawCLI = struct cliArgs; //!< Name substitute for easier handling

    /**
     * @brief writeDefaultConfigFile
     *
     * Invokes the creation of a default ini-file at the location
     * specified in <code>configFilePath</code>. The name of the ini-file
     * will be the name specfied in <code>applicationName</code>. To write camera specific settings,
     * the number of cameras must also be specified in numberOfCameras. All found runtime properties
     * must be passed as <code>QVector<propertyContainer></code>.
     * @param applicationName
     * Name of client application.
     * @param configFilePath
     * Path where the ini-file is supposed to reside
     * @param numberOfCameras
     * Number of cameras used by client application.
     */
    virtual void writeDefaultConfigFile(const QString  applicationName,
                                        const QString  configFilePath,
                                        const quint32& numberOfCameras,
                                        QVector<propertyContainer> &properties);

    /**
     * @brief writeRuntimeSpecificSettingsToFile
     *
     * Method writes all runtime properties to ini-file. The method is only concerned with
     * runtime related properties, not with general or module related properties.
     * @param applicationName
     * Name of client application.
     * @param configFilePath
     * Path where the ini-file is supposed to reside.
     * @param numberOfCameras
     * Number of cameras used by client application.
     */
    virtual void writeRuntimeSpecificSettingsToFile(const QString& applicationName,
                                                    const QString& configFilePath,
                                                    const quint32& numberOfCameras);

    /**
     * @brief readConfigFile
     *
     * Wrapper function to read in ini-file settings for runtime and for all
     * libTash modules which are specified in ini-file AND instanciated by client application.
     * Internally TashRuntime::parseCameraSettings() and TashRuntime::parseGeneralSettings() are
     * called to read in both property types. The runtime instance will store all properties
     * internally, ready for retrieval.
     * @return
     * <code>true</code> if ini-file was read successful otherwise, <code>false</code>
     */
    virtual bool readConfigFile();

    /**
     * @brief parseLibTashModuleProperties
     *
     * Method to parse properties for modules which are associated with a camera which is used.
     * Even if the specific application does not utilize a camera directly, all properties whill
     * be parsed for camera0 space.
     * @param key
     * List of key identifers, which are the descriptors of each property defined in ini-file.
     */
    virtual void parseLibTashModuleProperties(const QStringList &key);

    /**
     * @brief parseTashRuntimeProperties
     *
     * Method to parse all runtime related properties from ini-file. All runtime properties
     * are defined by an initial runtime tag which is specified in tashtego::TASHGENERALGROUP
     * constant.
     * @param keys
     * List of key identifers, which are the descriptors of each property defined in ini-file.
     */
    virtual void parseTashRuntimeProperties(const QStringList &keys);

    /**
     * @brief configFileExists
     *
     * Simple getter function which checks the existence of an ini-file.
     * @param iniFilePath
     * Path to ini-file location as QString reference.
     * @return
     * <code>true</code> if file exists, otherwise <code>false</code>
     */
    virtual bool configFileExists(const QString& iniFilePath);

    /**
     * @brief configFieldExist
     *
     * Simple getter to check whether an settings field exists in ini-file.
     * @param key
     * Key descriptor of property
     * @param group
     * If the property group of the field is known, it can be specified here
     * @return
     * <code>true</code> if file exists, otherwise <code>false</code>
     */
    virtual bool configFieldExist(const QString &key, const QString &group = "");


    /**
     * @brief createConfigEntry
     *
     * Helper function format entries to ini-file into correct layout.
     * @param value
     * Value to be written as QVariant
     * @param type
     * Datatype of passed values as TashRuntime::ConfType
     * @return
     * Ini-file entry as QString consisting of the value itself and it's datatype
     * in brackets appended.
     */
    virtual QVariant createConfigEntry(QVariant value, ConfType type);

    /**
     * @brief toTypedQVariant
     *
     * Returns a QVariant object, which holds the input value with the correct
     * datatype setting. Internally used to reshape property values to correct
     * format.
     * @param value
     * Value as QVariant object
     * @param type
     * Datatype description as TashRuntime::ConfType
     * @return
     * QVariant object with correctly set datatype.
     */
    virtual QVariant toTypedQVariant(QVariant value, ConfType type);

    /**
     * @brief parseRequiredLibTashModuleProperties
     *
     * This method requests all module properties from all libTash modules which are registered to the
     * meta-object space. In order to do this the default ctor of each libTash class used is called.
     * By that, all modules which need to define ini-file settings declare default properties
     * in their ctor call, which than be parsed by the runtime. For the automatic construction and
     * destruction of each module tashtego::core::TashFactory is utilized.
     * @param modulePropertyList
     * Reference to Qvector container to be used for all properties which need to be written to
     * ini-file by libTash modules.
     * @return
     * <code>true</code> on success, <code>false</code> if an error occured.
     */
    virtual bool parseRequiredLibTashModuleProperties(QVector<propertyContainer> &modulePropertyList);

    /**
     * @brief fillFastAccessValues
     *
     * Method should be called from init() routine, to set up the fast access structure with values which
     * will be needed very often in a lot of different parts of libTash.
     * @return
     * <code>true</code> on success, <code>false</code> if an error occured.
     */
    virtual bool fillFastAccessValues();

    //Static members
    static QPointer<TashRuntime> s_runtimeInstance;                         //!< Smartpointer to runtime instance

    //Members
    bool                                    m_bConfigFileExists;            //!< Boolean flag denotes if ini-file exists or not
    bool                                    m_bIsInitialized;               //!< Boolean flag denotes whether runtime instance is initialized
    bool                                    m_bUseDBus;                     //!< Boolean flag denotes whether runtime utilizes DBus for message propagation
//    bool                                    m_bEnableOSSignalHandling;      //!< Boolean flag denotes whether runtime uses custom signal handler
    QString                                 m_ApplicationName;              //!< QString which stores name of client application
    QString                                 m_IniFilePath;                  //!< QString whcih stores path to ini-file location
    quint32                                 m_NumberOfCameras;              //!< Number of used cameras as an unsigned integer as specified while ctor invocation
    QSettings*                              m_pSettings;                    //!< QSettings object which accesses application's ini-file
    tashtego::core::TashOSSignalHandler*    m_pSIGHandler;                  //!< tashtego::core::TashOSSignalHandler object which is the interface to OS signals
    tashtego::helper::TashCLIParser*        m_pCLIParser;                   //!< Command line argument parser which handles startup changes defined by command line flags
    TashRawCLI                              m_CLIArgs;                      //!< Container struct which stores argument count and arguments in one place
    const QMap<ConfType, QString>           m_ConfTypes;                    //!< Map with all defined config types supported by runtime
    QMap<QString, QVariant>                 m_UserDefinedRuntimeProperties; //!< Map container for storage of user defined runtime poperties
    QStringList                             m_OpenMemoryMaps;               //!< String list with identifiers of all open memory maps
    QVector<tashRuntimeStates>              m_RuntimeStates;                //!< Storage location for all enabled runtime cli flags
    QVector<fastAccessCameraValues>         m_fastAccessValues;             //!< Container for fast access values which will be needed all over the tashtego scope
};

}
typedef QPointer<core::TashRuntime> tashtegoRuntime;
}


#endif //TASHRUNTIME_H


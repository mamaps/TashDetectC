/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashfactory.cpp
 * @author Michael Flau
 * @brief
 * @details
 * @date June 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started June 2015
 */

#include <QString>
#include <QMetaType>
#include <QMutex>
#include <QDebug>

#include "tashfactory.h"

namespace tashtego {

namespace core {


/*
 *
 */
TashFactory* TashFactory::m_pFactory = 0;

/**
 * @brief TashFactory::TashFactory
 * @param parent
 */
TashFactory::TashFactory(QObject *parent) : QObject(parent)
{

}

/**
 * @brief TashFactory::createFactory
 * @return
 */
TashFactory* TashFactory::createFactory() {
    static QMutex mutex;

    mutex.lock();
    if (!m_pFactory )
        m_pFactory = new TashFactory();
    mutex.unlock();

    qDebug() << "Factory object created!";

    return m_pFactory;
}

/**
 * @brief TashFactory::destroyFactory
 */
void TashFactory::destroyFactory() {
    static QMutex mutex;

    mutex.lock();
    if ( m_pFactory ) {
        delete m_pFactory;
        m_pFactory = 0;
    }
    mutex.unlock();

    qDebug() << "Factory object destroyed!";
}

/**
 * @brief TashFactory::getQMetaTypeID
 * @param name
 * @return
 */
int TashFactory::getQMetaTypeID(QString &name) {
    int id = QMetaType::type( name.toStdString().c_str() );

    if( id == 0 ) {
        qDebug() << "Error can't find object with name: " << name;
        emit creationError(name);
    }

    return id;
}

/**
 * @brief TashFactory::createSharableObject
 * @param name
 * @return
 */
QSharedPointer<TashObject> TashFactory::createSharableObject(QString &name) {
    int id = this->getQMetaTypeID(name);
    return createSafeObjectFromID(id);
}

/**
 * @brief TashFactory::createUnsafeObject
 * @param name
 * @return
 */
TashObject* TashFactory::createUnsafeObject(QString &name) {
    int id = this->getQMetaTypeID(name);
    return createUnsafeObjectFromID(id);
}

/**
 * @brief TashFactory::createSafeObjectFromID
 * @param id
 * @return
 */
QSharedPointer<TashObject> TashFactory::createSafeObjectFromID(const int &id) {

    if ( id <= 0 ) {
        qCritical() << "Given ID must be greater than 0!";
        emit creationError(id);
        return QSharedPointer<TashObject>(nullptr);
    }

    emit objectCreated(id);
    return QSharedPointer<TashObject>(
                static_cast<TashObject*>( QMetaType::create(id) ), TashFactory::releaseDummy );
}

/**
 * @brief TashFactory::createUnsafeObjectFromID
 * @param id
 * @return
 */
TashObject* TashFactory::createUnsafeObjectFromID(const int &id) {

    if (id <= 0 ) {
        emit creationError(id);
        return nullptr;
    }

    emit objectCreated(id);
    return static_cast<TashObject*>( QMetaType::create(id) );
}

void TashFactory::releaseDummy(TashObject* obj) {
    Q_UNUSED(obj);
}

}
}


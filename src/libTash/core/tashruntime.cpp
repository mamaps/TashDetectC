/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashruntime.cpp
 * @author Michael Flau
 * @brief
 * @details
 * @date September 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started September 2015
 */

//Header
#include "tashruntime.h"
//Linux
#ifdef Q_OS_LINUX
#include <signal.h>
//Windows
#elif Q_OS_WIN32

#endif

//buildnumber
#include "../build.h"

//Qt
#include <QFileInfo>
#include <QSettings>
#include <QDebug>
#include <QPointer>
#include <QMetaObject>
#include <QMetaProperty>
#include <QMetaType>
#include <QMutex>
#include <QMutexLocker>
#include <QCoreApplication>
#include <QTimer>
#include <QRegularExpression>
#include <QVector>
#include <QSharedPointer>
#include <QStringList>
#include <QStringBuilder>
#include <QSettings>
#include <QMessageLogContext>
#include <QVectorIterator>
#include <QMapIterator>
#include <QDir>

//tashtego
#include "tashconst.h"
#include "tashobject.h"
#include "../helper/tashtime.h"
#include "../helper/tashfileinfo.h"
#include "../helper/tashcliparser.h"
#include "tashfactory.h"
#include "../cmd/tashdbusconnector.h"
#include "tashossignalhandler.h"

namespace tashtego{
namespace core{

QPointer<TashRuntime> TashRuntime::s_runtimeInstance = QPointer<TashRuntime>(nullptr);

TashRuntime::TashRuntime(const QString& applicationName,
                         const QString& inifile,
                         const quint32& cameras,
                         QObject *parent):
    QObject(parent),
    m_bConfigFileExists(false),
    m_bIsInitialized(false),
    m_bUseDBus(false),
//    m_bEnableOSSignalHandling(false),
    m_ApplicationName(applicationName),
    m_IniFilePath(inifile),
    m_NumberOfCameras(cameras),
    m_pSettings(nullptr),
    m_pSIGHandler(nullptr),
    m_pCLIParser(nullptr),
    m_CLIArgs({0,nullptr}),
    m_ConfTypes({{String           ,"string"},            {Character      ,"char"} ,{Integer      ,"integer"},
                 {Double           ,"double"},            {Float          ,"float"},{Boolean      ,"bool"},
                 {Unsigned         ,"unsigned"},          {Long           ,"long"} ,{LongLong     ,"long_long"},
                 {UnsignedLongLong ,"unsigned_long_long"},{Short          ,"short"},{UnsignedShort,"unsigned_short"},
                 {Unknown          ,"unknown"}}),
    m_UserDefinedRuntimeProperties({}),
    m_OpenMemoryMaps({}),
    m_RuntimeStates({})
{}

TashRuntime::TashRuntime(const QString& applicationName,
            const QString& inifile,
            const quint32& cameras,
            int argc,
            char** argv,
            QObject *parent):
    QObject(parent),
    m_bConfigFileExists(false),
    m_bIsInitialized(false),
    m_bUseDBus(false),
//    m_bEnableOSSignalHandling(false),
    m_ApplicationName(applicationName),
    m_IniFilePath(inifile),
    m_NumberOfCameras(cameras),
    m_pSettings(nullptr),
    m_pSIGHandler(nullptr),
    m_pCLIParser(nullptr),
    m_CLIArgs({argc, argv}),
    m_ConfTypes({{String           ,"string"},            {Character      ,"char"} ,{Integer      ,"integer"},
                 {Double           ,"double"},            {Float          ,"float"},{Boolean      ,"bool"},
                 {Unsigned         ,"unsigned"},          {Long           ,"long"} ,{LongLong     ,"long_long"},
                 {UnsignedLongLong ,"unsigned_long_long"},{Short          ,"short"},{UnsignedShort,"unsigned_short"},
                 {Unknown          ,"unknown"}}),
    m_UserDefinedRuntimeProperties({}),
    m_OpenMemoryMaps({}),
    m_RuntimeStates({})
{}

QPointer<core::TashRuntime> TashRuntime::getRuntimeObject(const QString& applicationName,
                                                          const QString& inifile,
                                                          const quint32& cameras) {
    static QMutex mutex;
    static QMutexLocker locker(&mutex);

    if ( s_runtimeInstance.isNull() ) {
#ifdef TASHVERBOSE
        qDebug() << "Creating the tashtego runtime instance...";
#endif
        s_runtimeInstance = QPointer<TashRuntime>(new TashRuntime(applicationName, inifile, cameras));
    }
    return s_runtimeInstance;
}

QPointer<core::TashRuntime> TashRuntime::getRuntimeObject(const QString& applicationName,
                                                          const QString& inifile,
                                                          const quint32& cameras,
                                                          int argc,
                                                          char** argv) {
    static QMutex mutex;
    static QMutexLocker locker(&mutex);

    if ( s_runtimeInstance.isNull() ) {
#ifdef TASHVERBOSE
        qDebug() << "Creating the tashtego runtime instance with cli support...";
#endif
        s_runtimeInstance = QPointer<TashRuntime>(new TashRuntime(applicationName, inifile, cameras, argc, argv));
    }
    return s_runtimeInstance;
}

TashRuntime::~TashRuntime () {
#ifdef TASHVERBOSE
    qDebug() << "Runtime destruction initiated...";
#endif
    auto forcedRewrite = hasRuntimeState(tashRuntimeStates::REWRITE_INI);
    //Write default ini-file upon runtime destruction,
    //if no file with the specified name was found
    if ( !configFileExists() || forcedRewrite ) {
#ifdef TASHVERBOSE
        qDebug() << "Required app information:";
        qDebug() << "Application name: " << m_ApplicationName;
        qDebug() << "Inifilepath: " << m_IniFilePath;
        qDebug() << "Cameras: " << m_NumberOfCameras;
#endif

        if ( forcedRewrite && QFile::exists(m_IniFilePath) ) {
            auto status = QFile::remove(m_IniFilePath);

            if ( !status ) {
                qCritical() << "Failed to remove old ini file at: " << m_IniFilePath;
            }

        }

        //Call anyway, in case QSettinsg Object exists, true is returned
        if ( !setupINIFileHandling() ) {
            qCritical() << "Failed to setup ini-file handling!";
        }

        QVector<propertyContainer> properties;

        //Retrieve all properties from libTash objects
        if ( !this->parseRequiredLibTashModuleProperties(properties) ) {
            qCritical() << "Error while parsing required modules runtime properties!\n Default ini-file will be written without module properties!";
        }

#ifdef TASHVERBOSE
        qDebug() << "Start writing default ini-file...";
#endif
        //Write ini-file
        this->writeDefaultConfigFile(m_ApplicationName,
                               m_IniFilePath,
                               m_NumberOfCameras,
                               properties);
#ifdef TASHVERBOSE
        qDebug() << "Ini-file successfully written!";
#endif
        emit iniFileWritten();
    }

    if ( hasRuntimeState(tashRuntimeStates::PRINT_HELP ) ) {
        auto list =  m_pCLIParser->getformattedHelpDialog();
        foreach( QString str, list) {
            qDebug().noquote() << str;
        }
    }

    //Reset message handler if dbus logger was used
    if ( m_bUseDBus ) { qInstallMessageHandler(0); }

    //Destroy settings object
    if ( m_pSettings ) { m_pSettings->deleteLater(); }

    //Destory signalhandler object
    if ( m_pSIGHandler ) { m_pSIGHandler->deleteLater(); }

    //Destroy Object registry
    QMapIterator<QString, QObject*> objIt(objectRegistry());

    while (objIt.hasNext()) {
        objIt.next();
        objIt.value()->deleteLater();
    }

    //Destroy cli parser
    if ( m_pCLIParser ) { delete m_pCLIParser; }

#ifdef TASHVERBOSE
    qDebug() << "Finished runtime destruction...";
#endif
}

bool TashRuntime::init() {

    //Set init flag to true as default
    m_bIsInitialized = true;

    // Print credits to stdout/DBus
    {
        auto p = qDebug();
        p.noquote();
        p << this->getCredits();
    }

    //Initialize random number generation with current time
    qsrand(static_cast<uint>(helper::TashTime::getUnixTime()));

    /*
     * CLI Parser initalization
     */
    if ( !setupCLIHandling() ) {
        qCritical() << "Failed to initialize CLI handling, abort!";
        return m_bIsInitialized = false;
    }

    if ( hasRuntimeState(tashRuntimeStates::PRINT_HELP) || hasRuntimeState(tashRuntimeStates::REWRITE_INI) ) {
#ifdef TASHVERBOSE
        qDebug() << "premature return, help requested or forced ini file rewrite...";
#endif
//        emit shutdown();
        return m_bIsInitialized = false;
    }

    /*
     * Setup ini-file handling
     */

    //Check for alternative config file set via CLI
    if ( hasRuntimeState(tashRuntimeStates::ALTERNATE_INI_FILE) ) {
#ifdef TASHVERBOSE
        qDebug() << "Flag for alternative ini file was specified, taking precedence over hard coded!";
#endif
        m_IniFilePath = m_pCLIParser->getFieldValue("ini").toString();
    }

    if ( !setupINIFileHandling() ) {
        qCritical() << "Failed to setup ini-file handling!";
        return m_bIsInitialized = false;
    }

    if ( !configFileExists(m_IniFilePath) ) {
#ifdef TASHVERBOSE
        qDebug() << "No config file found, creating default config file at '" << m_IniFilePath << "'";
#endif
//        emit shutdown();
        return m_bIsInitialized = false;
    } else {
#ifdef TASHVERBOSE
        qDebug() << "Config file found, reading settings into property system...";
#endif
        readConfigFile();
    }


#ifdef TASHVERBOSE
        qDebug() << "Fetching fast access values from property system...";
#endif
    if ( !fillFastAccessValues() ) {
        qCritical() << "Failed to setup fast access structure!";
        return m_bIsInitialized = false;
    }

#ifdef TASHVERBOSE
    qDebug() << "Fast Access check:";
        for ( auto idx = 0; idx < getNumberOfInputSources(); ++idx ) {
            qDebug() << "Camera " << idx;
            qDebug() << "Width: " << getInputSourceWidth(idx);
            qDebug() << "Height: " << getInputSourceHeight(idx);
            qDebug() << "FPS: " << getInputSourceFPS(idx);
            qDebug() << "FPM: " << getInputSourceFPM(idx);
            qDebug() << "Datatype: " << getInputSourceDatatype(idx) << "\n";
        }
#endif

    /**
     * Handle OS Signals
     */
    auto enableOSSignalHandling = hasRuntimeState( tashRuntimeStates::USE_OS_SIGNALS );

    if ( enableOSSignalHandling ) {
#ifdef TASHVERBOSE
        qDebug() << "Flag for os signal handling was specified, taking precedence over ini file setting coded!";
#endif
    } else {
        //Extract ini file setting from meta object system
        enableOSSignalHandling = this->property(TASHGENERAL_HANDLEOSSIGNALSID).toBool();
    }

    //Enable signal handling bz application is desired, only for applications with Qt-Eventloop
    if ( enableOSSignalHandling ) {

        //Initialize OS Signal handling object
        m_pSIGHandler = new tashtego::core::TashOSSignalHandler();
        Q_CHECK_PTR(m_pSIGHandler);

        /*
         * Connect OS signal events to runtime shutdown signal
         * which triggers graceful shutdown of application
         */
        connect(m_pSIGHandler, SIGNAL(shutdown()), this, SIGNAL(shutdown()));

        //Setup custom OS signal handling
        if ( this->initOSSignalHandling() != 0 ) {
            qCritical() << "Error, was not able to bind OS Signals to custom behaviour!";
            return m_bIsInitialized = false;
        }
    }

    if ( hasRuntimeState(tashRuntimeStates::DRY_RUN) ) {
        auto graceTime = this->property(TASHGENERAL_DRYRUN_GRACE_TIME).toInt();
#ifdef TASHVERBOSE
        qDebug() << "Dryrun enabled, initiate shutdown in " << graceTime << " seconds...";
#endif
        QTimer::singleShot(graceTime*1000, this, SIGNAL(shutdown()));
    }

    //Check DBus usage
    m_bUseDBus = hasRuntimeState(tashRuntimeStates::USE_DBUS);

    if ( m_bUseDBus ) {
#ifdef TASHVERBOSE
        qDebug() << "Flag for dbus usage was specified, taking precedence over ini file setting coded!";
#endif
    } else {
        //Extract ini file setting from meta object system
        m_bUseDBus = this->property(TASHGENERAL_USEDBUSID).toBool();
    }

    /*
     * Handle alternative dbus usage
     */
    if ( m_bUseDBus ) {
#ifdef TASHVERBOSE
        qDebug() << "Installing custom DBus messagehandler...";
#endif
        //Install custom message handler
        qInstallMessageHandler(tashtegoMessageHandler);
    }

    /*
     * Check for states which are supposed to terminate application
     */
    for(auto state: m_RuntimeStates) {
        switch(state) {
            case tashRuntimeStates::DRY_RUN:
            case tashRuntimeStates::PRINT_HELP:
            case tashRuntimeStates::REWRITE_INI:
            m_bIsInitialized = false;
                break;
        default:
            m_bIsInitialized = true;
            break;
        }
    }

    return m_bIsInitialized;
}

qint32 TashRuntime::getInputSourceHeight(const qint32 &cameraIdx) const {

    auto status = this->isInitialized();
    Q_ASSERT( status );
    if ( !status ) {
        qCritical() << "Runtime object not initialized, can't proceed!";
        return 0;
    }

    status = cameraIdx >= 0;
    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "Input camera index is negative!";
        return 0;
    }

    status = (cameraIdx < this->getNumberOfInputSources());
    Q_ASSERT( status );
    if ( !status ) {
        qCritical() << "Input camera index exceeds number of registered cameras!";
        return 0;
    }

    Q_ASSERT( cameraIdx < m_fastAccessValues.size() );

    return m_fastAccessValues[cameraIdx].cameraImageHeight.value.toInt();
}

qint32 TashRuntime::getInputSourceWidth(const qint32 &cameraIdx) const {

    auto status = this->isInitialized();
    Q_ASSERT( status );
    if ( !status ) {
        qCritical() << "Runtime object not initialized, can't proceed!";
        return 0;
    }

    status = cameraIdx >= 0;
    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "Input camera index is negative!";
        return 0;
    }

    status = (cameraIdx < this->getNumberOfInputSources());
    Q_ASSERT( status );
    if ( !status ) {
        qCritical() << "Input camera index exceeds number of registered cameras!";
        return 0;
    }

    Q_ASSERT( cameraIdx < m_fastAccessValues.size() );

    return m_fastAccessValues[cameraIdx].cameraImageWidth.value.toInt();
}

qint32 TashRuntime::getInputSourceFPS(const qint32 &cameraIdx) const {

    auto status = this->isInitialized();
    Q_ASSERT( status );
    if ( !status ) {
        qCritical() << "Runtime object not initialized, can't proceed!";
        return 0;
    }

    status = cameraIdx >= 0;
    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "Input camera index is negative!";
        return 0;
    }

    status = (cameraIdx < this->getNumberOfInputSources());
    Q_ASSERT( status );
    if ( !status ) {
        qCritical() << "Input camera index exceeds number of registered cameras!";
        return 0;
    }

    Q_ASSERT( cameraIdx < m_fastAccessValues.size() );

    return m_fastAccessValues[cameraIdx].cameraFPS.value.toInt();
}

QString TashRuntime::getInputSourceDatatype(const qint32 &cameraIdx) const {

    auto status = this->isInitialized();
    Q_ASSERT( status );
    if ( !status ) {
        qCritical() << "Runtime object not initialized, can't proceed!";
        return QString("");
    }

    status = cameraIdx >= 0;
    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "Input camera index is negative!";
        return 0;
    }

    status = (cameraIdx < this->getNumberOfInputSources());
    Q_ASSERT( status );
    if ( !status ) {
        qCritical() << "Input camera index exceeds number of registered cameras!";
        return QString("");
    }

    Q_ASSERT( cameraIdx < m_fastAccessValues.size() );

    return m_fastAccessValues[cameraIdx].cameraImageDatatype.value.toString();
}

qint32 TashRuntime::getInputSourceFPM(const qint32 &cameraIdx) const {

    auto status = this->isInitialized();
    Q_ASSERT( status );
    if ( !status ) {
        qCritical() << "Runtime object not initialized, can't proceed!";
        return 0;
    }

    status = cameraIdx >= 0;
    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "Input camera index is negative!";
        return 0;
    }

    status = (cameraIdx < this->getNumberOfInputSources());
    Q_ASSERT( status );
    if ( !status ) {
        qCritical() << "Input camera index exceeds number of registered cameras!";
        return 0;
    }

    Q_ASSERT( cameraIdx < m_fastAccessValues.size() );

    return m_fastAccessValues[cameraIdx].cameraFPM.value.toInt();
}

int TashRuntime::getNumberOfInputSources() const{
    return m_NumberOfCameras;
}


void TashRuntime::listRuntimeProperties() {
    QList<QByteArray> dynNames = this->dynamicPropertyNames();

#ifdef TASHVERBOSE
    qDebug() << "Runtime instance has " << dynNames.size() << " dynamic properties:";
#endif

    foreach(QByteArray arr, dynNames) {
        QVariant val = QObject::property(arr.toStdString().c_str());
        qDebug() << "Property " << QString(arr) << ">>" << val.toString();
    }
}

void TashRuntime::writeDefaultConfigFile(const QString applicationName,
                                         const QString configFilePath,
                                         const quint32 &numberOfCameras,
                                         QVector<propertyContainer> &properties) {

    if ( !m_pSettings ) {
        qCritical() << "Invalid pointer to QSettings object!";
        return;
    }

    if ( numberOfCameras != m_NumberOfCameras ) {
#ifdef TASHVERBOSE
        qDebug() << "Changed number of cameras from" << m_NumberOfCameras << "to" << numberOfCameras;
#endif
        m_NumberOfCameras = numberOfCameras;
    }

    m_pSettings->beginGroup(applicationName);

    //write runtime properties
    writeRuntimeSpecificSettingsToFile(applicationName, configFilePath, numberOfCameras);

    //write module properties
    QVector<propertyContainer>::iterator it;
    QVector<internalTypeDescriptor>::iterator it2;

#ifdef TASHVERBOSE
    qDebug() << "Write camera specific settings...with " << properties.size() << " elements!";
#endif

    //for all cameras
    for ( quint32 camidx = 0; camidx < m_NumberOfCameras; ++camidx ) {
        m_pSettings->beginGroup(QString(TASHCAMIDENTIFIERGROUP).arg(camidx));
        m_pSettings->setValue(QString(TASHIMAGE_HEIGHTID),     createConfigEntry(QVariant(TASHIMAGEHEIGHT), Integer));
        m_pSettings->setValue(QString(TASHIMAGE_WIDTHID),      createConfigEntry(QVariant(TASHIMAGEWIDTH), Integer));
        m_pSettings->setValue(QString(TASHIMAGE_DATATYPEID),   createConfigEntry(QVariant("short"), String));
        m_pSettings->setValue(QString(TASHIMAGE_FRAMECOUNTID), createConfigEntry(QVariant(TASHIMAGECOUNTPERMAP), Integer));
        m_pSettings->setValue(QString(TASHIMAGE_FPSID),        createConfigEntry(QVariant(TASHIMAGECOUNTPERMAP), Integer));
        //find all groups
        for ( it = properties.begin(); it != properties.end(); ++it ) {
            m_pSettings->beginGroup( it->moduleName );
            for (it2 = it->properties.begin(); it2 != it->properties.end(); ++it2) {

                if ( it2->name.contains("#") ) {

                    QStringList parts = it2->name.split("#");
                    QString propertyName = parts.takeLast();

                    foreach ( QString str, parts ) {
                        m_pSettings->beginGroup(str);
                    }

                    m_pSettings->setValue( propertyName, createConfigEntry(it2->value, it2->type));

                    foreach ( QString str, parts ) {
                        Q_UNUSED(str)
                        m_pSettings->endGroup();
                    }

                } else {
                    m_pSettings->setValue(it2->name, createConfigEntry(it2->value, it2->type));
                }
            }
            m_pSettings->endGroup();
        }
        m_pSettings->endGroup();
    }

    m_pSettings->endGroup();

    m_pSettings->sync();
}

void TashRuntime::writeRuntimeSpecificSettingsToFile(const QString& applicationName, const QString& configFilePath, const quint32& numberOfCameras) {

    Q_UNUSED(numberOfCameras)

    if ( !m_pSettings ) {
        qCritical() << "Invalid pointer to QSettings object!";
        return;
    }

#ifdef TASHVERBOSE
    qDebug() << "Writing default ini-file settings for " << applicationName << " application";
#else
    Q_UNUSED(applicationName);
#endif

    m_pSettings->beginGroup(QString(TASHGENERALGROUP));
    m_pSettings->setValue(QString(TASHGENERAL_INIFILEPATHID),      createConfigEntry(QVariant(configFilePath), String));
    m_pSettings->setValue(QString(TASHGENERAL_USEDBUSID),          createConfigEntry(QVariant(TASHUSEDBUS), Boolean));
    m_pSettings->setValue(QString(TASHGENERAL_MAXWORKERTHREADSID), createConfigEntry(QVariant(TASHMAXTHREADSPERAPP), Integer));
    m_pSettings->setValue(QString(TASHGENERAL_HANDLEOSSIGNALSID),  createConfigEntry(QVariant(TASHHANDLEOSSIGNALS), Boolean));
    m_pSettings->setValue(QString(TASHGENERAL_DRYRUN_GRACE_TIME),  createConfigEntry(QVariant(TASHDRYRUNGRACETIME), Integer));

    if ( !m_UserDefinedRuntimeProperties.empty() ) {
        foreach(QString key, m_UserDefinedRuntimeProperties.keys() ) {
                m_pSettings->setValue(key, createConfigEntry(m_UserDefinedRuntimeProperties[key], qMetaTypeToConfType(m_UserDefinedRuntimeProperties[key].type())));
        }
    }

    m_pSettings->endGroup();
}

bool TashRuntime::readConfigFile() {

    Q_CHECK_PTR(m_pSettings);

    if ( !m_pSettings ) {
        qCritical() << "Invalid settings pointer!";
        return false;
    }

    m_pSettings->beginGroup(m_ApplicationName);

    QStringList keys = m_pSettings->allKeys();
#ifdef TASHVERBOSE
    qDebug() << "Parsing runtime properties from ini-file...";
#endif
    parseTashRuntimeProperties( keys );
#ifdef TASHVERBOSE
    qDebug() << "...done!";
    qDebug() << "Parsing LibTash module properties from ini-file...";
#endif
    parseLibTashModuleProperties( keys );
#ifdef TASHVERBOSE
    qDebug() << "...done!";
#endif
    return true;
}

void TashRuntime::parseLibTashModuleProperties(const QStringList& keys) {

    QRegularExpression regex( TASHCAMERAIDTAG );

    foreach(QString key, keys) {

        if ( key.contains(regex, nullptr) ) {
            QStringList tmp = key.split('/');
            QString identifier = tmp.join("-");
#ifdef TASHVERBOSE
            qDebug() << "Setting camera property: " << identifier;
#endif
            QObject::setProperty(identifier.toStdString().c_str(), QVariant(m_pSettings->value(key)));
        }
    }
}

void TashRuntime::parseTashRuntimeProperties(const QStringList& keys) {

    foreach (QString key, keys) {
        QStringList tmp = key.split('/');

#ifdef TASHVERBOSE
        qDebug() << "Parse general setting: " << tmp.last();
#endif

        if( tmp.first() == TASHGENERALGROUP ) {
            QObject::setProperty(tmp.last().toStdString().c_str(), QVariant(m_pSettings->value(key)));
        }
    }
}

bool TashRuntime::configFieldExist(const QString &key, const QString& group) {

    if ( group != "" ) {
#ifdef TASHVERBOSE
        qDebug() << "Group " << group << "does not exist. Creating group";
#endif
        m_pSettings->beginGroup( group );
    }

    return m_pSettings->contains(key);
}

bool TashRuntime::runtimePropertyExists(const QString &key) const{

    QList<QByteArray> dynNames = this->dynamicPropertyNames();

    foreach(QByteArray arr, dynNames) {
        if ( key == QString(arr) ) {
            return true;
        }
    }

    return false;
}


bool TashRuntime::isInitialized() const {
    return m_bIsInitialized;
}

bool TashRuntime::doDryRun() {

    if ( !m_CLIArgs.argv ) {
        return false;
    }

    return m_pCLIParser->hasField("dry");
}

bool TashRuntime::doHelpPrint() {

    if ( !m_CLIArgs.argv ) {
        return false;
    }

    return m_pCLIParser->hasField("help");
}

bool TashRuntime::doIniFileRewrite() {

    if ( !m_CLIArgs.argv ) {
        return false;
    }

    return m_pCLIParser->hasField("rewrite");
}

TashRuntime::ConfType TashRuntime::qMetaTypeToConfType(QVariant::Type type) {

    ConfType runtimeType;

    switch(type) {
    case QVariant::Bool:
        runtimeType = Boolean;
        break;
    case QVariant::Int:
        runtimeType = Integer;
        break;
    case QVariant::UInt:
        runtimeType = Unsigned;
        break;
    case QVariant::LongLong:
        runtimeType = LongLong;
        break;
    case QVariant::ULongLong:
        runtimeType = UnsignedLongLong;
        break;
    case QVariant::Double:
        runtimeType = Double;
        break;
    case QVariant::Char:
        runtimeType = Character;
        break;
    case QVariant::String:
        runtimeType = String;
        break;
    case QVariant::Invalid:
    default:
        runtimeType = Unknown;
        break;
    }

    return runtimeType;
}

QString TashRuntime::registerOpenedMemoryMap(const QString& filename, const QString& maptype) {

    auto status = filename.isEmpty();

    if ( status ) {
        qCritical() << "Error while registering memory map, empty filename!";
        return QString();
    }

    status = maptype.isEmpty();

    if ( status ) {
        qCritical() << "Error while registering memory map, empty maptype!";
        return QString();
    }

    auto mapTS      = tashtego::helper::TashTime::getNumericFullTimestamp();
    auto descriptor = QString("%1-%2-%3").arg(mapTS).arg(filename).arg(maptype);
    m_OpenMemoryMaps.append( descriptor );

    return descriptor;
}

bool TashRuntime::unregisterClosedMemoryMap(const QString& descriptor) {

    auto status = descriptor.isEmpty();

    if ( status ) {
        qCritical() << "Error input descriptor is empty, can't search for mmap id without identifier!";
        return !status;
    }

    status = this->hasOpenMemoryMaps();

    if ( !status ) {
        qWarning() << "There are no mmaps registered, can't deregister mmap which is not opened";
        qDebug() << m_OpenMemoryMaps;
        return status;
    }

    status = false;

    foreach (QString identifier, m_OpenMemoryMaps) {

        if ( identifier == descriptor ) {
#ifdef TASHVERBOSE
            qDebug() << "Deregister " << descriptor << "!";
#endif
            m_OpenMemoryMaps.removeAt( m_OpenMemoryMaps.indexOf(descriptor) );
            status = true;
            break;
        }
    }

    if ( !status ) {
        qWarning() << "No memory map with descriptor " << descriptor << " found!";
    }

    return status;
}

bool TashRuntime::hasOpenMemoryMaps() {
    return !m_OpenMemoryMaps.isEmpty();
}

QStringList TashRuntime::getOpenMemoryMaps() {
    return m_OpenMemoryMaps;
}

bool TashRuntime::hasOpenMemoryMapRegistered(const QString& strref) {
    auto openMmaps = getOpenMemoryMaps();

    foreach (QString mapid, openMmaps) {
        if ( mapid == strref ) {
            return true;
        }
    }

    return false;
}

void TashRuntime::registerObject(QString className, QObject *instance) {
    if ( !getObjectRegistry().contains(className) ) {
        getObjectRegistry()[className] = instance;
    }
}

QMap<QString, QObject*> TashRuntime::objectRegistry() {
    return getObjectRegistry();
}

bool TashRuntime::configFileExists() {
    return configFileExists(m_IniFilePath);
}

bool TashRuntime::configFileExists(const QString &iniFilePath) {
    using namespace helper;
    auto tmp(TashFileInfo::fileIsValid(iniFilePath) && TashFileInfo::isReadable(iniFilePath));
    return m_bConfigFileExists = tmp ;
}

void TashRuntime::exploreLibTashModuleProperties() {

    TashObject root;
    QObjectList modules = root.children();

    foreach(int id, getRuntimeInstanceIDs()) {
        const QMetaObject* tmpType = QMetaType::metaObjectForType(id);

        if ( !tmpType ) {
            qWarning() << "Invalid pointer returned for id " << id;
            continue;
        }

        qDebug() << "Libtash class: " << tmpType->className();
    }

    qDebug() << "TashObject has " << modules.size() << " classes which inherit from it!";

    foreach(QObject* module, modules) {
        const QMetaObject* pMetaObject = module->metaObject();
        qDebug() << "Found libTash class with name '" << pMetaObject->className() << "'";

        if ( (pMetaObject->propertyCount() - pMetaObject->propertyOffset()) > 0 ) {
            qDebug() << "Properties are:";
            for ( int idx = pMetaObject->propertyOffset(); idx < pMetaObject->propertyCount(); ++idx) {
                qDebug() << QString::fromLatin1( pMetaObject->property(idx).name());
            }
        } else {
            qDebug() << "No properties found!";
        }
    }
}

QVariant TashRuntime::createConfigEntry(QVariant value, ConfType type) {

    QString typestring = m_ConfTypes.contains(type) ? m_ConfTypes[type] : m_ConfTypes[Unknown];

    return QVariant(value.toString() + " [" + typestring + "]");
}

QVariant TashRuntime::property(const QString &name) {

    QMutex mutex;
    QMutexLocker lock(&mutex);

    if ( !runtimePropertyExists(name) ) {
        qWarning() << "There is no such property stored in current runtime: " << name;
        return QVariant();
    }

    QVariant tmp = QObject::property(name.toStdString().c_str());

    if ( !tmp.isValid() ) {
        qCritical() << "Given key yields an invalid config value!";
        return QVariant();
    }

    QRegularExpression regexp("\\[[a-z]+\\]");

    if ( !regexp.isValid() ) {
        qCritical() << "Given regular expression is not valid, abort!";
        return QVariant();
    }

    QString tmpstring = tmp.toString();

    QRegularExpressionMatch result = regexp.match(tmpstring);

    if ( !result.hasMatch() ) {
        qWarning() << "Property does not have a type description assuming type 'string'";
        return QVariant(tmpstring);
    }

    int offset = result.capturedStart();
    int length = tmpstring.length();

    if ( length <= offset ) {
        qCritical() << "Error while parsing inifile property!";
        return QVariant();
    }

    QString typestring = result.captured(0);
    QString valstring = tmpstring.mid(0, offset).trimmed();

    typestring = typestring.replace("[","").replace("]","");

    ConfType type = Unknown;

    if ( m_ConfTypes.values().contains(typestring) ) {
        type = m_ConfTypes.key(typestring);
    }

    return toTypedQVariant(QVariant(valstring), type);
}

bool TashRuntime::setProperty(const QString& name, const QVariant& value) {

    Q_ASSERT(!name.isEmpty());
    Q_ASSERT(value.isValid());

    if ( name.isEmpty() ) {
        qWarning() << "Property needs a name!";
        return false;
    }

    if ( !value.isValid() ) {
        qWarning() << "Property must be a valid QVariant value!";
        return false;
    }

    m_UserDefinedRuntimeProperties.insert(name, value);

    return true;
}

bool TashRuntime::setProperties(QMap<QString, QVariant> properties) {

    Q_ASSERT(!properties.isEmpty());

    if ( properties.isEmpty() ) {
        qWarning() << "Input argument must have valid properties elements set to be processed!";
        return false;
    }

    foreach (QString key, properties.keys()) {

        if ( !this->setProperty(key, properties[key]) ) {
            qWarning() << "Error while trying to set general properties!";
            return false;
        }
    }

    return true;
}

QVariant TashRuntime::toTypedQVariant(QVariant value, TashRuntime::ConfType type) {

    QVariant output;

    switch(type) {
    case Character:
        output = QVariant(value.toChar());
        break;
    case Integer:
    case Short:
        output = QVariant(value.toInt());
        break;
    case Double:
        output = QVariant(value.toDouble());
        break;
    case Float:
        output = QVariant(value.toFloat());
        break;
    case Boolean:
        output = QVariant(value.toBool());
        break;
    case Unsigned:
    case UnsignedShort:
        output = QVariant(value.toUInt());
        break;
    case Long:
    case LongLong:
        output = QVariant(value.toLongLong());
        break;
    case UnsignedLongLong:
        output = QVariant(value.toULongLong());
        break;
    case String:
    case Unknown:
    default:
            output = value;
    }

    return output;
}

bool TashRuntime::parseRequiredLibTashModuleProperties(QVector<propertyContainer> &modulePropertyList) {

    auto *pFactory = TashFactory::createFactory();

    Q_CHECK_PTR(pFactory);

    if ( !pFactory ) {
        qCritical() << "Failed to acquire factory object for libTash! Cannot proceed with property introspection!";
        return false;
    }


    auto metaObjectIDs = getMetaObjectIDs();
#ifdef TASHVERBOSE
    qDebug() << "Got " << metaObjectIDs.size() << " factory creatable modules available!";
#endif
    if ( metaObjectIDs.isEmpty() ) {
        qWarning() << "There are no objects which have processable properties, skip further introspection process!";
        return true;
    }

    QVectorIterator<int> it(metaObjectIDs);

    while(it.hasNext()) {

        int id = it.next();

        auto object     = pFactory->createSafeObjectFromID(id);
        auto objectName = QString(QMetaType::typeName(id)).split("::").last();

        Q_CHECK_PTR(object.data());

        if ( object.isNull() ) {
            qCritical() << "Could not create object with id " << id << ", skip!";
            continue;
        }
#ifdef TASHVERBOSE
        qDebug() << "Examining module " << objectName;
#endif
        /*
         * Base class objects should not be considered while fetching properties
         */
        if ( objectName.endsWith('*') ) {
            qWarning() << "Detected probable reference to abstract class, skip this one!";
            continue;
        }

        QList<QByteArray> rawProperties = object.data()->dynamicPropertyNames();       
        propertyContainer tmpContainer = {objectName, QVector<internalTypeDescriptor>()};

        foreach(QByteArray str, rawProperties) {
#ifdef TASHVERBOSE
            qDebug() << "Next property is: " << QString(str);
#endif
            QVariant property = object.data()->property( str.data() );

            if ( !property.isValid() ) {
                qCritical() << "Fetched invalid property, skip!";
                continue;
            }

            QString name = str;

            switch( static_cast<QMetaType::Type>( property.type() ) ) {
            case QMetaType::QString:
                tmpContainer.properties.append({name, property, String});
                break;
            case QMetaType::Char:
                tmpContainer.properties.append({name, property, Character});
                break;
            case QMetaType::Int:
                tmpContainer.properties.append({name, property, Integer});
                break;
            case QMetaType::Double:
                tmpContainer.properties.append({name, property, Double});
                break;
            case QMetaType::Float:
                tmpContainer.properties.append({name, property, Float});
                break;
            case QMetaType::Bool:
                tmpContainer.properties.append({name, property, Boolean});
                break;
            case QMetaType::UInt:
                tmpContainer.properties.append({name, property, Unsigned});
                break;
            case QMetaType::Long:
                tmpContainer.properties.append({name, property, Long});
                break;
            case QMetaType::LongLong:
                tmpContainer.properties.append({name, property, LongLong});
                break;
            case QMetaType::ULongLong:
                tmpContainer.properties.append({name, property, UnsignedLongLong});
                break;
            case QMetaType::Short:
                tmpContainer.properties.append({name, property, Short});
                break;
            case QMetaType::UShort:
                tmpContainer.properties.append({name, property, UnsignedShort});
                break;
            case QMetaType::UnknownType:
                tmpContainer.properties.append({name, property, Unknown});
            default:
                break;
            }

            modulePropertyList.append(tmpContainer);
        }
    }

    pFactory->destroyFactory();

    return true;
}

QStringList TashRuntime::getPropertyNamesContaining(const QString& stringpart, const quint32& cameraID) {

    QString             cam         = QString(TASHCAMIDENTIFIERGROUP).arg(cameraID);
    QList<QByteArray>   rawNames    = this->dynamicPropertyNames();
    QStringList         ret;

    foreach(QByteArray arr, rawNames) {
        QString raw = arr;

        if ( raw.contains(stringpart) && raw.contains(cam) ) {
            ret.append(raw);
        }
    }

    return ret;
}

QString TashRuntime::getPropertyNameContaining(const QString& stringpart, const quint32& cameraID) {

    QStringList list = getPropertyNamesContaining(stringpart, cameraID);

    if ( list.isEmpty() ) {
        qWarning() << "There is no such property with such a stringpart " << stringpart;
        return QString("");
    }

    QString delim("***");

    if ( list.size() > 1 ) {
        qWarning() << "More than one property with the given string content present, returning all names concatenated by " << delim;
        return list.join(delim);
    }

    return list.first();
}

QString TashRuntime::getLibTashVersion() {
    return QString("libTash v%1.%2.%3").arg(VERSION_MAJOR).arg(VERSION_MINOR).arg(BUILD);
}

QString TashRuntime::getCredits() {
    return QString("\n%1 build with %2\nDeveloped for:\nAlfred Wegener Institut\nOcean Acoustics Group (OZA)"
                   "\n\nDevelopers:\nDaniel P. Zitterbart\nMichael Flau\n").arg(m_ApplicationName).arg(getLibTashVersion());
}

qint64 TashRuntime::genID() {
    return genIDPara();
}

qint64 TashRuntime::genIDPara(QMutex* mutex, qint64 seed) {

    static qint64 cTmp = 0;

    if ( mutex ) {
        QMutexLocker lock_this(mutex);
        getRuntimeInstanceIDs().append( (++cTmp) + seed );
    } else {
        getRuntimeInstanceIDs().append( (++cTmp) + seed );
    }

    return cTmp;
}

bool TashRuntime::setupCLIHandling() {

#ifdef TASHVERBOSE
    qDebug() << "Checking CLI usage state...";
#endif

    using namespace helper;

    if ( (m_CLIArgs.argc > 0) && m_CLIArgs.argv ) {

#ifdef TASHVERBOSE
        qDebug() << "CLI usage enabled...";
#endif

        QList<TashCLIParser::TashCLIOption> refList;

        //Define help option
        refList.append({'h', "help", false, QVariant(false), Boolean, "Triggers help dialog and ends application"});
        //OS signal handling by application
        refList.append({'o', "ossh", false, QVariant(false), Boolean, "Enables OS signal handling by custom Tashtego signal handler"});
        //Use dbus for message handling
        refList.append({'d', "dbus", false, QVariant(false), Boolean, "Enables handling of all application messages through DBUS client instead of writing to stdout"});
        //Define alternative config file
        refList.append({'i', "ini", true, QVariant(m_IniFilePath), String, "Allows to set an alternative path to an application ini file"});
        //Define dryrun option
        refList.append({'n', "dry", false, QVariant(false), Boolean, "This flag will start the initialization process of the application and then end it after initialization is done"});
        //Define a flag for forced ini-file rewrite
        refList.append({'r', "rewrite", false, QVariant(false), Boolean, "This flag will force the client application to rewrite the ini-file based on all registered libTash objects, and then exit"});

        //Create a parser instance
        m_pCLIParser = new TashCLIParser(genIDPara(),
                                         m_CLIArgs.argc,
                                         m_CLIArgs.argv,
                                         refList);

        Q_CHECK_PTR(m_pCLIParser);

        if ( !m_pCLIParser->isInitialized() || !m_pCLIParser->hasParsedFieldList() ) {
            qCritical() << "Initializing command line parser instance failed, abort!";
            emit shutdown();
            return m_bIsInitialized = false;
        }

        connect( m_pCLIParser, SIGNAL(shutdown()), this, SIGNAL(shutdown()) );

#ifdef TASHVERBOSE
        m_pCLIParser->dumpCommandLineOptions();
#endif

    /*
     * Check states in current cli parser instance
     */

        if ( doHelpPrint() ) {
            m_RuntimeStates.append(tashRuntimeStates::PRINT_HELP);
        }

        if ( doDryRun() ) {
            m_RuntimeStates.append(tashRuntimeStates::DRY_RUN);
        }

        if ( doIniFileRewrite() ) {
            qDebug() << "REQUIRE INI FILE REWRITE...";
            m_RuntimeStates.append(tashRuntimeStates::REWRITE_INI);
        }

        if ( m_pCLIParser->hasField("ossh") ) {
            m_RuntimeStates.append(tashRuntimeStates::USE_OS_SIGNALS);
        }

        if ( m_pCLIParser->hasField("dbus") ) {
            m_RuntimeStates.append(tashRuntimeStates::USE_DBUS);
        }

        if ( m_pCLIParser->hasField("ini") ) {
            m_RuntimeStates.append(tashRuntimeStates::ALTERNATE_INI_FILE);
        }
    }

    return true;
}

bool TashRuntime::setupINIFileHandling() {

    if ( m_pSettings ) {
        qWarning() << "Settings object already exists...!";
        return true;
    }

    QSettings::setDefaultFormat(QSettings::IniFormat);

    auto list = m_IniFilePath.split("/", QString::SkipEmptyParts);
    auto filename = list.last();
    list.pop_back();
    QDir inidir(list.join("/"));
    auto absPath = inidir.absoluteFilePath(filename);

#ifdef TASHVERBOSE
    qDebug() << "Using absolute path to ini file " << absPath;
#endif

    m_pSettings = new QSettings(absPath, QSettings::IniFormat);

    Q_CHECK_PTR(m_pSettings);
    if ( !m_pSettings ) {
        qCritical() << "Allocating QSettings object failed! Abort runtime instantiation!";
        emit shutdown();
        return m_bIsInitialized = false;
    }

    //Change used codec
    m_pSettings->setIniCodec("UTF-8");

    return true;
}

void TashRuntime::tashtegoMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg) {

    QFileInfo fileinfo(QCoreApplication::applicationFilePath());

    QString filename = fileinfo.fileName().toLower();
    QString service = "";
    if ( filename == "tashlogger" ) {
        service = ST_TASHLOGGER;
    } else if ( filename == "tashbuffer" ) {
        service = ST_TASHBUFFER;
    } else if ( filename == "tashdetect" ) {
        service = ST_TASHDETECT;
    } else if ( filename == "tashcommander" ) {
        service = ST_TASHCOMMANDER;
    } else if ( filename == "tashpump" ) {
        service = ST_TASHPUMP;
    } else if ( filename == "tashfviprovider" ) {
        service = ST_TASHFVIPROVIDER;
    } else {
        service = ST_LIBTASH;
    }

    cmd::TashDBusConnector m_pDBusMsgLogger(0, service);

    QString file = "<font color=\"#EA6A00\">" % QString(context.file) % "</font>";
    QString line = "<font color=\"#ECDB00\">" % QString("%1").arg(context.line) % "</font>";

    QString processed = QString("%1::%2\t>> %3").arg(file).arg(line).arg(msg);

    processed = filename + " - " + processed;

    switch (type) {
        case QtDebugMsg:
        m_pDBusMsgLogger.log(processed, tashtego::cmd::TashDBusConnector::DBG);
        break;
    case QtInfoMsg:
        m_pDBusMsgLogger.log(processed, tashtego::cmd::TashDBusConnector::INF);
        break;
    case QtWarningMsg:
        m_pDBusMsgLogger.log(processed, tashtego::cmd::TashDBusConnector::WAR);
        break;
    case QtCriticalMsg:
        m_pDBusMsgLogger.log(processed, tashtego::cmd::TashDBusConnector::CRI);
        break;
    case QtFatalMsg:
        m_pDBusMsgLogger.log(processed, tashtego::cmd::TashDBusConnector::ERR);
        break;
    }
}

int TashRuntime::initOSSignalHandling() {
#ifdef Q_OS_LINUX

#ifdef TASHVERBOSE
    qDebug() << "Custom signal handling for Linux systems enabled...";
    qDebug() << "Setting up custom OS signal handlers...";
#endif
    struct sigaction hup, trm, sint;
#ifdef TASHVERBOSE
    qDebug() << "Setting signalhandler for SIGHUP...";
#endif
    hup.sa_handler = TashOSSignalHandler::hupSignalHandler;
    sigemptyset(&hup.sa_mask);
    hup.sa_flags = 0;
    hup.sa_flags |= SA_RESTART;

    if ( sigaction( SIGHUP, &hup, 0) > 0 ) {
        return 1;
    }
#ifdef TASHVERBOSE
    qDebug() << "Setting signalhandler for SIGTERM...";
#endif
    trm.sa_handler = TashOSSignalHandler::termSignalHandler;
    sigemptyset(&trm.sa_mask);
    trm.sa_flags |= SA_RESTART;

    if ( sigaction( SIGTERM, &trm, 0) > 0 ) {
        return 2;
    }
#ifdef TASHVERBOSE
    qDebug() << "Setting signalhandler for SIGINT...";
#endif
    sint.sa_handler = TashOSSignalHandler::intSignalHandler;
    sigemptyset(&sint.sa_mask);
    sint.sa_flags |= SA_RESTART;

    if ( sigaction( SIGINT, &sint, 0) > 0 ) {
        return 3;
    }
#ifdef TASHVERBOSE
    qDebug() << "Installed signal handlers for Linux systems...";
#endif

#elif Q_OS_WIN32
    qWarning() << "Signal handling for windows platforms, not yet implemented...!";
#endif

    return 0;
}

bool TashRuntime::fillFastAccessValues() {

    auto status = isInitialized();

    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "Can't fill fast access structure, runtime instance is not initialized!";
        return status;
    }

    const auto numCams = getNumberOfInputSources();
    QVector<fastAccessCameraValues> tmpvec(numCams,
                                           {
                                            {QString(""),QVariant(),ConfType::Unknown},
                                            {QString(""),QVariant(),ConfType::Unknown},
                                            {QString(""),QVariant(),ConfType::Unknown},
                                            {QString(""),QVariant(),ConfType::Unknown},
                                            {QString(""),QVariant(),ConfType::Unknown}
                                           }
                                          );

    status = numCams > 0;

    Q_ASSERT( status );

    if ( !status ) {
        qWarning() << "No camera defintion present in ini-file, can't proceed!";
        return status;
    }

    const QString dataType    = "data_type";
    const QString fps         = "framerate_in_Hz";
    const QString fpm         = "frames_per_map";
    const QString imageHeight = "image_height";
    const QString imageWidth  = "image_width";

#ifdef TASHVERBOSE
    qDebug() << "fetching properties:";
#endif

    for ( auto cidx = 0; cidx < numCams; ++cidx ) {

        fastAccessCameraValues tmp;

        //image width
        tmp.cameraImageWidth.value    = property( getPropertyNameContaining(imageWidth, cidx) );
        tmp.cameraImageWidth.name     = imageWidth;
        tmp.cameraImageWidth.type     = ConfType::Integer;
        //image height
        tmp.cameraImageHeight.value   = property( getPropertyNameContaining(imageHeight, cidx) );
        tmp.cameraImageHeight.name    = imageHeight;
        tmp.cameraImageHeight.type    = ConfType::Integer;
        //image fps
        tmp.cameraFPS.value           = property( getPropertyNameContaining(fps, cidx) );
        tmp.cameraFPS.name            = fps;
        tmp.cameraFPS.type            = ConfType::Integer;
        //image frames per map
        tmp.cameraFPM.value           = property( getPropertyNameContaining(fpm, cidx) );
        tmp.cameraFPM.name            = fpm;
        tmp.cameraFPM.type            = ConfType::Integer;
        //image data type
        tmp.cameraImageDatatype.value = property( getPropertyNameContaining(dataType, cidx) );
        tmp.cameraImageDatatype.name  = dataType;
        tmp.cameraImageDatatype.type  = ConfType::String;

        tmpvec[cidx] = qMove(tmp);
    }

    m_fastAccessValues = qMove(tmpvec);

    return status;
}

}
}

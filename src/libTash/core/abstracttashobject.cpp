/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file abstracttashobject.cpp
 * @author Michael Flau
 * @brief
 * @details
 * @date June 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started June 2015
 */

#include "abstracttashobject.h"

#include <QString>
#include <cassert>

namespace tashtego {

namespace core {

/**
 * @brief AbstractTashObject::AbstractTashObject
 * @param parent
 */
AbstractTashObject::AbstractTashObject(QObject *parent) :
    QObject(parent),
    m_bUse(true)
{

}

/**
 * @brief AbstractTashObject::~AbstractTashObject
 */
AbstractTashObject::~AbstractTashObject(){

}

/**
 * @brief AbstractTashObject::AbstractTashObject
 * @param other
 */
AbstractTashObject::AbstractTashObject(const AbstractTashObject &other) :
    QObject(),
    m_bUse(other.m_bUse)
{
    //assert(false && "LIBTASH PROHIBITS THE USE OF COPY CONSTRUCTORS! ABORT!");
}

}
}

/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashtypes.h
 * @author Michael Flau
 * @brief
 * @details
 * @date June 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started June 2015
 */

#ifndef TASHTYPES
#define TASHTYPES

#include "tashconst.h"

namespace tashtego {

namespace rawtypes {

/**
 * Raw RDE Header structure
 */
typedef struct {
    char ascii[4];
    quint32 bufferCount;
    quint32 imageHeaderDataSize;
    quint32 imageHeaderSize;
    quint32 fileSize;
} RDEHeaderFormat_r;

/**
 * Raw RDE frame structure
 */
typedef struct {
    qint32 counter;
    qint32 width;
    qint32 height;
    qint32 depth;
    qint32 channels;
    qint32 time;
    qint32 timems;
    quint32 tbd_a;
    quint32 tbd_b;
    quint32 tbd_c;
    quint32 tbd_d;
    quint32 tbd_e;
    quint32 tbd_f;
    quint32 tbd_g;
    quint32 tbd_h;
    qint16 image[TASHIMAGEPIXELCOUNT];
} RDEFrameFormat_r;

/**
 * Raw RDE memory map structure
 */
typedef struct {
    RDEHeaderFormat_r header;
    RDEFrameFormat_r frames[TASHIMAGECOUNTPERMAP];
} RDEFormat_r;

/**
 * Raw VMEM frame structure
 */
typedef struct {
    qint32 counter;
    qint32 width;
    qint32 height;
    qint32 depth;
    qint32 channels;
    qint32 time;
    qint32 timems;
    quint32 tbd_a;
    quint32 tbd_b;
    quint32 tbd_c;
    quint32 tbd_d;
    quint32 tbd_e;
    quint32 tbd_f;
    quint32 tbd_g;
    quint32 tbd_h;
    qint16 image[TASHIMAGEPIXELCOUNT];
} VMEMFrameFormat_r;

/**
 *
 */
typedef struct {
    qint32 counter;
    qint32 width;
    qint32 height;
    qint32 channels;
    qint32 time_unix;
    qint32 time_ms;
    qint32 other[TASHFVIHEADERFIELDS];
}FVIHeader_r;

/**
 *
 */
typedef struct {
    qint32 counter;
    qint32  width;
    qint32  height;
    qint32  depth;
    qint32  channels;
    qint32  time_unix;
    qint32  time_ms;
    quint32 tbd_a;
    quint32 tbd_b;
    quint32 tbd_c;
    quint32 tbd_d;
    quint32 tbd_e;
    quint32 tbd_f;
    quint32 tbd_g;
    quint32 tbd_h;
    qint16 data[TASHIMAGEPIXELCOUNT];
}FVIBody_r;

/**
 *
 */
typedef struct {
    FVIHeader_r header;
    qint16 data[TASHIMAGEPIXELCOUNT];
}FVIFile_r;

/**
 * Raw VMEM memory map structure
 */
typedef struct {
    VMEMFrameFormat_r frames[TASHIMAGECOUNTPERMAP];
} VMEMFormat_r;

/**
 * Raw IMEM frame structure
 */
typedef struct {
    double counter;
    double done;
    double offset;
    double high;
    double low;
    double imagelength;
    double ydown;
    double turn;
    double col1;
    double drawBinaryMask;
    double applyRunningMean;
    double applyGaussian;
    double rotation;
    double field;
    double height;
    double nblows;
    double blows[TASHBLOWSSZ * TASHDUMMYSZ];
    double pers_blows[TASHPERSBLOWS];
    double last_blow[TASHLASTBLOW];
    double last_click[TASHLASTCLICK];
    double latitude;
    double longitude;
    double speed;
    double course;
    double heading;
    double time;
    double detectvalue;
    double detectthresh;
    double detectice;
    double classify;
    double forward;
    double guidlines;
    double yDVec[TASHIMAGEWIDTH];
    double dummy[TASHDUMMYSZ * TASHDUMMYSZ];
    double cmap[TASH14BITCOLOR * TASHNUMBEROFCHANNELS];
    double rmeanboundaries[2];
    double gaussianSettings[2];
    double maskOffset;
    short  backgroundModel[TASHIMAGEPIXELCOUNT];
    short  image[TASHIMAGEPIXELCOUNT];
    short  fullimage[TASHIMAGEPIXELCOUNT];
} IMEMFrameFormat_r;

/**
 * Raw IMEM memory map structure
 */
typedef struct {
    IMEMFrameFormat_r frames[TASHIMAGECOUNTPERMAP];
} IMEMFormat_r;

/**
 * Raw EMEM memory structure
 */
typedef struct {
    double change;
    double px;
    double py;
    double timestamp;
    double lock;
    double index;
    double classn;
    double classt;
    double classf;
    double prob;
    double probt;
    double probf;
    double latitude;
    double longitude;
    double speed;
    double course;
    double heading;
    double distance;
    double elevation;
    double direction;
    double localcontrast;
    double globalcontrast;
    double auc;
    double move[10];
    double mititgate;
    double last;
    double yDown;
    double yDVec[TASHIMAGEWIDTH];
    double imBuf[TASHEVENTIMAGESCOUNT][TASHEVENTIMAGEHEIGHTSMALL][TASHEVENTIMAGEWIDTHSMALL];
    double largeBuf[TASHEVENTIMAGESCOUNT][TASHIMAGEHEIGHT][TASHEVENTIMAGEWIDTH];
} EMEMFrameFormat_r;

typedef struct {
    unsigned numSlots;
    unsigned slotsUsed;
    EMEMFrameFormat_r _slots[10];
} EMEMFormat_r;

/**
 *
 */
typedef struct {
    int counter;
    int time_unix;
    int time_ms;
} RAMDISKSlot_r;

/**
 *
 */
typedef struct {
    RAMDISKSlot_r buffer[TASHRAMDISKSLOTS];
} RAMDISKBuffer_r;

/**
 *
 */
typedef struct scoreraw{
    int tile_id;
    int start_x;
    int start_y;
    int width;
    int height;
    int score;
    int score2;
    int ignore;
}scoreHolder_r;

/**
 *
 */
typedef struct scorerawlist {
    int       writelock        = 0;
    int       readlock         = 0;
    int       framenumber      = 0;
    int       slotindex        = 0;
    int       elements         = 0;
    int       numslots         = SCORE_MAP_DEFAULT_DATA_FIELDS;
    int       time             = 0;
    int       timems           = 0;
    int       numdetectthreads = 0;
    int       scoreList[SCORE_MAP_DEFAULT_NUM_EVENT_FIELDS][SCORE_MAP_DEFAULT_DATA_FIELDS] = {{0,0,0,0,0,0,0,0}};
}scoreHolderList_r;

/**
 *
 */
typedef struct {
    scoreHolderList_r threadList[SCORE_MAP_DEFAULT_NUM_CORES];
} scoreMap_r;

/**
 *
 */
typedef struct {
    int x; //start x
    int y; //start y
    int w; //width
    int h; //height
    int n; //count
} dimRoi_r;

/**
 *
 */
typedef struct {
    int x; //start x
    int y; //start y
    int w; //width
    int h; //height
    int r; //
    int t; //tile count
} dimTr_r;

/**
 * mmap struct describing exactly one roi
 */
typedef struct {
    dimRoi_r    roidim;
    dimTr_r     trs[DIM_MAP_DEFAULT_TR_NUM];
} roi_r;

/**
 * mmap struct for detector dimensions
 */
typedef struct {
    int numRois;
    roi_r rois[DIM_MAP_DEFAULT_ROI_NUM];
} detectorDim_r;

/**
 * mmap struct for detector dimensions
 */
typedef struct {
  int framenumber;
  int time;
  int timems;
  short data[TASHIMAGEPIXELCOUNT];
} img_r;

/**
 * framebuffer memory map, written by tashBuffer
 */
typedef struct {
  bool isFilled;
  int current;
  img_r stack[IMGSTACK_MAP_DEFAULT_DEPTH];
} framebuffer_r;

/**
 * Cluster descriptor
 */
typedef struct {
    int id;
    int x1;
    int y1;
    int x2;
    int y2;
    int num_elements;
} cluster_r;

/**
 * Cluster map definition
 */
typedef struct {
    int framenumber;
    int numCluster;
    cluster_r cluster[CLUSTER_MAP_DEFAULT_CLUSTER_NUM];
}detectionClusterMap_r;

/*
 * sub-defintion for uicommands.mmap
 *
 * Defines a range
 * for various processing
 * tasks
 */
typedef struct {
    bool enabled;
    int left;
    int right;
} range_r;

/*
 * sub struct for visualization
 * of detector properties
 */
typedef struct {
    bool drawTiles;
    bool drawDims;
    bool drawScores;
    bool drawIgnored;
    int min;
    int max;
} detectVis_r;

/*
 * sub-defintion for uicommands.mmap
 *
 * Defines a struct for post processing
 * modules which need to mind a tile
 * score thrsold
 */
typedef struct {
    bool autoDetect;
    bool useBackgroundModel;
    double threshold;
} detectorThreshold_r;

typedef struct {
    int low;
    int high;
} contrastEnhancement_r;

/*
 * uicommands.mmap map defintion
 *
 * Memory map for
 * UI communication with
 */
typedef struct {
    bool enableBinaryMask;
    bool blackIsHot;
    range_r backgroundCorrection;
    range_r processingArea;
    detectVis_r detectorDepiction;
    detectorThreshold_r detectorThreshold;
    contrastEnhancement_r contrastValues;
} uiCommands_r;

/*
 * Definition of a single counter detection
 */
typedef struct counterDetection {
    char detectorName[50]   = "default_detector";
    int framenumber         = 0;
    int locked              = 0;
    int slotsUsed           = 0;
    int readable            = 0;
    int maxDetections = MAX_COUNTER_DETECTIONS;
    int _slots [MAX_COUNTER_DETECTIONS][MAX_FIELDS_PER_COUNTER_DETECTION] = {{0,0,0,0,0,0}};
} counterDetectionSlot_r;

/*
 * Definition of the counterdetection memory map
 */
typedef struct {
    int locked    = 0;
    int slotsUsed = 0;
    int readers   = 0;
    int maxSlots  = MAX_COUNTER_DETECTORS;
    bool _slotUsed[MAX_COUNTER_DETECTORS];
    counterDetectionSlot_r _slots[MAX_COUNTER_DETECTORS];
} counterDetections_r;

}

}

#endif // TASHTYPES


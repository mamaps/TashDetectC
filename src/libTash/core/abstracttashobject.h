/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file abstracttashobject.h
 * @author Michael Flau
 * @brief
 * @details
 * @date June 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started June 2015
 */

#ifndef ABSTRACTTASHOBJECT_H
#define ABSTRACTTASHOBJECT_H

#include <QObject>
#include <QtGlobal>
#include <QMetaType>
#include <QMutex>
#include <QMutexLocker>

#include "tashconst.h"

namespace tashtego {

#define REGISTER_METATYPE(NAME, PRINTNAME) \
    namespace { \
    MetaTypeRegistration<NAME > tashmeta(PRINTNAME); \
    }

namespace core {

/**
 * @brief The AbstractTashObject class
 */
class AbstractTashObject : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief AbstractTashObject
     * @param parent
     */
    explicit AbstractTashObject(QObject *parent = 0);

    /**
     * @brief ~AbstractTashObject
     */
    virtual ~AbstractTashObject();

    /**
     * @brief AbstractTashObject
     * @param other
     */
    AbstractTashObject(const AbstractTashObject& other);

signals:

public slots:

protected:
    /**
     * @brief init
     */
    virtual bool init() = 0;

    /**
     * @brief getClassName
     * @return
     */
    virtual QString getClassName() = 0;

private:

    //Member
    bool m_bUse;
};

}

/**
 * Template class used for meta type registration of
 * each derived class of AbstractTashObject
 */
template <typename T > class MetaTypeRegistration {
public:
    inline MetaTypeRegistration(const char* name){
        Q_UNUSED(name)
        int id = qRegisterMetaType<T >();
//        qDebug("Register new class with name %s id %d", name, id);
        getMetaObjectIDs().append(id);
    }
};

}
#endif // ABSTRACTTASHOBJECT_H

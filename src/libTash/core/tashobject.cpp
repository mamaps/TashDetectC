/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashobject.cpp
 * @author Michael Flau
 * @brief
 * @details
 * @date June 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started June 2015
 */

#include <QVariant>
#include <QPointer>
#include <QDebug>
#include <QRegularExpression>
#include <QRegularExpressionMatch>

#include "../cmd/tashservicetags.h"
#include "tashobject.h"

namespace tashtego {

namespace core {

TashObject::TashObject(AbstractTashObject *parent):
    AbstractTashObject(parent),
    m_pRunTime(NULL),
    m_InstanceID(-1),
    m_bIsInitialized(false),
    m_bHasRuntimeObject(false)
{

}

TashObject::TashObject(qint64 id, AbstractTashObject *parent):
    AbstractTashObject(parent),
    m_pRunTime(NULL),
    m_InstanceID(id),
    m_bIsInitialized(false),
    m_bHasRuntimeObject(false)
{
    if ( !this->init() ) {
#ifdef TASHVERBOSE
        qWarning() << "Intialization of TashObject failed!";
#endif
    }
}

TashObject::TashObject(qint64 id, const QPointer<core::TashRuntime> runtime, AbstractTashObject *parent):
    AbstractTashObject(parent),
    m_pRunTime(runtime),
    m_InstanceID(id),
    m_bIsInitialized(false),
    m_bHasRuntimeObject(!m_pRunTime.isNull())
{
    if ( !this->init() ) {
#ifdef TASHVERBOSE
        qWarning() << "Intialization of TashObject failed!";
#endif
    }
}

qint64 &TashObject::getObjectID(){
    return this->m_InstanceID;
}

bool TashObject::isInitialized() const {
    return this->m_bIsInitialized;
}

bool TashObject::isRuntimeAware() const {
    return m_bHasRuntimeObject;
}

bool TashObject::setRuntime(const QPointer<TashRuntime> &runtime) {

    if ( runtime.isNull() ) {
        qCritical() << "Given pointer to runtime instance is invalid!";
        return (m_bHasRuntimeObject = false);
    }

    if ( isRuntimeAware() ) {
        qWarning() << "Object has already a reference to the runtime object!";
        return true;
    }

    m_pRunTime = runtime;
    return m_bHasRuntimeObject = true;
}

bool TashObject::init() {

    if ( this->isInitialized() ) {
#ifdef TASHVERBOSE
        qDebug() << "TashObject instance is already initialized!";
#endif
        return true;
    }

    QVariant id = QVariant::fromValue(m_InstanceID);
    this->setProperty("instanceID", id);
    m_bIsInitialized    = true;
    emit initialized(m_InstanceID);

    return true;
}

QString TashObject::getClassName() {
    return QString("TashObject");
}

void TashObject::setObjectID(qint64 id) {
    m_InstanceID = id;
}

bool TashObject::setDefaultProperties() {
#ifdef TASHVERBOSE
    qDebug() << "TashObject::setDefaultProperties()";
#endif
    return true;
}

bool TashObject::parseRuntimeForProperties() {
#ifdef TASHVERBOSE
    qDebug() << "TashObject::parseRuntimeForProperties()";
#endif
    return true;
}

REGISTER_METATYPE(TashObject, "TashObject")
}
}


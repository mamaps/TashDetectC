/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashtypedmemorymap.h
 * @author Michael Flau
 * @brief
 * @details
 * @date June 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started June 2015
 */

#ifndef TASHTYPEDMEMORYMAP_H
#define TASHTYPEDMEMORYMAP_H

//Tashtego
#include "tashabstractmemorymap.h"
#include "../core/tashtypes.h"
#include "../helper/tashstringchecks.h"

//C header
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

//Qt header
#include <QMap>
#include <QVariant>
#include <QVector>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QDataStream>
#include <QSharedPointer>
#include <QScopedArrayPointer>
#include <QPointer>

#include <cstdio>
#include <iostream>

namespace tashtego {
namespace com {

/**
 * @class TashTypedMemoryMap
 * This template is one of the core classes used within the tashtego
 * software suite. Almost all process communication is being done via memory maps.
 * This template provides a generic approach, to access memory maps of different
 * structural shape. As the name implies, the shape of the memory map is predefined
 * by the datastructure definietion which is besing used at runtime, to create
 * instance of this template.
 */
template <typename T>
class TashTypedMemoryMap final: public TashAbstractMemoryMap {

public:

    /**
     * @brief TashTypedMemoryMap
     */
    explicit TashTypedMemoryMap(TashAbstractMemoryMap *parent = 0);

    /**
     * @brief TashTypedMemoryMap
     * @param id
     * @param runtime
     * @param parent
     */
    TashTypedMemoryMap(qint64 id,
                       const QPointer<core::TashRuntime> runtime,
                       const quint32 camidx,
                       const QString &searchfor = "",
                       TashAbstractMemoryMap *parent = 0);

    /**
     * @brief TashTypedMemoryMap
     * @param settings
     */
    TashTypedMemoryMap(qint64 id,
                       QMap<QString, QVariant> &settings,
                       const quint32 camidx,
                       TashAbstractMemoryMap *parent = 0);

    /**
     * @brief ~TashTypedMemoryMap
     */
    virtual ~TashTypedMemoryMap();

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() override;

    /**
     * @brief init
     * @param settings
     */
    virtual void initMap(QMap<QString, QVariant> &settings) override;

    /**
     * @brief open
     * @return
     */
    virtual bool openMap() override;

    /**
     * @brief close
     * @return
     */
    virtual bool closeMap() override;

    /**
     * @brief getUnsafeMapAccess
     * @return
     */
    T* getUnsafeMapAccess();

    /**
     * @brief getSafeMapAccess
     * @return
     */
    QSharedPointer<T> getSafeMapAccess();

    /**
     * @brief mapAlreadyExisted
     * @return
     */
    bool mapAlreadyExisted();

protected:
    /**
     * @brief init
     * @return
     */
    virtual bool init() override;

    /**
     * @brief parseMapSettingsFromRuntime
     * @return
     */
    virtual bool parseMapSettingsFromRuntime();

    /**
     * @brief createMap
     * @return
     */
    virtual bool createMap() override;

private:

    /**
     * @brief releaseMap
     */
    static void releaseMap(T* pMap);

    /*
     * Member
     */
    bool                    m_bHasMapSettings;  ///> Flag which denotes settings state
    const quint32           m_cCamIdx;          ///> Camera index
    QSharedPointer<T>       m_map;              ///> Smart pointer to memory map
    QMap<QString, QVariant> m_mapSettings;      ///> Settings for memory map
    QString                 m_searchFor;        ///>
};



template <typename T>
TashTypedMemoryMap<T>::TashTypedMemoryMap(TashAbstractMemoryMap *parent):
    TashAbstractMemoryMap(parent),
    m_bHasMapSettings(false),
    m_cCamIdx(-1),
    m_map(0),
    m_mapSettings(QMap<QString, QVariant>())
{
#ifdef TASHVERBOSE
    qDebug() << "Def. ctor, writing default mmap properties...";
#endif

    auto tmpStr = std::string();

    auto appendToMapGroup    = [&] (const char* propertyName) -> std::string& {
        tmpStr       = QString("%1#%2").arg(QString(TASHMMAPGROUP)).arg(QString(propertyName)).toStdString();
        return tmpStr;
    };

    //Write general mmap properties
    this->setProperty( appendToMapGroup(TASHMAP_HASGLOBALHEADERID).c_str(), true);
    this->setProperty( appendToMapGroup(TASHMAP_HASFRAMEHEADERID).c_str(),  true);
    this->setProperty( appendToMapGroup(TASHMAP_TYPEID).c_str(),            "typed");
    this->setProperty( appendToMapGroup(TASHMAP_FILEOPENID).c_str(),        "ReadWrite");
    this->setProperty( appendToMapGroup(TASHMAP_FILEACCESSID).c_str(),      "OwnerCanRead|OwnerCanWrite");
    this->setProperty( appendToMapGroup(TASHMAP_OPTIONSID).c_str(),         "MapShared|MapPopulate|MapNonBlock");
    this->setProperty( appendToMapGroup(TASHMAP_PROTECTIONID).c_str(),      "MapRead|MapWrite");
    this->setProperty( appendToMapGroup(TASHMAP_PATHID).c_str(),            "/mnt/mmap/cam.mmap");
    this->setProperty( appendToMapGroup(TASHMAP_OFFSETID).c_str(),          0);
}

template <typename T>
TashTypedMemoryMap<T>::TashTypedMemoryMap(qint64 id,
                                          const QPointer<tashtego::core::TashRuntime> runtime,
                                          const quint32 camIdx,
                                          const QString& searchfor,
                                          TashAbstractMemoryMap *parent):
    TashAbstractMemoryMap(id, runtime, parent),
    m_bHasMapSettings(false),
    m_cCamIdx(camIdx),
    m_map(0),
    m_mapSettings(QMap<QString, QVariant>()),
    m_searchFor(searchfor)
{
    auto status = init();
    Q_ASSERT(status);

    if (!status) {
        qCritical() << "TashTypedMemoryMap::init() failed...!";
        m_bIsInitialized = status;
        return;
    }

    initMap(m_mapSettings);
}

template <typename T>
TashTypedMemoryMap<T>::TashTypedMemoryMap(qint64 id,
                                          QMap<QString, QVariant> &settings,
                                          const quint32 camIdx,
                                          TashAbstractMemoryMap *parent):
    TashAbstractMemoryMap(id, parent),
    m_bHasMapSettings(settings.isEmpty() ? false : true),
    m_cCamIdx(camIdx),
    m_map(0),
    m_mapSettings(settings),
    m_searchFor("")
{
    auto status = init();
    Q_ASSERT(status);

    if (!status) {
        qCritical() << "TashTypedMemoryMap::init() failed...!";
        m_bIsInitialized = status;
        return;
    }

    initMap(m_mapSettings);
}

template <typename T>
TashTypedMemoryMap<T>::~TashTypedMemoryMap() {

#ifdef TASHVERBOSE
    qDebug() << "TypedMmap: Trying to close file";
#endif

    if ( isInitialized() && isOpen() ) {
#ifdef TASHVERBOSE
    qDebug() << "TypedMmap: not closed, close now...";
#endif
        closeMap();
    }
#ifdef TASHVERBOSE
    else {
    qDebug() << "TypedMmap: already closed...doing nothing!";
    }
#endif
}

template <typename T>
QString TashTypedMemoryMap<T>::getClassName() {
    return QString("TashTypedMemoryMap");
}

template <typename T>
void TashTypedMemoryMap<T>::initMap(QMap<QString, QVariant> &settings) {

    Q_ASSERT(this->m_bHasMapSettings);
    Q_ASSERT(!settings.isEmpty());

    if ( !this->m_bHasMapSettings || settings.isEmpty() ) {
        qWarning() << "No settings given to memory map access structure, cannot initialize memory map!";
        m_bIsOpen = m_bIsInitialized = false;
        return;
    }

    QStringList mandatoryFields;
    mandatoryFields.append("fileoptions");
    mandatoryFields.append("fileaccess");
    mandatoryFields.append("mapoptions");
    mandatoryFields.append("mapprotection");
    mandatoryFields.append("length");
    mandatoryFields.append("offset");
    mandatoryFields.append("filename");

    foreach(QString str, mandatoryFields) {

        Q_ASSERT(settings.contains(str));

        if ( !settings.contains(str)) {
            qWarning() << "Input settings do not contain settings field for " << str << ", cannot proceed!";
            m_bIsOpen = m_bIsInitialized = false;
            return;
        }
    }

    for ( auto it = settings.begin(); it != settings.end(); ++it) {
        switch( it.value().type() ) {
        case QMetaType::Int:
            if ( it.key() == "mapoptions" ) {
                setMapOptions( it.value().toInt() );
            } else if ( it.key() == "fileaccess" ) {
                setFileAccess( it.value().toInt() );
            } else if ( it.key() == "mapprotection" ) {
                setMapProtection( it.value().toInt() );
            } else if ( it.key() == "fileoptions" ) {
                setFilePermissions( it.value().toInt() );
            }
            break;
        case QMetaType::LongLong:
            if ( it.key() == "offset" ) {
                setOffset(static_cast<size_t>(it.value().toLongLong()));
            }

            if ( it.key() == "length" ) {
                m_Length = static_cast<size_t>(it.value().toLongLong());

                auto fval = static_cast<float>(m_Length);
                auto fval2= fval;
                QStringList orderlist;
                orderlist << "B" << "KB" << "MB" << "GB" << "TB" << "PB";
                auto cnt = 0;
                while ( std::floor(fval) > 1024.f ) {
                    fval /= 1024.f;
                    cnt++;

                    if ( cnt >= orderlist.size() ) {
                        cnt = 0;
                        fval = fval2;
                        break;
                    }
                }
#ifdef TASHVERBOSE
                qDebug().noquote() << "Setting length of memory map to" <<
                            fval << orderlist[cnt];
#endif
            }
            break;
        case QMetaType::QString:
            if ( it.key() == "filename" ) {
                m_Filename = it.value().toString();
            }
            break;
        default:
            qWarning() << "Unhandled type - " << it.value().typeName();
            break;
        }
    }

    m_bIsInitialized = true;
}

template<typename T>
bool TashTypedMemoryMap<T>::openMap() {

    bool status = this->isInitialized();

    Q_ASSERT( status );

    if ( !status ) {
        qWarning() << "Initialize TashMemory Map before open it!";
        return status;
    }

    int handle;

    if ( !mapExists() ) {
        qWarning() << "File does not exist, writing file first...";

        status = createMap();
        Q_ASSERT( status );

        if ( !status ) {
            qCritical() << "Map creation failed, abort!";
            return status;
        }

        status = mapExists();
        Q_ASSERT( status );

        if ( !status ) {
            qWarning() << "File creation failed, abort!";
            return status;
        }
    }

    handle = open(m_Filename.toStdString().c_str(), m_FileOptions, m_FileAccess);

    Q_ASSERT( handle > -1 );

    if ( handle < 0 ) {
        qCritical() << "Error while trying to open file!";
        perror("System error message: ");
        return false;
    }

    /*
     * Mapping the file to memory by passing it directly into a safe to
     * share QSharedPointer instance. Since Qt does not know how to handle
     * mmaps upon destruction, the way how it should be destoyed/released
     * must als be stated by passing the private method as function pointer
     * into the constructor call.
     */

    m_map = QSharedPointer<T>(static_cast<T*>( mmap(static_cast<void*>(&m_Offset),
                                                    m_Length,
                                                    m_MapProtection,
                                                    m_MapOptions,
                                                    handle,
                                                    0)),
                              TashTypedMemoryMap::releaseMap );

    /*
     * Store file handle to be able to
     * close file at destructor call
     * and by this avoid having dangling
     * unreturned filehandlers polluting the system
     */
    m_FileHandle = handle;

    Q_ASSERT( !m_map.isNull() );
    Q_ASSERT( m_map.data() != reinterpret_cast<void*>(-1) );

    if ( m_map.data() == reinterpret_cast<void*>(-1) ) {
        qCritical() << "Error while mapping file '" << m_Filename << "' to memory!";
        return false;
    }

    /*
     * Register memory map to runtime
     */
    auto dbg = qDebug();
    dbg << "Registering mmap to runtime...";

    auto descriptor = m_pRunTime->registerOpenedMemoryMap(m_Filename.split('/').last(),
                                                          QString(this->metaObject()->className()).split("::").last() );

    dbg << "done, verified by: " << descriptor;

    if ( descriptor.isEmpty() ) {
        qCritical() << "Error while registering mmap to runtime!";
    }

    m_MapDescriptor  = descriptor;

    return m_bIsOpen = true;
}

template<typename T>
bool TashTypedMemoryMap<T>::closeMap() {
#ifdef TASHVERBOSE
    qDebug() << "TASHTYPEDMEMORYMAP: CLOSEMAP()";
#endif

    auto status = this->isInitialized();

    Q_ASSERT( status );

    if ( !status ) {
        qWarning() << "Initialize and open TashTypedMemoryMap before closing it!";
        return status;
    }

    status = isOpen();

    Q_ASSERT( status );

    if ( !status ) {
        qWarning() << "Open TashTypedMemoryMap before closing it!";
        return status;
    }

    //Unmap file from memory
    if ( !m_map.isNull() ) {
        TashTypedMemoryMap::releaseMap( m_map.data() );
    }

    //Close open file
    if ( close( m_FileHandle ) != 0 ) {
        qCritical() << "Error while closing file " << m_Filename << "!";
        perror("System error message: ");
        return status = false;
    }

#ifdef TASHVERBOSE
    auto dbg = qDebug().nospace();
    dbg << "Deregistering mmap " << m_MapDescriptor << " to runtime...";
#endif

    if ( m_pRunTime->hasOpenMemoryMapRegistered(m_MapDescriptor) ) {
        status = m_pRunTime->unregisterClosedMemoryMap(m_MapDescriptor);
        if ( !status ) {
            qWarning() << "Failed to deregister mempory map instance from runtime!";
            return m_bIsOpen = status;
        }
    }

    m_bIsOpen = false;

#ifdef TASHVERBOSE
    dbg << " done!";
#endif

    return status;
}

template <typename T>
bool TashTypedMemoryMap<T>::parseMapSettingsFromRuntime() {

    using namespace tashtego::helper;

    if ( m_bHasMapSettings ) {
        qWarning() << "Map settings already parsed, or passed manually into constructor, abort parsing!";
        return m_bHasMapSettings;
    }

    auto status = m_pRunTime.isNull();
    Q_ASSERT(!status);
    if ( status ) {
        m_bHasRuntimeObject = m_bIsInitialized = !status;
        qCritical() << "Runtime object needed to create a typed memory map!";
        return m_bIsInitialized;
    }

    status = m_pRunTime->isInitialized();
    Q_ASSERT(status);
    if ( !status ) {
        m_bHasRuntimeObject = m_bIsInitialized = status;
        qCritical() << "Runtime object is not initialized!";
        return m_bIsInitialized;
    }

    QMap<QString,QVariant>  tmpMap;
    QString                 searchfor = m_searchFor;

#ifdef TASHVERBOSE
    qDebug() << "Search for " << searchfor << " for camera " << m_cCamIdx;
#endif

    QStringList mapsettings = m_pRunTime->getPropertyNamesContaining(searchfor, m_cCamIdx);

    status = mapsettings.isEmpty();
    Q_ASSERT( !status );
    if ( status ) {
        qCritical() << "Could not extract map access options, can not proceed. Abort!";
        return m_bHasMapSettings = m_bIsInitialized = !status;
    }

    if ( !searchfor.isEmpty() ) {

        foreach(QString str, mapsettings) {

            if ( str.contains("mmap_path") ) {
                tmpMap.insert(QString("filename"), m_pRunTime->property(str));
            }

            if ( str.contains("mmap_opening_offset") ) {
                tmpMap.insert(QString("offset"), m_pRunTime->property(str));
            }
        }

    } else {
        auto findProperty = [&] (const char* const id) {
            return m_pRunTime->property(m_pRunTime->getPropertyNameContaining(QString(id), m_cCamIdx));
        };

        auto findFilenameProperty = [&] (const char* const id) {

            QString finalValue;

            //Iterate through all open mmaps to find filenames which are not already openend
            if ( m_pRunTime->hasOpenMemoryMaps() ) {
                auto openMmaps = m_pRunTime->getOpenMemoryMaps();
                QStringList filenames;
                foreach (QString mmap_descriptor, openMmaps) {
                    auto parts = mmap_descriptor.split("-");
                    filenames.append(parts.at(1));
                }

                auto runtimeValues = m_pRunTime->getPropertyNamesContaining(QString(id), m_cCamIdx);

                //Compare
                for ( auto it = runtimeValues.cbegin(); it != runtimeValues.cend(); ++it ) {
                    for ( auto it2 = openMmaps.cbegin(); it2 != openMmaps.cend(); ++it2 ) {
                        QString value = m_pRunTime->property(*it).toString();

                        if ( (*it).contains(m_searchFor) ) {
                            if ( *it2 == value ) {
                                qDebug() << "Found open mmap handle to " << value;
                            } else {
                                qDebug() << "Found unopenened mmap filename: " << value;
                                finalValue = value;
                                break;
                            }
                        }
                    }
                }
            } else {
                finalValue = m_pRunTime->property(m_pRunTime->getPropertyNameContaining(QString(id), m_cCamIdx)).toString();
            }

            return QVariant( finalValue );
        };

        tmpMap.insert(QString("offset"), findProperty(TASHMAP_OFFSETID));
        tmpMap.insert(QString("filename"), findFilenameProperty(TASHMAP_PATHID));
    }

    tmpMap.insert(QString("length"), QVariant(static_cast<qint64>(sizeof(T))));

    const QChar optionDelimiter = '|';
    auto        split = [&] (const QString& toSplit, const QString& delim) {return toSplit.split(delim, QString::SkipEmptyParts);};

    foreach(QString str, mapsettings) {

        QString     identifier = split(str, "-").last();
        QVariant    tmp = m_pRunTime->property(str);

        Q_ASSERT( !tmp.isNull() );

        if ( tmp.isNull() ) {
            qCritical() << "Got invalid QVariant for property name " << str;
            return m_bHasMapSettings = false;
        }

        QString fieldvalue = tmp.toString();

        //Memory Map File Permissions
        if ( identifier == QString(TASHMAP_FILEOPENID) ) {

            QString pattern(TASHMAP_FILEOPENFLAGSREGEX);

            Q_ASSERT( TashStringChecks::checkStringAgainstRegex(fieldvalue, pattern) );

            if ( !TashStringChecks::checkStringAgainstRegex(fieldvalue, pattern) ) {
                qCritical() << "Extracted property value " << fieldvalue << " for field " << identifier << " did not pass validity check!";
                return m_bHasMapSettings = false;
            }
#ifdef TASHVERBOSE
            qDebug() << "Extracted values for 'mmap file options' are: " << fieldvalue;
#endif
            QStringList mmapoptions = split(fieldvalue, optionDelimiter);
            qint32      intermediateFlags = 0;

            foreach(QString value, mmapoptions) {
#ifdef TASHVERBOSE
                qDebug() << "New file access flag: " << value;
#endif
                intermediateFlags |= strToFileOptionType(value);
            }

#ifdef TASHVERBOSE
            qDebug() << "TashTypedMemMap: " << "intermediate flag values for file permissions are :" << intermediateFlags;
#endif
            tmpMap.insert(QString("fileoptions"), QVariant(intermediateFlags));

            //Extract values from transistion lists
            //Memory map file access options
        } else if ( identifier == QString(TASHMAP_FILEACCESSID) ) {

            QString pattern(TASHMAP_FILEACCESSREGEX);

            Q_ASSERT( TashStringChecks::checkStringAgainstRegex(fieldvalue, pattern) );

            if ( !TashStringChecks::checkStringAgainstRegex(fieldvalue, pattern) ) {
                qCritical() << "Extracted property value " << fieldvalue << " for field " << identifier << " did not pass validity check!";
                return m_bHasMapSettings = false;
            }

            auto mmapFileAccesOptions = split(fieldvalue, optionDelimiter);
            auto intermediateFlags    = 0;

            foreach(QString value, mmapFileAccesOptions) {
#ifdef TASHVERBOSE
                qDebug() << "New file access flag: " << value;
#endif
                intermediateFlags |= strToFileAccessType(value);
            }

            tmpMap.insert(QString("fileaccess"), QVariant(intermediateFlags));
#ifdef TASHVERBOSE
            qDebug() << "TashTypedMemMap: " << "intermediate flag values for file access are :" << intermediateFlags;
#endif

        } else if ( identifier == QString(TASHMAP_OPTIONSID) ) {
#ifdef __linux__
            QString pattern(TASHMAP_OPTIONSLINUXREGEX);
#else
            QString pattern(TASHMAP_OPTIONSREGEX);
#endif

            Q_ASSERT( TashStringChecks::checkStringAgainstRegex(fieldvalue, pattern) );

            if ( !TashStringChecks::checkStringAgainstRegex(fieldvalue, pattern) ) {
                qCritical() << "Extracted property value " << fieldvalue << " for field " << identifier << " did not pass validity check!";
                return m_bHasMapSettings = false;
            }

            QStringList mmapOptions = split(fieldvalue, optionDelimiter);
            qint32      intermediateFlags = 0;
#ifdef TASHVERBOSE
            qDebug() << "FIELDVALUE FOR MAP OPTIONS: " << fieldvalue;
#endif
            foreach(QString value, mmapOptions) {
#ifdef TASHVERBOSE
                qDebug() << "New map option flag: " << value;
#endif
                intermediateFlags |= strToMapOptionsType(value);
            }

            tmpMap.insert(QString("mapoptions"), QVariant(intermediateFlags));
#ifdef TASHVERBOSE
            qDebug() << "TashTypedMemMap: " << "intermediate flag values for mapoptions are :" << intermediateFlags;
#endif
        } else if ( identifier == QString(TASHMAP_PROTECTIONID) ) {
            QString pattern(TASHMAP_PROTECTIONREGEX);

            Q_ASSERT( TashStringChecks::checkStringAgainstRegex(fieldvalue, pattern) );

            if ( !TashStringChecks::checkStringAgainstRegex(fieldvalue, pattern) ) {
                qCritical() << "Extracted property value " << fieldvalue << " for field " << identifier << " did not pass validity check!";
                return m_bHasMapSettings = false;
            }

            QStringList mmapProtectionOptions = split(fieldvalue, optionDelimiter);
            qint32      intermediateFlags = 0;

            foreach(QString value, mmapProtectionOptions) {
#ifdef TASHVERBOSE
                qDebug() << "New map protection flag: " << value;
#endif
                intermediateFlags |= strToMapProtectionType(value);
            }

            tmpMap.insert(QString("mapprotection"), QVariant(intermediateFlags));
#ifdef TASHVERBOSE
            qDebug() << "TashTypedMemMap: intermediate flag values for map protection are :" << intermediateFlags;
#endif
        } else {
#ifdef TASHVERBOSE
            qDebug() << "Discarding identifier " << identifier;
#endif
        }
    }

    m_mapSettings = tmpMap;
    return m_bHasMapSettings = true;
}

template <typename T>
bool TashTypedMemoryMap<T>::createMap() {

    auto status = this->isInitialized();

    Q_ASSERT(status);

    //Check if object instance is initialized
    if ( !status ) {
        qWarning() << "Initialize TashMemory Map before open it!";
        return status;
    }

    status = mapExists();

    if ( status ) {
        qWarning() << "A file with same name/size already exists, skip creation!";
        return status;
    }

    //Check if file is already mapped
    if ( isOpen() ) {
        qWarning() << "File already mapped to memory, skip creation!";
        return status;
    }

    //All checks passed, open file only for writing operations
    QFile file(m_Filename);
    status = file.open( QIODevice::WriteOnly );

    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "Could not open file " << m_Filename << "!";
        return status;
    }

    //Initialize necessary objects for a write operation
    uint                        sz = static_cast<uint>(m_Length);
    QDataStream                 writer(&file);
    QScopedArrayPointer<char>   pStuffing(new char[sz]);

    //Prefill array to write to zero
    memset(pStuffing.data(), 0, static_cast<size_t>(sz));
    writer.writeBytes(pStuffing.data(), sz);

    Q_ASSERT(writer.status() != QDataStream::WriteFailed);

    //Check writer status
    if ( writer.status() == QDataStream::WriteFailed ) {
        qCritical() << "Error while writing to file via QDataStream!";
        status = false;
    } else {
        status = true;
    }

    //close file
    file.close();

    return status;
}

template <typename T>
T* TashTypedMemoryMap<T>::getUnsafeMapAccess () {

    Q_ASSERT( this->isInitialized() );

    if ( !(this->isInitialized()) ) {
        qCritical() << "Map is not initialized, hence not open!";
        return nullptr;
    }

    Q_ASSERT( this->isOpen() );

    if ( !(this->isOpen()) ) {
        qCritical() << "Map is not open! Abort!";
        return nullptr;
    }

    T* pTmp = this->m_map.data();

    Q_ASSERT( pTmp );

    if ( !pTmp ) {
        qCritical() << "Pointer to memory map is invalid, abort!";
        return nullptr;
    }

    return pTmp;
}

template <typename T>
QSharedPointer<T> TashTypedMemoryMap<T>::getSafeMapAccess () {

    Q_ASSERT(this->isInitialized());

    if ( !(this->isInitialized()) ) {
        qWarning() << "Map is not initialized, hence not open!";
        return QSharedPointer<T>(nullptr);
    }

    Q_ASSERT( this->isOpen() );

    if ( !(this->isOpen()) ) {
        qWarning() << "Map is not open! Abort!";
        return QSharedPointer<T>(nullptr);
    }

    Q_ASSERT(!this->m_map.isNull());

    if ( this->m_map.isNull() ) {
        qWarning() << "Pointer to memory map is invalid, abort!";
        return QSharedPointer<T>(nullptr);
    }

    return this->m_map;
}

template <typename T>
bool TashTypedMemoryMap<T>::mapAlreadyExisted() {
    return m_bMapExistedBefore;
}

template <typename T>
bool TashTypedMemoryMap<T>::init() {

    bool status = this->parseMapSettingsFromRuntime();
    Q_ASSERT( status );

    if ( !status ) {
        qCritical() << "Error while parsing properties from runtime!";
    }

    return m_bIsInitialized = status;
}

template <typename T>
void TashTypedMemoryMap<T>::releaseMap (T* pMap) {
    //Unmap file from memory
    if ( munmap( static_cast<void*>(pMap), sizeof(T) ) != 0 ){
        qCritical() << "Failed to unmap memory map!";
        perror("System error message: ");
    }
}
}

/*
 * Convenience wrappers, distributed all over tashtego namespace
 */

//Convenience wrapper for a RDE memory map
using TashMapAcc_Firstmap = tashtego::com::TashTypedMemoryMap<tashtego::rawtypes::RDEFormat_r>;
//Convenience wrapper for a VMEM memory map
using TashMapAcc_VMEM = tashtego::com::TashTypedMemoryMap<rawtypes::VMEMFormat_r>;
//Convenience wrapper for a IMEM memory map
using TashMapAcc_IMEM = tashtego::com::TashTypedMemoryMap<rawtypes::IMEMFormat_r>;
//Convenience wrapper for a Ramdisk memory map
using TashMapAcc_RAMDisk = tashtego::com::TashTypedMemoryMap<rawtypes::RAMDISKBuffer_r>;
//Convenience wrapper for mmap access to imagestack.mmap
using TashMapAcc_ImageStack = tashtego::com::TashTypedMemoryMap<rawtypes::framebuffer_r>;
//Convenience wrapper for mmap access to detections.mmap
using TashMapAcc_ScoreMap = tashtego::com::TashTypedMemoryMap<rawtypes::scoreMap_r>;
//Convenience wrapper for mmap access to fvi files
using TashMapAcc_FVIFile = tashtego::com::TashTypedMemoryMap<rawtypes::FVIFile_r>;
//Convenience wrapper for mmap access to detector range dimensions
using TashMapAcc_DetectDims = tashtego::com::TashTypedMemoryMap<rawtypes::detectorDim_r>;
//Convenience wrapper for mmap access to detection cluster map
using TashMapAcc_DetectionCluster = tashtego::com::TashTypedMemoryMap<rawtypes::detectionClusterMap_r>;
//Convenience wrapper for mmap access to ui command map
using TashMapAcc_UICommands = tashtego::com::TashTypedMemoryMap<rawtypes::uiCommands_r>;
//Convenience wrapper for mmap access to counter detections mmap
using TashMapAcc_CounterDetections = tashtego::com::TashTypedMemoryMap<rawtypes::counterDetections_r>;
//Convenience wrapper for mmap access to EMEM memory map
using TashMapAcc_EMEM = tashtego::com::TashTypedMemoryMap<rawtypes::EMEMFormat_r>;
}

#endif // TASHTYPEDMEMORYMAP_H

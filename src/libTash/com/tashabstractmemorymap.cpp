/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashabstractmemorymap.cpp
 * @author Michael Flau
 * @brief
 * @details
 * @date June 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started June 2015
 */
#include "tashabstractmemorymap.h"

#ifdef Q_OS_UNIX
//Linux C header
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#elif Q_OS_WIN32
// Windows C header
#elif Q_OS_MAC
// Apple C header
#endif


//Qt header
#include <QMap>
#include <QVariant>
#include <QString>
#include <QFile>
#include <QDebug>
#include <QDataStream>
#include <QScopedArrayPointer>


namespace tashtego {
namespace com {

TashAbstractMemoryMap::TashAbstractMemoryMap(TashObject *parent):
    TashObject(parent),
    m_pMap(0),
    m_FileHandle(0),
    m_Length(0),
    m_Offset(0),
    m_MapOptions(0),
    m_FileAccess(0),
    m_MapProtection(0),
    m_FileOptions(0),
    m_bIsOpen(false),
    m_bMapExistedBefore(false),
    m_bMapExistedFlagWritten(false),
    m_Filename(""),
    m_MapDescriptor("")
{

}

/**
 * @brief TashAbstractMemoryMap::TashAbstractMemoryMap
 * @param id
 * @param parent
 */
TashAbstractMemoryMap::TashAbstractMemoryMap(qint64 id, TashObject *parent):
    TashObject(id, parent),
    m_pMap(0),
    m_FileHandle(0),
    m_Length(0),
    m_Offset(0),
    m_MapOptions(0),
    m_FileAccess(0),
    m_MapProtection(0),
    m_FileOptions(0),
    m_bIsOpen(false),
    m_bMapExistedBefore(false),
    m_bMapExistedFlagWritten(false),
    m_Filename(""),
    m_MapDescriptor("")
{
    initTransitionLists();
}

/**
 * @brief TashAbstractMemoryMap::TashAbstractMemoryMap
 * @param id
 * @param filename
 * @param parent
 */
TashAbstractMemoryMap::TashAbstractMemoryMap(qint64 id, const QPointer<core::TashRuntime> runtime, TashObject *parent):
    TashObject(id, runtime, parent),
    m_pMap(0),
    m_FileHandle(0),
    m_Length(0),
    m_Offset(0),
    m_MapOptions(0),
    m_FileAccess(0),
    m_MapProtection(0),
    m_FileOptions(0),
    m_bIsOpen(false),
    m_Filename(""),
    m_MapDescriptor("")
{
    initTransitionLists();
}

/**
 * @brief TashAbstractMemoryMap::~AbstractTashObject
 */
TashAbstractMemoryMap::~TashAbstractMemoryMap(){
    if ( isInitialized() && isOpen() ) closeMap();
}

/**
 * @brief TashAbstractMemoryMap::createMap
 * @param len
 * @param filename
 * @return
 */
bool TashAbstractMemoryMap::createMap(size_t &len, QString filename) {

    bool status = false;

    if ( !m_bIsInitialized ) {
        qWarning() << "Initialize memory map before opening it!";
        return status;
    }

    QFile file(filename);
    status = file.exists();

    if ( status && ( static_cast<size_t>(file.size()) == len) ) {
        qWarning() << "A file with the same name/size already exists, skip creation!";
        return status;
    }

    if ( m_bIsOpen ) {
        qWarning() << "File already mapped to memory, skip creation!";
        return status;
    }

    status = file.open(QIODevice::WriteOnly);

    if ( !status ) {
        qCritical() << "Could not open file '" << filename << "'!";
        return status;
    }

    QDataStream writer(&file);
    QScopedArrayPointer<char> pStuffing(new char[static_cast<quint32>(len)]);

    memset(pStuffing.data(), 0, len);
    writer.writeBytes(pStuffing.data(), static_cast<quint32>(len));

    if ( writer.status() == QDataStream::WriteFailed ) {
        qCritical() << "Error while writing to file via QDataStream!";
        status = false;
    } else {
        status = true;
    }

    file.close();
    return status;
}

/**
 * @brief TashAbstractMemoryMap::openMap
 * @return
 */
bool TashAbstractMemoryMap::openMap() {

    bool status = false;

    if ( !isInitialized() ) {
        qCritical() << "Initialize memory map instance before invoking openMap()!";
        return status;
    }

    if ( !mapExists() ) {
        qCritical() << "File to be mapped does not exist, write dummy file first...";
        status = createMap();

        if ( !status || !mapExists() ) {
            qCritical() << "File creation failed, abort!";
            return status;
        }
    }

#ifdef Q_OS_UNIX
    qint32 handle = open(m_Filename.toStdString().c_str(), m_FileOptions, m_FileAccess);

    if ( handle == -1 ) {
        qCritical() << "Error while trying to open file!";
        perror("System error message: ");
        return status;
    }
    m_pMap = mmap(static_cast<void*>(&m_Offset),
                  m_Length,
                  m_MapProtection,
                  m_MapOptions,
                  handle,
                  0);

    if ( m_pMap == reinterpret_cast<void*>(-1) ) {
        qCritical() << "Error while mapping file '" << m_Filename << "' to memory!";
        return false;
    }
#elif Q_OS_WIN32
    qDebug("No memory map implementation for windows present!");
    return false;
#elif Q_OS_MAC
    qDebug("No memory map implementation for Apple devices present!");
    return false;
#endif

    auto descriptor = m_pRunTime->registerOpenedMemoryMap(m_Filename, QString(this->metaObject()->className()) );

    if ( descriptor.isEmpty() ) {
        qCritical() << "Error while registering mmap to runtime!";
    }

    m_MapDescriptor  = descriptor;
    m_FileHandle     = handle;
    return m_bIsOpen = status = true;
}

/**
 * @brief TashAbstractMemoryMap::closeMap
 * @return
 */
bool TashAbstractMemoryMap::closeMap() {

    qDebug() << "TASHABSTRACTMEMORYMAP: CLOSEMAP()";

    bool status = m_bIsInitialized;


    if ( !status ) {
        qCritical() << "Initialize and open memory map before closing it!";
        return status;
    }

    status = m_bIsOpen;

    if ( !status ) {
        qCritical() << "Open memory map before closing it!";
        return status;
    }

#ifdef Q_OS_UNIX
    if ( m_pMap ) {
        //Write changes to map before closing
        this->syncMap();

        if ( munmap( m_pMap, m_Length) != 0 ) {
            qCritical() << "Failed to unmap memory map!";
            perror("System error message: ");
            status = false;
        } else {
            status = true;
        }
    }

    if ( m_bIsOpen ) {
        if ( close( m_FileHandle) != 0 ) {
            qCritical() << "Error while closing file '" << m_Filename << "'!";
            perror("System error message: ");
            status = false;
        } else {
            status = true;
        }
    }
#elif Q_OS_WIN32 || Q_OS_WIN64
    qDebug("No memory map implementation for windows present!");
    return false;
#elif Q_OS_MAC
    qDebug("No memory map implementation for Apple devices present!");
    return false;
#endif

    if ( m_pRunTime->hasOpenMemoryMapRegistered(m_MapDescriptor) ) {
        status = m_pRunTime->unregisterClosedMemoryMap(m_MapDescriptor);
        if ( !status ) {
            qWarning() << "Failed to deregister mempory map instance from runtime!";
        }
    }

    m_bIsOpen = false;
    return status;
}

bool TashAbstractMemoryMap::syncMap() {

    Q_ASSERT(m_bIsInitialized);
    Q_ASSERT(m_bIsOpen);

    auto status = 0;
#ifdef Q_OS_UNIX

    status = msync(m_pMap, m_Length, MS_SYNC | MS_INVALIDATE);

    if ( !status ) {
        qCritical() << "Failed to sync memory map " << m_Filename;
        perror("System error message: ");
        return false;
    }

#elif Q_OS_WIN32 || Q_OS_WIN64
    qDebug("No memory map implementation for windows present!");
    return false;
#elif Q_OS_MAC
    qDebug("No memory map implementation for Apple devices present!");
    return false;
#endif
    return true;
}

/**
 * @brief TashAbstractMemoryMap::isOpen
 * @return
 */
bool TashAbstractMemoryMap::isOpen() const {
    return m_bIsOpen;
}

/**
 * @brief TashAbstractMemoryMap::mapExists
 * @param filename
 * @return
 */
bool TashAbstractMemoryMap::mapExists(QString filename){

    QString tmp     = filename.isEmpty() ? m_Filename : filename;
    QFile file(tmp);

    auto exists     = file.exists();
    auto sz_equal   = (file.size() == static_cast<qint64>(m_Length) + 4);

#ifdef TASHVERBOSE
    qDebug() << "FILE EXISTS: " << (exists && sz_equal);
    qDebug() << "FILENAME: "    << m_Filename;
    qDebug() << "FILE SIZE: "   << file.size() << " bytes";
    qDebug() << "COMP SIZE: "   << static_cast<qint64>(m_Length)+4 << " bytes";
#endif

    if ( !m_bMapExistedFlagWritten ) {
        m_bMapExistedBefore = (exists && sz_equal);
        m_bMapExistedFlagWritten = true;
    }

    return (exists && sz_equal);
}

/**
 * @brief TashAbstractMemoryMap::setPermission
 * @param perm
 * @return
 */
bool TashAbstractMemoryMap::setFilePermissions(const qint32 &perm) {

    if ( m_bIsOpen ) {
        qWarning() << "Can't set permissions on a currently opened memory map. Call closeMap() first!";
        return true;
    }

    qint32 tmpFileOpenFlags = 0;

#ifdef Q_OS_UNIX
    if ( perm & ReadOnly ) {
        tmpFileOpenFlags |= O_RDONLY;
    } else if ( perm & WriteOnly ) {
        tmpFileOpenFlags |= O_WRONLY;
    } else if ( perm & ReadWrite ) {
        tmpFileOpenFlags |= O_RDWR;
    }

    if ( tmpFileOpenFlags & O_WRONLY || tmpFileOpenFlags & O_RDWR ) {
        if ( perm & Append ) {
            tmpFileOpenFlags |= O_APPEND;
        } else if ( perm & Truncate ) {
            tmpFileOpenFlags |= O_TRUNC;
        } else if ( perm & Create ) {
            tmpFileOpenFlags |= O_CREAT;
        }

        if ( tmpFileOpenFlags & O_CREAT ) {
            tmpFileOpenFlags |= O_EXCL;
        }
    }
#elif Q_OS_WIN32
    qDebug("No memory map implementation for windows present!");
    return false;
#elif Q_OS_MAC
    qDebug("No memory map implementation for Apple devices present!");
    return false;
#endif

    m_FileOptions = tmpFileOpenFlags;
    return true;
}

/**
 * @brief TashAbstractMemoryMap::setAccess
 * @param accs
 * @return
 */
bool TashAbstractMemoryMap::setFileAccess(const qint32 &accs) {

    if ( m_bIsOpen ) {
        qWarning() << "Can't set file access permissions on a currently opened memory map. Call closeMap() first!";
        return true;
    }

    qint32 tmpFileAccessFlags = 0;
#ifdef Q_OS_UNIX
    QMap<eFileAccessModeFlags, qint32> accessors;
    accessors.insert(OwnerCanRead, S_IRUSR);
    accessors.insert(OwnerCanWrite, S_IWUSR);
    accessors.insert(OwnerCanExec, S_IXUSR);
    accessors.insert(GroupCanRead, S_IRGRP);
    accessors.insert(GroupCanWrite, S_IWGRP);
    accessors.insert(GroupCanExec, S_IXGRP);
    accessors.insert(OtherCanRead, S_IROTH);
    accessors.insert(OtherCanWrite, S_IWOTH);
    accessors.insert(OtherCanExec, S_IXOTH);

    QMap<eFileAccessModeFlags, qint32>::iterator it;

    for (it = accessors.begin(); it != accessors.end(); ++it) {
        qint32 val = static_cast<qint32>(it.key());
        if ( accs & val ) tmpFileAccessFlags |= it.value();
    }

#elif Q_OS_WIN32
    qDebug("No memory map implementation for windows present!");
    return false;
#elif Q_OS_MAC
    qDebug("No memory map implementation for Apple devices present!");
    return false;
#endif

    m_FileAccess = tmpFileAccessFlags;
    return true;
}

/**
 * @brief TashAbstractMemoryMap::setProtection
 * @param prot
 * @return
 */
bool TashAbstractMemoryMap::setMapProtection(const qint32 &prot) {

    if ( m_bIsOpen ) {
        qWarning() << "Can't set map access permissions on a currently opened memory map. Call closeMap() first!";
        return true;
    }

    qint32 tmpMapProtectionFlags = 0;
#ifdef Q_OS_UNIX
    QMap<eMapProtection, qint32> accessors;
    accessors.insert(MapRead, PROT_READ);
    accessors.insert(MapWrite, PROT_WRITE);
    accessors.insert(MapNone, PROT_NONE);
    accessors.insert(MapExec, PROT_EXEC);

    QMap<eMapProtection, qint32>::iterator it;

    for (it = accessors.begin(); it != accessors.end(); ++it) {
        qint32 val = static_cast<qint32>(it.key());
        if ( prot & val ) tmpMapProtectionFlags |= it.value();
    }

#elif Q_OS_WIN32
    qDebug("No memory map implementation for windows present!");
    return false;
#elif Q_OS_MAC
    qDebug("No memory map implementation for Apple devices present!");
    return false;
#endif

    m_MapProtection = tmpMapProtectionFlags;
    return true;
}

/**
 * @brief TashAbstractMemoryMap::setMapOptions
 * @param opt
 * @return
 */
bool TashAbstractMemoryMap::setMapOptions(const qint32 &opt) {

    if ( m_bIsOpen ) {
        qWarning() << "Can't set map options on a currently opened memory map. Call closeMap() first!";
        return true;
    }

    qint32 tmpMapOptions = 0;
#ifdef Q_OS_UNIX
    QMap<eMapOptions, qint32> options;
    options.insert(MapShared, MAP_SHARED);
    options.insert(MapPrivate, MAP_PRIVATE);
    options.insert(MapAnonymous, MAP_ANONYMOUS);
    options.insert(MapPopulate, MAP_POPULATE);
    options.insert(MapLocked, MAP_LOCKED);
    options.insert(MapDenyWrite, MAP_DENYWRITE);
    options.insert(MapGrowsDown, MAP_GROWSDOWN);
    options.insert(MapNonBlock, MAP_NONBLOCK);
//    options.insert(MapExec, MAP_EXECUTABLE);

    QMap<eMapOptions, qint32>::iterator it;

    for(it = options.begin(); it != options.end(); ++it) {
        if ( opt & static_cast<qint32>(it.key()) ) tmpMapOptions |= it.value();
    }

#elif Q_OS_WIN32
    qDebug("No memory map implementation for windows present!");
    return false;
#elif Q_OS_MAC
    qDebug("No memory map implementation for Apple devices present!");
    return false;
#endif

    m_MapOptions = tmpMapOptions;
    return true;
}

/**
 * @brief TashAbstractMemoryMap::setOffset
 * @param off
 * @return
 */
bool TashAbstractMemoryMap::setOffset(size_t off) {

    if ( m_bIsOpen ) {
        qWarning() << "Can't set map offset on a currently opened memory map. Call closeMap() first!";
        return true;
    }

    if ( off >= m_Length ) {
        qCritical() << "The offset cannot be greater than the actual filesize!";
        return false;
    }

    m_Offset = off;

    return true;
}

/**
 * @brief TashAbstractMemoryMap::initTransitionLists
 */
void TashAbstractMemoryMap::initTransitionLists() {

    //File open transition list
    m_MapFileOptionTransitionList.insert("ReadOnly",  ReadOnly);
    m_MapFileOptionTransitionList.insert("WriteOnly", WriteOnly);
    m_MapFileOptionTransitionList.insert("ReadWrite", ReadWrite);
    m_MapFileOptionTransitionList.insert("Append",    Append);
    m_MapFileOptionTransitionList.insert("Truncate",  Truncate);
    m_MapFileOptionTransitionList.insert("Create",    Create);
    m_MapFileOptionTransitionList.insert("Exclusive", Exclusive);

    //File access transition list
    m_MapFileAccessModeTransitionList.insert("OwnerCanRead", OwnerCanRead);
    m_MapFileAccessModeTransitionList.insert("OwnerCanWrite",OwnerCanWrite);
    m_MapFileAccessModeTransitionList.insert("OwnerCanExec", OwnerCanExec);
    m_MapFileAccessModeTransitionList.insert("GroupCanRead", GroupCanRead);
    m_MapFileAccessModeTransitionList.insert("GroupCanWrite",GroupCanWrite);
    m_MapFileAccessModeTransitionList.insert("GroupCanExec", GroupCanExec);
    m_MapFileAccessModeTransitionList.insert("OtherCanRead", OtherCanRead);
    m_MapFileAccessModeTransitionList.insert("OtherCanWrite",OtherCanWrite);
    m_MapFileAccessModeTransitionList.insert("OtherCanExec", OtherCanExec);

    //Map protection transition list
    m_MapProtectionTransitionList.insert("MapRead", MapRead);
    m_MapProtectionTransitionList.insert("MapWrite",MapWrite);
    m_MapProtectionTransitionList.insert("MapNone", MapNone);
    m_MapProtectionTransitionList.insert("MapExec", MapExec);

    //Map options transition list
    m_MapOptionsTransitionList.insert("MapShared",   MapShared);
    m_MapOptionsTransitionList.insert("MapPrivate",  MapPrivate);
    m_MapOptionsTransitionList.insert("MapAnonymous",MapAnonymous);
    m_MapOptionsTransitionList.insert("MapPopulate", MapPopulate);
#ifdef Q_OS_UNIX
    m_MapOptionsTransitionList.insert("MapLocked",    MapLocked);
    m_MapOptionsTransitionList.insert("MapDenyWrite", MapDenyWrite);
    m_MapOptionsTransitionList.insert("MapGrowsDown", MapGrowsDown);
    m_MapOptionsTransitionList.insert("MapNonBlock", MapNonBlock);
#endif

}

/**
 * @brief strToFileOpenType
 * @param typestr
 * @return
 */
TashAbstractMemoryMap::eFileOpenFlags TashAbstractMemoryMap::strToFileOptionType(const QString& typestr) {

    if ( typestr.isEmpty() ) {
        qWarning() << "Input argument does not contain anything valid!";
        return static_cast<eFileOpenFlags>(-1);
    }

    if ( !m_MapFileOptionTransitionList.contains(typestr) ) {
        qWarning() << "There is no such File Open flag for input string " << typestr;
        return static_cast<eFileOpenFlags>(-1);
    }

    return m_MapFileOptionTransitionList.value(typestr);
}

/**
 * @brief strToMapProtectionType
 * @param typestr
 * @return
 */
TashAbstractMemoryMap::eMapProtection TashAbstractMemoryMap::strToMapProtectionType(const QString& typestr) {

    if ( typestr.isEmpty() ) {
        qWarning()<< "Input argument does not contain anything valid!";
        return static_cast<eMapProtection>(-1);
    }

    if ( !m_MapProtectionTransitionList.contains(typestr) ) {
        qWarning();
        return static_cast<eMapProtection>(-1);
    }

    return m_MapProtectionTransitionList.value(typestr);
}

/**
 * @brief strToFileAccessType
 * @param typestr
 * @return
 */
TashAbstractMemoryMap::eFileAccessModeFlags TashAbstractMemoryMap::strToFileAccessType(const QString& typestr) {

    if ( typestr.isEmpty() ) {
        qWarning()<< "Input argument does not contain anything valid!";
        return static_cast<eFileAccessModeFlags>(-1);
    }

    if ( !m_MapFileAccessModeTransitionList.contains(typestr) ) {
        qWarning();
        return static_cast<eFileAccessModeFlags>(-1);
    }

    return m_MapFileAccessModeTransitionList.value(typestr);
}

/**
 * @brief strToMapOptionsType
 * @param typestr
 * @return
 */
TashAbstractMemoryMap::eMapOptions TashAbstractMemoryMap::strToMapOptionsType(const QString& typestr) {

    if ( typestr.isEmpty() ) {
        qWarning()<< "Input argument does not contain anything valid!";
        return static_cast<eMapOptions>(-1);
    }

//    qDebug() << "ABSTRACT - SEARCHING FOR TYPESTRING: " << typestr;

    if ( !m_MapOptionsTransitionList.contains(typestr) ) {
        qWarning() << "ABSTRACT - NO TYPESTRING CALLED: " << typestr;
        return static_cast<eMapOptions>(-1);
    }

//    qDebug() << "ABSTRACT - TYPESTRING: " << typestr << " FOUND!";

    return m_MapOptionsTransitionList.value(typestr);
}



//REGISTER_METATYPE(TashAbstractMemoryMap*, "TashAbstractMemoryMap")

}
}


/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashabstractmemorymap.h
 * @author Michael Flau
 * @brief
 * @details
 * @date June 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started June 2015
 */

#ifndef TASHABSTRACTMEMORYMAP_H
#define TASHABSTRACTMEMORYMAP_H

//Qt
#include <QMap>

//tashtego
#include "../core/tashobject.h"

namespace tashtego {
namespace com {

/**
 * @brief The TashAbstractMemoryMap class
 * This is the very base class for any logic related to
 * process communication via memory mapped files.
 * @details
 * As all the data forwarding within tashtego is carried out via memory maps,
 * the need for a unified solution across all client apps was at hand.
 *
 * Introduction
 * ------------
 * To initialize a memory map sucessfully certain requirements must be met before
 * the map can be created:
 * - The user which is starting the app needs read/write permission to the location where the file is supposed to reside.
 *   Within tashtego the mmap location for all mmaps is usually <code>/mnt/mmap/</code>. The user tashtego should have
 *   full access rights to this location.
 * - The introspection process must have detected the mmap-accessor instance during first app run, to write default settings
 *   the ini-file (Refer to TashRuntime manual page for further info about this).
 * - The default settings in the ini-file must be adapted to the client app's need.
 *
 * @note
 * An excellent introduction to the usage of memory maps and the relates OS-functions can be found in the manpages:
 * @code
 * ~$> man mmap
 * @endcode
 *
 * Map Settings
 * ------------
 * The ini-file representation of an mmap object comes with a bunch of settings represented as flags which the dev using this lib needs to
 * be aware of.
 *
 * |Flag |Type |OS-derivate|Description|
 * |:---:|:---:|:---------:|:---------:|
 *
 * The class is kept abstract with the intention to enforce that any derived class has to setup its own
 * retrieval method to access the actual memory which is mapped in RAM. Thus, only general initialization
 * and release logic is kept here which is almost certainly always applicable for any kind of mappable memory structure.
 * The most important point to this class (and each which is derived from it) is to understand, that this class does not
 * reflect a certain field structure mapped to memory. Mapped files are *not* directly accessible via a TashAbstractMemoryMap object.
 * Moreover this class and it's derivates are intended to be used as resource management objects which grant safe access *to* a mapped memory range.
 * Since it can not be known in advance of which structure the mapped file is going to be, no default
 * method to access a mapped file is provided here. It lies with the sub-class author's responsibility
 * to provide a method which returns access to the mapped file with the correct field structure associated to it.
 */
class TashAbstractMemoryMap : public core::TashObject
{
    Q_OBJECT
public:

    /**
     * @brief The eOpenFlags enum
     * Abstraction of C flags which depict the
     * access mode of a file intended to be opened.
     * This was merely implemented to avoid having
     * C headers pollute the whole namespace
     * by putting them into a header file. That way the headers
     * can be kept in the actual source file of this class.
     */
    enum eFileOpenFlags{
        ReadOnly        = 0x001, // O_RDONLY
        WriteOnly       = 0x002, // O_WRONLY
        ReadWrite       = 0x004, // O_RDWR
        Append          = 0x008, // O_APPEND
        Truncate        = 0x010, // O_TRUNC
        Create          = 0x020, // O_CREAT
        Exclusive       = 0x040  // O_EXCL
    };

    /**
     * @brief The eModeFlags enum
     * This enum is an abstraction to the POSIX C access
     * flags which can be used to set file access rights
     * when opening a file. This was merely implemented to
     * avoid having C headers pollute the whole namespace
     * by putting them into a header file. That way the headers
     * can be kept in the actual source file of this class.
     */
    enum eFileAccessModeFlags{
        OwnerCanRead    = 0x001, // S_IRUSR
        OwnerCanWrite   = 0x002, // S_IWUSR
        OwnerCanExec    = 0x004, // S_IXUSR
        GroupCanRead    = 0x008, // S_IRGRP
        GroupCanWrite   = 0x010, // S_IWGRP
        GroupCanExec    = 0x020, // S_IXGRP
        OtherCanRead    = 0x040, // S_IROTH
        OtherCanWrite   = 0x100, // S_IWOTH
        OtherCanExec    = 0x200  // S_IXOTH
    };

    /**
     * @brief The eMapProtectFlags enum
     */
    enum eMapProtection{
        MapRead         = 0x001, // PROT_READ
        MapWrite        = 0x002, // PROT_WRITE
        MapNone         = 0x004, // PROT_NONE
        MapExec         = 0x008 // PROT_EXEC
    };

    /**
     * @brief The eMapOptions enum
     */
    enum eMapOptions {
        MapShared       = 0x001, // MAP_SHARED
        MapPrivate      = 0x002, // MAP_PRIVATE
        MapAnonymous    = 0x004, // MAP_ANOYMOUS
        MapPopulate     = 0x008, // MAP_POPULATE
#ifdef Q_OS_LINUX
        MapLocked       = 0x010, // MAP_LOCKED
        MapDenyWrite    = 0x020, // MAP_DENYWRITE
        MapGrowsDown    = 0x040, // MAP_GROWSDOWN
        MapNonBlock     = 0x080  // MAP_NONBLOCK
#endif
    };

    /**
     * @brief TashAbstractMemoryMap
     * @param parent
     */
    explicit TashAbstractMemoryMap(TashObject *parent = 0);

    /**
     * @brief TashAbstractMemoryMap
     * @param id
     * @param parent
     */
    TashAbstractMemoryMap(qint64 id, TashObject *parent = 0);

    /**
     * @brief TashAbstractMemoryMap
     * @param id
     * @param filename
     * @param parent
     */
    TashAbstractMemoryMap(qint64 id, const QPointer<core::TashRuntime> runtime, TashObject *parent);

    /**
     * @brief ~TashAbstractMemoryMap
     */
    virtual ~TashAbstractMemoryMap();

    /**
     * @brief init
     * @return
     */
    virtual bool init() = 0;

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() = 0;

    /**
     * @brief initMap
     * @param settings
     */
    virtual void initMap(QMap<QString, QVariant> &settings) = 0;

    /**
     * @brief openMap
     * @return
     */
    virtual bool openMap();

    /**
     * @brief closeMap
     * @return
     */
    virtual bool closeMap();

    /**
     * @brief syncMap
     * @return
     */
    virtual bool syncMap();

    /**
     * @brief isOpen
     * @return
     */
    bool isOpen() const;

    /**
     * @brief mapExists
     * @param filename
     * @return
     */
    bool mapExists(QString filename = "");

protected:

    /**
     * @brief createMap
     * @return
     */
    virtual bool createMap() = 0;

    /**
     * @brief createMap
     * @param len
     * @param filename
     * @return
     */
    virtual bool createMap(size_t &len, QString filename);

    /**
     * @brief setPermission
     * @param perm
     * @return
     */
    bool setFilePermissions(const qint32 &perm); //eFileOpenFlags

    /**
     * @brief setAccess
     * @param accs
     * @return
     */
    bool setFileAccess(const qint32 &accs); //eFileAccessModeFlags

    /**
     * @brief setProtection
     * @param prot
     * @return
     */
    bool setMapProtection(const qint32 &prot); //eMapProtection

    /**
     * @brief setMapOptions
     * @param opt
     * @return
     */
    bool setMapOptions(const qint32 &opt); //eMapOptions

    /**
     * @brief setOffset
     * @param off
     * @return
     */
    bool setOffset(size_t off);

    /**
     * @brief initTransitionLists
     */
    void initTransitionLists();

    /**
     * @brief strToFileOpenType
     * @param typestr
     * @return
     */
    eFileOpenFlags strToFileOptionType(const QString& typestr);

    /**
     * @brief strToMapProtectionType
     * @param typestr
     * @return
     */
    eMapProtection strToMapProtectionType(const QString& typestr);


    /**
     * @brief strToFileAccessType
     * @param typestr
     * @return
     */
    eFileAccessModeFlags strToFileAccessType(const QString& typestr);

    /**
     * @brief strToMapOptionsType
     * @param typestr
     * @return
     */
    eMapOptions strToMapOptionsType(const QString& typestr);

    //Member
    void*   m_pMap;
    qint32  m_FileHandle;
    size_t  m_Length;
    size_t  m_Offset;
    qint32  m_MapOptions;
    qint32  m_FileAccess;
    qint32  m_MapProtection;
    qint32  m_FileOptions;
    bool    m_bIsOpen;
    bool    m_bMapExistedBefore;
    bool    m_bMapExistedFlagWritten;
    QString m_Filename;
    QString m_MapDescriptor;

    QMap<QString, eMapOptions>          m_MapOptionsTransitionList;
    QMap<QString, eMapProtection>       m_MapProtectionTransitionList;
    QMap<QString, eFileAccessModeFlags> m_MapFileAccessModeTransitionList;
    QMap<QString, eFileOpenFlags>       m_MapFileOptionTransitionList;
};
}
}

//Q_DECLARE_METATYPE(tashtego::com::TashAbstractMemoryMap*)
Q_DECLARE_METATYPE(tashtego::com::TashAbstractMemoryMap::eFileOpenFlags)
Q_DECLARE_METATYPE(tashtego::com::TashAbstractMemoryMap::eFileAccessModeFlags)
Q_DECLARE_METATYPE(tashtego::com::TashAbstractMemoryMap::eMapProtection)
Q_DECLARE_METATYPE(tashtego::com::TashAbstractMemoryMap::eMapOptions)

#endif // TASHABSTRACTMEMORYMAP_H

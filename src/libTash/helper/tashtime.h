/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashtime.h
 * @author Michael Flau
 * @brief
 * @details
 * @date August 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Portugal Cove South, NF - Canada, Started August 2015
 */

#ifndef TASHTIME_H
#define TASHTIME_H

#include "../core/tashobject.h"

namespace tashtego{
namespace helper{

class TashTime: public core::TashObject
{

    Q_OBJECT

public:

    /**
     * @brief TashTime
     * @param parent
     */
    explicit TashTime(TashObject *parent = 0);

    /**
     * @brief TashTime
     * @param id
     * @param parent
     */
    TashTime(qint64 id, TashObject *parent = 0);

    /**
     * @brief ~TashTime
     */
    ~TashTime() = default;

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() override;

public slots:


    /**
     * @brief getDateTimeStringFromUnixTime
     * @param ts
     * @param format
     * @return
     */
    static QString getDateTimeStringFromUnixTime(quint64 ts, QString format = "");

    /**
     * @brief getUnixTime
     * @return
     */
    static qint64  getUnixTime();

    /**
     * @brief getNumericLocalTime
     * @return
     */
    static qint32  getNumericUTCDate();

    /**
     * @brief getNumericUTCTime
     * @return
     */
    static qint32  getNumericUTCTime();

    /**
     * @brief getNumericFullTimestamp
     * @return
     */
    static quint64 getNumericFullTimestamp();

    /**
     * @brief getNumericDate
     * @return
     */
    static qint32  getNumericDate();

    /**
     * @brief getNumericTime
     * @return
     */
    static qint32  getNumericTime();

    /**
     * @brief getUnixTimeString
     * @return
     */
    static QString getUnixTimeString();

    /**
     * @brief getNumericLocalTimeString
     * @return
     */
    static QString getNumericUTCDateString();

    /**
     * @brief getNumericUTCTimeString
     * @return
     */
    static QString getNumericUTCTimeString();

    /**
     * @brief getNumericUTCTimestamp
     * @return
     */
    static quint64 getNumericUTCTimestamp();

    /**
     * @brief getNumericUTCTimestampString
     * @return
     */
    static QString getNumericUTCTimestampString();

    /**
     * @brief getNumericFullTimestampString
     * @return
     */
    static QString getNumericFullTimestampString();

    /**
     * @brief getNumericDateString
     * @return
     */
    static QString getNumericDateString();

    /**
     * @brief getNumericTimeString
     * @return
     */
    static QString getNumericTimeString();

    /**
     * @brief getNumericSeconds
     * @return
     */
    static qint32 getNumericSeconds();

    /**
     * @brief getNumericMicroSeconds
     * @return
     */
    static qint32 getNumericMicroSeconds();

    /**
     * @brief getNumericMilliSeconds
     * @return
     */
    static qint32 getNumericMilliSeconds();

    /**
     * @brief getSecondsAsString
     * @return
     */
    static QString getSecondsAsString();

    /**
     * @brief getMillisecondsAsString
     * @return
     */
    static QString getMillisecondsAsString();

    /**
     * @brief getMicrosecondsAsString
     * @return
     */
    static QString getMicrosecondsAsString();

protected:

    /**
     * @brief getCurrentTimeInSeconds
     * @return
     */
    inline static time_t getCurrentTimeInSeconds();

    /**
     * @brief getCurrentTimeInMicroseconds
     * @return
     */
    inline static qint32 getCurrentTimeInMicroseconds();

    /**
     * @brief getCurrentTimeInMilliseconds
     * @return
     */
    inline static qint32 getCurrentTimeInMilliseconds();
};

}
}


Q_DECLARE_METATYPE(tashtego::helper::TashTime)

#endif //TASHTIME_H


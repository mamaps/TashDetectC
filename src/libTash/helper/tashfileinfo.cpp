/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "tashfileinfo.h"

#include <QFileInfo>
#include <QString>

namespace tashtego {
namespace helper {

QString TashFileInfo::getClassName(){
    return QString("TashFileInfo");
}

bool TashFileInfo::pathIsValid(const QString& path) {

    if ( path.isEmpty() ) {
        qDebug() << "ERROR: constructing an empty file object from empty input string";
        return false;
    }

    QFileInfo info(path);

    if ( !info.isAbsolute() ) {
        info.makeAbsolute();
    }

    return info.isDir();
}

bool TashFileInfo::fileIsValid(const QString& file) {

    if ( file.isEmpty() ) {
        qDebug() << "ERROR: constructing an empty file object from empty input string";
        return false;
    }

    QFileInfo info(file);

    if ( !info.isAbsolute() ) {
        info.makeAbsolute();
    }

    return info.isFile();
}

bool TashFileInfo::isHidden(const QString& file) {

    if ( file.isEmpty() ) {
        qDebug() << "ERROR: constructing an empty file object from empty input string";
        return false;
    }

    QFileInfo info(file);

    if ( !info.isAbsolute() ) {
        info.makeAbsolute();
    }

    return info.isHidden();
}

bool TashFileInfo::isLink(const QString& file) {

    if ( file.isEmpty() ) {
        qDebug() << "ERROR: constructing an empty file object from empty input string";
        return false;
    }

    QFileInfo info(file);

    if ( !info.isAbsolute() ) {
        info.makeAbsolute();
    }

    return info.isSymLink();
}

bool TashFileInfo::isExecutable(const QString& file) {

    if ( file.isEmpty() ) {
        qDebug() << "ERROR: constructing an empty file object from empty input string";
        return false;
    }

    QFileInfo info(file);

    if ( !info.isAbsolute() ) {
        info.makeAbsolute();
    }

    return info.isExecutable();
}

bool TashFileInfo::isReadable(const QString& file) {

    if ( file.isEmpty() ) {
        qDebug() << "ERROR: constructing an empty file object from empty input string";
        return false;
    }

    QFileInfo info(file);

    if ( !info.isAbsolute() ) {
        info.makeAbsolute();
    }

    if ( !info.exists() ) {
        qDebug() << "Invalid file object!";
        return false;
    }

    return info.isReadable();
}

bool TashFileInfo::isWriteable(const QString& file) {

    if ( file.isEmpty() ) {
        qDebug() << "ERROR: constructing an empty file object from empty input string";
        return false;
    }

    QFileInfo info(file);

    if ( !info.isAbsolute() ) {
        info.makeAbsolute();
    }

    if ( !info.exists() ) {
        qDebug() << "Invalid file object!";
        return false;
    }

    return info.isWritable();
}

REGISTER_METATYPE(TashFileInfo, "TashFileInfo")
}
}


/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashstringchecks.h
 * @author Michael Flau
 * @brief
 * @details
 * @date September 2017
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started September 2017
 */

#ifndef TASHSTRINGCHECKS_H
#define TASHSTRINGCHECKS_H

#include "../core/tashobject.h"

namespace tashtego {
namespace helper {

/**
 * @brief The TashStringChecks class
 */
class TashStringChecks : public core::TashObject
{
    Q_OBJECT
public:
    /**
     * @brief TashStringChecks
     * @param parent
     */
    TashStringChecks(TashObject *parent = 0);

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() override;

public slots:
    /**
     * @brief checkStringAgainstRegex
     * @param input
     * @param pattern
     * @return
     */
    static bool checkStringAgainstRegex(const QString &input, const QString &pattern);
};

}
}

#endif // TASHSTRINGCHECKS_H

/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef TASHIO_H
#define TASHIO_H

#include "../core/tashconst.h"
#include "../core/tashruntime.h"
#include <QPointer>
//class QPointer<class T>;

namespace tashtego {
namespace helper {

template <typename T> std::string type_name();

/**
 * @brief openMemoryMap function template
 */
template <typename T>
T* openMemoryMap( const tashtego::tashtegoRuntime runtime, const qint32& camidx, const QString& identifier ) {

    auto status = runtime->isInitialized();
    Q_ASSERT(status);
    if ( !status ) {
        qCritical() << "Passed runtime object is not initialized, abort mmap creation!";
        return nullptr;
    }

    tashtego::getMetaObjectIDs().append( qRegisterMetaType<T>() );

    auto *p = new T(tashtego::core::TashRuntime::genID(),
                    runtime,
                    camidx,
                    identifier);

    Q_CHECK_PTR( p );
    status = p ? true : false;
    if ( !status ) {
        qCritical() << "Invalid reference to memory map, abort instanciation!";
        return nullptr;
    }

    status = p->isInitialized();
    Q_ASSERT(status);

    if ( !status ) {
        qCritical() << "Failed to initialize " << identifier << " memory map!";
        if (p) delete p;
        return nullptr;
    }

    status = p->openMap();

    if ( !status ) {
        qCritical() << "Failed to open " << identifier << " memory map.";
        if (p) delete p;
        return nullptr;
    }

    return p;
}

/**
 * @brief genMapIdentifier
 * @param filename
 * @param maptype
 * @param timestamp
 * @return
 */
//QString genMapIdentifier(const QString& filename, const QString& maptype, const quint64& timestamp) {
//    return QString("%1-%2-%3").arg(timestamp).arg(filename).arg(maptype);
//}


}
}
#endif //TASHIO_H

/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashstringchecks.cpp
 * @author Michael Flau
 * @brief
 * @details
 * @date September 2017
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started September 2017
 */

#include "tashstringchecks.h"

#include <QRegularExpression>
#include <QRegularExpressionMatch>

namespace tashtego {
namespace helper {

TashStringChecks::TashStringChecks(TashObject *parent):
TashObject(parent){}

QString TashStringChecks::getClassName() {
    return QString("TashStringChecks");
}

bool TashStringChecks::checkStringAgainstRegex(const QString &input, const QString &pattern) {
    if ( input.isEmpty() ) {
        qCritical() << "Cannot check an empty string!";
        return false;
    }

    if ( pattern.isEmpty() ) {
        qCritical() << "Cannot check against an empty pattern!";
        return false;
    }

    QRegularExpression regex(pattern);

    if ( !regex.isValid() ) {
        qCritical() << "Invalid regular expression pattern given: " << pattern;
        return false;
    }

    QRegularExpressionMatch result = regex.match(input);

    if ( !result.hasMatch() || result.capturedStart() != 0 ) {
        qDebug() << "Regex test failed value:" << input << " pattern: " << pattern;
        return false;
    }

    return true;
}

}
}

/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashtime.cpp
 * @author Michael Flau
 * @brief
 * @details
 * @date August 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started August 2015
 */

#include <QDateTime>
#include <QString>
#include <QDebug>
#include <time.h>
#include <sys/time.h>

#include "tashtime.h"

namespace tashtego{
namespace helper{

TashTime::TashTime(TashObject *parent):
    TashObject(parent)
{

}

TashTime::TashTime(qint64 id, TashObject *parent):
    TashObject(id, parent)
{

}

QString TashTime::getClassName() {
    return QString("TashTime");
}

QString TashTime::getDateTimeStringFromUnixTime(quint64 ts, QString format) {

    time_t     t    = static_cast<qint64>(ts);
    struct tm *pTm  = localtime(&t);
    char       date[30];

    if ( format.isEmpty() )
        format = "%Y%m%d-%H%M%S";

    strftime(date, sizeof(date), "%Y%m%d-%H%M%S", pTm);
    return QString(date);
}

qint64 TashTime::getUnixTime() {
    return static_cast<qint64>( getCurrentTimeInSeconds() );
}

qint32  TashTime::getNumericUTCDate() {
    time_t raw = getCurrentTimeInSeconds();
    struct tm time;

    gmtime_r(&raw, &time);

    qint32 retDate  =  ((time.tm_year + 1900)  * 10000);
    retDate         += ((time.tm_mon + 1) * 100);
    retDate         += time.tm_mday;

    return retDate;
}

qint32  TashTime::getNumericUTCTime() {
    time_t raw = getCurrentTimeInSeconds();
    struct tm time;

    gmtime_r(&raw, &time);

    qint32 retTime  =  (time.tm_hour * 10000);
    retTime         += (time.tm_min * 100);
    retTime         += time.tm_sec;

    return retTime;
}

quint64 TashTime::getNumericUTCTimestamp() {
    quint64 date = static_cast<quint64>(getNumericUTCDate()) * 1000000;
    quint64 time = static_cast<quint64>(getNumericUTCTime());
    return (date + time);
}

quint64 TashTime::getNumericFullTimestamp() {
    quint64 date = static_cast<quint64>(getNumericDate()) * 1000000;
    quint64 time = static_cast<quint64>(getNumericTime());
    return (date + time);
}

qint32  TashTime::getNumericDate() {

    time_t raw = getCurrentTimeInSeconds();
    struct tm time;

    localtime_r(&raw, &time);

    qint32 retDate  =  ((time.tm_year + 1900)  * 10000);
    retDate         += ((time.tm_mon + 1) * 100);
    retDate         += time.tm_mday;

    return retDate;
}

qint32  TashTime::getNumericTime() {
    time_t raw = getCurrentTimeInSeconds();
    struct tm time;

    localtime_r(&raw, &time);

    qint32 retTime  =  (time.tm_hour * 10000);
    retTime         += (time.tm_min * 100);
    retTime         += time.tm_sec;

    return retTime;
}

QString TashTime::getUnixTimeString() {
    QString ret = QString("%1").arg( getUnixTime() );
    return ret;
}

QString TashTime::getNumericUTCDateString() {
    QString ret = QString("%1").arg( getNumericUTCDate() );
    return ret;
}

QString TashTime::getNumericUTCTimeString() {
    QString ret = QString("%1").arg( getNumericUTCTime() );
    return ret;
}

QString TashTime::getNumericUTCTimestampString() {
    QString ret = QString("%1-%2").arg(getNumericUTCDateString(), getNumericUTCTimeString());
    return ret;
}

QString TashTime::getNumericFullTimestampString() {
    QString ret = QString("%1-%2").arg(getNumericDateString(), getNumericTimeString());
    return ret;
}

QString TashTime::getNumericDateString() {
    QString ret = QString("%1").arg( getNumericDate() );
    return ret;
}

QString TashTime::getNumericTimeString() {
    QString ret = QString("%1").arg( getNumericTime() );
    return ret;
}

qint32 TashTime::getNumericSeconds() {
    return getCurrentTimeInSeconds();
}

qint32 TashTime::getNumericMicroSeconds() {
    return getCurrentTimeInMicroseconds();
}

qint32 TashTime::getNumericMilliSeconds() {
    return getCurrentTimeInMilliseconds();
}

QString TashTime::getSecondsAsString() {
    return QString("%1").arg( getCurrentTimeInSeconds() );
}

QString TashTime::getMillisecondsAsString() {
    return QString("%1").arg( getCurrentTimeInMilliseconds() );
}

QString TashTime::getMicrosecondsAsString() {
    return QString("%1").arg( getCurrentTimeInMicroseconds() );
}

time_t TashTime::getCurrentTimeInSeconds() {

    time_t rawTime = time(NULL);

    if ( rawTime == static_cast<time_t>(-1) ) {
        qCritical() << "TashTime, failed to get system time! Cancel!";
    }

    return rawTime;
}

qint32 TashTime::getCurrentTimeInMicroseconds() {
    timeval t;

    if ( gettimeofday(&t, 0) != 0 ) {
        qCritical() << "TashTime, failed to get system time in micor seconds! Cancel!";
        return -1;
    }

    return static_cast<qint32>(t.tv_usec);
}

qint32 TashTime::getCurrentTimeInMilliseconds() {

    qint32 t = getCurrentTimeInMicroseconds();
    return  t <= 0 ? t : (static_cast<float>(t) / 1000.f);
}

REGISTER_METATYPE(TashTime, "TashTime")
}
}


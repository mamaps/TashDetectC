/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashwaiter.h
 * @author Michael Flau
 * @brief
 * @details
 * @date August 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Cape Race, NF - Canada, Started August 2015
 */

#ifndef TASHWAITER_H
#define TASHWAITER_H

#include "../core/tashobject.h"

namespace tashtego{
namespace helper{

/**
 * @brief The TashWaiter class
 */
class TashWaiter: public core::TashObject
{
	Q_OBJECT
public:

	/**
	 * @brief TashWaiter
	 * @param parent
	 */
    explicit TashWaiter(tashtego::core::TashObject *parent = 0);

	/**
	 * @brief TashWaiter
	 * @param id
	 * @param parent
	 */
    TashWaiter(qint64 id, tashtego::core::TashObject *parent = 0);

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() override;

public slots:

    /**
     * @brief nsecWait
     */
    static void usecWait(tashtego::core::TashObject *i = 0);

    /**
     * @brief nsecWait
     * @param nsecs
     */
    static void usecWait(ulong usecs, tashtego::core::TashObject *i = 0);

    /**
     * @brief msecWait
     */
    static void msecWait(tashtego::core::TashObject *i = 0);

    /**
     * @brief msecWait
     * @param msecs
     */
    static void msecWait(ulong msecs, tashtego::core::TashObject *i = 0);

    /**
     * @brief secWait
     */
    static void secWait(tashtego::core::TashObject *i = 0);

    /**
     * @brief secWait
     * @param secs
     */
    static void secWait(ulong secs, tashtego::core::TashObject *i = 0);

};

}
}

Q_DECLARE_METATYPE(tashtego::helper::TashWaiter)

#endif //TASHWAITER_H


/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashwaiter.cpp
 * @author Michael Flau
 * @brief
 * @details
 * @date August 2015
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Cape Race, NF - Canada, Started August 2015
 */

#include <QThread>

#include "../core/tashconst.h"
#include "tashwaiter.h"

namespace tashtego{
namespace helper{

TashWaiter::TashWaiter(tashtego::core::TashObject *parent):
    tashtego::core::TashObject(parent)
{
    init();
}

TashWaiter::TashWaiter(qint64 id, tashtego::core::TashObject *parent):
    tashtego::core::TashObject(id, parent)
{
    init();
}

QString TashWaiter::getClassName() {
    return QString("TashWaiter");
}

void TashWaiter::usecWait(TashObject *i) {
    if( i != 0 )
        i->thread()->usleep(TASHDEFAULTUSECWAIT);
    else
        QThread::usleep(TASHDEFAULTUSECWAIT);
}

void TashWaiter::usecWait(ulong usecs, TashObject *i) {
    if( i != 0 )
        i->thread()->usleep(usecs);
    else
        QThread::usleep(usecs);
}

void TashWaiter::msecWait(TashObject *i) {
    if( i != 0 )
        i->thread()->msleep(TASHDEFAULTMSECWAIT);
    else
        QThread::msleep(TASHDEFAULTMSECWAIT);
}

void TashWaiter::msecWait(ulong msecs, TashObject *i) {
    if( i != 0)
        i->thread()->msleep(msecs);
    else
        QThread::msleep(msecs);
}

void TashWaiter::secWait(TashObject *i) {
    if( i != 0)
        i->thread()->sleep(TASHDEFAULTSECWAIT);
    else
        QThread::sleep(TASHDEFAULTSECWAIT);
}

void TashWaiter::secWait(ulong secs, TashObject *i) {
    if( i != 0)
        i->thread()->sleep(secs);
    else
        QThread::sleep(secs);
}

REGISTER_METATYPE(TashWaiter, "TashWaiter")
}
}

/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef TASHFILEINFO_H
#define TASHFILEINFO_H


#include "../core/tashobject.h"

class QFileObject;

namespace tashtego {
namespace helper {

class TashFileInfo final: public core::TashObject
{
    Q_OBJECT
//    using namespace tashtego::core;

public:
    TashFileInfo()   = default;
    ~TashFileInfo()  = default;
    TashFileInfo(const TashFileInfo& info)  = default;
    TashFileInfo(TashFileInfo&& info) = default;

    virtual QString getClassName() override;

    static bool pathIsValid(const QString& path);
    static bool fileIsValid(const QString& file);
    static bool isHidden(const QString& file);
    static bool isLink(const QString& file);
    static bool isExecutable(const QString& file);
    static bool isReadable(const QString& file);
    static bool isWriteable(const QString& file);
};
}
}

Q_DECLARE_METATYPE(tashtego::helper::TashFileInfo)

#endif // TASHFILEINFO_H

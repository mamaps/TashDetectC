/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashcliparser.cpp
 * @author Michael Flau
 * @brief
 * @details
 * @date November 2016
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started September 2016
 */

#include "tashcliparser.h"

#include "../core/tashruntime.h"

#include <QList>
#include <QString>
#include <QVariant>

namespace tashtego {
namespace helper {


TashCLIParser::TashCLIParser(TashObject *parent):
tashtego::core::TashObject(parent),
m_bHasParsedFieldList(false),
m_argc(0),
m_ppArgv(nullptr) {

}

TashCLIParser::TashCLIParser(qint64 id, int& argc, char** argv, const QList<TashCLIOption> refList, TashObject *parent):
tashtego::core::TashObject(id, parent),
m_bHasParsedFieldList(false),
m_argc(argc),
m_ppArgv(argv),
m_RefList(refList){

    if ( refList.isEmpty() ) {
        qWarning() << "Passed arugment reference list is empty, will not find any command line arguments!";
    }

    if ( !checkArgv((const qint32)argc, (const char**)argv) ) {
        qCritical() << "Input argument list did not pass sanity check, abort!";
        m_bHasParsedFieldList = m_bIsInitialized = false;
        m_argc      = 0;
        m_ppArgv    = nullptr;
        return;
    }

    m_bHasParsedFieldList = parseArgv((const qint32)m_argc, (const char**)m_ppArgv);

    if ( !init() )  {
#ifdef TASHVERBOSE
        qWarning() << "CLI parser not initialized!";
#endif
    }
}

bool TashCLIParser::init() {
    return (m_bIsInitialized = m_bHasParsedFieldList);
}

QString TashCLIParser::getClassName() {
    return QString("TashCLIParser");
}

bool TashCLIParser::hasField(const QString& field){

    auto status( hasParsedFieldList() && isInitialized() );

    if ( !status ) {
        qCritical() << "Parser instance is not initialized, abort current operation!";
        return status;
    }

    //Set status deliberately to false to
    //prevent a true ret value for empty lists
    status = false;

    //Traverse value list, and search for matching field descriptor
    for ( auto it = m_ValList.begin(); it != m_ValList.end(); ++it ) {
        if ( ( (*it).multiFlag == field ) || ( (*it).singleFlag == QString(field) ) ) {
            status = true;
            break;
        }
    }

    return status;
}

bool TashCLIParser::fieldHasValue(const QString& field){

    auto status( hasParsedFieldList() && isInitialized() );

    if ( !status ) {
        qCritical() << "Parser instance is not initialized, abort current operation!";
        return status;
    }

    status = hasField(field);

    if ( !status ) {
        qCritical() << "There is no such field called " <<  field << ", abort operation";
        return status;
    }

    for ( auto it = m_ValList.begin(); it != m_ValList.end(); ++it ) {
        if ( ( (*it).multiFlag == field ) || ( (*it).singleFlag == QString(field) ) ) {
            status = (*it).hasValue;
            break;
        }
    }

    return status;
}

QVariant TashCLIParser::getFieldValue(const QString& field){

    if ( !( hasParsedFieldList() && isInitialized() ) ) {
        qCritical() << "Parser instance is not initialized, abort current operation!";
        return QVariant();
    }

    if ( !hasField(field) ) {
        qCritical() << "There is no such field called " <<  field << ", abort operation";
        return QVariant();
    }

    for ( auto it = m_ValList.begin(); it != m_ValList.end(); ++it ) {
        if ( ( (*it).multiFlag == field ) || ( (*it).singleFlag == QString(field) ) ) {
            return (*it).value;
        }
    }
#ifdef TASHVERBOSE
    qDebug() << "Could not find a value for given field name " << field;
#endif
    return QVariant();
}

core::TashRuntime::ConfType TashCLIParser::getFieldType(const QString& field) {

    if ( !( hasParsedFieldList() && isInitialized() ) ) {
        qCritical() << "Parser instance is not initialized, abort current operation!";
        return core::TashRuntime::Unknown;
    }

    if ( !hasField(field) ) {
        qCritical() << "There is no such field called " <<  field << ", abort operation";
        return core::TashRuntime::Unknown;
    }

    for ( auto it = m_ValList.begin(); it != m_ValList.end(); ++it ) {
        if ( ( (*it).multiFlag == field ) || ( (*it).singleFlag == QString(field) ) ) {
            return (*it).type;
        }
    }
#ifdef TASHVERBOSE
    qDebug() << "Could not find a type for given field name " << field << ", defaulting to type 'unknown'";
#endif
    return core::TashRuntime::Unknown;
}

bool TashCLIParser::hasParsedFieldList(){
    return m_bHasParsedFieldList;
}

bool TashCLIParser::setReferenceList(const QList<TashCLIOption> refList) {
    if ( !( hasParsedFieldList() && isInitialized() ) ) {
        qCritical() << "Parser instance is not initialized, abort current operation!";
        return false;
    }

    if ( refList.isEmpty() ) {
        qWarning() << "Passed reference List is empty, this will have no effect!";
    }

    //replace old ref list
    m_RefList = refList;

    //recheck list
    return parseArgv((const qint32)m_argc, (const char**)m_ppArgv);
}

bool TashCLIParser::appendReferenceList(const QList<TashCLIOption> refList) {
    if ( !( hasParsedFieldList() && isInitialized() ) ) {
        qCritical() << "Parser instance is not initialized, abort current operation!";
        return false;
    }

    if ( refList.isEmpty() ) {
        qWarning() << "Passed reference List is empty, this will have no effect!";
    }

    //append to existing ref list
    m_RefList.append(refList);

    //recheck list
    return parseArgv((const qint32)m_argc, (const char**)m_ppArgv);
}

bool TashCLIParser::setArgumentList(int argc, char** argv ) {

    if ( !checkArgv((const qint32)argc, (const char**)argv) ) {
        qCritical() << "Input arguments list did not pass sanity check!";
        return false;
    }

    m_argc   = argc;
    m_ppArgv = argv;

    if ( !parseArgv((const qint32)m_argc, (const char**)m_ppArgv) ) {
        qCritical() << "Parsing of cli argument list failed!";
        return m_bHasParsedFieldList = false;
    }

    return true;
}

void TashCLIParser::dumpCommandLineOptions() {
#ifdef TASHVERBOSE
    qDebug() << "\n\nDump of parsed command line arguments:";
    for ( auto it = m_ValList.begin(); it != m_ValList.end(); ++it ) {
        qDebug() << "\nSingle flag: " << (*it).singleFlag;
        qDebug() << "Multi flag: " << (*it).multiFlag;
        qDebug() << "Has value: " << (*it).hasValue;
        qDebug() << "Value: " << (*it).value;
    }
    qDebug() << "\n";
#endif
}

QStringList TashCLIParser::getformattedHelpDialog() {

    QStringList output;
    QString text("The following cli flags are available for this application:");

    output.append(text);

    for ( auto it = std::begin(m_RefList); it < std::end(m_RefList); ++it ) {
        auto tmp = QString("-%1, --%2       %3").arg(it->singleFlag).arg(it->multiFlag).arg(it->description);
        output.append(tmp);
    }

    return output;
}

bool TashCLIParser::parseArgv(const qint32 &argc, const char **argv) {

    auto    status(true);
    auto    nextIsValue(false);
    auto**  argvTmp = argv;

    //Reset value List, if parsing wasn't called at program upstart
    if ( m_bIsInitialized ) {
        m_ValList.clear();
    }

    for ( auto idx = 0; idx < argc; ++idx, ++argvTmp ) {
        auto* rawFlag = *argvTmp;
        auto flag     = QString(rawFlag);

        if ( nextIsValue ) {

            //set is next field is value flag back to false
            nextIsValue = false;

            //Handle what happens if next string is a flag and not a value
            if ( flag.startsWith("-") || flag.startsWith("--") ) {
                qWarning() << "Current flag is missing value, will use default value from reflist!";

                for ( auto it = m_RefList.begin(); it != m_RefList.end(); ++it ) {

                    if ( (*it).singleFlag == m_ValList.last().singleFlag ) {
                        m_ValList.last().value      = (*it).value;
                        m_ValList.last().type       = (*it).type;
                        m_ValList.last().hasValue   = true;
                        break;
                    }
                }

                //If there is no default value in ref list, abort and terminate
                if ( !m_ValList.last().hasValue ) {
                    qWarning() << "Could not find default value for option " << m_ValList.last().singleFlag << "|" << m_ValList.last().multiFlag << " abort parsing!";
                    status = false;
                    break;
                }

                //Set argv on slot back to not miss flag option
                --idx;
                --argvTmp;
                continue;
            }

            //Extract next field as value from prior flag according to set type in reference list
            switch( m_ValList.last().type ) {
                case core::TashRuntime::Boolean:
                m_ValList.last().value = flag == "true" ? QVariant(true) : QVariant(false);
                break;
                case core::TashRuntime::String:
                m_ValList.last().value = QVariant(flag);
                break;
                case core::TashRuntime::Character:
                m_ValList.last().value = QVariant(flag.at(0));
                break;
                case core::TashRuntime::Integer:
                m_ValList.last().value = QVariant(flag.toInt());
                break;
                case core::TashRuntime::Double:
                m_ValList.last().value = QVariant(flag.toDouble());
                break;
                case core::TashRuntime::Float:
                m_ValList.last().value = QVariant(flag.toFloat());
                break;
                case core::TashRuntime::Unsigned:
                m_ValList.last().value = QVariant(flag.toUInt());
                break;
                case core::TashRuntime::Long:
                case core::TashRuntime::LongLong:
                m_ValList.last().value = QVariant(flag.toLongLong());
                break;
                case core::TashRuntime::UnsignedLongLong:
                m_ValList.last().value = QVariant(flag.toULongLong());
                break;
                case core::TashRuntime::Short:
                m_ValList.last().value = QVariant(flag.toShort());
                break;
                case core::TashRuntime::UnsignedShort:
                m_ValList.last().value = QVariant(flag.toUShort());
                break;
                case core::TashRuntime::Unknown:
                qWarning() << "Reflist says value is of unknown type, storing as string " << flag;
                m_ValList.last().value = QVariant(flag);
                break;
            }

            m_ValList.last().hasValue = true;

        } else {

            if ( flag.startsWith("--")) {
#ifdef TASHVERBOSE
                qDebug() << "Found new multi flag " << flag;
#endif
                auto trimmedFlag = flag.right(flag.length()-2);

                if ( trimmedFlag.length() <= 1 ) {
                    qWarning() << "Detected multi character flag with no value or single char instead of string, abort parsing!";
                    status = false;
                    break;
                }

                for ( auto it = m_RefList.begin(); it != m_RefList.end(); ++it ) {

                    if ( (*it).multiFlag == trimmedFlag ) {
                        m_ValList.append({(*it).singleFlag, trimmedFlag, false, QVariant(), (*it).type, (*it).description});
                        nextIsValue = (*it).hasValue;
                        break;
                    }
                }

            } else if ( flag.startsWith("-") ) {
#ifdef TASHVERBOSE
                qDebug() << "Found new single flag " << flag;
#endif
                auto trimmedFlag = flag.right(flag.length()-1);
#ifdef TASHVERBOSE
                qDebug() << "Trimmed single flag is " << trimmedFlag;
#endif
                if ( trimmedFlag.length() > 1 || trimmedFlag.length() == 0 ) {
                    qWarning() << "Detected single character flag which is empty or too long, abort parsing!";
                    status = false;
                    break;
                }

                for ( auto it = m_RefList.begin(); it != m_RefList.end(); ++it ) {

                    if ( (*it).singleFlag == trimmedFlag ) {
                        m_ValList.append({trimmedFlag.at(0), (*it).multiFlag, false, QVariant(), (*it).type, (*it).description});
                        nextIsValue = (*it).hasValue;
                        break;
                    }
                }
            }
        }
    }

    return status;
}

bool TashCLIParser::checkArgv(const qint32 &argc, const char **argv) {
    Q_CHECK_PTR(argv);
    auto status(true);

    if ( !argv ) {
        qCritical() << "Invalid pointer passed to method, abort!";
        status = false;
    }

    if ( !status ) {
        return status;
    }

    for ( auto idx = 0; idx < argc; ++idx) {
        Q_CHECK_PTR(argv[idx]);

        if ( !argv[idx] ) {
            qCritical() << "Invalid pointer passed to method at index " << idx;
            status = false;
        }
    }

    return status;
}

REGISTER_METATYPE(TashCLIParser, "TashCLIParser")
}
}



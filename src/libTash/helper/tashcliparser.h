/*
 * libTash, an image processing library to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


/**
 * @file tashcliparser.h
 * @author Michael Flau
 * @brief
 * @details
 * @date November 2016
 * @copyright Alfred Wegener Institute for Polar and Marine Research
 * Ocean Acoustics Group (OZA)
 * Bremerhaven - Germany, Started September 2016
 */

#ifndef TASHCLIPARSER_H
#define TASHCLIPARSER_H

#include "../core/tashobject.h"

namespace tashtego {

//Forward declare TashRuntime type enum
namespace core {
    enum TashRuntime::ConfType unsigned short;
}

namespace helper {


/**
 * @brief The TashCLIParser class
 *
 * Manages arguments which have been passed to application
 * at it's upstart. This class is meant to work as a submodule of
 * the tashtego runtime and as that will depend on the lifetime of the
 * runtime object.
 */
class TashCLIParser:public core::TashObject
{
    Q_OBJECT

    /**
     * @brief The cliOption struct
     * Helper object which congregates information about a single
     * command line flag and it's associated value.
     */
    struct cliOption {
        QChar singleFlag;
        QString multiFlag;
        bool hasValue;
        QVariant value;
        core::TashRuntime::ConfType type;
        QString description;
    };

signals:
    /**
     * @brief shutdown
     * This module may cause a shutdown
     * of the application if certain flags are set.
     * For instance the -h ? or -v flag will cause
     * the application to exit gracefully.
     */
    void shutdown();

public:
    /**
     * public interface type for the cliOption object
     * for convenient access.
     */
    using TashCLIOption = struct cliOption;
    /**
     * @brief TashCLIParser
     * Dummy ctor which is mandatory for automatic runtime introspection
     * @warning DO NOT CALL THIS CONSTRUCTOR MANUALLY, the created object will not be usable.
     *
     * @param parent
     * Parent instance by which the module instance of TashCLIParser will be created
     */
    explicit TashCLIParser(TashObject *parent = 0);
    /**
     * @brief TashCLIParser
     * Preferred ctor to construct a valid TashCLIParser instance.
     * Default behaviour is to parse the passed argument list right away and
     * check it against the passed reference list.
     * Appending the reference list or passing an alternative argument list is possible
     * at later moments.
     *
     * Usage example:
     * @code
     *  QList<helper::TashCLIParser::TashCLIOption> refList;
     *
     *  //OS signal handling by application
     *  refList.append({'o', "ossh", false, QVariant(false), Boolean});
     *  //Use dbus for message handling
     *  refList.append({'d', "dbus", false, QVariant(false), Boolean});
     *  //Define alternative config file
     *  refList.append({'i', "ini", true, QVariant(m_IniFilePath), String});
     *
     *  //Create a CLI parser instance
     *  m_pCLIParser = new helper::TashCLIParser(genIDPara(), argc, argv, refList);
     *
     *  Q_CHECK_PTR(m_pCLIParser);
     *
     *  if ( !m_pCLIParser->isInitialized() || !m_pCLIParser->hasParsedFieldList() ) {
     *      qCritical() << "Initializing command line parser failed, abort!";
     *      emit shutdown();
     *      return m_bIsInitialized = false;
     *  }
     *
     *  connect(m_pCLIParser, SIGNAL(shutdown()), this, SIGNAL(shutdown()));
     *  //Print all parsed cli args to std out
     *  m_pCLIParser->dumpCommandLineOptions();
     *
     * //Check for alternative config file set via CLI
     * if ( m_pCLIParser && m_pCLIParser->isInitialized() ) {
     *  if ( m_pCLIParser->hasField("ini") ) {
     *      qDebug() << "Flag for alternative ini file was specified, taking precedence over hard coded!";
     *      m_IniFilePath = m_pCLIParser->getFieldValue("ini").toString();
     *  }
     * }
     * @endcode
     *
     * @param argc
     * Number of arguments being passed to module instance
     * @param argv
     * List of arguments as raw char pointer list
     * @param refList
     * QList of type helper::TashCLIParser::TashCLIOption which holds all reference flags which
     * will be regarded as valid flags to the respective application
     */
    TashCLIParser(qint64 id,
                  int& argc,
                  char** argv,
                  const QList<TashCLIOption> refList,
                  TashObject *parent = 0);
    /**
     * default dtor
     */
    virtual ~TashCLIParser() = default;
    /**
     * default cctor
     */
    TashCLIParser(const TashCLIParser& rvalue) = default;
    /**
     * default mctor
     */
    TashCLIParser(TashCLIParser&& rvalue) = default;
    /**
     * @brief init
     * Overrides init method of TashObject, will set repective
     * instance flags to true if initialization was succesful
     */
    virtual bool init() override;

    /**
     * @brief getClassName
     * Returns the name of the class. The class name is stored in a member variable,
     * which is initialized at construction time.
     * @return
     * Class name as a QString object
     */
    virtual QString getClassName() override;

    /**
     * @brief hasField
     * Boolean getter which will search the parsed argument list for
     * a flag which matches the input string.
     * @param field
     * String which should be either the single or multi flag
     * descriptor of the argument being searched for.
     * @return
     * Returns true or false based on search result.
     */
    virtual bool hasField(const QString& field);
    /**
     * @brief fieldHasValue
     * Boolean getter which can be used to find out whether a cli argument has
     * an associated value or not.
     * @param field
     * String descriptor of the argument which should resemble the single- or
     * multi flag.
     * @return
     * Returns true or false based on search result.
     */
    virtual bool fieldHasValue(const QString& field);
    /**
     * @brief getFieldValue
     * Getter which will return the associated value of the searched argument.
     * If searched flag has no value an invalid QVariant object will be returned.
     * If there is a value it will be returned as QVariant object. If the user is not aware
     * of the actual type of returned value, TashCLIParser::getFieldType() can be used
     * to find out about the actual datatype.
     * @param field
     * String descriptor of the argument which should resemble the single- or
     * multi flag.
     * @return
     * QVariant which holds value of flag searched for
     */
    virtual QVariant getFieldValue(const QString& field);
    /**
     * @brief getFieldType
     * Returns the datatype of the value of the searched flag as a
     * tashtego::core::TashRuntime::ConfType enum value.
     * @param field
     * String descriptor of the argument which should resemble the single- or
     * multi flag.
     * @return
     * Enum value of type tashtego::core::TashRuntime::ConfType.
     */
    virtual core::TashRuntime::ConfType getFieldType(const QString& field);
    /**
     * @brief hasParsedFieldList
     * Simple boolean getter to check whether the module instance has a parsed
     * argument value list of all command line arguments.
     * @return
     * Returns true/false based on internal state of argument value list.
     */
    virtual bool hasParsedFieldList();
    /**
     * @brief setReferenceList
     * This method can be used to set an alternative argument reference list,
     * against which the actual argument list will be rechecked. The old reference
     * list will be replaced by the new one.
     * @param refList
     * New argument reference list as QList object of TashCLIOption objects.
     * @return
     * true or false based on whether the rechecking of the argument list against
     * the new reference list was successful.
     */
    virtual bool setReferenceList(const QList<TashCLIOption> refList);
    /**
     * @brief appendReferenceList
     * This method can be used to add additional arguments to the reference list.
     * The argument list will be rechecked against the new reference list.
     * @param refList
     * Argument reference list as QList object of TashCLIOption objects which shall
     * be appended to existing one.
     * @return
     * true or false based on whether the rechecking of the argument list against
     * the new reference list was successful.
     */
    virtual bool appendReferenceList(const QList<TashCLIOption> refList);
    /**
     * @brief setArgumentList
     * Set another raw argument list. This method will replace the old argument count
     * and argument list with the new one. This method triggers also a reprocessing, naturally
     * of the new list.
     * @param argc
     * Number of arguments being passed to module instance
     * @param argv
     * List of arguments as raw char pointer list
     * @return
     * true or false based on whether the checking of the new argument list against
     * the existing reference list was successful.
     */
    virtual bool setArgumentList(int argc, char **argv);
    /**
     * @brief dumpCommandLineOptions
     * Simple stdout dump of found and valid command line arguments which
     * have been passed to current lifecycle of the application.
     */
    virtual void dumpCommandLineOptions();

    /**
     * @brief getformattedHelpDialog
     * @return
     */
    virtual QStringList getformattedHelpDialog();

protected:
    /**
     * @brief parseArgv
     * Parsed the raw argument list and stores it's found
     * content in m_ValList. Only values which are also found in the
     * passed reference list are regarded as valid and will be stored.
     * Further more it will parse the corresponding value and store it
     * as QVariant object of datatype being specified in the reference list.
     * @todo
     * The parser module should be able to parse multiple single flags which start after
     * the same dash e.g. '-doi' as opposed to '-d -o -i'
     * @param argc
     * Number of arguments being passed to module instance
     * @param argv
     * List of arguments as raw char pointer list
     * @return
     * true or false based on whether parsing of argument list was successful.
     */
    virtual bool parseArgv(const qint32 &argc, const char **argv);
    /**
     * @brief checkArgv
     * Simple sanity check of raw input pointers. Makes it easy to
     * deal with raw pointers within instance. If check is not passed
     * whole instance will be rendered uninitialized and therefore not usable.
     * @param argc
     * Number of arguments being passed to module instance
     * @param argv
     * List of arguments as raw char pointer list
     * @return
     * true or false based on whether raw input arguments pass sanity check.
     */
    virtual bool checkArgv(const qint32 & argc, const char **argv);

    /*
     * Members
     */
    bool                 m_bHasParsedFieldList;///> flag which denotes whether cli arguments have been parsed
    qint32               m_argc;               ///> number of cli arguments
    char**               m_ppArgv;             ///> raw argument list
    QList<TashCLIOption> m_RefList;            ///> reference list of arguments which can be expected by application
    QList<TashCLIOption> m_ValList;            ///> actual value list of arguments which were passed to application
};

}
}

Q_DECLARE_METATYPE(tashtego::helper::TashCLIParser)

#endif // TASHCLIPARSER_H

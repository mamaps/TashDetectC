#-------------------------------------------------
#
# LibTash Project created by QtCreator 2015-06-02T19:39:47
#
#-------------------------------------------------

#Major version
VERSION_MAJOR = 0
#Minor version
VERSION_MINOR = 2

#Custom flags
QMAKE_CXXFLAGS_DEBUG *= -Werror -Wextra
QMAKE_CXXFLAGS_RELEASE *= -Werror -Wextra -O3

#macros
DEFINES += "VERSION_MAJOR=$$VERSION_MAJOR"\
           "VERSION_MINOR=$$VERSION_MINOR"\
#           "TASHVERBOSE"

VERSION += $${VERSION_MAJOR}.$${VERSION_MINOR}.$${VERSION_BUILD}

#Qt components
QT       += core xml dbus
QT       -= gui

#target name
TARGET = tash

#target directory
BINARY_DESTINATION = ../../bin/libTash
DESTDIR = $$BINARY_DESTINATION

#project template
TEMPLATE = lib

#compiler settings
CONFIG += staticlib \
          c++14 \
          release
#          debug \

#Source and header files
SOURCES += core/tashconst.cpp \
    core/abstracttashobject.cpp \
    core/tashfactory.cpp \
    core/tashobject.cpp \
    core/tashruntime.cpp \
    core/tashossignalhandler.cpp \
    com/tashabstractmemorymap.cpp \
    cmd/tashdbusconnector.cpp \
    helper/tashtime.cpp \
    core/tashabstractframesorter.cpp \
    helper/tashwaiter.cpp\
    ip/tashimagesegmentinfo.cpp\
    ip/tashabstractprocessor.cpp \
    ip/tashmsswprocessor.cpp \
    core/tashtypes.cpp \
    ip/tashabstractimageprocessingkernel.cpp \
    ip/tashmediansortprocessormodule.cpp \
    ip/tashmediansortworker.cpp \
    helper/tashmapio.cpp \
    helper/tashfileinfo.cpp \
    helper/tashcliparser.cpp \
    helper/tashstringchecks.cpp

HEADERS += core/tashconst.h \
    build.h \
    libtash.h \
    tashhelper.h \
    tashcore.h \
    tashcom.h \
    taship.h \
    tashcmd.h \
    core/abstracttashobject.h \
    core/tashfactory.h \
    core/tashobject.h \
    core/tashruntime.h \
    core/tashtypes.h \
    core/tashossignalhandler.h \
    com/tashabstractmemorymap.h \
    com/tashtypedmemorymap.h \
    cmd/tashdbusconnector.h \
    helper/tashtime.h \
    core/tashabstractframesorter.h \
    helper/tashwaiter.h\
    ip/tashimagesegmentinfo.h\
    ip/tashabstractsegmentaccessor.h\
    ip/tashtile.h \
    ip/tashimagesegmentaccessor.h \
    ip/tashmsswaccessor.h \
    ip/tashabstractprocessor.h \
    ip/tashmsswprocessor.h \
    ip/tashabstractimageprocessingkernel.h \
    ip/tashmediansortcommon.h \
    ip/tashmediansortprocessormodule.h \
    ip/tashmediansortworker.h \
    core/tashframesorter.h \
    ip/tashimage.h \
    helper/tashmapio.h \
    helper/tashfileinfo.h \
    helper/tashcliparser.h \
    helper/tashstringchecks.h

Release:DESTDIR     = ../../build/libTash/release
Release:OBJECTS_DIR = ../../build/libTash/release/.obj
Release:MOC_DIR     = ../../build/libTash/release/.moc
Release:RCC_DIR     = ../../build/libTash/release/.rcc
Release:UI_DIR      = ../../build/libTash/release/.ui

Debug:DESTDIR     = ../../build/libTash/debug
Debug:OBJECTS_DIR = ../../build/libTash/debug/.obj
Debug:MOC_DIR     = ../../build/libTash/debug/.moc
Debug:RCC_DIR     = ../../build/libTash/debug/.rcc
Debug:UI_DIR      = ../../build/libTash/debug/.ui

#Installation routine
unix {
#Install header
INSTALL_PREFIX = ../../include/libTash
INSTALL_HEADERS = $$HEADERS
include(../../scripts/headerinstall.pri)

INSTALL_COMMAND = echo "installing headers..." && find . -name \"*.h\" | cpio -updm --verbose

#Install binaries
target.files = libtash.a
target.path  = ../../bin/libTash/
INSTALLS += target

build_nr.commands = "python2.7 ../../scripts/genBuildNumber.py build.h"
build_nr.depends  = FORCE
QMAKE_EXTRA_TARGETS += build_nr
PRE_TARGETDEPS += build_nr

#Install headers
do_header_install.commands = $$INSTALL_COMMAND $$INSTALL_PREFIX
do_header_install.depends = FORCE
QMAKE_EXTRA_TARGETS += do_header_install
POST_TARGETDEPS += do_header_install

}

/*
 * TashDetectC, an image processing application to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "backendworker.h"

#include <QObject>
#include <QCoreApplication>
#include <QThread>
#include <QTimer>
#include <QFile>

/**
 * @brief main
 * Application entry point. Starts a worker thrad and allocates backgroundworker
 * object to run in this thread.
 * @param argc
 * Argument count
 * @param argv
 * Comand line arguments
 * @return
 * Applications exit code, defined by Qt Event loop
 */
int main( int argc, char *argv[] )
{
    QCoreApplication a(argc, argv);

    auto *pWorkerThread = new QThread();

    Q_CHECK_PTR(pWorkerThread);

    if ( !pWorkerThread ) {
        qCritical() << "Error while allocating new QThread object on heap, shutdown gracefully...";
        return EXIT_FAILURE;
    }

    auto *pWorker       = new BackendWorker(argc, argv);

    Q_CHECK_PTR(pWorker);

    if ( !pWorker ) {
        qDebug() << "Error while allocating tashtego Background worker object on heap, shutdown gracefully...";
        return EXIT_FAILURE;
    }

    //associate background worker with thread
    pWorker->moveToThread(pWorkerThread);

    // Termination chain
    // This is the succession in which the objects should be deinitialized
    // 1. worker emits BackendWorker::finished() --> QThread::quit()
    // 2. thread emits QThread::finished() --> BackendWorker::deleteLater()
    // 3. thread emits QThread::finished() --> QThread::deleteLater()
    // 4. thread emits QThread::finished() --> QCoreApplication::quit()

    //Worker invoked connections
    QObject::connect(pWorker, SIGNAL(finished()), pWorkerThread, SLOT(quit()));


    //Thread invoked connections
    QObject::connect(pWorkerThread, SIGNAL(finished()), pWorkerThread, SLOT(deleteLater()));
    QObject::connect(pWorkerThread, SIGNAL(finished()), pWorker, SLOT(deleteLater()));
    QObject::connect(pWorkerThread, SIGNAL(finished()), &a, SLOT(quit()));
    QObject::connect(pWorkerThread, SIGNAL(started()), pWorker, SLOT(process()));

    //Core application invoked connections
    QObject::connect(&a, SIGNAL(aboutToQuit()), pWorker, SLOT(stop()));

    //start thread
    pWorkerThread->start();

    return a.exec();
}

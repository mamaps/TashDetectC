/*
 * TashDetectC, an image processing application to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */


#include "backendworker.h"

#include <QDir>
#include <QThread>
#include <QCoreApplication>
#include <QTimer>
#include <QElapsedTimer>
#include <QSharedPointer>
#include <QMutex>
#include <QVector>
#include <QPointer>

#include <thread>
#include <vector>
#include <algorithm>
#include <utility>

Q_DECLARE_METATYPE (tashtego::TashMapAcc_ImageStack)
Q_DECLARE_METATYPE (tashtego::TashMapAcc_ScoreMap)
Q_DECLARE_METATYPE (tashtego::TashMapAcc_DetectDims)
Q_DECLARE_METATYPE (tashtego::TashMapAcc_IMEM)
Q_DECLARE_METATYPE (TashMSSWAccessor_int16)

BackendWorker::BackendWorker(int argc, char **argv, QObject *parent) :
    QObject(parent),
    m_bIsInitialized(false),
    m_bStop(false),
    m_pInputMap(nullptr),
    m_pOutputMap(nullptr),
    m_pIMEM(nullptr),
    m_pSorter(nullptr),
    m_pProcessor(nullptr),
    m_argc(argc),
    m_argv(argv)
{
#ifdef TASHVERBOSE
    qDebug() << "Starting creation of backendworker instance...";
#endif
}

BackendWorker::~BackendWorker() {
#ifdef TASHVERBOSE
    qDebug() << "Starting destruction of backendworker...";
#endif
    cleanup();
#ifdef TASHVERBOSE
    qDebug() << "done!\n";
#endif
}

void BackendWorker::process() {

    using namespace tashtego::core;
    using namespace tashtego::com;
    using namespace tashtego::ip;

#ifdef TASHVERBOSE
    qDebug() << "Entering worker thread init phase...";
#endif

    m_bIsInitialized = this->initThread();

    if ( !m_bIsInitialized ) {
        qWarning() << "Premature app termination requested...";
        QCoreApplication::processEvents();
        emit finished();
        return;
    }

#ifdef TASHVERBOSE
    qDebug() << "Entering worker thread processing phase...";
#endif

    forever {

        m_pSorter->sort();

        if ( m_bStop ) {
            break;
        }

        emit running();
        tashtego::helper::TashWaiter::msecWait(5);
        QCoreApplication::processEvents();
    }

    emit finished();
#ifdef TASHVERBOSE
    qDebug() << "Reached end of BackendWorker::process()";
#endif
}

void BackendWorker::stop() {
    m_bStop = true;
}

void BackendWorker::handleNewFrame(quint32 idx, quint32 frameidx) {

    auto* pOutMap = m_pOutputMap->getUnsafeMapAccess();

    Q_CHECK_PTR(pOutMap);

    if ( !pOutMap ) {
        qCritical() << "Got invalid reference to output memory map, can't proceed!";
        return;
    }

    auto* pInMap  = m_pInputMap->getUnsafeMapAccess();

    Q_CHECK_PTR(pInMap);

    if ( !pInMap ) {
        qCritical() << "Got invalid reference to output memory map, can't proceed!";
        return;
    }

    const qint32 time   = pInMap->stack[idx].time;
    const qint32 timems = pInMap->stack[idx].timems;

    for( auto lidx = 0; lidx < tashtego::SCORE_MAP_DEFAULT_NUM_CORES; ++lidx) {
        pOutMap->threadList[lidx].time        = time;
        pOutMap->threadList[lidx].timems      = timems;
        pOutMap->threadList[lidx].slotindex   = lidx;
        pOutMap->threadList[lidx].framenumber = frameidx;
    }

    qint32        myidx = idx;
    QElapsedTimer myTimer;
    myTimer.start();

    m_framenumberbuffer.pop_back();
    m_framenumberbuffer.push_front(frameidx);

    //process frame
    m_pProcessor->process(myidx);

    //Sync written data to map
    m_pOutputMap->syncMap();

    auto max = std::max(m_framenumberbuffer.begin(), m_framenumberbuffer.end());
    auto min = std::min(m_framenumberbuffer.begin(), m_framenumberbuffer.end());

    qDebug() << tashtego::helper::TashTime::getNumericFullTimestamp() << "-" << frameidx << " - Max " << *min << " - Min " << *max << " - Diff " << *min - *max << " - " << myTimer.elapsed() << " ms";
}

void BackendWorker::handleIdle() {
#ifdef TASHVERBOSE
    qDebug() << "Catched idle cycle!";
#endif
}

bool BackendWorker::initThread() {

    using namespace tashtego;
    using namespace tashtego::core;
    using namespace tashtego::cmd;
    using namespace tashtego::ip;
    using namespace tashtego::com;
    using namespace tashtego::rawtypes;
    using namespace tashtego::helper;

    //Register input memory map format
    auto id = qRegisterMetaType<TashMapAcc_ImageStack>();
    getMetaObjectIDs().append(id);
#ifdef TASHVERBOSE
    qDebug() << "Registered specialized mmap type for IMEM to metaobject system with ID: " << id;
#endif

    //Register output memory map format
    id = qRegisterMetaType<TashMapAcc_ScoreMap>();
    getMetaObjectIDs().append(id);
#ifdef TASHVERBOSE
    qDebug() << "Registered specialized mmap type for ScoreMap to metaobject system with ID: " << id;
#endif

    id = qRegisterMetaType<TashMapAcc_DetectDims>();
    getMetaObjectIDs().append(id);
#ifdef TASHVERBOSE
    qDebug() << "Registered specialized mmap type for detector dimension interface to metaobject system with ID: " << id;
#endif

    id = qRegisterMetaType<TashMapAcc_IMEM>();
    getMetaObjectIDs().append(id);
#ifdef TASHVERBOSE
    qDebug() << "Registered specialized mmap type for detector dimension interface to metaobject system with ID: " << id;
#endif

    id = qRegisterMetaType< TashMSSWAccessor_int16 >();
    getMetaObjectIDs().append(id);
#ifdef TASHVERBOSE
    qDebug() << "Registered specialized accessor type for interfacing a multiscale frame to metaobject system with ID: " << id;
#endif

    const QString appname = APPNAME;
    const qint32  cams    = 1;
    const QString path    = QString("../cfg/%1.ini").arg(appname);
    const qint32  camidx  = cams - 1;

    if ( !QFile::exists(path) ) {
        qCritical() << "Can't find ini-file by specified path: " << path;
        return false;
    }

    //Start tashtego runtime instance
    m_pRuntime = TashRuntime::getRuntimeObject(appname,
                                              path,
                                              cams,
                                              m_argc,
                                              m_argv);

    Q_ASSERT(!m_pRuntime.isNull());
    Q_CHECK_PTR(m_pRuntime.data());

    //Setup signal for shutdown
    connect(m_pRuntime.data(), SIGNAL(shutdown()), this, SLOT(stop()));

    //Initialize runtime
    bool hasValidRuntime = m_pRuntime->init();

    //If there is a valid ini file runtime is rated valid
    if ( hasValidRuntime ) {

        //Create input access to imagestack.mmap
        m_pInputMap = openMemoryMap<TashMapAcc_ImageStack>(m_pRuntime, camidx, QString("ImageStack"));
        Q_CHECK_PTR(m_pInputMap);
        Q_ASSERT(m_pInputMap->isInitialized());
        Q_ASSERT(m_pInputMap->isOpen());

        //Create output to detections.mmap
        m_pOutputMap = openMemoryMap<TashMapAcc_ScoreMap>(m_pRuntime, camidx, QString("ScoreMap"));
        Q_CHECK_PTR(m_pOutputMap);
        Q_ASSERT(m_pOutputMap->isInitialized());
        Q_ASSERT(m_pOutputMap->isOpen());

        //Create mmap access to IMEM
        m_pIMEM = openMemoryMap<TashMapAcc_IMEM>(m_pRuntime, camidx, QString("IMEM"));
        Q_CHECK_PTR(m_pIMEM);
        Q_ASSERT(m_pIMEM->isInitialized());
        Q_ASSERT(m_pIMEM->isOpen());

        //Get user cfg value if app should initialize mutlithreaded
        bool initializeMultithreaded = m_pRuntime->property("init_multithreaded").toBool();

        //Create a Multiscale instance for each frame stored in input memory map
        qint32 numFrames = m_pRuntime->property("num_mssw_frames").toInt();
        numFrames        = (numFrames == 0) ? tashtego::IMGSTACK_MAP_DEFAULT_DEPTH : numFrames;

        /*
         * Single threaded initialization, slow
         */
        if ( !initializeMultithreaded ) {

            for ( qint32 idx = 0; idx < numFrames; ++idx ) {

                auto framePointer = TashImage<qint16>(TashRuntime::genID(),
                                                      m_pRuntime->getInputSourceWidth(camidx),
                                                      m_pRuntime->getInputSourceHeight(camidx),
                                                      TashImage<qint16>::Format_Grayscale16,
                                                      m_pInputMap->getUnsafeMapAccess()->stack[idx].data);

                qDebug() << "Creating Multiscale instance " << idx+1 << "/" << numFrames;

                QElapsedTimer timer;
                timer.start();
                auto* pTmpAcc =  new TashMSSWAccessor_int16(TashRuntime::genID(),
                                                              camidx,
                                                              m_pRuntime,
                                                              framePointer);
                Q_CHECK_PTR(pTmpAcc);

                if ( !pTmpAcc ) {
                    qCritical() << "Error while allocating Multiscale instance " << idx+1 << ", abort!";
                    return false;
                }

                m_pMultiscaleBuffer[idx] = QPointer<TashMSSWAccessor_int16>(pTmpAcc);

                qDebug() << "Multiscale instance " << idx << "/" << numFrames << " created in " << static_cast<double>(timer.elapsed()) / 1000.0 << " secs\n";

                if ( (!m_pMultiscaleBuffer[idx]) || (!m_pMultiscaleBuffer[idx]->isInitialized()) ) {
                    qCritical() << "Error while allocating multiscaler instance!";
                    return false;
                }
            }
        /*
         * Multi threaded initialization
         */
        } else {

            const qint32 allocCycles    = m_pRuntime->property("num_mssw_ini_cycles").toInt();
            const qint32 threadsToAlloc = numFrames / allocCycles;
            qint32       framecounter   = 0;
            std::vector<std::thread> threadList(threadsToAlloc);

            //Create initialization lambda, which is being passed to thread objects
            auto myThreadAlloc = [&] (quint64 id, qint32 camidx, TashImage<qint16> frame, const tashtego::tashtegoRuntime &runtime, qint32 vecIdx/*, QMutex* mutex*/) {
                auto* pTmpAcc = new TashMSSWAccessor_int16(id, camidx, runtime, frame);
                Q_CHECK_PTR(pTmpAcc);
                m_pMultiscaleBuffer[vecIdx] = QPointer<TashMSSWAccessor_int16>( pTmpAcc );
            };

            //Create all thread objects an join them after creation
            for ( auto tidx = 0; tidx < allocCycles; ++tidx ) {

                qDebug() << "Starting " << threadsToAlloc << " MSSW allocations in cycle " << tidx+1 << "/" << allocCycles;
                QElapsedTimer timer;
                timer.start();

                for ( auto tidx = 0; tidx < threadsToAlloc; ++tidx, ++framecounter ) {

                    auto framePointer = TashImage<qint16>(TashRuntime::genID(),
                                                          m_pRuntime->getInputSourceWidth(camidx),
                                                          m_pRuntime->getInputSourceHeight(camidx),
                                                          TashImage<qint16>::Format_Grayscale16,
                                                          m_pInputMap->getUnsafeMapAccess()->stack[framecounter].data);


                    threadList[tidx] = std::thread(myThreadAlloc,
                                                   TashRuntime::genID(),
                                                   camidx,
                                                   framePointer,
                                                   m_pRuntime,
                                                   framecounter);
                }

                for ( auto tIt = threadList.begin(); tIt != threadList.end(); ++tIt ) {
                    tIt->join();
                }

                qDebug() << "Allocated " << threadsToAlloc << " MSSW instances in " << static_cast<double>(timer.elapsed()) / 1000.f << " secs";
            }
        }

        QCoreApplication::processEvents();
        tashtego::helper::TashWaiter::msecWait(500);

        QElapsedTimer  timer;
#ifdef TASHVERBOSE
        qDebug() << "Getting access to background model data...";
#endif
        timer.start();

        //Create exactly one multiscale instance for the background model matrix
        auto pBGM = TashImage<qint16>(TashRuntime::genID(),
                                      m_pRuntime->getInputSourceWidth(camidx),
                                      m_pRuntime->getInputSourceHeight(camidx),
                                      TashImage<qint16>::Format_Grayscale16,
                                      m_pIMEM->getUnsafeMapAccess()->frames[0].backgroundModel);
        //Allocate a multiscale instance
        TashMSSWAccessor_int16* pAcc = new TashMSSWAccessor_int16(TashRuntime::genID(),
                                                                      camidx,
                                                                      m_pRuntime,
                                                                      pBGM);
        //Store allocated instance in a smart pointer
        const QPointer<TashMSSWAccessor_int16>    pBGMModel   = QPointer<TashMSSWAccessor_int16>(pAcc);
#ifdef TASHVERBOSE
        qDebug() << "...BGM data loaded in " << timer.elapsed() / 1000.0 << " secs";
#endif


        /*
         * Create a processor module
         */
        {
            auto dbg = qDebug().nospace();
            dbg << "Creating instance of processor module...";
            m_pProcessor = new TashMedianSortProcessorModule(TashRuntime::genID(),
                                                             m_pRuntime,
                                                             m_pMultiscaleBuffer,
                                                             pBGMModel,
                                                             camidx,
                                                             m_pOutputMap->getSafeMapAccess());

            Q_CHECK_PTR(m_pProcessor);

            if ( !m_pProcessor ) {
                qCritical() << "Failed to allocate a detector object!";
                return false;
            }

            dbg << "done!";
        }

        /*
         * Create a frame sorter object for imagestack,
         * using a lambda object to adapt framesorter template behaviour
         */
        {
            auto dbg = qDebug().nospace();
            dbg << "Creating instance of frame sorter...";
            auto indexFinder = [] (quint32 numFrames, QSharedPointer<framebuffer_r> ptr) -> QMap<qint8,quint32> {
                QMap<qint8, quint32> map;
                for ( auto idx = 0u; idx < numFrames; ++idx ) {
                    map[idx] = ptr->stack[idx].framenumber;
                }
                return map;
            };

            m_pSorter = new TashFrameSorter<framebuffer_r, decltype(indexFinder)>(TashRuntime::genID(),
                                                                                  m_pInputMap->getSafeMapAccess(),
                                                                                  indexFinder,
                                                                                  50, //Wait 50 ms before skip
                                                                                  numFrames);//30 frames in stack
            Q_CHECK_PTR(m_pSorter);
            connect(m_pSorter, SIGNAL(newFrame(quint32,quint32)), this, SLOT(handleNewFrame(quint32,quint32)));
            connect(m_pSorter, SIGNAL(idle()), this, SLOT(handleIdle()));

            dbg << "done!";
        }

        /*
         * Create a deque object for debugging
         */
        m_framenumberbuffer = std::deque<quint32>(numFrames, 1);
    }

    if ( !hasValidRuntime ) {
        qWarning() << "Terminating application, STOP signal received...";
    } else {
#ifdef TASHVERBOSE
        qDebug() << "Sucessfully initialized thread!";
#endif
        emit started();
    }

    return hasValidRuntime;
}

void BackendWorker::cleanup() {
#ifdef TASHVERBOSE
    qDebug() << "Starting app cleanup...";
    qDebug() << "Deleting multiscale instances...";
#endif
    if ( !m_pMultiscaleBuffer.isEmpty() ) {
        auto counter = 1;
        for ( auto it = m_pMultiscaleBuffer.begin(); it != m_pMultiscaleBuffer.end(); ++it, ++counter) {
#ifdef TASHVERBOSE
            auto dbg = qDebug().nospace();
            dbg << "Delete instance " << counter << "...";
#endif
            auto v = it.value();

            if (!v.isNull() && v->isInitialized()) {
                auto *p = v.data();
                delete p;
                p = nullptr;
            } else {
                qWarning() << "Invalid mssw instance!";
            }
#ifdef TASHVERBOSE
            dbg << "done";
#endif
        }
    }
#ifdef TASHVERBOSE
    qDebug() << "Done!";
    qDebug() << "Delete input mmap accessor...";
#endif
    if ( m_pInputMap && m_pInputMap->isInitialized() ) {
        delete m_pInputMap;
        m_pInputMap = nullptr;
    }
#ifdef TASHVERBOSE
    qDebug() << "Done!";
    qDebug() << "Delete IMEM mmap accessor...";
#endif
    if ( m_pIMEM && m_pIMEM->isInitialized() ) {
        delete m_pIMEM;
        m_pIMEM = nullptr;
    }
#ifdef TASHVERBOSE
    qDebug() << "Done!";
    qDebug() << "Delete output mmap accessor...";
#endif
    if ( m_pOutputMap && m_pOutputMap->isInitialized() ) {
        delete m_pOutputMap;
        m_pOutputMap = nullptr;
    }
#ifdef TASHVERBOSE
    qDebug() << "Done!";
    qDebug() << "Delete sorter object...";
#endif
    if ( m_pSorter && m_pSorter->isInitialized() ) {
        delete m_pSorter;
    }
#ifdef TASHVERBOSE
    qDebug() << "Done!";
    qDebug() << "Delete runtime object...";
#endif
    if ( (!m_pRuntime.isNull() && m_pRuntime->isInitialized()) ||
         (!m_pRuntime->isInitialized() && m_pRuntime->doHelpPrint())) {
        auto*  pR = m_pRuntime.data();
        delete pR;
        pR = nullptr;
    }
#ifdef TASHVERBOSE
    qDebug() << "Done, finished app cleanup!";
#endif
}

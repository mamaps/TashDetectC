/*
 * TashDetectC, an image processing application to detect thermal anomalies in infrared image footage.
 * Copyright (C) <2015-2018>  <Michael Flau, Alfred-Wegener Institute, Ocean Acoustics Group (OZA)>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef BACKENDWORKER_H
#define BACKENDWORKER_H

#include <QObject>
#include "libtash.h"

#include <deque>

using TashMSSWAccessor_int16 = tashtego::ip::TashMSSWAccessor<qint16>;

/**
 * @brief The BackendWorker class
 * Worker class of TashDetectC. This class executes all the detection
 * logic which are being provided by libTash.
 *
 * The class itself is meant to run inside of a QThread object. it implements
 * it's own process method, which runs continously. The processing of a new frame
 * is being triggered by the frame index. A sorter object informs the backendworker
 * about unprocessed frames, which then are being processed by the created detection object.
 */
class BackendWorker final: public QObject
{
    Q_OBJECT

    template <typename T> using TashMSSWBuffer = QMap<qint32,QPointer<tashtego::ip::TashMSSWAccessor<T>>>;

public:

    /**
     * @brief BackendWorker
     * Default constructor to call for class instanciation.
     * @param m_argc
     * Argument count
     * @param m_argv
     * CLI Arguments
     * @param parent
     * parent object
     */
    explicit BackendWorker(int m_argc, char** m_argv, QObject *parent = 0);

    virtual ~BackendWorker();
    BackendWorker(const BackendWorker&) = default;
    BackendWorker(BackendWorker&&) = default;
    BackendWorker& operator=(const BackendWorker&) = default;
    BackendWorker& operator=(BackendWorker&&) = default;

signals:

    /**
     * @brief finished
     * Is emitted when object's ::process method is terminating
     */
    void finished();
    /**
     * @brief running
     * Is emitted in every duty cycle of the worker object
     */
    void running();
    /**
     * @brief newFrame
     * Is emitted when a new frame is present
     */
    void newFrame();
    /**
     * @brief started
     * Is emitted when worker object started
     */
    void started();

public slots:

    /**
     * @brief process
     * Main processing routine. From here all actions are being triggered
     * based on frame input. This is also the roting which runs inside of
     * the worker thread.
     */
    virtual void process();
    /**
     * @brief stop
     * Method which will switch the stop flag to true, in order to stop execution
     * of the object's eventloop and initialize the destruction of the instance.
     */
    virtual void stop();

protected slots:

    /**
     * @brief handleNewFrame
     * Triggered by BackendWorker::process() if a new frame is present.
     * The whole detections logic resides in this routine.
     * @param idx
     * Continous frame number
     * @param frameidx
     * Framebuffer index
     */
    void handleNewFrame(quint32 idx, quint32 frameidx);

    /**
     * @brief handleIdle
     * Debug function to see if idles are handled differently from actual frame
     * events.
     */
    void handleIdle();

protected:

    /**
     * @brief initThread
     * Initialization function, sets up all logic and objects, connects all slots
     * and singals.
     * @return
     * Boolean, true if initialization was correct, false if not
     */
    bool initThread();

    /**
     * @brief cleanup
     * Deinitialization routine, being called from dtor to release all allocated
     * memory regions.
     */
    void cleanup();

private:
    bool                                                   m_bIsInitialized;
    bool                                                   m_bStop;
    QString                                                m_IniFilePath;
    quint32                                                m_currentFrameIndex;
    tashtego::tashtegoRuntime                              m_pRuntime;
    tashtego::TashMapAcc_ImageStack                        *m_pInputMap;
    tashtego::TashMapAcc_ScoreMap                          *m_pOutputMap;
    tashtego::TashMapAcc_IMEM                              *m_pIMEM;
    TashMSSWBuffer<qint16>                                  m_pMultiscaleBuffer;
    tashtego::core::TashAbstractFrameSorter                *m_pSorter;
    tashtego::ip::TashMedianSortProcessorModule            *m_pProcessor;
    std::deque<quint32>                                     m_framenumberbuffer;
    int                                                     m_argc;
    char**                                                  m_argv;
};

#endif // BACKENDWORKER_H

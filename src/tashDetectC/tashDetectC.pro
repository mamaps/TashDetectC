#-------------------------------------------------
#
# TashDetectC Project created by QtCreator 2015-06-03T00:15:40
#
#-------------------------------------------------
QT       += core dbus
QT       -= gui

QMAKE_CXXFLAGS_RELEASE *= -O3

CONFIG   += console \
            c++14 \
            release
#            debug

CONFIG   -= app_bundle
TEMPLATE = app
TARGET   = tashDetectC
DESTDIR  = ../../bin/

INCLUDEPATH += ../../include/libTash

LIBS += -L../../bin/libTash -ltash

SOURCES += main.cpp \
           backendworker.cpp

HEADERS += backendworker.h

OTHER_FILES += ../../cfg/tashDetectC.ini

DEFINES += APPNAME=\\\"$$TARGET\\\" \


Release:DESTDIR     = ../../build/tashDetect/release
Release:OBJECTS_DIR = ../../build/tashDetect/release/.obj
Release:MOC_DIR     = ../../build/tashDetect/release/.moc
Release:RCC_DIR     = ../../build/tashDetect/release/.rcc
release:UI_DIR      = ../../build/tashDetect/release/.ui

Debug:DESTDIR     = ../../build/tashDetect/debug
Debug:OBJECTS_DIR = ../../build/tashDetect/debug/.obj
Debug:MOC_DIR     = ../../build/tashDetect/debug/.moc
Debug:RCC_DIR     = ../../build/tashDetect/debug/.rcc
Debug:UI_DIR      = ../../build/tashDetect/debug/.ui

unix {
    target.path = ../../bin/
    INSTALLS += target
}


#!/usr/bin/python2.7

#Simple script to generate a build number while compiling a tashtego app or lib module

import sys
import os

if not len(sys.argv) > 1:
	print "Cannot execute, needs input path to build.h"
	exit(-1)

libtashpath = sys.argv[1]

if not os.path.exists(libtashpath):
	print "Project does not have a buildnumber file, creating one!"
	fd = open(libtashpath,'w')
	content = "/*Automatically generated buildnumber header file*/\n#ifndef BUILD_H\n#define BUILD_H\n#define BUILD 0\n#endif //BUILD_H\n"
	fd.write(content)
	fd.close()
else:
	fd = open(libtashpath,'r')
	content = fd.read()
	fd.close()
	for line in content.split('\n'):
		words = line.split(' ')
		for word in words:
			if word == "BUILD":
				idx = words.index(word)
				build = int(words[idx+1])
				build = build + 1
				print "New build has number:", build
				fd = open(libtashpath,'w')
				content = "/*Automatically generated buildnumber header file*/\n#ifndef BUILD_H\n#define BUILD_H\n#define BUILD " + str(build) + "\n#endif //BUILD_H\n"
				fd.write(content)
				fd.close()
				
	

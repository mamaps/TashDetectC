TEMPLATE = subdirs

CONFIG += ordered

SUBDIRS +=  src/libTash/libTash.pro \
            src/tashDetectC/tashDetectC.pro\

OTHER_FILES = doc/export/* \
              scripts/* \
              *.md

#libtash
libTash.file            = src/libTash/libTash.pro
#tashDetect
tashDetectC.file        = src/tashDetectC/tashDetectC.pro
tashDetectC.depends     = libTash


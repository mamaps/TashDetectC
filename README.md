# TashDetectC v0.2
### Disclaimer
Technical data and information provided herein shall be considered non-binding and may be subject to change without notice.  

### Credits
**Ocean Acoustics Lab**  
**Alfred-Wegener-Institute**  
**Helmholtz-Zentrum für Polar- und Meeresforschung**  
**Am Handelshafen 12**  
**27570 Bremerhaven**  
**Germany**  

  
**Project lead:**  
[Dr. Olaf Boebel](<mailto:olaf.boebel@awi.de?Subject=Tashtego-TashDetectC%20Request>)
  
**Developer:**  
[Michael Flau](<mailto:michael.flau@awi.de?Subject=Tashtego-TashDetectC%20Request&cc=olaf.boebel@awi.de>)
  
**Former Developers:**  
Dr. Daniel Zitterbart  

### tl;dr
This software aims at detecting whales based on radiance anomalies in infrared video footage by analyzing the temporal change of the  spatial pattern of radiances.
## License
See the [license](LICENSE.md) file for further information on licensing of TashDetectC.
## Changelog
See the [changelog](CHANGELOG.md) for further information on the latest changes.
## Description
TashDetectC is the core module of the Tashtego software suite for IR-based whale detection (Intern. Patent Nr. WO 2013/037344 A1). TashDetectC's main intent is to detect local radiance (i.e. intensity) changes in infrared video footage, employing a multi-scale tiling algorithm. TashDetectC is designed as background application (daemon), ingesting a video data steam with as continuous stream of tile-specific detection score metrics as output.  
## Configuration
TashDetectC is flexible in it's configuration. By changing property values in the startup file, the user has full control over the way TashDetectC will analyze given image data. The [tashDetect ini-file](cfg/tashDetect.ini) defines all startup values, which may be altered by the user. In addition, there are also certain options available via command line flags. For further information on which parameter changes what, refer to [Usage](#usage).
### The tiling algorithm
TashDetectC employs a multi-scale tiling algorithm, analyzing local image changes within the full image (global) context. The full image is 1) subdivided into (custom configurable) Region of interest (ROI), which are 2) subdivided further into smaller Tile-ranges (TR), which are 3) split further into individual tiles.  Division into ROI, TRs, and Tiles is static. The tiling algorithm uses these 4 bounding types for subdividing a full image frame into logical and directly addressable instances. The following bounding types are defined:  
1. *Frameaccessor* (**FA**)
2. *Region of interest* (**ROI**)
3. *Tile-range* (**TR**)
4. *Tile* (**T**)

Table 1 describes the relationship between bounding types while Figure [1](#tiling-graphic) exemplifies one possible realization.

|Type    |Contains all Region of Interests|Contains all Tile Ranges of ROI|Contains all Tiles within respective TR|Contains Data Reference (Pixels) of respective Tile|
|:------:|:------------------------------:|:-----------------------------:|:-------------------------------------:|:-------------------------------------------------:|
|**FA**  |:heavy_check_mark:              |:heavy_check_mark:             |:heavy_check_mark:                     |:heavy_check_mark:                                 |
|**ROI** |:x:                             |:heavy_check_mark:             |:heavy_check_mark:                     |:heavy_check_mark:                                 |
|**TR**  |:x:                             |:x:                            |:heavy_check_mark:                     |:heavy_check_mark:                                 |
|**T**   |:x:                             |:x:                            |:x:                                    |:heavy_check_mark:                                 |  
*Tab.1: Relationship between bounding types*
<br/><br/>

![tiling-graphic](doc/resources/tiling.png "Figure 1")
*Fig.1: Example of tiling for a image frame utilizing the tiling module of TashDetectC*


Each bounding type has several parameters which are user settable, in order to cover the correct image area. For further information on how to define a bounding type, refer to [Usage](#user-settable-parameters).

### The detection algorithm
The detection algorithm is based on a comparison of the radiance of a local tile at a specific tile range position with the radiances of horizontally adjacent tiles. A detailed description of the algorithm will be given in a forthcoming publication (Zitterbart et al., 2019, in preparation). 

### Optimization
In order to stay within the time margin dictated by the utilized camera's frame interval, TashDetectC is designed as multi-threaded application, which is able to distribute it's payload over as many CPU cores as
the user specifies or physically present. Figure [2](#threading) shows how computation is carried out in parallel over a subset of CPU core.

![threading](doc/resources/threading.png "Figure 2")
*Fig.2: Threading model of TashDetectC*

### Requirements
#### Operating System
TashDetectC is tested and productively used on Ubuntu Linux 14.04 and Ubuntu Linux 16.04.

#### 3rd party software
TashDetectC makes use of the Open Source version of the [Qt Software Development Framework](https://www.qt.io/). Refer to [Installation](#installation) for further information on how to install Qt.

### Process communication
TashDetectC is part of the Tashtego software which is designed as a multi-process system. It
manages process communication via memory mapped files. By default all memory mapped files will be
created in `/mnt/mmap/`. Since the path is not within the user's home directory, this path should
be made read/write accessible for the user which runs TashDetectC. Alternatively, changes the memory
map's creation paths to a user space located residence.

Figure [3](#processIO) shows the I/O interactions, tashDetect carries out. While in init phase, the application will read all startup values from the ini-file in `cfg/`. The binary itself resides in `bin/`.

![processIO](doc/resources/processIO.png "Figure 3")
*Fig.3: I/O relationships between TashDetectC and connected memorymaps*

TashDetectC accesses 2 memory maps for data input. `/mnt/mmap/firstimage` is accessed while executing the initialization phase. Here, TashDetectC accesses the static background model, to augment the detected events based on their distance. This step is optional and can be disabled via ini-file.

`/mnt/mmap/imagestack.mmap` is used as the preceding framebuffer were frames of the last X seconds are kept before they are overwritten. The size of the framebuffer is defines by the preceding writer process which provides the buffer for TashDetectC. However, the size of the buffer has to be specifed in the ini file so that the utilized frame sorter algorithm in TashDetectC is aware of how many frames to deal with.

`/mnt/mmap/detections.mmap` is created and written by TashDetectC. The memory map has a predefined layout as shown in figure [3](#processIO). TashDetectC will use as many Scoremap slots as there are worker threads specified in the ini-file. This allows the workers to dump their detections into their predefined memory slot without having to mind access restrictions.
Each slot is a so called *scoreholder list*. The scoreholder list has a global header and a statically defined amount of slots to write to. Each slot again has a statically defined amount of fields. A slot is referred to as one *detection*, stating
- tile position
- tile score without background-model enhancement
- tile score with background-model enhancement
- width and height of tile

## Usage
### Installation
To run TashDetectC, specific software dependencies must be met.

#### Required Ubuntu repository packages
The following repository packages are required to build TashDetectC
- `build-essential`
- `libxrender-dev`
- `libgles2-mesa`
- `libgles2-mesa-dev`
- `libglu1-mesa`
- `libglu1-mesa-dev`
- `libegl1-mesa`
- `libegl1-mesa-dev`
- `libgl1-mesa-dev`
- `libgl1-mesa-glx`

Run the following command in a terminal window, to install the required packages from the Ubuntu repository

```bash
$ sudo apt-get update
$ sudo apt-get -y install build-essential libxrender-dev libgles2-mesa libgles2-mesa-dev libglu1-mesa libglu1-mesa-dev libegl1-mesa libegl1-mesa-dev libgl1-mesa-dev libgl1-mesa-glx
```

#### Install Qt
1. Download a recent source tarball version of Qt from the [Qt download website](http://download.qt.io/official_releases/qt/).
2. In the directory where the downloaded Qt tarball lies, execute

```bash
$ tar -xJvf qt-everywhere-src-5.9.4.tar.xz
```

3. `cd` into the extracted directory and execute

```bash
$ QTVERSION='Qt-5.9' #Adapt this to your version
$ PATH='/usr/local/static'
$ INSTDIR=$($PATH'/'$QTVERSION)
$ sudo mkdir -p $INSTDIR
$ ./configure -prefix $INSTDIR -hostprefix $INSTDIR -extprefix $INSTDIR -bindir $INSTDIR/bin -headerdir $INSTDIR/include -libdir $INSTDIR/lib -archdatadir $INSTDIR/arch -plugindir $INSTDIR/plugins -libexecdir $INSTDIR/libexec -importdir $INSTDIR/import -qmldir $INSTDIR/qml -datadir $INSTDIR/data -docdir $INSTDIR/doc -translationdir $INSTDIR/translations -sysconfdir $INSTDIR/etc/xdg -hostbindir $INSTDIR/bin/hostbin -hostlibdir $INSTDIR/lib/hostlib -hostdatadir $INSTDIR/data/hostdata -release -opensource -static -c++std c++14 -accessibility -no-qml-debug -qt-libpng -qt-libjpeg -no-alsa -no-pulseaudio -silent -no-cups -nomake examples -nomake tests -xcb -qt-xcb -opengl -verbose -system-xkbcommon
```

Qt should now be configured and ready to compile run

```bash
$ make -j6
```

to build Qt. Replace the number after `-j` with number of CPU cores you want to use for the build process. This will take a while to finish. After Qt is compiled execute

```bash
$ sudo make install
```

to install Qt to the specified installation directory. The last step is to make the qmake binary accessible from the whole filesystem.

```bash
sudo ln -s $INSTDIR/bin/hostbin/qmake /usr/bin/qmake
```

#### Build TashDetectC
TashDetectC is written in C++14. A GNU compiler version >= 4.8 should be sufficient to compile TashDetectC it should have been installed along with the `build-essential` package. From the TashDetectC root directory, run

```bash
$ qmake && make
```

The command will start the build process of TashDetectC. Once the compilation is finished, the built TashDetectC executable will be located in `bin/`.
### User settable parameters
#### Startup parameters via ini-file
All defined values in the startup file tashDetect.ini are referred to as *properties*. Each property follows a predefined notation:
```ini
PROPERTY_DESCRIPTOR=VALUE[DATA_TYPE]
```
The property descriptor itself has a pathlike notation and is a result of the introspection procedure, by which each software module
communicates its required property values to the runtime, which in turn is responsible for the ini-file generation. The pathlike descriptor typically consists of the following parts
```ini
CAMERA_ID\MODULE_NAME\PROPERTY_NAME
```
A software module is *always* associated with a *CAMERA_ID* in order to know which module instance is processing data of which camera at runtime. The *MODULE_NAME* describes the instantiated software module itself.

A software module can have one, multiple or none user settable properties.
##### Memory Map Accessors
Memory maps represent the main communication point for all applications within the tashtego ecosystem. The memory map accessor module provides a mean to read from and write to an opened memory map in a safe way. In order to open the memory map as the user requires, it the following properties may be changed by the user:

|Property name           |Module Descriptor  |Possible Values                     |Data type  |Description|
|:----------------------:|:-----------------:|:----------------------------------:|:---------:|:---------------------------------------------------------------------------------|
|`fileaccess`            |`TashMapAcc_MODULE`|See [DevDoc](#further-documentation)|`[string]` |File access rights of mapped file                                                 |
|`fileoptions`           |`TashMapAcc_MODULE`|See [DevDoc](#further-documentation)|`[string]` |File access options with which mapped file is opened                              |
|`mapoptions`            |`TashMapAcc_MODULE`|See [DevDoc](#further-documentation)|`[string]` |Memory access rights of mapped virtual memory                                     |
|`mapprotection`         |`TashMapAcc_MODULE`|See [DevDoc](#further-documentation)|`[string]` |Memory access options with which mapped file is opened                            |
|`mmap_has_frame_header` |`TashMapAcc_MODULE`|See [DevDoc](#further-documentation)|`[bool]`   |Defines if memory mapped file implements a local-, slot associated header         |
|`mmap_has_global_header`|`TashMapAcc_MODULE`|See [DevDoc](#further-documentation)|`[bool]`   |Defines if memory mapped file implements a global header for shared values        |
|`mmap_opening_offset`   |`TashMapAcc_MODULE`|See [DevDoc](#further-documentation)|`[integer]`|Offset from file beginning at which map pointer is pointing to                    |
|`mmap_path`             |`TashMapAcc_MODULE`|See [DevDoc](#further-documentation)|`[string]` |Absolute path to memory mapped file. Application must have correct access rights  |
|`mmap_type`             |`TashMapAcc_MODULE`|See [DevDoc](#further-documentation)|`[string]` |Type of map, can either be static(if a structure definition is present) or generic|

##### Tiling Module
TashDetectC implements the [tiling algorithm](#the-tiling-algorithm) as buffer which consists of single tiling instances. Each tiling instance manages exactly one memory area which resembles to a possible camera frame. Since the tiling should be consistent over all frames in the framebuffer, all tiling instances are initialized with the same properties:

|Property name                           |Module Descriptor              |Possible Values                                                  |Data type  |Description                                                                                                          |
|:--------------------------------------:|:-----------------------------:|:----------------------------------------------------------------|:---------:|:--------------------------------------------------------------------------------------------------------------------|
|`number_of_rois`                        |`TashMSSWAccessor_DATATYPE`    |Integer value, must be 1 (at the moment only 1 ROI is supported) |`[integer]`|Number of Region interest                                                                                            |
|`use_tiling`                            |`TashMSSWAccessor_DATATYPE`    |Boolean value, `true` or `false`                                 |`[bool]`   |If enabled, tiling will be activated, otherwise the ROI provides only a reference to the subsection of the image     |
|`ROI_X__height`                         |`TashMSSWAccessor_DATATYPE`    |Integer value > 0 and <= height of input camera frame            |`[integer]`|Defines the height of the region of interest                                                                         |
|`ROI_X__width`                          |`TashMSSWAccessor_DATATYPE`    |Integer value > 0 and <= width of input camera frame             |`[integer]`|Defines the width of the region of interest                                                                          |
|`ROI_X__x`                              |`TashMSSWAccessor_DATATYPE`    |Integer value >= 0 and < width of input camera frame             |`[integer]`|Defines where the y-position of the left upper corner of the region of interest is located in the camera frame area  |
|`ROI_X__y`                              |`TashMSSWAccessor_DATATYPE`    |Integer value >= 0 and < height of input camera frame            |`[integer]`|Defines where the x-position of the left upper corner of the region of interest is located in the camera frame area  |
|`ROI_X__number_trs`                     |`TashMSSWAccessor_DATATYPE`    |Integer value > 0                                                |`[integer]`|Defines how many tile ranges should be created within ROI bounds                                                     |
|`ROI_X__Tilerange_Y__mind_unfitting_end`|`TashMSSWAccessor_DATATYPE`    |Boolean value, `true` or `false`                                 |`[bool]`   |Dis-/Enables tile squeezing a the end of a tile range. An additional tile will be matched into the remaining TR space|
|`ROI_X__Tilerange_Y__tile_height`       |`TashMSSWAccessor_DATATYPE`    |Integer value > 0 and < TR height                                |`[integer]`|Defines the height of the tiles within the tile range                                                                |
|`ROI_X__Tilerange_Y__tile_width`        |`TashMSSWAccessor_DATATYPE`    |Integer value > 0 and < TR height                                |`[integer]`|Defines the width of the tiles within the tile range                                                                 |
|`ROI_X__Tilerange_Y__tile_overlap_x`    |`TashMSSWAccessor_DATATYPE`    |Integer value >= 0 and < Tile width                              |`[integer]`|Defines the horizontal overlap between adjacent tiles in tile range                                                  |
|`ROI_X__Tilerange_Y__tile_overlap_y`    |`TashMSSWAccessor_DATATYPE`    |Integer value >= 0 and < Tile height                             |`[integer]`|Defines the vertical overlap between adjacent tiles in tile range                                                    |
|`ROI_X__Tilerange_Y__range_start_x`     |`TashMSSWAccessor_DATATYPE`    |Integer value >= 0 and < (ROI width - TR width)                  |`[integer]`|Defines the horizontal position of the tile range relative to it's bounding Region of interest x start position      |
|`ROI_X__Tilerange_Y__range_start_y`     |`TashMSSWAccessor_DATATYPE`    |Integer value >= 0 and < (ROI height - TR height)                |`[integer]`|Defines the vertical position of the tile range relative to it's bounding Region of interest y start position        |

##### Detector Module
The detector module is the core component of TashDetectC. Its main task is the validation of each tile in a tiling module instance. To perform this task as per formant as possible, the detector module functions also as a scheduling unit which distributes the actual workload which, consists of the complete tile set of a tile module, to the worker instances, the detector module creates in order to analyze the complete tile set. The [detection algorithm](##the-detection-algorithm) is carried out in parallel in all allocated worker instances over an locally adjacent set of tile confined in a tile range, see the [description](#the-tiling-algorithm) for further info. The parameters which are user settable, comprise settings for the detection algorithm, as well as settings concerning how the detection process is arranged over multiple threads. The following table gives a complete overview of all settings, which can be altered via the startup ini-file:

|Property name                     |Module Descriptor              |Possible Values                                              |Data type  |Description|
|:--------------------------------:|:-----------------------------:|:------------------------------------------------------------|:---------:|:-----------------------------------------------------------------------------------------------------------------------------------------------------------|
|`frame_buffer_limit`              |`TashMedianSortProcessorModule`|Integer > 0                                                  |`[integer]`|Number of frames in frame buffer                                                                                                                            |
|`max_number_of_threads`           |`TashMedianSortProcessorModule`|Integer > 0 and < Number of CPU cores                        |`[integer]`|Number of threads to use for processing                                                                                                                     |
|`reserve_num_threads_from_max`    |`TashMedianSortProcessorModule`|Integer >= 0 and < 'reserve_num_threads_from_max'            |`[integer]`|Number of threads to reserve for side operations                                                                                                            |
|`processing_strategy`             |`TashMedianSortProcessorModule`|See [DevDoc](#further-documentation)                         |`[string]` |Determines the way tiles are being processed by Detector                                                                                                    |
|`score_validity_boundary`         |`TashMedianSortProcessorModule`|Integer value >= 0 and <= `accumulate_score_over_num_frames` |`[integer]`|Static threshold which triggers tile activation,*deprecated*                                                                                                |
|`compute_stddev_over_num_frames`  |`TashMedianSortProcessorModule`|Integer value > 0 and <= `accumulate_score_over_num_frames`  |`[integer]`|Determines the amount of already computed standard deviations of processed tiles in the past over which the mean standard deviation will be computed       |
|`accumulate_score_over_num_frames`|`TashMedianSortProcessorModule`|Integer value > 0 and < size of framebuffer                  |`[integer]`|Determines the number of scores to take into account, to compute a tile's score                                                                             |
|`consider_num_max_values`         |`TashMedianSortProcessorModule`|Integer value > 0 and < (TileWidth X TileHeight)             |`[integer]`|Determines the number of maximum values to take into account for the center tile's median normalized maximum value                                          |
|`multiply_lr_stddev_mean_with`    |`TashMedianSortProcessorModule`|Integer value > 0 and < width of tile range in tiles         |`[integer]`|Determines the weight by which the mean of standard deviations of adjacent tiles is multiplied with                                                         |
|`processing_kernel_type`          |`TashMedianSortProcessorModule`|See [DevDoc](#further-documentation)                         |`[string]` |Describes the kernel type to install to the processing unit as worker object                                                                                |
|`use_background_model`            |`TashMedianSortProcessorModule`|Boolean `true` or `false`                                    |`[bool]`   |Dis-/enables usage of a static background model to improve tile scores of tiles which cover an area more distant than tiles at other positions              |
|`use_presence_ratio_filter`       |`TashMedianSortProcessorModule`|Boolean `true` or `false`                                    |`[bool]`   |Dis-/enables blacklisting of tiles which show a continuous high tile score which indicates a tile activation by a static object, which is not to be detected|
|`presence_ratio_threshold`        |`TashMedianSortProcessorModule`|Float value between 0.0 and 1.0                              |`[float]`  |Sets the threshold at which a tile is regarded as *blacklisted*, it will not be considered further until it presence ration is lowered|ujn

#### Command line flags
Apart from settings which can be altered by changeing values of properties in the ini-file, TashDetectC provides some command line flags, which can be used to change the behavior of the application or for debugging purposes. The following table describes each command line flag:

|*Short Flag*|*Long Flag*|Value              |Description                                                   |Default enabled         |
|:----------:|:---------:|:-----------------:|:-------------------------------------------------------------|:----------------------:|
|`-n`        |`--dry`    |None               |Executes init phase and terminates controlled                 |no                      |
|`-h`        |`--help`   |None               |Prints help dialog and exits                                  |no                      |
|`-i`        |`--ini`    |`/PATH/TO/INI/FILE`|Option to specifiy location of an alternative ini-file to load|no                      |
|`-d`        |`--dbus`   |None               |Dis-/enables usage of DBus                                    |as specified in ini-file|
|`-o`        |`--ossh`   |None               |Dis-/Enables usage of OS Interrupts for app termination       |as specified in ini-file|
|`-r`        |`--rewrite`|None               |Forces rewrite of the ini-file in `cfg/`                      |no                      |

### Run TashDetectC
From the root directory, navigate to `bin/` and execute `$ ./tashDetectC` to start the application.
#### Runtime verbosity
Right now there is no command line flag available, which controls the verbosity level. However TashDetectC provides additional output at runtime if it is compiled with the `-DTASHVERBOSE` macro enabled.
## Further documentation
Additional documentation is available by building the developer documentation using
[Doxygen](http://www.stack.nl/~dimitri/doxygen/). To build the developer documentation
execute `$ doxygen doc/doxygen/tashtego.doc` in the root directory. The built documentation can be opened within any browser by navigating to `doc/doxygen/export/html/index.html`.
